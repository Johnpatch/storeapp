## Bootsrap Responsive Multi-Vendor, MultiLanguage Online Shop Platform

Current versions:

* Codeigniter 3.1.9
* Bootstrap 3.3.7

## Support of following features

## Easy installation in 3 steps
1. Import database.sql to your mysql
2. Set hostname, username and password in application/config/database.php
3. Set your site domain in application/config/config.php - $config['base_url'] = 'http://storeapp.com';
4. Opss I forgot for last 4 step... ENJOY! ;)

Little explain for installation - if you paste installation of project in directory something like http://localhost/SHOP, you must 
set this directory to application/config/config.php - $config['base_url'] = 'http://localhost/SHOP'; and must remove from .htaccess file
"RewriteBase /" line because css and js files will not be loaded! But you must know that the best way to install this platform is to set it
in root http://localhost directory or if you want to be in other directory just set virtual host for this and point him to there.
(example: http://shop.dev - point to localhost/shop directory). How to create virtual hosts you can read here: http://goo.gl/UvpYMG

## Login to administration with
User: admin, 
Pass: admin