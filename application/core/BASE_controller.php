<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class BASE_Controller extends MX_Controller
{
    protected $auth;

    /*
     * header information includes following contents
     * controller title, icon, description
     */
    protected $head;
    protected $perpage = 10;
    protected $route_prefix;
    protected $table;
    /*
     * Langauage Settings
     */
    protected $page_title;
    protected $page_subtitle;
    protected $panel_title;

    protected $index_page_title;
    protected $index_page_subtitle;
    protected $index_panel_title;

    protected $form_description;
    protected $table_description;
    protected $search_description;
    //Success settings
    protected $success_return_text;


    //DB field name corresponding to branch_id: especially for store information
    protected $branch_id_field = 'branch_id';
    //Additional conditions to get current setting
    protected $additional_conditions = array();
//    //scripts for publish url
//    protected $publish_script_url;
//
//    //scripts for publish url
//    protected $confirm_script_url;

    //Indicates whether index page has preview
    protected $index_has_preview = true;

    //Indicates whether publish page has preview
    protected $publish_has_preview = true;

    //Indicates whether index page has search form
    protected $index_has_search = true;

    //Indicates whether index page has order function
    protected $has_order = false;

    //Indicates whether index page has add button
    protected $has_add = false;
    /*
     * Input Fields Description
     * group: label
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|photo|video...]
     * default: default value if initial value is not set
     * label: input label description
     * options: optional, valid when type belongs to [radio, dropdown]
     * checked: 
     * help: help block description
     * required: whether this field is requried or not:this is not used in validation rules, add to validation rules again.
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values. 'key' refers to the key of $row array
     * showIfExternal: array(key=>value). shows this element when (key=value)
     *          value can take array values. 'key' refers to the key of $externals array
     * rules: validation settings
     */
    /* example
         * [
                [
                    "label"=>'Twitter link',//twitter_link,
                    'show'=>[],
                    'group'=>false,//indicates whether this is an element or an input
                    'array'=>false,
                    'name'=>'TWITTER_ACTIVE_YN',
                    'type'=>'radio',
                    'options'=>[
                        'Y'=>'Post a twitter link',
                        'N'=>'Do not post a twitter link'
                    ]
                ],
                [
                    "label"=>'Twitter account',
                    'show'=>[
                        'TWITTER_ACTIVE_YN'=>'Y'
                    ],
                    'group'=>false,//indicates whether this is an element or an input
                    'array'=>false,
                    'name'=>'TWITTER_LINK',
                    'type'=>'link',
                    'prefix'=>'http://www.twitter.com/',
                    'header'=>'Enter the Twitter account you want to post',
                    'help'=>'Log in to the Twitter page and enter the number describe in Settings>
                        Page Information>Twitter Page ID.
                        Entry Example1234567890'
                ],
                [
                    "label"=>'Facebook link',
                    'show'=>[],
                    'group'=>false,//indicates whether this is an element or an input
                    'array'=>false,
                    'name'=>'FACEBOOK_LINK',
                    'type'=>'link',
                    'prefix'=>'http://www.facebook.com/',
                    'header'=>'Enter the Facebook account you want to post',
                    'help'=>'Log in to the Facebook page and enter the number describe in Settings>
                        Page Information>Facebook Page ID.
                        Entry Example1234567890'
                ],
                [
                    "label"=>'Facebook account',
                    'show'=>[
                        'FACEBOOK_ACTIVE_YN'=>'Y'
                    ],
                    'group'=>false,//indicates whether this is an element or an input
                    'array'=>false,
                    'name'=>'FACEBOOK_LINK',
                    'type'=>'link',
                    'prefix'=>'http://www.twitter.com/',
                    'header'=>'Enter the Twitter account you want to post',
                    'help'=>'Log in to the Twitter page and enter the number describe in Settings>
                        Page Information>Twitter Page ID.
                        Entry Example1234567890'
                ],
                [
                    "label"=>'Blog and HP link settings',//"blog_hp_link",
                    "group"=>true,
                    "array"=>true,
                    //add button, delete button configuration should be customized
                    "inputs"=>[
                        [
                            "group"=>false,
                            "array"=>false,
                            "name"=>"TYPE",
                            "type"=>"dropdown",
                            "options"=>[
                                "1"=>"Twitter",
                                "2"=>"Facebook",
                                "3"=>"Blog",//...
                            ]
                        ],
                        [
                            "group"=>false,
                            "array"=>false,
                            "name"=>"LINK",
                            "type"=>"text",
                        ]
                    ]
                ],
            ];
         */
    protected $fields;

    protected $has_index;

    ////flag depending on change of pagetitle,paneltitle,


    //Preview settings
    protected $preview_url;
    protected $index_preview_url;
    protected $publish_preview_url;

    //Index settings

    /* Search Fields
     * name: search field name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     */
    protected $search_fields;

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;


    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->auth = $this->session->userdata('user');
        $this->initialize();

        /*
         * $fields = [input|group]
         * group=[input|group]
         */

    }

    //Initilize Custom Controller Settings
    protected function initialize() {

        $head['description'] = "";
        $this->head = $head;
    }

    //redirects within this controller
    protected function redirect($url) {
        return redirect($this->redirect_url($url));
    }

    //redirect url within this controller
    protected function redirect_url($url) {
        return site_url($this->route_prefix.$url);
    }

    protected function login_check()
    {
        /*if (!$this->session->userdata('logged_in')) {
            redirect('admin');
        }*/
        if(!$this->get_session_data($this->token, 'logged_in')) {
            redirect('admin');
        }
        $this->username = $this->session->userdata('logged_in');
    }


    //Common controller logic implementation

    //Set values of all fields in $input
    protected function set_field_values(&$fields, &$input) {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->set_field_values($field["inputs"], $input);
            }
            $name = element("name", $field);
            if ($name !== false) {
                $field["value"] = element($name, $input, element("default", $field, NULL));
            }
        }
    }

    protected function set_validation_rules($fields) {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->set_validation_rules($field["inputs"]);
            }
            $name = element("name", $field);
            if ($name !== false) {
                $rules = element("rules", $field, "");
                $this->form_validation->set_rules($name, $field['label'], $rules);
            }
        }
    }

    protected function set_errors($fields) {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->set_errors($field["inputs"]);
            }
            $name = element("name", $field);
            if ($name !== false) {
                $field["error"] = form_error($name,'<div class="error">', '</div>');
            }
        }
    }

    protected function get_non_array_inputs_by_fields($fields, &$input) {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            $array = element("array", $field);
            if ($array == true) continue;
            if ($group === true) {
                $this->get_non_array_inputs_by_fields($field["inputs"], $input);
            }
            $name = element("name", $field);
            $type = element("type", $field);
            if ($name !== false) {
                if ($type == "file") {
                    $value = $input[$name];
                    if (strstr($value, ",")) {
                        $url = "attachments/slide_images/".random_string().".png";
                        file_put_contents($url, base64_decode(explode(",", $value)[1]));
                        $input[$name] = base_url($url);
                    }
                }
            }
        }
    }

    /*
     * Typical DB Operations
     */

    //returns db values to be edited in publish page
    protected function db_get_row($id) {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
        $row = $this->db->get($this->table)->row_array();
        return $row;
    }

    protected function db_save($id, $data) {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
        if ($this->db->count_all_results($this->table) > 0) {//update data
            $data['update_time'] = time();
            if ($this->has_index == true) $this->db->where('id', $id);
            else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
            if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
            $this->db->set($data)->update($this->table);
        } else {
            $data['create_time'] = time();
            $data['update_time'] = time();
            $this->db->insert($this->table, $data);
        }
        die();
    }

    //Overridable functions for special save cases
    protected function db_before_save($id, $data) {}
    protected function db_after_save($id, $data) {}

    protected function db_delete($id) {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
        $this->db->delete($this->table);
    }

    protected function db_order($id_array, $order_array) {
        $len = count($id_array);
        $this->db->trans_start();
        $table = $this->table;
        for ($i = 0; $i < $len; $i++) {
            $id = $id_array[$i];
            $order = $order_array[$i];
            $this->db->query("UPDATE $table SET `ORDER`=$order WHERE `ID`=$id");
        }
        $this->db->trans_complete();
    }

    public function count_rows($params) {
        return $this->db->count_all_results($this->table);
    }

    protected function get_rows($params, $perpage, $page) {
        return $this->db->get($this->table, $perpage, max($page - 1, 0) * $perpage)->result_array();
    }

    /*
     * Search function which should be overriden in Child Controllers
     * @params: search conditions
     * @return  search result rows 
     */
    public function index($page = 0) {
        $user = $this->auth;
        if ($this->has_index == false) {
            $this->redirect('publish');
        }
        $data = array();

        $input = $this->input->get();
        $perpage = $this->input->get('perpage');
        if ($perpage == 0) {
            $perpage = $this->perpage;
        }
        $this->set_field_values($this->search_fields, $input);
        $input["perpage"] = $perpage;
        $rowscount = $this->count_rows($input);
        $data['branch_id'] = $user['branch_id'];
        $data['add_url'] = $this->redirect_url('publish/');
        $data['preview_url'] = $this->index_preview_url? $this->index_preview_url : $this->preview_url;
        $data["delete_url"] = $this->redirect_url('delete/');
        $data["order_url"] = $this->redirect_url('order/');
        $data['rows'] = $this->get_rows($input, $perpage, $page);
        $data['links_pagination'] = pagination($this->redirect_url('index'), $rowscount, $perpage, count(explode("/", $this->route_prefix)) + 1);
        $data["table_fields"] = $this->table_fields;
        $data['search_fields'] = $this->search_fields;

        $data["has_order"] = $this->has_order;
        $data["has_add"] = $this->has_add;
        $data['params'] = $input;
        $data['page_title'] = $this->index_page_title;
        $data['page_subtitle'] = $this->index_page_subtitle;
        $data['panel_title'] = $this->index_panel_title;
        $data['table_description'] = $this->table_description;
        $data["form_description"] = $this->search_description;
        $data["perpage_options"] = $this->perpage_options;
        $data["perpage"] = $perpage;
        $data["has_preview"] = $this->index_has_preview;
        $data['has_search'] = $this->index_has_search;
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('template/list', $data);
        $this->load->view('_parts/footer');
    }

    public function publish($id = 0) {
        $user = $this->auth;
        $old = $this->session->flashdata('old');
        $row = $this->db_get_row($id);

        if (is_array($old)) $row = $old;
        if ($row == NULL) $row = array();
        $this->set_field_values($this->fields, $row);
        $data["branch_id"] = $user['branch_id'];
        $data["fields"] = $this->fields;
        $data["confirm_url"] = $this->redirect_url('confirm').($id > 0 ? "/$id" : "");
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        $data["form_description"] = $this->form_description;
        $data['preview_url'] = $this->publish_preview_url ? $this->publish_preview_url : $this->preview_url;
        $data['has_preview'] = $this->publish_has_preview;
        $data["errors"] = $this->session->flashdata("errors");
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('template/publish', $data);
        $this->load->view('_parts/footer');
    }

    public function confirm($id = 0) {
        $input = $this->input->post();

        $this->session->set_flashdata('old', $input);
        $data = $input;
        $back_url = $input["back_url"];
        $this->set_field_values($this->fields, $input);
        $this->set_validation_rules($this->fields);

        if ($this->form_validation->run() == FALSE) {
            $error = array();
            $this->set_errors($this->fields);
            redirect($back_url);
        }

        $data["fields"] = $this->fields;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        $data["form_description"] = $this->form_description;
        $data["save_url"] = $this->redirect_url('save'.($id > 0 ? "/$id" : ""));
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('template/confirm', $data);
        $this->load->view('_parts/footer');
    }

    public function save($id = 0) {
        $input = $this->input->post();
        $input[$this->branch_id_field] = $this->auth['branch_id'];
        $input = array_merge($input, $this->additional_conditions);
        if ($this->has_index == true) $input["ID"] = $id;
        $file_inputs = array();
        $this->get_non_array_inputs_by_fields($this->fields, $input);
        $this->db_before_save($id, $input);
        $id = $this->db_save($id, $input);
        $this->db_after_save($id, $input);
        redirect($this->route_prefix."success");
    }

    public function success() {
        $data['back_text'] = $this->success_return_text;
        $data['back_url'] = site_url($this->has_index == true ? $this->route_prefix : $this->route_prefix."publish");
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('template/success', $data);
        $this->load->view('_parts/footer');
    }

    public function delete($id) {
        $this->db_delete($id);
        $this->redirect('');
    }

    public function order() {
        $id_array = $this->input->post('ID');
        $order_array = $this->input->post('ORDER');
        if (is_array($id_array)) {
            $this->db_order($id_array, $order_array);
        }
        $this->redirect('');
    }
}
