<?php

class MY_Model extends CI_Model
{
    protected $table;
    public function __construct() {
        parent::__construct();
    }
    
    public function get_row($conditions) {
        
        foreach ($conditions as $key => $value) {
            $this->db->where($key, $value);
        }
        $row = $this->db->get($this->table)->row_array();
        return $row;
    }



    public function save($data, $conditions) {
        if ($this->db->where($conditions)->count_all_results($this->table) > 0) {//update data
            $data['update_time'] = time();
            $this->db->where($conditions)->set($data)->update($this->table);
        } else {
            $data['create_time'] = time();
            $data['update_time'] = time();
            $this->db->insert($this->table, $data);
        }
    }

    public function delete($id) {
        $this->db->where('id', $id)->delete($this->table);
    }

    public function delete_where($conditions) {
        $this->db->where($conditions)->delete($this->table);
    }

    public function update_order($id_array, $order_array) {
        $len = count($id_array);
        $this->db->trans_start();
        $table = $this->table;
        for ($i = 0; $i < $len; $i++) {
            $id = $id_array[$i];
            $order = $order_array[$i];
            $this->db->query("UPDATE $table SET `ORDER`=$order WHERE `ID`=$id");
        }
        $this->db->trans_complete();
    }

    public function insert_batch($data) {
        $this->db->insert_batch($this->table, $data);
    }
}
