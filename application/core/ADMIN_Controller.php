<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ADMIN_Controller extends MX_Controller
{

    protected $username;

    protected $allowed_img_types;
    protected $history;
    protected $table;

    protected $auth;

    /*
     * header information includes following contents
     * controller title, icon, description
     */
    protected $head;
    protected $perpage = 10;
    protected $perpage_options = [5, 10, 20, 50];
    protected $route_prefix;
    protected $page_title;
    protected $page_subtitle;
    protected $panel_title;
    protected $page_icon;

    //DB field name corresponding to branch_id: especially for store information
    protected $branch_id_field = 'branch_id';
    //Additional conditions to get current setting
    protected $additional_conditions = array();
    //scripts for publish url
    protected $publish_script_url;

    //scripts for publish url
    protected $confirm_script_url;

    //Indicates whether index page has preview
    protected $index_has_preview = true;

    //Indicates whether publish page has preview
    protected $publish_has_preview = true;

    //Indicates whether index page has search form
    protected $index_has_search = true;

    //Indicates whether index page has order functionf
    protected $has_order;

    //Indicates whether index page has add button
    protected $has_add;

    protected $has_questions = false;
    /*
     * Input Fields Description
     * group: label
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|`box|radio|textarea|static|dropdown|color|photo|video...]
     * default: default value if initial value is not set
     * label: input label description
     * options: optional, valid when type belongs to [radio, dropdown]
     * checked: 
     * help: help block description
     * required: whether this field is requried or not:this is not used in validation rules, add to validation rules again.
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values. 'key' corresonds the key of $row array
     * showIfExternal: array(key=>value). shows this element when (key=value)
     *          value can take array values. 'key' corresonds the key of $externals array
     * rules: validation settings
     */
    protected $fields;
    protected $confirm_fields;
    /* external_fields
     * static values that this controller references
     * This values may take another table's record value or user-customized values
     * takes same value format as $fields
     */
    protected $external_fields;
    protected $has_index;

    ////flag depending on change of pagetitle,paneltitle,
    protected $title_index = false;
    protected $page_title1;
    protected $page_subtitle1;
    protected $panel_title1;
    protected $list_description1;


    //Preview settings
    protected $preview_url;
    protected $index_preview_url;
    protected $publish_preview_url;

    //Index settings

    /* Search Fields
     * name: search field name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     */
    protected $search_fields = [];

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;


    //Publish settings
    protected $form_description;
    protected $list_description;

    //Confirm settings

    //Success settings
    protected $success_return_text;
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation'));
        //$this->auth = $this->session->userdata('user');
        $this->auth = $this->get_session_data($this->token, 'user');

        $head['logged_in'] = $this->get_session_data($this->token, 'logged_in'); 
        $head['token'] = $this->token;
        $this->head = $head; 
        $this->initialize();
        if($this->table == 'branches')
            $this->branch_id_field = 'ID';
    }
    //Initilize Custom Controller Settings
    protected function initialize()
    {
        $this->head['description'] = "";
    }
    //redirects within this controller
    protected function redirect($url)
    {
        return redirect($this->redirect_url($url.'?token='.$this->token));
    }
    //redirect url within this controller
    protected function redirect_url($url)
    {
        return site_url($this->route_prefix . $url);
    }
    protected function login_check()
    {
        /* if (!$this->session->userdata('logged_in')) {
            redirect('admin');
        } */
        //if(!$this->get_session_data($this->token, 'logged_in')) {
        //    redirect('admin');
       // }
        // $this->username = $this->session->userdata('logged_in');
        $this->username = $this->get_session_data($this->token, 'logged_in');
    }
    //Common controller logic implementation

    //Set values of all fields in $input
    protected function set_field_values(&$fields, &$input, $is_array = false)
    {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->set_field_values($field["input"], $input, element("array", $field));
            }
            $name = element("name", $field);
            if ($name !== false) {
                $field["value"] = element($name, $input, element("default", $field, $is_array ? array() : NULL));
            }
            $label_edit = element("label_edit", $field);
            if($label_edit){
                $label_name = element("label_name", $field);
                $field['label'] = element($label_name, $input, element("default", $field, $is_array ? array() : NULL));
            }
        }
    }

    protected function set_validation_rules($fields)
    {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->set_validation_rules($field["input"]);
            }
            $name = element("name", $field);
            if ($name !== false) {
                $rules = element("rules", $field, "");
                $this->form_validation->set_rules($name, element('label', $field, $name), $rules);
            }
        }
    }

    protected function set_errors($fields, &$errors)
    {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->set_errors($field["input"], $errors);
            }
            $name = element("name", $field);
            if ($name !== false) {
                $field["error"] = form_error($name, '<div class="error">', '</div>');
                if ($field["error"] != "") $errors[$name] = $field["error"];
            }
        }
    }

    protected function get_errors(&$fields, $errors)
    {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            if ($group === true) {
                $this->get_errors($field["input"], $errors);
            }
            $name = element("name", $field);
            if ($name !== false) {
                $field["error"] = element($name, $errors);
            }
        }
    }

    protected function get_non_array_inputs_by_fields($fields, &$input, $origin)
    {
        foreach ($fields as &$field) {
            $group = element("group", $field);
            $array = element("array", $field);
            if ($array == true) continue;
            if ($group === true) {
                $this->get_non_array_inputs_by_fields($field["input"], $input, $origin);
                continue;
            }
            $name = element("name", $field);
            $type = element("type", $field);
            $label_edit = element("label_edit", $field);
            if ($name !== false) {
                if ($type != "static") $input[$name] = $origin[$name];
                if($label_edit){
                    $label_name = element("label_name", $field);
                    $input[$label_name] = $origin[$label_name];
                }
                if ($type == "file" || $type == "pdf" || $type == "csv") {
                    $value = $input[$name];
                    if (strstr($value, ",")) {
                        $extension = "";
                        if ($type == "file") $extension = ".png";
                        elseif ($type == "pdf") $extension = ".pdf";
                        elseif ($type == "csv") $extension = ".csv";
                        $url = "attachments/slide_images/" . random_string() . $extension;
                        file_put_contents($url, base64_decode(explode(",", $value)[1]));
                        $input[$name] = $url;
                    }
                }
            }
        }
    }

    /*
     * Typical DB Operations
     */

    //returns db values to be edited in publish page
    protected function db_get_row($id)
    {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
        $row = $this->db->get($this->table)->row_array();
        return $row;
    }

    protected function db_save($id, $data, $time = '')
    {
        
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
        if ($this->db->count_all_results($this->table) > 0) {//update data
            if(!empty($time)){
                $data['update_time'] = $time;
            }else{
                $data['update_time'] = time();
            }
            
            if ($this->has_index == true) $this->db->where('id', $id);
            else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
            if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
            if($this->table == 'branches'){
                if(empty($data['PASSWORD']))
                    unset($data['PASSWORD']);
                else
                    $data['PASSWORD'] = md5($data['PASSWORD']);
            }
            $this->db->set($data)->update($this->table);
        } else {
            if(!empty($time)){
                $data['create_time'] = $time;
                $data['update_time'] = $time;
            }else{
                $data['create_time'] = time();
                $data['update_time'] = time();
            }
            
            $this->db->insert($this->table, $data);
            $id = $this->db->insert_id();
        }
        return $id;
    }

    //Overridable functions for special save cases
    protected function db_before_save($id, &$data)
    {
    }

    protected function db_after_save($id, $data)
    {
    }

    protected function db_delete($id)
    {
        if($this->route_prefix != 'admin/basic/content/'){
            if ($this->has_index == true) $this->db->where('id', $id);
            else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
            if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
            $this->db->delete($this->table);
        }else{
            if ($this->has_index == true) $this->db->where('id', $id);
            else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
            $result = $this->db->get($this->table)->row_array();

            $this->db->where($this->branch_id_field, $this->auth['branch_id']);
            $this->db->where('CREATE_TIME', $result['CREATE_TIME']);
            $this->db->delete($this->table);
        }
        
    }

    protected function db_order($id_array, $order_array)
    {
        $len = count($id_array);
        $this->db->trans_start();
        $table = $this->table;
        for ($i = 0; $i < $len; $i++) {
            $id = $id_array[$i];
            $order = $order_array[$i];
            $this->db->query("UPDATE $table SET `ORDER`=$order WHERE `ID`=$id");
        }
        $this->db->trans_complete();
    }

    protected function build_query($params) {
        $this->db->where('BRANCH_ID', $this->auth["branch_id"]);
        if ($this->has_order) $this->db->order_by("ORDER", "ASC");
    }

    protected function count_rows($params)
    {
        $this->build_query($params);
        return $this->db->count_all_results($this->table);
    }

    protected function get_rows($params, $perpage, $page)
    {
        $this->build_query($params);
        return $this->db->get($this->table, $perpage, $page)->result_array();
    }

    /*
     * Search function which should be overriden in Child Controllers
     * @params: search conditions
     * @return  search result rows 
     */
    public function index($page = 0)
    {
        $this->get_flush_session_data($this->token, 'old');
        $user = $this->auth;
        if ($this->has_index == false) {
            $this->redirect('publish');
        }
        $data = array();

        $input = $this->input->get();
        $perpage = $this->input->get('perpage');
        if ($perpage == 0) {
            $perpage = $this->perpage;
        }
        if ($this->index_has_search) {
            $this->set_field_values($this->search_fields, $input);
        }
        $input["perpage"] = $perpage;
        $rowscount = $this->count_rows($input);
        $data["branch_id"] = $this->auth['branch_id'];
        $data['add_url'] = $this->redirect_url('publish/');
        $data['preview_url'] = $this->index_preview_url? $this->index_preview_url : $this->preview_url;
        $data["delete_url"] = $this->redirect_url('delete/');
        $data["order_url"] = $this->redirect_url('order').'?token='.$this->token;
        if($page  >= $rowscount)
            $page = 0;
        $data['rows'] = $this->get_rows($input, $perpage, $page);
        $data['links_pagination'] = pagination($this->redirect_url('index'), $rowscount, $perpage, count(explode("/", $this->route_prefix)) + 1);
        $data["table_fields"] = $this->table_fields;
        $data['search_fields'] = $this->search_fields;

        $data["has_order"] = $this->has_order;
        $data["has_add"] = $this->has_add;
//        $data['has_preview'] = true;
        $data['params'] = $input;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        /////////////////////////////title with index
        $data['page_title1'] = $this->page_title1;
        $data['page_subtitle1'] = $this->page_subtitle1;
        $data['panel_title1'] = $this->panel_title1;
        $data['list_description1'] = $this->list_description1;
        $data['title_index'] = $this->title_index;
        //
        $data["form_description"] = $this->form_description;
        $data["list_description"] = $this->list_description;
        $data["page_icon"] = $this->page_icon;
        $data["perpage_options"] = $this->perpage_options;
        $data["perpage"] = $perpage;
        $data["has_preview"] = $this->index_has_preview;
        $data['has_search'] = $this->index_has_search;
        $data['route_prefix'] = $this->route_prefix;
        
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$user['branch_id'])->get('branches')->row_array();
        $data['token'] = $this->token;
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/list', $data);
        $this->load->view('_parts/footer');
    }

    public function publish($id = 0)
    {
        
        $old = $this->get_flush_session_data($this->token, 'old');

        $row = $this->db_get_row($id);
        if (is_array($old)) $row = $old;
        if ($row == NULL) $row = array();
        
        if($this->publish_script_url == 'topmenu/topmenu' && $id != 0 && ($row['TYPE'] == 0 || $row['MENU_TYPE'] == 8)){
            if($row['MENU_TYPE'] != '8')
                $this->fields[1]['type'] = 'static';
            else{
                $this->fields[1]['input'][0]['options'] = [];
                $this->fields[1]['input'][0]['options']['11'] = lang('web_view_settings');
            }
            if($row['TYPE'] == '0' && $row['MENU_TYPE'] == '1')
                $this->fields[1]['options'] = ["0"=>'通常（基本機能: 店舗情報）'];
            else if($row['TYPE'] == '0' && $row['MENU_TYPE'] == '2')
                $this->fields[1]['options'] = ["0"=>'通常（基本機能: クーポン）'];
            else if($row['TYPE'] == '0' && $row['MENU_TYPE'] == '3')
                $this->fields[1]['options'] = ["0"=>'通常（基本機能: イベント）'];
            else if($row['TYPE'] == '0' && $row['MENU_TYPE'] == '4')
                $this->fields[1]['options'] = ["0"=>'通常（基本機能: ムービー）'];
            else if($row['TYPE'] == '0' && $row['MENU_TYPE'] == '5')
                $this->fields[1]['options'] = ["0"=>'通常（基本機能: スタンプ）'];
            else if($row['TYPE'] == '0' && $row['MENU_TYPE'] == '6')
                $this->fields[1]['options'] = ["0"=>'通常（基本機能: カタログ）'];
            else if($row['TYPE'] == '0' && $row['MENU_TYPE'] == '7')
                $this->fields[1]['options'] = ["0"=>'通常（基本機能: 予約）'];
            else if($row['TYPE'] == '0' && $row['MENU_TYPE'] == '9')
                $this->fields[1]['options'] = ["0"=>'通常（基本機能: 問い合わせ'];
            //else if($row['TYPE'] == '0' && $row['MENU_TYPE'] == '8')
            //    $this->fields[1]['options'] = ["0"=>'通常（基本機能: webビュー）'];
            
            if($row['MENU_TYPE'] != '8'){
                unset($this->fields[1]['required']);
                unset($this->fields[1]['rules']);
                unset($this->fields[1]['group']);
                unset($this->fields[1]['input']);
            }
        }
        if($this->route_prefix == 'admin/inquiry/management/' && $row['TYPE'] == 2){
            unset($this->fields[1]);
            $this->fields[0]['label'] = lang('inquiry_address');
            $this->fields[0]['name'] = 'ADMIN_EMAIL';
            $this->fields[0]['rules'] = 'valid_email';
        }
        $this->set_field_values($this->fields, $row);
        $errors = $this->session->flashdata("errors");
        if ($errors) {
            $this->get_errors($this->fields, $errors);
        }
        if($this->route_prefix == 'admin/inquiry/inquiries/'){
            $result = $this->db->query("select inquiry.*, inquiry_setting.`NAME` from inquiry inner join inquiry_setting on inquiry.SETTING_ID = inquiry_setting.ID and inquiry.BRANCH_ID = inquiry_setting.BRANCH_ID and inquiry_setting.STATUS_SHOW = 'Y'
            where
            inquiry.CREATE_TIME = '".$row['CREATE_TIME']."'
            and inquiry.BRANCH_ID = ".$this->auth['branch_id'])->result_array();
            $this->fields = [];
            foreach($result as $key => $value){
                
                $temp = [
                    "value"=>$value['SETTING_VALUE'],
                    "type"=>"text_readonly",
                    "label"=>$value['NAME'],
                ];
                array_push($this->fields, $temp);
            }
            $data['only_view'] = true;
        }
        if($this->route_prefix == 'admin/basic/content/' && $id != 0){
            $result = $this->db->query("select * from company_profile where CREATE_TIME = '".$row['CREATE_TIME']."' and BRANCH_ID = ".$this->auth['branch_id'])->result_array();
            $this->fields = [];
            $this->fields = [
                [
                    "name"=>"TYPE",
                    "type"=>"dropdown",
                    "label"=>lang("category"),
                    "options" => [
                        '1' => lang('company_profile'),
                        '2' => lang('store_guide'),
                        '3' => lang('pricing'),
                        '4' => lang('FAQ'),
                        '5' => lang('notice_setting'),
                        '6' => lang('terms_service'),
                        '7' => lang('privacy_policy'),
                        '8' => lang('information_security'),
                        '9' => lang('about_this_app'),
                    ],
                    "value" => $result[0]['TYPE']
                ]
                ];
            foreach($result as $key => $value){
                
                $temp = [
                    "label"=>lang("staff_photo"),
                    "type"=>"file",
                    "name"=>"IMAGE[]",
                    "help"=>lang("staff_photo_help"),
                    "value"=>$value['IMAGE']
                ];
                array_push($this->fields, $temp);
                $temp = [
                    "label"=>lang("title"),
                    "type"=>"text",
                    "name"=>"TITLE[]",
                    "value"=>$value['TITLE'],
                    "function"=>true
                ];
                array_push($this->fields, $temp);
                $temp = [
                    "label"=>lang("comment"),
                    "type"=>"textarea",
                    "name"=>"COMMENT[]",
                    "value"=>$value['COMMENT'],
                    "function"=>true
                ];

                array_push($this->fields, $temp);
            }
        }
        $data["branch_id"] = $this->auth['branch_id'];
        $data["id"] = $id;
        $data["fields"] = $this->fields;
        $data["confirm_url"] = $this->redirect_url('confirm') . ($id > 0 ? "/$id" : "") . "?token=" . $this->token;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        $data["form_description"] = $this->form_description;
        $data["page_icon"] = $this->page_icon;
        $data['preview_url'] = $this->publish_preview_url ? $this->publish_preview_url : $this->preview_url;
        $data['script_url'] = $this->publish_script_url;
        $data['route_prefix'] = $this->route_prefix;
        if($this->route_prefix == 'admin/coupon/' || $this->route_prefix == 'admin/survey/' || $this->route_prefix == 'admin/push/' || $this->route_prefix == 'admin/basic/free/' || $this->route_prefix == 'admin/stamp/stampprivilege/'){
            $data['page_subtitle'] = lang('new_reg');
            $data['panel_title'] = lang('new_reg');
            $data['route_prefix'] = $this->route_prefix.'publish';
        }
        if($this->route_prefix == 'admin/inquiry/management/'){
            $data['page_subtitle'] = lang('create_a_new');
            $data['panel_title'] = lang('create_a_new');
        }
        if($this->route_prefix == 'admin/event/' || $this->route_prefix == 'admin/basic/content/'){
            $data['page_subtitle'] = lang('post_reg');
            $data['panel_title'] = lang('post_reg');
        }
        $data['has_preview'] = $this->publish_has_preview;
        $data["errors"] = $this->session->flashdata("errors");
        $data["has_questions"] = $this->has_questions;
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $data['token'] = $this->token;
        if($id != 0 && isset($this->edit_title) && !empty($this->edit_title)){
            $data['panel_title'] = $this->edit_title;
            $data['page_subtitle'] = $this->edit_title;
        }
        if(isset($this->is_basic_add))
            $data['is_basic_add'] = true;
        
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/publish', $data);
        $this->load->view('_parts/footer');
    }

    public function confirm($id = 0)
    {
        $input = $this->input->post();
        if($this->confirm_script_url == 'topmenu/confirm' && !isset($input['TYPE'])){
            unset($this->fields[1]);
        }
        if($this->route_prefix == 'admin/coupon/'){
            if(empty($input['COUPON_START_DATE']))
                $input['COUPON_START_DATE'] = '2000-01-01 00:00';
            if(empty($input['COUPON_END_DATE']))
                $input['COUPON_END_DATE'] = '2099-12-31 23:59';
        }
        if($this->route_prefix == 'admin/event/' || $this->route_prefix == 'admin/survey/'){
            if(empty($input['START_DATE']))
                $input['START_DATE'] = '2000-01-01';
            if(empty($input['END_DATE']))
                $input['END_DATE'] = '2099-12-31';
        }
        if($this->route_prefix == 'admin/inquiry/management/'){
            $row = $this->db_get_row($id);
            if($row['TYPE'] == 2){
                unset($this->fields[1]);
                $this->fields[0]['label'] = lang('inquiry_address');
                $this->fields[0]['name'] = 'ADMIN_EMAIL';
                unset($this->fields[0]['rules']);
                unset($this->fields[0]['required']);
            }
        }
        if($this->confirm_script_url == 'catalog/confirm'){
            if($input['TYPE'] == 1){
                unset($this->fields[4]['input'][0]['rules']);
            }else if($input['TYPE'] == 2){
                unset($this->fields[2]['rules']);
            }
        }

        if($this->route_prefix == 'admin/basic/content/'){
            $this->fields = [
                [
                    "name"=>"TYPE",
                    "type"=>"dropdown",
                    "label"=>lang("category"),
                    "options" => [
                        '1' => lang('company_profile'),
                        '2' => lang('store_guide'),
                        '3' => lang('pricing'),
                        '4' => lang('FAQ'),
                        '5' => lang('notice_setting'),
                        '6' => lang('terms_service'),
                        '7' => lang('privacy_policy'),
                        '8' => lang('information_security'),
                        '9' => lang('about_this_app'),
                    ],
                    "value"=>$input['TYPE']
                ]
            ];
            foreach($input['IMAGE'] as $key => $value){
                
                $temp = [
                    "label"=>lang("staff_photo"),
                    "type"=>"file",
                    "name"=>"IMAGE[]",
                    "help"=>lang("staff_photo_help"),
                    "value" => $value
                ];
                array_push($this->fields, $temp);
                $temp = [
                    "label"=>lang("title"),
                    "type"=>"text",
                    "name"=>"TITLE[]",
                    "value" => $input['TITLE'][$key]
                ];
                array_push($this->fields, $temp);
                $temp = [
                    "label"=>lang("comment"),
                    "type"=>"textarea",
                    "name"=>"COMMENT[]",
                    "value" => $input['COMMENT'][$key]
                ];
                array_push($this->fields, $temp);
            }
        }
        //$this->session->set_flashdata('old', $input);
        if($this->route_prefix != 'admin/basic/content/'){
            $this->set_session_data($input['token'], 'old', $input);
            $data = $input;
            $back_url = $input["back_url"];
            $this->set_field_values($this->fields, $input);
            $this->set_validation_rules($this->fields);

            if ($this->form_validation->run() == FALSE) {
                $error = array();
                $this->set_errors($this->fields, $error);
                if (count($error)  > 0) {
                    $this->session->set_flashdata("errors", $error);
                    redirect($back_url);
                }
            }
        }

        $data["fields"] = $this->fields;
//        $data["row"] = $row;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        $data["form_description"] = $this->form_description;
        $data["page_icon"] = $this->page_icon;
        $data['script_url'] = $this->confirm_script_url;
        $data["save_url"] = $this->redirect_url('save' . ($id > 0 ? "/$id" : "")) . '?token=' . $this->token;
        $data['route_prefix'] = $this->route_prefix;
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/confirm', $data);
        $this->load->view('_parts/footer');
    }

    public function save($id = 0)
    {
        $input = $this->input->post();
        if($this->route_prefix == 'admin/inquiry/management/'){
            $row = $this->db_get_row($id);
            if($row['TYPE'] == 2){
                unset($this->fields[1]);
                $this->fields[0]['label'] = lang('inquiry_address');
                $this->fields[0]['name'] = 'ADMIN_EMAIL';
                unset($this->fields[0]['rules']);
                unset($this->fields[0]['required']);
            }
        }
        if($this->route_prefix == 'admin/basic/content/'){
            $insertTime = time();
            
            if($id != 0){
                $res = $this->db->where('ID', $id)->get($this->table)->row_array();
                $this->db->where('CREATE_TIME', $res['CREATE_TIME']);
                $this->db->delete($this->table);
            }
            $id = 0;
            foreach($input['TITLE'] as $key => $value){
                $this->fields = [
                    [
                        "name"=>"TYPE",
                        "type"=>"dropdown",
                        "label"=>lang("category"),
                        "options" => [
                            '1' => lang('company_profile'),
                            '2' => lang('store_guide'),
                            '3' => lang('pricing'),
                            '4' => lang('FAQ'),
                            '5' => lang('notice_setting'),
                            '6' => lang('terms_service'),
                            '7' => lang('privacy_policy'),
                            '8' => lang('information_security'),
                            '9' => lang('about_this_app'),
                        ],
                        "value"=>$input['TYPE']
                    ]
                ];
                $temp = [
                    "label"=>lang("staff_photo"),
                    "type"=>"file",
                    "name"=>"IMAGE",
                    "help"=>lang("staff_photo_help"),
                    "value" => $input['IMAGE'][$key*2]
                ];
                array_push($this->fields, $temp);
                $temp = [
                    "label"=>lang("title"),
                    "type"=>"text",
                    "name"=>"TITLE",
                    "value" => $input['TITLE'][$key]
                ];
                array_push($this->fields, $temp);
                $temp = [
                    "label"=>lang("comment"),
                    "type"=>"textarea",
                    "name"=>"COMMENT",
                    "value" => $input['COMMENT'][$key]
                ];
                array_push($this->fields, $temp);
                $data = array();
                $data['token'] = $input['token'];
                $data['TYPE'] = $input['TYPE'];
                $data['IMAGE'] = $input['IMAGE'][$key*2];
                $data['TITLE'] = $input['TITLE'][$key];
                $data['COMMENT'] = $input['COMMENT'][$key];
                $non_array_input = array();
                $this->get_non_array_inputs_by_fields($this->fields, $non_array_input, $data);
                $non_array_input[$this->branch_id_field] = $this->auth['branch_id'];
        
                $non_array_input = array_merge($non_array_input, $this->additional_conditions);
                
                if ($this->has_index == true) $non_array_input["ID"] = $id;

                $this->db_save($id, $non_array_input, $insertTime);

                
            }
            
            $this->get_flush_session_data($this->token, 'old');
        }else{
            $file_inputs = array();
            $non_array_input = array();
            $this->get_non_array_inputs_by_fields($this->fields, $non_array_input, $input);
            $non_array_input[$this->branch_id_field] = $this->auth['branch_id'];
    
            $non_array_input = array_merge($non_array_input, $this->additional_conditions);
            
            if ($this->has_index == true) $non_array_input["ID"] = $id;
            $this->db_before_save($id, $non_array_input);
    
            if(isset($non_array_input['HEADER_IMAGE_DEL_YN']) && $non_array_input['HEADER_IMAGE_DEL_YN'] == 'Y'){
                $non_array_input['HEADER_IMAGE_DEL_YN'] = 'N';
                $non_array_input['HEADER_IMAGE'] = '';
            }
            if(isset($non_array_input['FOOTER_IMAGE_DEL_YN']) && $non_array_input['FOOTER_IMAGE_DEL_YN'] == 'Y'){
                $non_array_input['FOOTER_IMAGE_DEL_YN'] = 'N';
                $non_array_input['FOOTER_IMAGE'] = '';
            }
            if(isset($non_array_input['IMAGE_FOR_LIST_YN']) && $non_array_input['IMAGE_FOR_LIST_YN'] == 'Y'){
                $non_array_input['IMAGE_FOR_LIST_YN'] = 'N';
                $non_array_input['IMAGE_FOR_LIST'] = '';
            }
            $id = $this->db_save($id, $non_array_input);
            $this->db_after_save($id, $input);
            $this->get_flush_session_data($this->token, 'old');
        }
        
        redirect($this->route_prefix . "success" . ($id > 0 ? "/$id" : "") . '?token=' . $this->token);
    }

    public function success($id = 0)
    {
        $data['page_title'] = $this->page_title;
        $data['back_text'] = $this->success_return_text;
        $data['back_url'] = site_url($this->has_index == true ? $this->route_prefix : $this->route_prefix . "publish") . "?token=".$this->token;
        
        $data['token'] = $this->token;
        $data['route_prefix'] = $this->route_prefix;
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/success', $data);
        $this->load->view('_parts/footer');
    }

    public function delete($id) {
        $this->db_delete($id);
        $this->redirect('');
    }

    public function order() {
        $id_array = $this->input->post('ID');
        $order_array = $this->input->post('ORDER');
        if (is_array($id_array)) {
            $this->db_order($id_array, $order_array);
        }
        $this->redirect('');
    }
}
