<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/** load the CI class for Modular Extensions **/
require dirname(__FILE__).'/Base.php';

/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library replaces the CodeIgniter Controller class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Controller.php
 *
 * @copyright	Copyright (c) 2015 Wiredesignz
 * @version 	5.5
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
class MX_Controller 
{
	public $autoload = array();
	protected $token =  'dfjsaksdafkldsjdkjfd';

	public function __construct() 
	{
		$class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
		log_message('debug', $class." MX_Controller Initialized");
		Modules::$registry[strtolower($class)] = $this;	
		/* copy a loader instance and initialize */
		$this->load = clone load_class('Loader');
		$this->load->initialize($this);	
		/* autoload module items */
		$this->load->_autoloader($this->autoload);
		$this->load->database();
		if(isset($_GET['token']))
			$this->token = $_GET['token'];
		if(isset($_POST['token']))
			$this->token = $_POST['token'];
	}	
	public function __get($class) 
	{
		return CI::$APP->$class;
	}
	public function get_session_data($token , $key) {
		$query = $this->db->query("select DATA from token_table where TOKEN='".$token."'");
		$result = $query->result_array();
		if(count($result) < 1) {	
			return null;
		}
		else {
			$json_string = $result[0]['DATA'];
			$data = json_decode($json_string, TRUE);
			if(isset($data[$key])) {
				$retval = $data[$key];
				if(is_string($retval))
					return $this->is_japan_unicode($retval);
				else {
					foreach ($retval as $key1 => $value1) {
						if(is_string($value1)) {
							$retval[$key1] = $this->is_japan_unicode($value1);
						}
					}
					return $retval;
				}
			}
			else
				return null;
		}
	}
	public function set_session_data($token, $key, $val) {
		if(is_array($val)) {
			foreach ($val as $key1 => $item) {
				if(is_string($item)) {
					if(strpos($item, '<p>') !== false) {
						$item = preg_replace('/[\n\r]+/', '', $item);
						$item = str_replace('"','\"',$item);
						$val[$key1] = $item;
					}
					else {
						$item = trim( preg_replace( '/(\r\n)|\n|\r/', '\\n', $item ) );
						$item = str_replace('"','\"',$item);
						$val[$key1] = $item;
					}
				}
			}
		}
		$query = $this->db->query("select DATA from token_table where TOKEN='".$token."'");
		$result = $query->result_array();
		$data = json_decode($result[0]['DATA'], TRUE);
		$data[$key] = $val;
		$str = json_encode($data, JSON_UNESCAPED_UNICODE);
		$sql = "update token_table set DATA = '".$str."' where TOKEN='".$token."'"; 
		$this->db->query($sql);
	} 
	public function get_flush_session_data($token, $key) {
		$query = $this->db->query("select DATA from token_table where TOKEN='".$token."'");
		$result = $query->result_array();
		$json_string = $result[0]['DATA'];
		$data = json_decode($json_string, TRUE);

		if(isset($data[$key])) {
			$retval = $data[$key];
			if(is_string($retval))
				$retval = $retval;
			else {
				foreach ($retval as $key1 => $value1) {
					if(is_string($value1)) {
						$retval[$key1] = $value1;
					}
				}
			}
			unset($data[$key]);
		}

		else {
			$retval = null;
		}
		$str = json_encode($data);
		$sql = "update token_table set DATA = '".$str."' where TOKEN='".$token."'"; 
		$this->db->query($sql);
		return $retval;
	}
	function codepoint_encode($str) {
        return substr(json_encode($str), 1, -1);
    }
	function codepoint_decode($str) {
        return json_decode(sprintf('"%s"', $str));
	}
	function is_japan_unicode($str){
		$is_japan = 0;
		$change_str = $str;
		//if(strlen($str) % 5 == 0){
			for($i = 0; $i<strlen($str);$i+=5){
				if($str[$i] == 'u'){
					$is_japan = 1;
				}else{
					$is_japan = 0;
				}
			}
			$new_str = '';
			if($is_japan == 1){
				for($i = 0;$i<strlen($str);$i+=5){
					$new_str .= '\\'.$str[$i].$str[$i+1].$str[$i+2].$str[$i+3].$str[$i+4];
				}
				$change_str = $this->codepoint_decode($new_str);
			}
		//}
		return $change_str;
	}

}