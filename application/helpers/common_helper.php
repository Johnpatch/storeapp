<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

//////////////////////////////////////////////////////////////////////
//PARA: Date Should In YYYY-MM-DD Format
//RESULT FORMAT:
// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'        =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
// '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
// '%m Month %d Day'                                            =>  3 Month 14 Day
// '%d Day %h Hours'                                            =>  14 Day 11 Hours
// '%d Day'                                                        =>  14 Days
// '%h Hours %i Minute %s Seconds'                                =>  11 Hours 49 Minute 36 Seconds
// '%i Minute %s Seconds'                                        =>  49 Minute 36 Seconds
// '%h Hours                                                    =>  11 Hours
// '%a Days                                                        =>  468 Days
//////////////////////////////////////////////////////////////////////
function dateDifference($date_1, $date_2) {
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    if ($datetime1 > $datetime2) {
        $datetime2 = $datetime1;
    }

    $interval = date_diff($datetime1, $datetime2);

    $unit = array(
        'year' => 'years',
        'month' => 'months',
        'day' => 'days',
        'hours' => 'hours',
        'minutes' => 'minutes',
        'seconds' => 'seconds'
    );
    $text = "";
    if ($interval->y > 0)
        $text .= " %y $unit[year] ";
    if ($interval->m > 0)
        $text .= " %m $unit[month] ";
    if ($interval->d > 0)
        $text .= " %d $unit[day] ";
    if ($interval->h > 0)
        $text .= " %h $unit[hours]";
    if ($interval->i > 0)
        $text .= " %i $unit[minutes]";
    if ($interval->s > 0 && $interval->i == 0 && $interval->h == 0 && $interval->d == 0 && $interval->m == 0 && $interval->y == 0)
        $text .= " %s $unit[seconds]";

    return $interval->format($text);
}

function chooseURL($str){
    if(substr($str, 0, 11) == 'attachments'){
        return base_url().$str;
    } else {
        return $str;
    }
}

function generate_url($url , $token = '') {
    return base_url($url).'?token='.$token;
}