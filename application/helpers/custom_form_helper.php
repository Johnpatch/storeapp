<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function color_view($color)
{
    return $color != "" ? "<div class='color-view' style='background-color: $color;'></div>" : lang('none');
}

function hidden_input($field, $is_view)
{
    $name = $field["name"];
    $value = $field["value"];
    return "<input type='hidden' name='$name' value='$value'>";
}

/*function datetime_input($name, $datetime) {
    $default_selected_year = '';
    $default_selected_month = '';
    $default_selected_date = '';
    $default_selected_time = '';
    $default_selected_minute = '';

    $start_year = 2013;
    $last_year = 2022;

    if($datetime != '') {
        $date_array = explode("-", $datetime);
        $default_selected_year = $date_array[0];
        $default_selected_month = $date_array[1];
        $default_selected_date = $date_array[2];

        $time_string = explode(" ", $datetime);
        $time_array = explode(':', $time_string[1]);
        $default_selected_time = $time_array[0];
        $default_selected_minute = $time_array[1];
    }

    $default_last_year = ($default_selected_year == '2099') ? "<option>2099</option>" : "<option>---</option>";
    $year_option_tag = "<input type='hidden' name='$name'><select>$default_last_year";
    for($y = $start_year; $y <= $last_year; $y ++) {
        $selected = ($default_selected_year == $y) ? 'selected' : '';
        $year_option_tag .= "<option value='".$y."' $selected>$y</option>";
    }
    $year_option_tag .= "</select>".lang('year');

    $month_option_tag = "<select><option>---</option>";
    for($m = 1; $m <= 12; $m ++) {
        $selected = ($default_selected_month == $m) ? 'selected' : '';
        $month_option_tag .= "<option value='".$m."' $selected>$m</option>";
    }
    $month_option_tag .= "</select>".lang('month');

    $day_option_tag = "<select><option>---</option>";
    for($d = 1; $d <= 31; $d ++) {
        $selected = ($default_selected_date == $d) ? 'selected' : '';
        $day_option_tag .= "<option value='".$d."' $selected>$d</option>";
    }
    $day_option_tag .= "</select>".lang('day');

    $time_option_tag = "<select><option>---</option>";
    for($t = 1; $t <= 23; $t ++) {
        $selected = ($default_selected_time == $t) ? 'selected' : '';
        $time_option_tag .= "<option value='".$t."' $selected>$t</option>";
    }
    $time_option_tag .= "</select>".lang('time');

    $minute_option_tag = "<select><option>---</option>";
    for($min = 1; $min <= 59; $min ++) {
        $selected = ($default_selected_minute == $min) ? 'selected' : '';
        $minute_option_tag .= "<option value='".$min."' $selected>$min</option>";
    }
    $minute_option_tag .= "</select>".lang('minute');

    return $year_option_tag.$month_option_tag.$day_option_tag.$time_option_tag.$minute_option_tag;
}

function date_input($name, $date) {
    $default_selected_year = '';
    $default_selected_month = '';
    $default_selected_date = '';

    $start_year = 2013;
    $last_year = 2022;

    if($date != '') {
        $date_array = explode("-", $date);
        $default_selected_year = $date_array[0];
        $default_selected_month = $date_array[1];
        $default_selected_date = $date_array[2];
    }

    $default_last_year = ($default_selected_year == '2099') ? "<option>2099</option>" : "<option>---</option>";
    $year_option_tag = "<input type='hidden' name='$name'><select>$default_last_year";
    for($y = $start_year; $y <= $last_year; $y ++) {
        $selected = ($default_selected_year == $y) ? 'selected' : '';
        $year_option_tag .= "<option value='".$y."' $selected>$y</option>";
    }
    $year_option_tag .= "</select>".lang('year');

    $month_option_tag = "<select><option>---</option>";
    for($m = 1; $m <= 12; $m ++) {
        $selected = ($default_selected_month == $m) ? 'selected' : '';
        $month_option_tag .= "<option value='".$m."' $selected>$m</option>";
    }
    $month_option_tag .= "</select>".lang('month');

    $day_option_tag = "<select><option>---</option>";
    for($d = 1; $d <= 31; $d ++) {
        $selected = ($default_selected_date == $d) ? 'selected' : '';
        $day_option_tag .= "<option value='".$d."' $selected>$d</option>";
    }
    $day_option_tag .= "</select>".lang('day');

    return $year_option_tag.$month_option_tag.$day_option_tag;
}*/

function date_input($field, $is_view)
{
    $name = $field["name"];
    $value = $field["value"];
    $class = (isset($field["class"]))? $field["class"] : '';
    if ($is_view) return $value;
    return "
                <input class='form-control form-control-inline input-medium date-picker cursor-pointer $class' name='$name' size='16' type='text' value='$value'/>
            ";
}

function datetime_input($field, $is_view)
{
    if ($is_view) return element("value", $field);
    $picker_content_class = ($field['picker_type'] == 'single') ? '' : 'from_to_datetime_content';
    $picker_input_class = ($field['picker_type'] == 'single') ? '' : 'from_to_datetime_input';
    $datetime_container = "
        <div class='input-group date form_datetime $picker_content_class' data-date=''>
            <input type='text' readonly name='$field[name]' class='form-control height-35 cursor-pointer $picker_input_class' value='$field[value]'>
            <span class='input-group-btn'>
                <button class='btn default date-reset' type='button'><i class='fa fa-times'></i></button>
                <button class='btn default date-set' type='button'><i class='fa fa-calendar'></i></button>
            </span>
        </div>";
    return $datetime_container;
}

function time_input($field, $is_view)
{
    if ($is_view) return element("value", $field);
    $value = ($field["value"] != '') ? $field["value"] : "00-00";
    $time_container = "
        <input type='text' name='$field[name]' value='$value' class='form-control timepicker timepicker-24'>";
    return $time_container;
}

function perpage_select($name, $value)
{
    $ret = "<select name='$name' class='pagination-content'>";
    $options = [5, 10, 20, 50];
    foreach ($options as $item) {
        $selected = $item == $value ? "selected" : "";
        $ret .= "<option value='$item' $selected>$item</option>";
    }
    $ret .= "</select>";
    return $ret;
}

function radio_input($field, $is_view)
{
    $ret = "";
    $value = $field["value"];
    if ($is_view) {
        foreach ($field["options"] as $val => $text) {
            if ($val == $value) return $text;
        }
        return $ret;
    }
    if(isset($field['inline']) && $field['inline'] == true) {
        $label_html = '<label class="radio-inline">';
        $end_html = "</label>";
    } else {
        $label_html = '<div class="radio"><label>';
        $end_html = "</label></div>";
    }

    foreach ($field["options"] as $val => $text) {
        $ret .= $label_html . form_radio([
                'name' => $field['name'],
                'value' => $val,
                'checked' => ($value == $val),
                'class' => 'styled'
            ]) . $text.$end_html;
    }
    return $ret;
}

function radio_image_input($field, $is_view)
{
    $ret = "";
    $value = $field["value"];
    if ($is_view) {
        foreach ($field["options"] as $val => $text) {
            if ($val == $value){
                $source_name = $text[0];
                return '<img style="width: 100px;" src="'.base_url($source_name).'" />';
            }
        }
        return $ret;
    }
    if(isset($field['inline']) && $field['inline'] == true) {
        $label_html = '<label class="radio-inline">';
        $end_html = "</label>";
    } else {
        $label_html = '<div class="radio"><label>';
        $end_html = "</label></div>";
    }

    foreach ($field["options"] as $val => $text) {
        $source_name = $text[0];
        if($text[1] == false){
            if($text[2] == true)
                $ret .= $label_html . form_radio([
                    'name' => $field['name'],
                    'value' => $val,
                    'data-url' => $source_name,
                    'checked' => 1,
                    'class' => 'styled'
                ]);
            else
                $ret .= $label_html . form_radio([
                    'name' => $field['name'],
                    'value' => $val,
                    'data-url' => $source_name,
                    'checked' => ($value == $val),
                    'class' => 'styled'
                ]);
        }
        else
            $ret .= $label_html . form_radio([
                'name' => $field['name'],
                'value' => $val,
                'data-url' => $source_name,
                'checked' => ($value == $val),
                'class' => 'styled',
                'disabled' => true
            ]);
        if(strstr($source_name, 'slide_images'))
            $ret .= '<img style="width: 100px;" src="'.base_url($source_name).'" />';
        else
            $ret .= '<img style="width: 100px;"  src="'.base_url($source_name).'" />';
        $ret .= $end_html;
    }
    return $ret;
}

function radio_view($field, $value)
{
    $ret = "";
    foreach ($field["options"] as $val => $text) {
        if ($val == $value) return $text;
    }
    return $ret;
}

function dropdown_view($field, $value)
{
    $ret = "";
    foreach ($field["options"] as $val => $text) {
        if ($val == $value) return $text;
    }
    return $ret;
}

/*
 * name: input's name
 * checked: checked value ex: Y
 * unchecked: unchecked value ex: N
 */
function checkbox_input($field, $is_view = false)
{

    $name = $field["name"];
    $text = $field['text'];
    $value = $field['value'];
    $class = $field['class'];
    if(empty($class))
        $class = '';
    $checked_v = $field['checked'];
    $unchecked_v = $field['unchecked'];
    if(isset($field['hide_text']))
        $hide_text = $field['hide_text'];
    if(isset($field['show_text']))
        $show_text = $field['show_text'];
    $checked = $value == $field['checked'];
    $value = $checked ? $field['checked'] : $field['unchecked'];
    $disabled = $field['disabled'];
    if ($is_view){
        if(!empty($show_text) && !empty($hide_text)){
            return $checked ? "<div>$show_text</div>" : "<div>$hide_text</div>";
        }else{
            return $checked ? "<div>$text</div>" : "";      
        }
    } 
    $id = rand();

    $result = "
        <div class='custom-checkbox-content checkbox " . ($checked ? ' selected ' : '') .$class . "' ".($disabled ? 'disabled' : '')." data-id='$id' data-checked-v='$checked_v' data-unchecked-v='$unchecked_v'>
            <div class='check-icon' style='display: inline-block;'>
                <i class='fa fa-check' style='display: none;'></i>
            </div>
            <label style='vertical-align: top'>$text</label>
        </div>
        <input type='hidden' name='$name' value='$value' id='check-input-$id'>
    ";
   
    
    return $result;
}

function textarea_input($field, $is_view = false)
{
    $name = $field["name"];
    $value = $field["value"];
    $function = $field['function'];
    if ($is_view) return "<span style='white-space: pre-wrap;'>".$value."</span>";
    return "<textarea class='form-control custom-text-area' rows='8' ".(!empty($function) ? " onchange='updatePreview()' " : '') ." cols='12' name='$name'>$value</textarea>";
}

function city_dropdown($field, $is_view)
{
    $options = [
        "" => '選択してください',
        1 => '北海道',
        2 => '青森県',
        3 => '秋田県',
        4 => '岩手県',
        5 => '山形県',
        6 => '宮城県',
        7 => '福島県',
        8 => '東京都',
        9 => '神奈川県',
        10 => '埼玉県',
        11 => '千葉県',
        12 => '群馬県',
        13 => '栃木県',
        14 => '茨城県',
        15 => '新潟県',
        16 => '長野県',
        17 => '山梨県',
        18 => '静岡県',
        19 => '愛知県',
        20 => '岐阜県',
        21 => '三重県',
        22 => '富山県',
        23 => '石川県',
        24 => '福井県',
        25 => '大阪府',
        26 => '京都府',
        27 => '兵庫県',
        28 => '奈良県',
        29 => '和歌山県',
        30 => '滋賀県',
        31 => '岡山県',
        32 => '広島県',
        33 => '鳥取県',
        34 => '島根県',
        35 => '山口県',
        36 => '香川県',
        37 => '徳島県',
        38 => '愛媛県',
        39 => '高知県',
        40 => '福岡県',
        41 => '佐賀県',
        42 => '長崎県',
        43 => '熊本県',
        44 => '大分県',
        45 => '宮崎県',
        46 => '鹿児島県',
        47 => '沖縄県'
    ];

    $ret = "";
    $name = $field["name"];
    $value = $field["value"];
    $ret .= "<select class='form-control' name='$name'>";
    foreach ($options as $key => $val) {
        $selected = $key == $value ? "selected" : "";
        if ($is_view && $selected == 'selected') {
            return $val;
        }
        $ret .= "<option value='$key' $selected>$val</option>";
    }
    $ret .= "</select>";
    return $ret;
}

function dropdown_input($field, $is_view = false)
{
    $ret = "";
    $value = $field["value"];
    if ($is_view) {
        foreach ($field["options"] as $val => $text) {
            if ($val == $value) return $text;
        }
        return $ret;
    }
    $name = $field["name"];
    $label = isset($field['text']) ? $field['text'] : '';
    $class = isset($field['class']) ? $field['class'] : '';
    $ret .= "<select  class='form-control cursor-pointer padding-5 $class' style='margin-top: 5px;' name='$name' data-value='$value'>";
    foreach ($field["options"] as $key => $val) {
        $selected = $key == $value ? "selected" : "";
        $ret .= "<option value='$key' $selected>$val</option>";
    }
    $ret .= "</select>".$label;
    return $ret;
}

function bloghp_input($field, $is_view = false)
{

    $inputs = $field["input"];
    $video_name = element('name', $inputs[0]);
    $comment_name = element('name', $inputs[1]);
    $video_placeholder = element('placeholder', $inputs[0]);
    $comment_placeholder = element('placeholder', $inputs[1]);
    $video_default = element("default", $inputs[0]);
    $comment_default = element("default", $inputs[1]);
    $button = element("button", $field);
    $result =
        "<div class='video-public-section'>";

    /*hide select button when is_view*/
    if (!$is_view) $result .=
        "<div class='custom-btn-select video-item'>
                    <input type='text' class='form-control' id='current_video_input' placeholder='$video_placeholder' value='$video_default' />
                    <input type='text' class='form-control' id='current_comment_input' placeholder='$comment_placeholder' value='$comment_default'/>
                </div>
                <a class='common-btn-red-small custom-btn video-add-button " . ($is_view ? ' hidden ' : '') . "' data-video-name='$video_name' data-comment-name='$comment_name'>
                    <i class='fa fa-plus'></i>
                    上記の動画を追加する
                </a>
                <hr />
                ";

    $result .= "<div class='photo-comments-list'></div>
            </div>
            <script>
        ";

    $values = array();
    $values[0] = $inputs[0]["value"];
    $values[1] = $inputs[1]["value"];
    if (element("value", $inputs[0]) == false) $len = 0;
    else
        $len = count($values[0]);


    for ($i = 0; $i < $len; $i++) {
        $id = rand();
        $video_value = $values[0][$i];
        $comment_value = $values[1][$i];
        $result .= "
                add_video('$id', '$video_name', '$video_value', '$comment_name', '$comment_value', '" . ($is_view ? 'readonly' : '') . "');";
    }
    $result .= '</script>';
    return $result;
}

function bloghp_dropdown_input($field, $is_view = false)
{

    $inputs = $field["input"];
    $video_name = element('name', $inputs[0]);
    $comment_name = element('name', $inputs[1]);
    $video_placeholder = element('placeholder', $inputs[0]);
    $comment_placeholder = element('placeholder', $inputs[1]);
    $video_default = element("default", $inputs[0]);
    $comment_default = element("default", $inputs[1]);
    $button = element("button", $field);
    $options = element("options", $inputs[0]);
    $result =
        "<div class='video-public-section'>";

    /*hide select button when is_view*/

    $hidden = $is_view ? "hidden" : "";
    $select = "<select class='cursor-pointer form-control col-md-6' id='current_select_input' placeholder='$video_placeholder' style='width: 45%;padding: 3px;'>";
    foreach ($options as $key=>$val) {
        $select .= "<option value='$key'>$val</option>";
    }
    $select .= "</select>";
    $result .=
        "<div class='custom-btn-select video-item $hidden'>
                    $select
                    <input type='text' class='form-control' id='current_comment_input' placeholder='$comment_placeholder' value='$comment_default'/>
                </div>
                <a class='common-btn-red-small custom-btn hp-add-button " . ($is_view ? ' hidden ' : '') . "' data-video-name='$video_name' data-comment-name='$comment_name'>
                    <i class='fa fa-plus'></i>
                    上記のサイトを追加する
                </a>
                <hr class='$hidden'/>
                ";

    $result .= "<div class='photo-comments-list'></div>
            </div>
            <script>
        ";

    $values = array();
    $values[0] = $inputs[0]["value"];
    $values[1] = $inputs[1]["value"];
    if (element("value", $inputs[0]) == false) $len = 0;
    else
        $len = count($values[0]);


    for ($i = 0; $i < $len; $i++) {
        $id = rand();
        $video_value = $values[0][$i];
        $comment_value = $values[1][$i];
        $result .= "
                add_hp('$id', '$video_name', '$video_value', '$comment_name', '$comment_value', '" . ($is_view ? 'readonly' : '') . "');";
    }
    $result .= '</script>';
    return $result;
}

function text_input($field, $is_view = false)
{
    $name = $field["name"];
    $value = $field["value"];
    $function = $field['function'];
    $addon = element("addon", $field);
    $class = $field["class"];
    if ($is_view) return $addon . $value;
    if ($addon) {
        $addon = "<span class='input-group-addon'>$addon</span>";
        return "<div class='input-group'>" . $addon . "<input  class='form-control' type='text' name='$name' value='$value'></div>";
    } else {
        return "<input  class='form-control $class' type='text' ".(!empty($function) ? " onchange='updatePreview'" : '' )." name='$name' value='$value'>";
    }
}

function text_readonly_input($field, $is_view = false)
{
    $name = $field["name"];
    $value = $field["value"];
    $addon = element("addon", $field);
    if ($is_view) return $addon . $value;
    if ($addon) {
        $addon = "<span class='input-group-addon'>$addon</span>";
        return "<div class='input-group'>" . $addon . "<input readonly class='form-control' type='text' name='$name' value='$value'></div>";
    } else {
        return "<input readonly class='form-control' type='text' name='$name' value='$value'>";
    }
}

function password_input($field, $is_view = false)
{
    if ($is_view) return "-";
    $name = $field["name"];
    $value = $field["value"];
    if($is_view)
        return "<input  class='form-control' type='password' name='$name' value='$value'>";
    else
        return "<input  class='form-control' type='password' name='$name' value=''>";
}

function email_input($field, $is_view = false)
{
    $name = $field["name"];
    $value = $field["value"];

    if ($is_view) return $value;
    return "<input class='form-control' type='email' name='$name' value='$value'>";
}

function ckeditor_input($field, $is_view)
{
    $name = $field["name"];
    $value = $field["value"];
    if ($is_view == true) return '<div>'.$value.'</div>';
    $id = 'ckeditor-input-' . rand();
    $set_mode = isset($field["start_mode"]) ?
        ("CKEDITOR.config.startupMode = '" . $field["start_mode"] . "';") : '';

    $result = "
        <textarea name='$name' id='$id' >$value</textarea>
        <script>
            CKEDITOR.replace('$id',{
                on: {
                    blur: function(evt) {
                        $('#$id').text(evt.editor.getData());
                        updatePreview();
                    }
                }
            });
            CKEDITOR.config.entities = true;
            CKEDITOR.config.toolbarGroups = [
                { name: 'document', groups: [ 'mode'] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks' ] },
                { name: 'links', groups: [ 'Link', 'Unlink'] },
                { name: 'basicstyles', groups: ['basicstyles'] },
                { name: 'insert', groups: ['Image', 'Table'] },
            ];
            $set_mode
        </script>
    ";
    return $result;
}

function blog_editor_input($field, $is_view)
{
    
    $name = $field["name"];
    $value = $field["value"];
    if ($is_view == true) return $value;
    $id = 'blog-editor-input-' . rand();
    $result = "
        <textarea name='$name' id='$id'>$value</textarea>
        <script>tinymce.init({selector:'textarea#$id'});</script>
    ";
    return $result;
}


function static_input($field, $is_view = false)
{
    $options = element('options', $field);
    $value = $field["value"];
    if ($options) {
        foreach ($options as $key => $val) {
            if ($key == $value) return $val;
        }
    }
    return $value;
}

function anchor_input($field, $is_view = false)
{
    $value = $field["value"];
    return $value;
}

function number_input($field, $is_view = false)
{
    $name = $field["name"];
    $value = $field["value"];
    $class = $field['class'];
    if(empty($class))
        $class = '';
    $addon = element("addon", $field);
    if ($is_view) return $addon . $value;
    if ($addon) {
        $addon = "<span class='input-group-addon'>$addon</span>";
        return "<div class='input-group'>" . $addon . "<input  class='form-control' type='number' name='$name' value='$value' min='0'></div>";
    } else {
        return "<input  class='form-control ".$class."' type='number' name='$name' value='$value' min='0'>";
    }
}

function map_image($field, $is_view = false)
{
    $url = base_url('attachments/map.png');
    return "<img src='$url'/>";
}

function qr_code_image($field, $is_view = false)
{
    $name = $field['name']; 
    $value = $field['value'];
    if(empty($value)){
        $value = rand();
        $text = base_url().'admin/stamp/stampprivilege/scan_qrcode/'.$value;
        $SERVERFILEPATH = $_SERVER['DOCUMENT_ROOT'].'/qrcode';
        $file_name = $SERVERFILEPATH."/".$value.".png";
        $CI =& get_instance();
        $CI->load->library('phpqrcode/qrlib'); // load library 
        QRcode::png($text,$file_name);
    }
    
    $url = base_url()."qrcode/".$value.".png";
    
    return "<img src='$url'/><input type='hidden' name='$name' value='$value' />";
}

function file_input($field, $is_view = false)
{
    // return "<input type='text' name='$name' value='$value'>";
    $id = rand();
    $name = $field["name"];
    $value = $field["value"];
    if (strncmp($value, "attachments", strlen("attachments")) == 0) {
        $src_value = base_url($value);
    } else {
        $src_value = $value;
    }
    /*if(!file_exists($src_value)) {
        if($value == "") {
            $src_value = base_url('assets/imgs/no-image.png');
        }
    }*/
    $result = "
        <img class='preview" . ($value ? '' : ' hidden') . "' id='image-$id' src='$src_value' />
        <div class='custom-btn-select " . ($is_view ? 'hidden' : '') . "'>
            <a data-id='$id' class='btn btn-primary image-selection-btn btn-xlg btn_image" . ($value ? ' hidden' : '') . "' id='btn_select_$id' >
            画像選択
            </a>
            <a data-id='$id' class='btn btn-primary btn-xlg change-btn btn_image btn_image_change" . ($value ? '' : ' hidden') . "' id='btn_change_$id' >
            画像選択
            </a>
            <div class='hidden'>
                <input type='text' data-id='$id' id='input-$id' name='$name' value='$value'/>
                <input type='file' data-id='$id' id='file-$id' class='file' accept='image'/>
            </div>
        </div>
    ";

    return $result;
}

function pdf_input($field, $is_view = false)
{
    // return "<input type='text' name='$name' value='$value'>";
    $id = rand();
    $name = $field["name"];
    $value = $field["value"];
    $icon = base_url("assets/imgs/pdf.png");
    $result = "
    <div>
        <p id='filename-$id'></p>
        <img class='preview" . ($value ? '' : ' hidden') . "' id='image-$id' src='$icon' />
        </div>

        <div class='" . ($is_view ? 'hidden' : '') . "'>
            <a data-id='$id' class='btn btn-primary btn-xlg w-icon-red-btn btn_image btn_image_select" . ($value ? ' hidden' : '') . "' id='btn_select_$id' >
            PDFの選択
            </a>
            <a data-id='$id' class='btn btn-primary btn-xlg w-icon-red-btn btn_image btn_image_change" . ($value ? '' : ' hidden') . "' id='btn_change_$id' >
            変更する
            </a>
            <div class='hidden'>
                <input type='text' data-id='$id' id='input-$id' name='$name' value='$value'/>
                <input type='file' data-id='$id' data-type='pdf' id='file-$id' class='file' accept='image'/>
            </div>
        </div>
    ";

    return $result;
}

function csv_input($field, $is_view = false)
{
    // return "<input type='text' name='$name' value='$value'>";
    $id = rand();
    $name = $field["name"];
    $value = $field["value"];
    $icon = base_url("assets/imgs/csv.jpg");
    $result = "
    <div>
        <p id='filename-$id'></p>
        <img class='preview" . ($value ? '' : ' hidden') . "' id='image-$id' src='$icon' />
        </div>

        <div class='" . ($is_view ? 'hidden' : '') . "'>
            <a data-id='$id' class='btn btn-danger btn-lg w-icon-red-btn btn_image btn_image_select" . ($value ? ' hidden' : '') . "' id='btn_select_$id' >
            ファイル選択
            </a>
            <a data-id='$id' class='btn btn-danger btn-lg btn_image w-icon-red-btn btn_image_change" . ($value ? '' : ' hidden') . "' id='btn_change_$id' >
            ファイル選択
            </a>
            <div class='hidden'>
                <input type='text' data-id='$id' id='input-$id' name='$name' value='$value'/>
                <input type='file' data-id='$id' data-type='pdf' id='file-$id' class='file' accept='image'/>
            </div>
        </div>
    ";

    return $result;
}

/*
 * Photo input
 *
 */
function photo_input($field, $is_view = false)
{

    $inputs = $field["input"];
    $image_name = element('name', $inputs[0]);
    $comment_name = element('name', $inputs[1]);
    $button = element("button", $field);
    $result = "<div class='photo-public-section margin-top-10'>";

    /*hide select button when is_view*/
    if (!$is_view)
        $result .= "
                <a class='btn btn-primary btn-xlg w-icon-red-btn photo-selection-button " . ($is_view ? ' hidden ' : '') . "'>$button</a>
                <input type='file' id='photo-selection-file' class='hidden' data-image-name='$image_name' data-comment-name='$comment_name' />
                <hr />";

    $result .= "<div class='photo-comments-list'></div>
            </div>
            <script>
        ";

    $values = array();
    $values[0] = $inputs[0]["value"];
    $values[1] = $inputs[1]["value"];
    if (element("value", $inputs[0]) == false) $len = 0;
    else
        $len = count($values[0]);
    for ($i = 0; $i < $len; $i++) {
        $id = rand();
        $image_value = $values[0][$i];
        if (strncmp($image_value, "attachments", strlen("attachments")) == 0) {
            $src_value = base_url($image_value);
        } else {
            $src_value = $image_value;
        }
        $comment_value = $values[1][$i];
        $result .= "
                add_photo('$id', '$image_name', '$image_value', '$src_value', '$comment_name', '$comment_value', '" . ($is_view ? 'readonly' : '') . "');";
    }
    $result .= '</script>';
    return $result;
}

function video_input($field, $values, $readonly = false)
{

    $video_name = element('name', $field);
    $comment_name = element('comment', $field);

    $result =
        "<div class='video-public-section'>";

    /*hide select button when is_view*/
    if (!$readonly) $result .=
        "<div class='video-item'>
                    <input type='text' class='form-control' id='current_video_input' placeholder='Video URL' value='https://www.youtobe.com/watch?v='  />
                    <input type='text' class='form-control' id='current_comment_input' placeholder='comment' value=''/>
                </div>
                <button type='button' class='video-add-button " . ($readonly ? ' hidden ' : '') . "' data-video-name='$video_name' data-comment-name='$comment_name'>
                    <i class='fa fa-plus'></i>
                    Add the above video
                </button>
                <hr />
                <label>List of currently released videos</label>
                ";

    $result .= "<div class='photo-comments-list'></div>
            </div>
            <script>
        ";


    if (element($video_name, $values) == false) $len = 0;
    else $len = count($values[$video_name]);
    for ($i = 0; $i < $len; $i++) {
        $id = rand();
        $video_value = $values[$video_name][$i];
        $comment_value = $values[$comment_name][$i];
        $result .= "
                add_video('$id', '$video_name', '$video_value', '$comment_name', '$comment_value', '" . ($readonly ? 'readonly' : '') . "');";
    }
    $result .= '</script>';
    return $result;
}

function form_group($content, $class = "")
{
    if(strpos($class, '[]') !== false){
        $class = str_replace('[]', '', $class);
    }
    return "<tr class='$class'>$content</tr>";
}

function control_label($content, $is_required = false, $label_name = '', $is_view = false)
{
    if ($is_required == true) $required = required_mark();
    else $required = "";
    if($label_name == '' || $is_view == true)
        return "<td class='col-md-3'>$content $required <input type='hidden' name='$label_name' value='$content' /></td>";
    else
        return "<td class='col-md-3'>
            <input type='text' name='$label_name' class='form-control' value='$content' />
        </td>";
}

function form_input_wrap($content)
{
    return "<td class='col-md-9'>$content</td>";
}

function help_block($content)
{
    return "<span class='help-block help-area'>$content</span>";
}

function required_mark()
{
    $text = lang('required');
    return "<span class='required-mark'>$text</span>";
}


function color_input($field, $is_view)
{

    if ($is_view == true) {
        $color = $field["value"];
        return $color != "" ? "<div class='color-view' style='background-color: $color;'></div>" : lang('none');
    }

    $id = rand();
    $name = $field["name"];
    $value = $field["value"];
    $color_array = array(
        array('#ffeaeb', '#f7f9e4', '#e3f9e6', '#e7fbf8', '#e3f7ff', '#ebebf7', '#eeeef7', '#eeeeee'),
        array('#fe979f', '#fec97e', '#a9d163', '#75d9d9', '#7dd3ed', '#b098c8', '#f8a4c8', '#aaaaaa'),
        array('#fd2831', '#fe933a', '#6cb036', '#2ab1b1', '#2d83d2', '#9454d8', '#fd2e93', '#666666'),
        array('#8c1c1f', '#a95413', '#41691f', '#196a6a', '#164168', '#542c54', '#8c1e54', '#111111'),
    );

    $result = '<p>
                    <i class="fa fa-sort-desc"></i>
                    <span>おすすめカラー</span>
                </p>';

    foreach ($color_array as $line) {
        $result .= '<div class="color-line"=>';
        foreach ($line as $color) {
            $selected = "";
            if ($color == $value) $selected = "selected";
            $result .= "<div class='coloritem $selected' data-id='$id' data-color='$color' style='background-color: $color' ></div>";
        }
        $result .= '</div>';
    }


    $result .= "<p>
                    <i class='fa fa-sort-desc'></i>
                    <span>カラーパレット選択</span>
                </p>
               <input type='color' class='colpicker hide-origin-color-picker' name='$name' value='$value' id='color-input-$id'>
                <a class='select-color-button colpicker' onclick=$('#color-input-$id').click()><i class='fa fa-plus color-plus-button'></i></a>
                ";

    return $result;
}

/*function form_element($field, $value, $is_view = false) {
    $help = element('help', $field);
    $label = element('label', $field);
    $required = element('required', $field);
    $group = element('group', $field);
    $content = "";
    $between = element("between", $field, "");


    $input_array = array();
    if ($group == true) {
        foreach ($field["input"] as $field_input) {
            $input_array[] = $field_input;
        }
        $values = $value;
    } else {
        $input_array[] = $field;
        $values = [$field["name"]=>$value];
    }


    //Switching by Input type
    foreach ($input_array as $single) {
        $name = element('name', $single);
        $type = element('type', $single);
        $help = element('help', $single);
        $required = element('required', $single);
        $input = "";
        $value = $values[$name];
        if ($is_view) $input = hidden_input($name, $value);
        if ($type == "color") {
            $input .= $is_view ? color_view($value) : color_input($name, $value);
        }
        elseif ($type == "hidden") {
            $input .= $is_view ? "" : hidden_input($name, $value);
        }
        elseif ($type == "date") {
            $input .= $is_view ? $value : date_input($name, $value);
        }
        elseif ($type == 'datetime') {
            $input .= $is_view ? $value : datetime_input($name, $value, $id);
        }
        elseif ($type == 'textarea') {
            $input .= $is_view ? $value : textarea_input($name, $value);
        }
        elseif ($type == 'checkbox') {
            $input .= checkbox_input($single, $value, $is_view);
        }
        elseif ($type == 'radio') {
            $input .= $is_view ? radio_view($single, $value) : radio_input($single, $value);
        }
        elseif ($type == 'file') {
            // $input .= $is_view ? file_view($name, $value) : file_input($name, $value);
            $input .= file_input($name, $value, $is_view);
        }
        elseif ($type == 'text') {
            $input .= text_input($name, $value, $is_view);
        }
        elseif ($type == 'static') {
            $input .= static_input($single, $value, $is_view);
        }
        elseif ($type == 'email') {
            $input .= email_input($name, $value, $is_view);
        }
        elseif ($type == 'ckeditor') {
            $input.= ckeditor_input($name, $value, $is_view);
        }
        elseif ($type == 'dropdown') {
            $input .= $is_view ? dropdown_view($single, $value) : dropdown_input($single, $value);
        }
        elseif ($type == 'city_dropdown') {
            $input .= city_dropdown($single, $value, $is_view);
        }
        else if ($type == 'photo') {
            $input .= photo_input($single, $is_view);
        } else if ($type == 'video') {
            $input .= video_input($single, $is_view);
        }
        elseif ($type == 'password') {
            $input .= password_input($name, $value, $is_view);
        }
        if ($content != "") {
            $content .= $between;
        }

        $content .= $input;
    }


    if ($is_view == false && $help != false) {
        $content .= help_block($help);
    }
    $content = form_input_wrap($content);
    $label = control_label($label, $required);
    return form_group($label.$content);
}*/

function form_input($field, $is_view)
{
    $type = element("type", $field);
    if ($type == "color") {
        $input = color_input($field, $is_view);
    } elseif ($type == "hidden") {
        $input = hidden_input($field, $is_view);
    } elseif ($type == "date") {
        $input = date_input($field, $is_view);
    } elseif ($type == 'datetime') {
        $input = datetime_input($field, $is_view);
    } elseif ($type == 'time') {
        $input = time_input($field, $is_view);
    } elseif ($type == 'textarea') {
        $input = textarea_input($field, $is_view);
    } elseif ($type == 'checkbox') {
        $input = checkbox_input($field, $is_view);
    } elseif ($type == 'radio') {
        $input = radio_input($field, $is_view);
    } elseif ($type == 'radio_image') {
        $input = radio_image_input($field, $is_view);
    } elseif ($type == 'file') {
        $input = file_input($field, $is_view);
    } elseif ($type == 'text') {
        $input = text_input($field, $is_view);
    } elseif ($type == 'text_readonly') {
        $input = text_readonly_input($field, $is_view);
    } elseif ($type == 'static') {
        $input = static_input($field, $is_view);
    } elseif ($type == 'email') {
        $input = email_input($field, $is_view);
    } elseif ($type == 'ckeditor') {
        $input = ckeditor_input($field, $is_view);
    } elseif ($type == 'blog_editor') {
        $input = blog_editor_input($field, $is_view);
    } elseif ($type == 'dropdown') {
        $input = dropdown_input($field, $is_view);
    } elseif ($type == 'city_dropdown') {
        $input = city_dropdown($field, $is_view);
    } else if ($type == 'photo') {
        $input = photo_input($field, $is_view);
    } else if ($type == 'video') {
        $input = video_input($field, $is_view);
    } elseif ($type == 'password') {
        $input = password_input($field, $is_view);
    } elseif ($type == 'bloghp') {
        $input = bloghp_input($field, $is_view);
    } elseif ($type == 'bloghp_dropdown') {
        $input = bloghp_dropdown_input($field, $is_view);
    } elseif ($type == "pdf") {
        $input = pdf_input($field, $is_view);
    } elseif ($type == "csv") {
        $input = csv_input($field, $is_view);
    } elseif ($type == 'anchor') {
        $input = anchor_input($field, $is_view);
    } elseif ($type == 'number') {
        $input = number_input($field, $is_view);
    } elseif ($type == 'map_image') {
        $input = map_image($field, $is_view);
    } elseif ($type == 'qr_code') {
        $input = qr_code_image($field, $is_view);
    }

    return $input;
}

function render_element($field, $is_view = false)
{
    $help = element('help', $field);
    $label = element('label', $field);
    $required = element('required', $field);
    $group = element('group', $field);
    $header = element('header', $field);
    $content = "";
    if (!$is_view && $header) $content = $header;
    $between = element("between", $field, "");
    $error = element("error", $field);
    $array = element("array", $field);
    $name = element("name", $field);
    $input_array = array();
    if ($group == true) {
        foreach ($field["input"] as $field_input) {
            $input_array[] = $field_input;
        }
    } else {
        $input_array[] = $field;
    }

    if ($array == true) {
        if($header)
            $content .= form_input($field, $is_view);
        else
            $content = form_input($field, $is_view);
    } else {
        foreach ($input_array as $single) {
            $input = "";
            if ($is_view && $single["type"] != "static") $input = hidden_input($single, $is_view);
            $input .= form_input($single, $is_view);

            if ($content != "") {
                $content .= $between;
            }
            $content .= $input;
        }
    }

//    var_dump($error);

    $content_class = $name;
    if ($is_view == false) {
        if ($help != false) {
            $content .= help_block($help);
        }
        if ($error) {
            $content_class .= " has-error";
            $content .= help_block($error);
        }
    }
    $content = form_input_wrap($content);
    $label_edit = element("label_edit", $field);
    if($label_edit){
        $label_name = element("label_name", $field);
        $label = control_label($label, $required, $label_name, $is_view);
    }
    else
        $label = control_label($label, $required);
    return form_group($label . $content, $content_class);
}

function content_view($field, $value)
{
    $group = element('group', $field);
    $between = element("between", $field, "");
    $input_array = array();
    if ($group == true) {
        foreach ($field["input"] as $field_input) {
            $input_array[] = $field_input;
        }
        $values = $value;
    } else {
        $input_array[] = $field;
        $values = [$field["name"] => $value];
    }

    $content = "";
    foreach ($input_array as $single) {
        $name = element('name', $single);
        $type = element('type', $single);
        $help = element('help', $single);
        $required = element('required', $single);
        $input = "";
        $value = $values[$name];
        if ($type == "color") {
            $input .= color_view($name, $value);
        } elseif ($type == "hidden") {
            $input .= hidden_input($name, $value);
        } elseif ($type == "date") {
            $input .= $value;
        } elseif ($type == 'datetime') {
            $input .= $value;
        } elseif ($type == 'textarea') {
            $input .= $value;
        } elseif ($type == 'checkbox') {
            $input .= checkbox_input($single, $value, true);
        } elseif ($type == 'radio') {
            $input .= radio_view($single, $value);
        } elseif ($type == 'file') {
            // $input .= $is_view ? file_view($name, $value) : file_input($name, $value);
            $input .= file_input($single, true);
        } elseif ($type == 'text') {
            $input .= $value;
        } elseif ($type == 'dropdown') {
            $input .= dropdown_view($single, $value);
        } else if ($type == 'photo') {
            $input .= photo_input($single);
        }
    }

    if ($content != "") {
        $content .= $between;
    }
    $content .= $input;

    return $content;
}