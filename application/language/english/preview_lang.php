<?php

//navbar menu
$lang['cps_dash_1'] = 'ご来店で1スタンププレゼント！';
$lang['btn_receive_stamp'] = 'スタンプを受け取る';
$lang['cp_about_body'] = 'スタンプが４個貯まると、コンプリートチケットが発行されます。ご利用の際は店頭でチケット画面をご提示下さい。';
$lang['cp_benefit_discount'] = 'お会計から1,000円引き！';