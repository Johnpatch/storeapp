<?php
//common
$lang['required'] = '必須';
$lang['status'] = 'ステータス';
$lang['cancel'] = 'キャンセル';
$lang['close'] = '閉じる';
$lang['csv_download'] = 'CSV ダウンロード';
//navbar menu
$lang['home'] = 'Home';
$lang['password_change'] = 'パスワードの変更';
$lang['password'] = 'パスワード';
$lang['reset_password'] = 'パスワードリセット';

//password change form
$lang['security'] = 'セキュリティ';
$lang['changed'] = '変化されました!';
$lang['change_my_password'] = 'パスワードの変更';
$lang['new_password'] = '新規パスワード';
$lang['update'] = '更新';
$lang['password_strength'] = 'パスワードの強度';
$lang['generate_password'] = 'パスワード作成';

//logout
$lang['logout'] = 'ログアウト';

//Sidebar-menu toggler
$lang['show_menu'] = 'メニューを見せる';
$lang['hide_menu'] = 'メニューを隠す';


//Sidebar Menu
$lang['basic_menu_settings'] = '基本設定';
$lang['agency_basic_information'] = '代理店基本情報';
$lang['webview_app'] = 'ウエブビューアプリ';
$lang['branch_management'] = '支店管理';
$lang['check_edit_branch_information'] = '支店情報の確認/編集';
$lang['layout_settings'] = 'レイアウト設定';
$lang['slide_image_settings'] = 'スライド画像設定 ';
$lang['header_footer_image_settings'] = 'ヘッダーフッター画像・編集';
$lang['header_footer_image_setting'] = 'ヘッダー・フッター画像設定';
$lang['page_color_settings'] = 'ページカラー設定';
$lang['membership_card_page_color_settings'] = 'メンバーシップカードページカラー設定';
$lang['top_menu_settings'] = 'トップメニュー設定';
$lang['edit_top_menu'] = 'トップメニュー編集';
$lang['basic_information'] = '基本コンテンツ';
$lang['store_information'] = 'ストア情報';
$lang['store_request'] = 'ストア要求';
$lang['photo_information'] = '写真情報';
$lang['photo_gallery'] = 'フォトギャラリー';
$lang['video_information'] = '動画情報';
$lang['video_registration'] = 'ビデオ登録';
$lang['movie'] = 'ムービー';
$lang['list_of_links'] = 'リンクリスト';
$lang['terms_and_service'] = '契約条件とサービス';
$lang['terms_service'] = '利用規約';
$lang['privacy_policy'] = 'プライバシーポリシー';
$lang['company_profile'] = '会社案内';
$lang['company_career'] = '会社でのキャリア';
$lang['president'] = 'プレジデント';
$lang['staff'] = 'スタッフ';
$lang['corporate_philosophy'] = '企業理念';
$lang['corporate_profile'] = '企業プロファイル';
$lang['business_introduction'] = 'ビジネス';
$lang['history'] = '事業履歴';
$lang['access'] = 'アクセス';
$lang['facebook'] = 'Facebook';
$lang['facebook_url'] = 'Facebook Url';
$lang['twitter'] = 'Twitter';
$lang['twitter_url'] = 'Twitter Url';
$lang['instgram'] = 'Instagram';
$lang['instagram_url'] = 'Instagram Url';
$lang['product_menu'] = '製品メニュー';
$lang['create'] = '作成';
$lang['edit_delete'] = '編集/削除';
$lang['coupon'] = 'クーポン';
//$lang['news_events'] = 'ニュース/イベント';
$lang['news_events'] = '投稿コンテンツ';
$lang['survey'] = 'アンケート';
$lang['questionnaire'] = 'アンケート';
$lang['aggregate'] = '集約';
//Push notification
$lang['push_notification'] = 'プッシュ配信';
$lang['individual_push'] = '個別プッシュ';
$lang['survey_push'] = 'アンケートプッシュ';
$lang['survey_setting'] = '編集/削除';
$lang['delivery_target'] = 'デリバリターゲット';
$lang['history_statics'] = 'プッシュ履歴の統計';
$lang['search_list'] = '検索/リスト';
$lang['delivery_type_help'] = "<span class='help-block help-down-area' style='padding: 15px;'><b>プッシュ通知の内容を選択してください。</b></span>";
$lang['push_form_description'] = "作成したクーポンや通知情報をプッシュ通知で顧客に配信できます。";/*<br>
                                <span class='red'>* Android 使用設定はオンになっていません。 Android プッシュ配信が必要な場合は、管理者に問い合わせてください。</span><br>
                                <span class='red'>*  iOS 使用設定はオンになっていません。 iOS のプッシュ配信が必要な場合は、管理者に問い合わせてください。</span>";*/
$lang['new_push_notification'] = "新規プッシュ通知";
$lang['auto_push_notification'] = '自動プッシュ配信';
$lang['birthday_push'] = '誕生日配信';
$lang['welcome_push'] = 'ウェルカムプッシュ';
$lang['store_stamp_information'] = 'スタンプ情報の保存';
$lang['stamp_usage_settings'] = 'スタンプの使用設定';
$lang['individual_location_information'] = "ロケーション情報による個別配信";
$lang['csv_upload_distribution'] = "CSVアップロード配布";
$lang['delivery_stamp_cards'] = "完成しそうな人にスタンプカードを配布。";
$lang['delivered_coupon_unused'] = "未使用のクーポンに配布。";
$lang['delivery_stamp_distribution'] = "前回スタンプ配布日以降、お越した方へのお渡し。";
$lang['csv_file'] = "CSV ファイル";
$lang['delivery_time_setting'] = "配信時間設定";
$lang['delivery_10'] = "即時";
$lang['make_reservation'] = "時間指定";
$lang['delivery_area'] = "配送地域";
$lang['within_two_radius'] = "店舗から半径2km以内の人。";
$lang['within_five_radius'] = "店舗から半径5km以内の人。";
$lang['within_ten_radius'] = "店舗から半径10km以内の人。";
$lang['sex'] = "性別";
$lang['male'] = "男";
$lang['female'] = "女";
$lang['age'] = "歳";
$lang['last_stamp_distribution'] = "前スタンプ配布日からの日付";
$lang['stamps_complete'] = "完成するスタンプの数";
$lang['delivery_date'] = "配布日";
$lang['correction_confirmation'] = "訂正/確認";
$lang['delivery_datetime'] = "配送日時";
$lang['delivery_type'] = "配信対象";
$lang['delivery_target'] = "配信対象詳細";
$lang['coupon'] = "クーポン";
$lang['coupon_setting_edit'] = "編集/削除";
$lang['coupon_create'] = "新規クーポン作成";
$lang['coupon_edit'] = "クーポン編集";
$lang['coupon_description'] = "クーポンを作成および発行したり、プッシュ通知で配布したりできます。<br>
クーポンに画像を表示したい場合は、事前に画像を用意してください。";
//Auto Push
$lang['auto_push_notification'] = '自動プッシュ通知';

$lang['welcome_push'] = 'ようこそプッシュ';
$lang['auto_push_notification'] = '自動プッシュ配信';
$lang['welcome_push'] = 'ようこそプッシュ';
$lang['automatic_push_notification'] = '自動プッシュ配信';
$lang['automatic_push'] = '自動プッシュ';
//Store Stamp
$lang['store_stamp_information'] = '店舗スタンプ情報';
$lang['stamp_usage_settings'] = 'スタンプの使用設定';
$lang['stamp_image_registration'] = 'スタンプ画像の登録';
$lang['stamp_and_privilege_setting'] = 'スタンプと権限設定';
$lang['card_list'] = 'カードリスト';
$lang['progress'] = '進行状況';
$lang['benefits_usage_status'] = '利点使用量';
$lang['stampsetting_page_title'] = '店舗スタンプ情報';
$lang['stamp_usage_settings'] = 'スタンプの使用設定';
$lang['stampsetting_panel_title'] = 'スタンプの使用設定';
$lang['stampsetting_form_description'] = '"訪問スタンプ機能"をご利用いただけます。 使用する場合は、"有効"を選択します。';
$lang['use_of_stamp'] = 'スタンプの使用';
$lang['effectiveness'] = '有効';
$lang['invalid'] = '無効';
$lang['stampimage_page_title'] = '店舗スタンプ情報';
$lang['stampimage_page_space'] = 'スタンプ画像登録';
$lang['stampimage_panel_title'] = 'スタンプ画像の一覧';
$lang['stampimage_form_description'] = 'スタンプの画像は登録できます。<br/>カード3枚まで登録できます。';
$lang['add_stamp_image'] = '新しいスタンプイメージを登録する';
$lang['showing'] = '表示する';
$lang['of'] = 'の';
$lang['stampimageadd_page_title'] = '店舗スタンプ情報';
$lang['stampimageadd_page_space'] = '新規作成';
$lang['stampimageadd_panel_title'] = 'スタンプ画像情報';
$lang['stamp_image_registration'] = 'スタンプ画像登録';
$lang['stamp_image'] = 'スタンプ画像<br/>(jpeg, jpg, git, png)';
$lang['image_selection'] = '画像選択';
$lang['format'] = 'フォーマット';
$lang['img_extensions'] = 'jpeg, jpg, gif, png';
$lang['recommended_size'] = '推奨サイズ';
$lang['recommended_size_desc'] = '120 × 120';
$lang['capacity'] = '容量';
$lang['capacity_desc'] = '最大 300 KB のイメージを登録';
$lang['return_to_list'] = 'リストに戻る';
$lang['stampprivilege_page_title'] = '店舗スタンプ情報';
$lang['stamp_and_privilege_setting'] = 'スタンプと権限設定';
$lang['card_list'] = 'カードリスト';
$lang['stampprivilege_form_description'] = 'スタンプの情報と特典を設定することができます。';
$lang['alert_stamp_setting_off'] = '* スタンプ使用設定はオンになっていません。 お使いになる際はスタンプ使用設定画面より設定してください。';
$lang['stamp_title'] = 'スタンプタイトル';
$lang['alert_stamp_title'] = 'スタンプのタイトルは無効です。';
$lang['entry_example'] = '入力例';
$lang['stamp_title_desc'] = '<span class="help-tab-gray">記入例</span>ご来店で１スタンププレゼント！スタンプ12個で500OFF券！';
$lang['stamp_details'] = '詳細';
$lang['alert_stamp_details'] = '詳細は有効ではありません。';
$lang['stamp_detail_desc'] = '<span class="help-tab-gray">記入例</span>4名様以上ご来店でドリンク1杯無料！';
$lang['stamp_number'] = 'スタンプ付与数';
$lang['alert_stamp_number'] = 'スタンプの数は無効です';
$lang['stamp_number_desc'] = '付与するスタンプ数です。(1個～100個まで)';
$lang['stamp_max_number'] = 'スタンプ上限数';
$lang['stamp_max_number_desc'] = 'コード認証によるスタンプ付与時の上限数です。<br>
アプリ側で指定された数よりもこちらの数が優先されます。';
$lang['stamp_complete_number'] = 'コンプリート数';
$lang['alert_stamp_complete_number'] = '完全番号が無効です';
$lang['stamp_complete_number_desc'] = '特典が付与されるまでのスタンプ数です。(4個～100個まで)';
$lang['stamp_card_valid_days'] = 'スタンプカード有効日数';
$lang['alert_stamp_card_valid_days'] = 'スタンプカードの有効日は無効です';
$lang['stamp_card_valid_days_desc'] = 'スタンプカードの有効日数が有効期限となります。<br>
2枚目以降のスタンプカード有効期限は、スタンプカード発効日からの有効期限になります。';
$lang['day'] = '日';
$lang['stamp_mark'] = 'スタンプ画像（アップ）';
$lang['alert_stamp_mark'] = 'スタンプは無効です。';
$lang['privilege_details'] = '特典詳細';
$lang['alert_privilege_details'] = '権限詳細は有効ではありません。';
$lang['privilege_details_desc'] = '<span class="help-tab-gray">記入例</span>ジェルネイル50％OFF';
$lang['bonus_image'] = '特典イメージ画像';
$lang['bonus_image_desc'] = '<span class="help-tab-gray">形式</span>jpeg,jpg,gif,png<br>
<span class="help-tab-gray">推 奨サイズ</span>640×430<br>
<span class="help-tab-gray">容 量</span>300KB以内の画像を登録してください';
$lang['stamp_mark_image_desc'] = '<span class="help-tab-gray">形式</span>jpeg,jpg,gif,png<br>
<span class="help-tab-gray">推 奨サイズ</span>120×120<br>
<span class="help-tab-gray">容 量</span>300KB以内の画像を登録してください';
$lang['reward_expiration_date'] = '特典有効期限日数';
$lang['alert_reward_expiration_date'] = '報酬の有効期限は無効です';
$lang['reward_expiration_date_desc'] = '特典が付与されてから特典が使用できるまでの日数となります。';
$lang['terms_and_conditions'] = 'ご利用条件 / 注意事項';
$lang['stamp_once_a_day'] = 'スタンプ1日1回制限';
$lang['stamp_once_a_day_desc'] = '1日に1回しかスタンプが押せなくなります。(午前4時にリセット)';
$lang['max_number_of_stores_per_day'] = '1日当たりの最大取得店舗数';
$lang['max_number_of_stores_per_day_desc'] = '1日で取得できる最大店舗数。(午前4時にリセット)';
$lang['web_view'] = 'Web ビュー';
$lang['web_view_settings'] = 'Webビュー';
$lang['webviewsetting_page_title'] = 'Web ビューの設定';
$lang['webviewsetting_page_space'] = 'Web ビューの設定';
$lang['webviewsetting_panel_title'] = 'Web ビューの設定';
$lang['webview_settings_text'] = 'Web ビューの設定';
$lang['Show'] = '表示';
$lang['Hide'] = '非表示';
$lang['webview_add_btn_label'] = '上記のWebビューを追加する';
$lang['list_of_webviews'] = 'Webビューのリスト';
$lang['progress'] = '進行状況';
$lang['benefits_usage_status'] = 'メリット使用状況';
$lang['return_to_toppage'] = 'Homeに戻る';
$lang['register_success'] = '登録成功';
$lang['webview_title'] = 'タイトル';
$lang['webview_url'] = 'WebビューURL';
//Web view
$lang['web_view'] = 'Web ビュー';
$lang['web_view_setting_screen'] = 'Webビュー設定画面';
$lang['stamp_and_privilege_setting'] = 'スタンプと権限の設定';
$lang['user_information'] = 'ユーザ情報';
$lang['user_setting'] = 'ユーザー設定情報';
$lang['analysis'] = '分析';
$lang['analysis1'] = 'セグメント統計';
$lang['segment_statistics'] = 'セグメント統計';
$lang['downloads_by_day'] = 'ダウンロード数(日別)';
$lang['downloads_by_month'] = 'ダウンロード数(月別)';
$lang['number_of_stamps_by_day'] = 'スタンプ付与数(日別)';
$lang['number_of_stamps_monthly'] = 'スタンプ付与数(月別)';
$lang['number_of_stamp_benefits_used_by_day'] = 'スタンプ特典利用数(日別)';
$lang['number_of_stamp_benefits_used_by_month'] = 'スタンプ特典利用数(月別)';
$lang['coupon_usage_daily'] = 'クーポン利用数(日別)';
$lang['push_notification_ctr'] = 'プッシュ通知CTR';
$lang['favorites_by_day'] = 'お気に入り(日別)';
$lang['favorites_by_month'] = 'お気に入り(月別)';
//Manage
$lang['management'] = '管理';
$lang['qr_code'] = 'QRコード';
$lang['login_account_information'] = 'ログインアカウント情報';

$lang['preview_app_ios'] = 'プレビュー·アプリ(ios)';
$lang['preview_app_android'] = 'プレビューアプリ(Android)';
$lang['login_account_information'] = 'ログインアカウント情報';
$lang['appication_file_for_application'] = 'アプリケーション用アプリケーションファイル';

$lang['appication_file_for_application'] = '申請用アプリファイル';
//Help
$lang['help'] = 'ヘルプ';
$lang['frequently_asked_quetions'] = 'よくある質問';
$lang['app_creation_manual'] = 'アプリ作成マニュアル';
//Member
$lang['member_page'] = "メンバーページ";
//////////////////////////Global text
$lang['wrong_username_or_password'] = 'ユーザー名またはパスワードが正しくありません!';
$lang['login'] = 'ログイン';
$lang['wrong_username_or_password'] = 'ユーザー名またはパスワードが正しくありません。!';
$lang['save'] = "保存";
$lang['done'] = '完了';
$lang['edit'] = '編集';
$lang['registration_has_been_completed'] = '登録が完了しました。';
$lang['photo'] = '写真';
$lang['name'] = '名前';
$lang['birthday'] = '誕生日';
$lang['president_photo'] = '社長写真';
$lang['president_photo_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png 画像を登録してください。<br>
<span class='help-tab-gray'>推奨サイズ</span> 600*600<br>
<span class='help-tab-gray'>容量</span> 300 KBで画像を登録してください。";
$lang['staff_photo'] = '写真';
$lang['staff_photo_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png 画像を登録してください。<br>
<span class='help-tab-gray'>推奨サイズ</span> 600*600<br>
<span class='help-tab-gray'>容量</span> 300 KBで画像を登録してください。";
$lang['registration_has_been_completed'] = '登録が完了しました。';
$lang['confirm_input_contents'] = '入力内容の確認';
$lang['none'] = 'なし';
$lang['register_with_this_content'] = 'この内容で登録';
$lang['back_to_fix'] = '修正に戻る';
$lang['back'] = '戻る';
$lang['go_back_to_the_top_page'] = 'トップページに戻る';
$lang['fix'] = '修正';
$lang['delete'] = '削除';
$lang['add'] = '追加';
$lang['other'] = 'その他';
$lang['cpn'] = 'ホーム';
$lang['map'] = 'MAP';
$lang['cal'] = 'イべソト';
$lang['cmt'] = 'イソフォ';
$lang['store_name'] = '店舗名';
$lang['search'] = '検索';
$lang['refer'] = '参照';
$lang['no_results_found'] = '結果が見つかりません';
$lang['mail'] = 'メール';
$lang['no_results_found'] = '結果が見つかりません。';
$lang["blog_and_hp_link_settings"] = "ブログおよびHPリンクの設定";
$lang["please_select"] = "選択してください...";
$lang["blog"] = "ブログ";
$lang["pc_homepage"] = "PC ホームページ";
$lang["smartphone_page"] = "スマホページ";
$lang["hot_pepper"] = "トウガラシ";
$lang["shopping"] = "Shopping";
$lang["job_information"] = "ジョブ情報";
$lang["design_settings"] = "デザイン設定";
$lang["company_name"] = "会社名";
$lang["password"] = "パスワード";
$lang["confirm"] = "確認";
$lang["email"] = "Eメール";
$lang["lang_en"] = "英語";
$lang["lang_jp"] = "日本語";
$lang['lang_cn'] = '中国語';
$lang['lang_thai'] = 'タイ語';
$lang["mobile_show"] = "プレビューを閉じる";
$lang["mobile_hide"] = "プレビューを開く";
$lang["sidebar_hide"] = "メニューを閉じる";
$lang["mobile_header"] = "";
$lang["mobile_footer_wrench"] = "";
$lang["year_jp"] = "年";
$lang["month_jp"] = "月";
$lang["day_jp"] = "日";
$lang["os"] = "OS";
$lang["age"] = "年齢";
$lang["sex"] = "性別";
$lang["search_lists"] = "検索リスト";
$lang["number_of_download"] = "ダウンロード数";
$lang["graph"] = "グラフ";
$lang["10_queries"] = "最大 10 個のクエリーを設定できます。";
$lang["sort_by_dragging"] = "カラムをドラッグしてソートできます。";
$lang["publishing_settings"] = "公開設定";
$lang["shop_name"] = "ショップ名:　";
$lang["OBU_shop"] = "ショップ";
$lang["my_profile"] = "私のプロフィール";
$lang["my_balance"] = "私のバランス";
$lang["messages"] = "メッセージ";
$lang["account_settings"] = "アカウント設定";
$lang['comment'] = "コメント";
$lang['video_url'] = "ビデオURL";
$lang['add_this_site'] = "このサイトを追加";
$lang['add_this_video'] = "このビデオを追加";
$lang['add_photo'] = "写真を追加";
$lang['photo_gallery'] = "フォトギャラリー";
$lang['photo_information'] = "写真情報";
$lang['video_information'] = "ビデオ情報";
$lang['pdf_information'] = "PDF情報";
$lang['pdf'] = "PDF";
$lang["thumbnail"] = "サムネイル";
$lang['instruction'] = "説明";
$lang['category'] = "カテゴリ";
$lang['no_matching_data_found'] = '一致するデータは見つかりません。';
$lang['news'] = 'ニュース';
$lang['events'] = 'イベント';
$lang['all'] = "全て";
$lang['manual'] = "マニュアル";
$lang['bulk_update'] = "一括更新";
//////////////////////////Design Settings
//Basic setting

//Layout Setting
$lang['layout'] = 'レイアウト';
$lang['success_layout'] = '<small>編集</small> デザイン設定';
$lang['layout_settings'] = 'レイアウト設定';
$lang["panel_layout"] = 'パネル型レイアウト';
$lang['panel_layout_template'] = 'パネル型パターン';
$lang["list_layout"] = 'リスト型レイアウト';
$lang["margin_between_menus"] = 'メニュー間余白有無';
$lang["with_margins"] = 'マージンと共に';
$lang["no_margin"] = 'マージンなし';
$lang["latest_news_background_color"] = "最新ニュースの背景色";
$lang["latest_news_text_color"] = "最新ニューステキストの色";
$lang["latest_news_headline_color"] = "最新ニュースのヘッドラインの色";
$lang["latest_coupon_background_color"] = "最新のクーポン背景色";
$lang["latest_coupon_text_color"] = "最新のクーポンテキストの色";
$lang["latest_coupon_headline_color"] = "最新のクーポンヘッドラインの色";
$lang['return_to_layout_setting_editing'] = 'レイアウト設定編集に戻る';
$lang['title'] = 'タイトル';

//Slide image setting
$lang['slide'] = 'スライド';
$lang['register_and_set_slide'] = "トップページに表示するスライド画像/画像を登録、設定します。";
$lang["sign_up"] = '追加';
$lang['display_image'] = '画像の表示';
$lang['display'] = '状態';
$lang['transition_destination'] = '移行先';
$lang['publication_date'] = '公開日';
$lang['state'] = '状態';
$lang['indicate'] = '表示';
$lang['hidden'] = '非表示';
$lang['search_condition'] = '検索条件';
$lang['year'] = '年';
$lang['month'] = '月';
$lang['day'] = '日';
$lang['display'] = '表示';
$lang['only_when_displayed'] = '表示時のみ';
$lang['number_of_serch_results_displayed'] = '検索結果の表示数';
$lang['cases'] = 'ケース';

$lang['posting_period'] = '投稿期間';
$lang['image'] = '画像';
$lang['link_destination'] = 'リンク先';
$lang['publishing_settings'] = '公開設定';
$lang['news_events_page'] = 'ニュース/イベントページ';
$lang['coupon_page'] = 'Cクーポンページ';
$lang['web_page'] = 'Webページ(URL 指定)';
//$lang['none'] = 'なし';
$lang['format'] = 'フォーマット';
$lang['recommended_size'] = '推奨サイズ';
$lang['capacity'] = '容量';

//admin/basic/company_profile/
$lang['basic_company_profile_page_title']="会社概要";
$lang['basic_company_profile_page_subtitle']="編集";
$lang['basic_company_profile_panel_title']="会社概要";
$lang['basic_company_profile_content'] = "テキスト";
$lang['basic_company_profile_status_show'] = "表示";
$lang['basic_company_profile_status_show_y'] = "示す";
$lang['basic_company_profile_status_show_n'] = "非表示";
$lang['basic_company_profile_form_description'] = "会社のプロファイルを変更してください。";
$lang['basic_company_profile_success_return_text'] = "戻る";
$lang['basic_company_profile_form_description'] = "会社概要を変更してください。";
//admin/basic/corporate_philosophy/
$lang['basic_corporate_philosophy_page_title']="企業理念";
$lang['basic_corporate_philosophy_page_subtitle']="編集";
$lang['basic_corporate_philosophy_panel_title']="企業理念";
$lang['basic_corporate_philosophy_content'] = "テキスト";
$lang['basic_corporate_philosophy_status_show'] = "ディスプレイ";
$lang['basic_corporate_philosophy_status_show_y'] = "表示";
$lang['basic_corporate_philosophy_status_show_n'] = "非表示";
$lang['basic_corporate_philosophy_form_description'] = "企業理念を変更してください";
$lang['basic_corporate_philosophy_success_return_text'] = "戻る";
//admin/basic/business_introduction/
$lang['basic_business_introduction_page_title']="ビジネス紹介";
$lang['basic_business_introduction_page_subtitle']="編集";
$lang['basic_business_introduction_panel_title']="ビジネス紹介";
$lang['basic_business_introduction_content'] = "テキスト";
$lang['basic_business_introduction_status_show'] = "ディスプレイ";
$lang['basic_business_introduction_status_show_y'] = "表示";
$lang['basic_business_introduction_status_show_n'] = "非表示";
$lang['basic_business_introduction_form_description'] = "ビジネス紹介を変更してください。";
$lang['basic_business_introduction_success_return_text'] = "戻る";
//admin/basic/business_history/
$lang['basic_business_history_page_title']="事業履歴";
$lang['basic_business_history_page_subtitle']="編集";
$lang['basic_business_history_panel_title']="事業履歴";
$lang['basic_business_history_content'] = "テキスト";
$lang['basic_business_history_status_show'] = "ディスプレイ";
$lang['basic_business_history_status_show_y'] = "表示";
$lang['basic_business_history_status_show_n'] = "非表示";
$lang['basic_business_history_form_description'] = "事業履歴を変更してください。";
$lang['basic_business_history_success_return_text'] = "戻る";
//admin/basic/access/
$lang['basic_access_page_title']="アクセス";
$lang['basic_access_page_subtitle']="編集";
$lang['basic_access_panel_title']="アクセス";
$lang['basic_access_content'] = "テキスト";
$lang['basic_access_status_show'] = "ディスプレイ";
$lang['basic_access_status_show_y'] = "表示";
$lang['basic_access_status_show_n'] = "非表示";
$lang['basic_access_form_description'] = "アクセスを変更してください。";
$lang['basic_access_success_return_text'] = "戻る";
//admin/basic/facebook/
$lang['basic_facebook_page_title']="Facebook";
$lang['basic_facebook_page_subtitle']="編集";
$lang['basic_facebook_panel_title']="Facebook";
$lang['basic_facebook_content'] = "テキスト";
$lang['basic_facebook_status_show'] = "ディスプレイ";
$lang['basic_facebook_status_show_y'] = "表示";
$lang['basic_facebook_status_show_n'] = "非表示";
$lang['basic_facebook_form_description'] = "貴方のfacebookを変更してください。";
$lang['basic_company_profile_success_return_text'] = "戻る";

//admin/basic/twitter/
$lang['basic_twitter_page_title']="Twitter";
$lang['basic_twitter_page_link']="Twitterリンク";
$lang['post_a_twitter_link']="Twitterリンクを投稿する";
$lang['do_not_post_a_twitter_link']="Twitterリンクを投稿しないでください";
$lang['twitter_account']="Twitterアカウント";
$lang['twitter_account_header']="投稿する Twitter アカウントを入力してください。";
$lang['twitter_account_help']="Twitterページにログインし、設定*に説明されている番号を入力します。<Page Information>Twitter ページ ID。";
$lang['facebook_link']="Facebookリンク";
$lang['facebook_account_header']="投稿する Facebook アカウントを入力します。";
$lang['facebook_account_help']="Facebookページにログインし、
設定＞ページ情報＞FacebookページID
に記載されている数字を入力して下さい。";
$lang['post_a_facebook_link']="Facebookリンクを掲載する";
$lang['do_not_post_a_facebook_link']="Facebookリンクを掲載しない";
$lang['facebook_account']="Facebook account";
$lang['basic_twitter_page_subtitle']="編集";
$lang['basic_twitter_panel_title']="Twitter";
$lang['basic_twitter_content'] = "テキスト";
$lang['basic_twitter_status_show'] = "ディスプレイ";
$lang['basic_twitter_status_show_y'] = "表示";
$lang['basic_twitter_status_show_n'] = "非表示";
$lang['basic_twitter_form_description'] = "貴方のTwitterを変更してください。";
$lang['basic_twitter_success_return_text'] = "戻る";

//admin/basic/instagram/
$lang['basic_instagram_page_title']="Instagram";
$lang['basic_instagram_page_subtitle']="編集";
$lang['basic_instagram_panel_title']="Instagram";
$lang['basic_instagram_content'] = "テキスト";
$lang['basic_instagram_status_show'] = "ディスプレイ";
$lang['basic_instagram_status_show_y'] = "表示";
$lang['basic_instagram_status_show_n'] = "非表示";
$lang['basic_instagram_form_description'] = "貴方のInstagramを変更してください。";
$lang['basic_instagram_success_return_text'] = "戻る";



//admin/basic/president/
$lang['basic_president_page_title']="社長";
$lang['manage_from_president']="代表挨拶";
$lang['basic_president_page_subtitle']="編集";
$lang['basic_president_panel_title']="プレジデント";
$lang['basic_president_content'] = "テキスト";
$lang['basic_president_status_show'] = "表示";
$lang['basic_president_status_show_y'] = "示す";
$lang['basic_president_status_show_n'] = "非表示";
$lang['basic_president_form_description'] = "社長プロファイルを変更してください。";
$lang['basic_president_success_return_text'] = "戻る";
$lang['admin_president_list_description']="プレジデントリスト";
$lang['admin_president_president_setting']="プレジデント設定";
$lang['admin_president_setting_edit']="トップメニュー設定/編集";
$lang['admin_president_edit_president']="プレジデント編集";
$lang['admin_president_index_fix'] = "修正";
$lang['admin_president_index_delete'] = "削除";
$lang['admin_president_index_president_name'] = "プレジデント名前";
$lang['admin_president_index_display'] = "表示";
$lang['admin_president_index_order'] = "順序";
$lang['price'] = "価格";
$lang['menu'] = "メニュー";
$lang['html'] = "HTML";
$lang['html_1'] = "Html 1";
$lang['html_2'] = "Html 2";
$lang['html_3'] = "Html 3";

//admin/basic/president/list

///////////success
$lang['admin_president_edit_success_return_text'] = "社長ページの設定/編集に戻る";
$lang['admin_president_edit_success_return_text'] = "プレジデントページに戻り設定/編集";
//admin/basic/staff/
$lang['basic_staff_page_title']="スタッフ";
$lang['basic_staff_page_subtitle']="編集";
$lang['basic_staff_panel_title']="スタッフ";
$lang['basic_staff_content'] = "テキスト";
$lang['basic_staff_status_show'] = "ディスプレイ";
$lang['basic_staff_status_show_y'] = "表示";
$lang['basic_staff_status_show_n'] = "非表示";
$lang['basic_staff_form_description'] = "スタッフのプロファイルを変更してください。";
$lang['basic_staff_success_return_text'] = "戻る";
$lang['admin_staff_list_description']="スタッフリスト";
$lang['admin_staff_staff_setting']="スタッフ設定";
$lang['admin_staff_setting_edit']="トップメニュー設定 /編集";
$lang['admin_staff_edit_staff']="スタッフ編集";
$lang['admin_staff_index_fix'] = "改修";
$lang['admin_staff_index_delete'] = "削除";
$lang['admin_staff_index__name'] = " 名前";
$lang['admin_staff_index_display'] = "ディスプレイ";
$lang['admin_staff_index_order'] = "順番";

//admin/basic/staff/list

///////////success
$lang['admin_staff_edit_success_return_text'] = "スタッフページの設定/編集に戻る";


//admin/basicsetting/agency/
$lang['basicsetting_agency_page_title']="代理の基本情報";
$lang['basicsetting_agency_page_subtitle']="編集";
$lang['basicsetting_agency_panel_title']="代理店基本情報";
$lang['basicsetting_agency_content'] = "テキスト";
$lang['basicsetting_agency_status_show'] = "表示";
$lang['basicsetting_agency_status_show_y'] = "表示する";
$lang['basicsetting_agency_status_show_n'] = "非表示";
$lang['basicsetting_agency_form_description'] = "会社を変更してください。";
$lang['basicsetting_agency_success_return_text'] = "戻る";

//admin/basicsetting/webview/
$lang['basicsetting_webview_page_title']="ウエブビュー情報";
$lang['basicsetting_webview_page_subtitle']="編集";
$lang['basicsetting_webview_panel_title']="ウエブビュー情報";
$lang['basicsetting_webview_content'] = "テキスト";
$lang['basicsetting_webview_status_show'] = "表示";
$lang['basicsetting_webview_status_show_y'] = "表示する";
$lang['basicsetting_webview_status_show_n'] = "非表示";
$lang['basicsetting_webview_form_description'] = "ウエブビュー";
$lang['basicsetting_webview_success_return_text'] = "戻る";
//admin/analysis/segment_statistics/
$lang['analysis_segment_statistics_page_title']="セグメント統計";
$lang['analysis_segment_statistics_page_subtitle']="分析";
$lang['analysis_segment_statistics_panel_title']="セグメント統計";
$lang['analysis_segment_statistics_content'] = "テキスト";
$lang['analysis_segment_statistics_status_show'] = "ディスプレイ";
$lang['analysis_segment_statistics_status_show_y'] = "表示";
$lang['analysis_segment_statistics_status_show_n'] = "非表示";
$lang['analysis_description'] = "お気に入りに登録された数を日別に確認することが出来ます。下の検索条件より閲覧したい年月を選択して検索ボタンを押下してください。<br>
<span style='color: red'>* 分析集計は翌日反映されます。</span>";
$lang['analysis_month_description'] = "お気に入りに登録された数を月別に確認することが出来ます。下の検索条件より閲覧したい年を選択して検索ボタンを押下してください。<br>
<span style='color: red'>* 分析集計は翌日反映されます。</span>";
$lang['analysis_pushnotificaiotn_serach_description'] = "※データは毎日深夜12時に更新されます。<br>
※アプリインストール時にプッシュ通知受信をOFFにされた場合は配信対象外となります。<br>
※未読か既読の判断はプッシュ通知から詳細ページへ遷移したか否かが基準となります。<br>
&nbsp;&nbsp;&nbsp;&nbsp; ただし、配信から72時間以降に詳細ページへ遷移しても既読ではなく未読扱いとなります。<br>
※iOSの未読数値にはアンインストールユーザー、その他配信エラーが含まれます。<br>
※アンインストールを想定として、過去6ヶ月でアプリを開いているユーザーをアクティブとしてコンバージョン計測しております。";
$lang['notice'] = "お知らせ";
//download analysis
$lang['analysis_segment_statistics_description'] = "アプリをダウンロードしたユーザーのセグメントを見ることができます。 次の検索条件から参照する年および月を選択し、検索ボタンを押してください。";
$lang['analysis_segment_statistics_error'] = "* 分析結果は翌日反映されます。";
$lang['downloads_day_description'] = "アプリがダウンロードされた数を日別に確認することが出来ます。<br>下の検索条件より閲覧したい年月を選択して検索ボタンを押下してください。";
$lang["downloads_month_description"] = "アプリがダウンロードされた数を月別に確認することが出来ます。<br>
下の検索条件より閲覧したい年を選択して検索ボタンを押下してください。";
$lang['analysis_stamp_day_description'] = 'スタンプ獲得数を日別に確認することが出来ます。下の検索条件より閲覧したい年月を選択して検索ボタンを押下してください。<br><span style="color: red">* 分析集計は翌日反映されます。</span>';
$lang['analysis_stamp_month_description'] = 'スタンプ獲得数を月別に確認することが出来ます。下の検索条件より閲覧したい年を選択して検索ボタンを押下してください。<br><span style="color: red">* 分析集計は翌日反映されます。</span>';
$lang['analysis_stamp_benefit_day_description'] = 'チケット利用数を日別に確認することが出来ます。下の検索条件より閲覧したい年月を選択して検索ボタンを押下してください。<br><span style="color: red">* 分析集計は翌日反映されます。</span>';
$lang['analysis_stamp_benefit_month_description'] = 'チケット利用数を月別に確認することが出来ます。下の検索条件より閲覧したい年を選択して検索ボタンを押下してください。<br><span style="color: red">* 分析集計は翌日反映されます。</span>';
$lang['analysis_coupon_usage_description'] = 'クーポン利用数を日別に確認することが出来ます。下の検索条件より閲覧したい年月を選択して検索ボタンを押下してください。<br><span style="color: red">* 分析集計は翌日反映されます。</span>';
$lang['analysis_push_ctr_description'] = '配信したプッシュ通知の配信数や開封率を確認することが出来ます。下の検索条件より閲覧したい年月を選択して検索ボタンを押下してください。<br><span style="color: red">* 分析集計は翌日反映されます。</span>';
$lang['coupon_usage_daily'] = "クーポン利用(日別)";
$lang['downloads_day'] = "ダウンロード (日別)";
$lang['specify_year_month'] = "年月の指定";
$lang['do_not_use'] = "使用しない";
$lang['use'] = "使用する";
$lang['series_format'] = "{series.name}: <b>{point.percentage:.1f}%</b>";
$lang['point_format'] = "<b>{point.name}</b>: {point.percentage:.1f} %";
$lang['survey_description'] = 'アンケートの回答率を確認することが出来ます。<br>下の検索条件より閲覧したい年月を選択して検索ボタンを押下してください。';
//Admin/Design/Layout
$lang['design_setting'] = 'デザイン設定';
$lang['layout_setting_edit'] = 'レイアウト設定/編集';
$lang['layout_settings'] = 'レイアウト設定';
$lang['layout_type'] = 'レイアウトタイプ';
$lang['panel_layout'] = 'パネル型レイアウト';
$lang['list_layout'] = 'リスト型レイアウト';
$lang['with_margins'] = 'マージンと共に';
$lang['no_margin'] = 'マージンなし';
$lang['latest_announcement'] = '最新お知らせ背景色';
$lang['help_announcement'] = '最新お知らせの背景色を設定できます。';
$lang['latest_notice'] = '最新お知らせ文字色';
$lang['help_notice'] = '最新お知らせの文字色を設定できます。';
$lang['latest_title'] = '最新お知らせ見出し色';
$lang['help_title'] = '最新お知らせの見出し色を設定できます。';
$lang['coupon_back_color'] = '最新お知らせ背景色';
$lang['help_coupon_back_color'] = '最新お知らせの背景色を設定できます。';
$lang['coupon_font_color'] = '最新お知らせ文字色';
$lang['help_coupon_font_color'] = '最新クーポンの背景色を設定できます。';
$lang['coupon_title_color'] = '最新お知らせ見出し色';
$lang['help_coupon_title_color'] = '最新クーポンの見出し色を設定できます。';
//Admin/Design/Layout/confirm
$lang['confrim_layout_type'] = 'トップレイアウト形式';
$lang['confirm_panel_layout'] = 'リスト型レイアウト';
$lang['confirm_list_layout'] = "パネル型レイアウト";
$lang['confrim_margin_between_menus'] = "メニュー間余白有無";
$lang['confirm_with_margins'] = "マージンと共に";
$lang['confirm_no_margin'] = "マージンなし";
$lang['confirm_latest_announcement'] = "最新お知らせ背景色";
$lang['confirm_help_announcement'] = "最新お知らせの背景色を設定できます。";
$lang['confirm_coupon_font_color'] = "最新お知らせ文字色";
$lang['confirm_help_coupon_font_color'] = "最新クーポンの背景色を設定できます。";
$lang['confirm_latest_title'] = "最新お知らせ見出し色";
$lang['help_title'] = "最新お知らせの見出し色を設定できます。";
$lang['confirm_coupon_back_color'] = "最新クーポン背景色";
$lang['help_coupon_back_color'] = "最新クーポンの背景色を設定できます。";
$lang['confirm_coupon_title_color'] = "最新クーポン文字色";
$lang['confirm_help_coupon_title_color'] = "最新クーポンの見出し色を設定できます。";
$lang['confirm_latest_title'] = "最新のニュースヘッドラインの色";
$lang['help_title'] = "最新の発表のタイトルカラーを設定できます。";
$lang['confirm_coupon_back_color'] = "最新のクーポン背景色";
$lang['help_coupon_back_color'] = "最新のクーポンの背景色を設定できます。";
$lang['confirm_coupon_title_color'] = "最新のクーポンの文字色";
$lang['confirm_help_coupon_title_color'] = "最新のクーポンのタイトル色を設定できます。";
$lang['layout_design_confirm_coupon_headline_color'] = "最新クーポン見出し色";
$lang['confirm_help_coupon_title_color'] = "最新クーポンの見出し色を設定できます。";
//Admin//design/success
$lang['admin_design_layout_success_return_text'] = "レイアウト設定に戻ります。";

////Admin/Design/slide---table
//$lang['design_setting'] = '設計設定';
$lang['slide_image_setting_editing'] = 'スライド画像設定 /編集';
$lang['slide_image_setting_editing'] = 'スライド画像設定·編集';
$lang['slide_image_settings'] = 'スライド画像設定 ';
$lang['Posting period'] = '投稿期間';
$lang['image_jpeg'] = 'イメージ(jpeg,jpg,gif,png)';
$lang['link_destination'] = 'リンク先';
$lang['link_news_event_page'] = 'ニュース/イベントページ';
$lang['link_coupon_page'] = 'クーポンページ';
$lang['link_web_page'] = 'ウエブページ(URL指定)';
$lang['link_none'] = 'なし';
$lang['url'] = 'URL';
$lang['status_show_publishing_settings'] = '公開設定';
$lang['status_show_indicate'] = '表示';
$lang['status_show_hidden'] = '非表示';
$lang['publication_date'] = '公開日';
$lang['slide_fix'] = '修正';
$lang['slide_delete'] = '削除';
$lang['slide_store_name'] = '店舗名';
$lang['slide_table_display_image'] = '画像の表示';
$lang['slide_table_link_destination'] ='リンク先';
$lang['slide_table_news_event_page'] = 'クーポンページ';
$lang['slide_table_web_page'] = 'ウエブページ(URL指定)';
$lang['slide_table_none'] = 'なし';
$lang['slide_table_status'] = 'ステータス';
$lang['slide_register_image'] = 'トップページに表示するスライド写真・画像を登録設定します。';
$lang['slide_image_return'] = "戻る";

/////////admin/design/slide/publish2
$lang['design_slide_publish2_posting_period'] = "投稿期間";
$lang['design_slide_publish2_image'] = "イメージ(jpeg,jpg,gif,png)";
$lang['design_slide_publish2_image_help'] = "<span class='help-tab-gray'>形式</span> jpeg,jpg,gif,png<br>
<span class='help-tab-gray'>推奨サイズ</span> 640*320<br>
<span class='help-tab-gray'>容量</span>300 KBで画像を登録してください。";
$lang['design_slide_publish2_link_destination'] = "リンク先";
$lang['design_slide_publish2_news_events_page'] = "ニュース/イベントページ";
$lang['design_slide_publish2_coupon_page'] = "クーポンページ";
$lang['design_slide_publish2_web_page'] = "ウエブページ(URL指定)";
$lang['design_slide_publish2_none'] = "なし";
$lang['design_slide_publish2_url'] = "URL";
$lang['design_slide_publish2_url_help'] = "<span class='help-tab-red'>外部ブラウザとしてで開く場合</span><br>
[URL to link]#target=_blank</br>上記フォーマットで設定してください。";
$lang['design_slide_publish2_publishing_settings'] = "公開設定";
$lang['design_slide_publish2_indicate'] = "表示";
$lang['design_slide_publish2_hidden'] = "非表示";

//////admin/design/slide/confirm

$lang['design_slide_confirm_publication_date'] = "投稿期間";
$lang['design_slide_confirm_image'] = "イメージ";
$lang['design_slide_confirm_image_help'] = "<span class='help-tab-gray'>形式</span> jpeg,jpg,gif,png<br>
<span class='help-tab-gray'>推奨サイズ</span> 640*470<br>
<span class='help-tab-gray'>容量</span>300 KBで画像を登録してください。";
$lang['design_slide_confirm_link_destination'] = "移行先";
$lang['design_slide_confirm_link_news'] = "ニュース/イベントページ";
$lang['design_slide_confirm_link_coupon_page'] = "クーポンページ";
$lang['design_slide_confirm_link_webpage'] = "ウエブページ(URL指定)";
$lang['design_slide_confirm_link_none'] = "なし";
$lang['design_slide_confirm_status_show_display'] = '表示';
$lang['design_slide_confirm_status_show_display_y'] = "表示";
$lang['design_slide_confirm_status_show_display_n'] = "非表示";


//Admin/design/headerfooter
$lang['headerfooter_design_setting'] = 'デザイン設定';
$lang['headerfooter_image_edit'] = 'ヘッダー/フッターイメージ / 編集';
$lang['headerfooter_header_footer_image_settings'] = 'ヘッダーフッター画像・編集';
$lang['headerfooter_header_image'] = 'ヘッダーイメージ(jpeg,jpg,gif,png)';
$lang['headerfooter_footer_image'] = 'フッターイメージ(jpeg,jpg,gif,png)';
$lang['headerfooter_register'] = 'トップページに表示するヘッダー画像およびフッター画像を登録して設定します。';
$lang['headerfooter_return'] = 'ページカラー設定/編集に戻る';

///////Admin/design/headerfooter/confirm
$lang['confirm_headerfooter_header_image'] = "ヘッダーイメージ";
$lang['confirm_delete'] = "削除";
$lang['confirm_header_image_help'] = "<span class='help-tab-gray'>形式</span> jpeg,jpg,gif,png</br>
<span class='help-tab-gray'>推奨サイズ</span>640*84</br>
<span class='help-tab-gray'>容量</span>300KB以内の画像を登録してください</br>
";
$lang['confirm_headerfooter_footer_image'] = "フッターイメージ";
$lang['confirm_footer_image_help'] = "<span class='help-tab-gray'>形式</span> jpeg,jpg,gif,png</br>
<span class='help-tab-gray'>推奨サイズ</span>640*100</br>
<span class='help-tab-gray'>容量</span>300KB以内の画像を登録してください</br>
";


//Admin/design/pagecolor
$lang['pagecolor_edit_design_settings'] = '編集デザイン設定';
$lang['pagecolor_space'] = 'ページカラー設定';
$lang['pagecolor_pagecolor_settings'] = 'ページカラー設定';
$lang['pagecolor_pattern'] = '色のパターン';
$lang['pagecolor_help_color_pattern'] = '*アイテム別のカラー設定は優先順位がございます。';
$lang['pagecolor_title_bar_back_color'] = 'タイトルバー背景色';
$lang['pagecolor_help_title_bar_back_color'] = 'アプリのトップ画面中央にタイトルバーの色を設定できます。';
$lang['pagecolor_title_bar_font_color'] = 'タイトルバー文字色';
$lang['pagecolor_help_title_bar_font_color'] = 'タイトルバー背景色の設定が可能です。';
$lang['pagecolor_page_back_color'] = 'ページ背景色';
$lang['pagecolor_help_page_back_color'] = 'アプリ全体の背景色を設定できます。';
$lang['pagecolor_page_back_image'] = 'ページ背景イメージ(jpeg,jpg,gif,png)';
$lang['pagecolor_help_page_back_image'] = 'アプリ全体の背景色を設定できます。';
$lang['pagecolor_page_font_color'] = 'ページテキスト色';
$lang['pagecolor_help_page_font_color'] = 'アプリケーションのテキストの色を設定できます。';
$lang['pagecolor_button_back_color'] = 'ボタン背景色';
$lang['pagecolor_help_button_back_color'] = 'ボタンの背景色を設定できます。';
$lang['cardcolor_design_settings'] = 'デザイン設定';
$lang['cardcolor_settings'] = 'カードの色設定';
$lang['cardcolor_membership'] = 'メンバーシップカードページの色設定';
$lang['cardcolor_background_color'] = '背景色';
$lang['cardcolor_help_background_color'] = '会員証の背景色を設定できます。';
$lang['cardcolor_font_color_letter_color'] = 'レター色';
$lang['cardcolor_help_font_color_letter_color'] = '会員証のテキストカラーを設定できます。';
$lang['cardcolor_form_description'] = '会員カードに使用するテキストの色と背景色を選択できます。';
$lang['cardcolor_return'] = '会員証ページのカラー設定/編集に戻る';
////admin/design/pagecolor/confirm
$lang['confirm_pagecolor_title_bar_back_color'] = "タイトルバー背景色";
$lang['confirm_pagecolor_help_title_bar_back_color'] = "アプリのトップ画面中央にタイトルバーの色を設定できます。";
$lang['confirm_pagecolor_title_bar_font_color'] = "タイトルバー文字色";
$lang['confirm_pagecolor_help_title_bar_font_color'] = "タイトルバーのテキストの色を設定できます。";
$lang['confirm_pagecolor_page_back_color'] = "ページ背景色";
$lang['confirm_pagecolor_help_page_back_color'] = "アプリ全体の背景色を設定することができます。";
$lang['confirm_pagecolor_page_back_image'] = "ページ背景イメージ";
$lang['confirm_pagecolor_help_page_back_image'] = "アプリ全体の背景色を設定することができます。";
$lang['confirm_pagecolor_page_font_color'] = "レター色";
$lang['confirm_pagecolor_help_page_font_color'] = "アプリケーションのテキストの色を設定できます。";
$lang['confirm_pagecolor_button_back_color'] = "ボタン背景色";
$lang['confirm_pagecolor_help_button_back_color'] = "ボタンの背景色を設定できます。";

//Admin/design/topmenu
$lang['design_topmenu_top_menu_setting'] = 'トップメニュー設定';
$lang['design_topmenu_top_menu_setting_editing'] = 'トップメニュー設定 / 編集';
$lang['design_topmenu_edit_top_menu'] = 'トップメニュー編集';
$lang['design_topmenu_top_layout_types'] = 'トップレイアウト形式';
$lang['design_topmenu_type'] = '形式';
$lang['design_topmenu_web_page_format'] = 'ウエブページ形式';
$lang['design_topmenu_list_format'] = 'リスト形式';
$lang['design_topmenu_menu'] = 'メニュー';
$lang['design_topmenu_images_only'] = 'イメージのみ';
$lang['design_topmenu_top_menu_name'] = 'トップメニュー名';
$lang['design_topmenu_image_jpeg'] = 'イメージ(jpeg,jpg,gif,png)';
$lang['design_topmenu_help_image_jpeg'] = " 画像を登録してください。<span class='help-tab-gray'>推奨サイズ</span> 360*280<br/>
<span class='help-tab-gray'>容量</span> 300 KBで画像を登録してください。";
$lang['design_topmenu_display_page_url'] = 'ページurl表示';
$lang['design_topmenu_help_display_rul'] = "*設定されたページの内容によっては、プロパティが表示されない場合があります。<br/>
<span class='help-tab-red'>PDF ファイルの場合</span><br/>
 http://docs.google.com/viewer?url=[PDFのURLはこちら] To<br/>
 <span class='help-tab-red'>外部ブラウザで開く</span><br/>
 [リンク先のURL]#target =_blank<br/>
上記の形式で設定してください。";
$lang['design_topmenu_device'] = 'デバイス';
$lang['design_topmenu_device_ios'] = 'iOS';
$lang['design_topmenu_device_android'] = 'Android';
$lang['design_status_show_display'] = '表示';
$lang['design_status_show_indicate'] = '表示';
$lang['design_status_show_hidden'] = '非表示';
$lang['design_status_show_help'] = "表示するメニューがない場合は、プライベートを選択します。";
$lang['design_topmenu_order'] = '整列順序';
$lang['design_topmenu_order_help'] = "*小さい方の番号が最初に表示されます。<br/>
(例: 0-#1-#2-#3-#4)";

$lang['design_topmenu_button_type'] = 'ボタン背景形式';
$lang['design_topmenu_button_type_image'] = 'イメージ';
$lang['design_topmenu_button_type_color'] = 'カラー';
$lang['design_topmenu_image_for_list'] = 'リストボタンイメージ(jpeg,jpg,gif,png)';
$lang['design_topmenu_image_for_list_help'] = "の画像を登録してください。<span class='help-tab-gray'>推奨サイズ</span> 640*350<br/>
<span class='help-tab-gray'>容量</span> 300 KB 以下";
$lang['design_topmenu_button_type'] = 'オーバーレイ';
$lang['design_topmenu_button_type_effectiveness'] = '有効';
$lang['design_topmenu_button_type_invalid'] = "無効";
$lang['design_topmenu_button_type_help'] = "ボタンイメージを暗くする.";
$lang['design_topmenu_title_disp_yn'] = 'タイトル表示';
$lang['design_topmenu_title_disp_yn_indicate'] = '表示';
$lang['design_topmenu_title_disp_yn_hidden'] = '非表示';
$lang['design_topmenu_title_text_color'] = 'タイトル表示';
$lang['design_topmenu_title_text_color_indicate'] = '表示';
$lang['design_topmenu_title_text_color_hidden'] = '非表示';
$lang['design_topmenu_title_position'] = 'タイトルポジション';
$lang['design_topmenu_title_position_middle'] = '真ん中';
$lang['design_topmenu_title_position_up'] = '上';
$lang['design_topmenu_title_position_under'] = '下';
$lang['design_topmenu_button_back_color'] = 'ボタンイメージ色';
$lang['design_topmenu_button_back_color_help'] = "画像が設定されていないときは背景色を設定できます。";
$lang['form_description_space'] = "";
$lang['success_return_text_return'] = "戻る";

////Admin/topmenu/publish
$lang['admin_topmenu_publish_top_layout_types'] = "トップレイアウトタイプ";
$lang['admin_topmenu_publish_panel_layout'] = 'パネル型レイアウト';
$lang['admin_topmenu_publish_list_layout'] = 'リスト型レイアウト';
$lang['admin_topmenu_publish_type'] = "タイプ";
$lang['admin_topmenu_publish_type_web_page'] = "WEBページ形式";
$lang['admin_topmenu_publish_type_list_format'] = "リスト形式";
$lang['admin_topmenu_publish_type_menu'] = "メニュー";
$lang['admin_topmenu_publish_type_images_only'] = "イメージのみ";
$lang['admin_topmenu_publish_menu_name_top_menu_name'] = "トップメニュー名";
$lang['admin_topmenu_publish_image_for_panel_image'] = "イメージ(jpeg,jpg,gif,png)";
$lang['admin_topmenu_publish_image_for_panel_image_help'] = " 画像を登録してください。<br><span class='help-tab-gray'>推奨サイズ</span> 640*500<br/>
<span class='help-tab-gray'>容量</span> 300 KBで画像を登録してください。";
$lang['admin_topmenu_list_button_image_help'] = " 画像を登録してください。<br><span class='help-tab-gray'>推奨サイズ</span> 640*70<br/>
<span class='help-tab-gray'>容量</span> 300 KBで画像を登録してください。";
$lang['admin_topmenu_publish_view_page_url'] = "表示ページURL";
$lang['admin_topmenu_publish_view_page_url_help'] = "※設定されたページの内容によっては正常に表示されない場合がありますので確認ご了承の上ご利用ください。<br/>
<span class='help-tab-red'>PDFファイルの場合</span><br/>
http://docs.google.com/viewer?url=[ここをPDFのURLを指定]<br/>
<span class='help-tab-red'>外部ブラウザで開く場合</span><br/>
【リンクしたいURL】#target=_blank<br/>
上記の形式にて設定してください。";
$lang['admin_topmenu_publish_device'] = 'デバイス';
$lang['admin_topmenu_publish_device_ios'] = "iOS";
$lang['admin_topmenu_publish_device_android'] = "Android";
$lang['admin_topmenu_publish_status_show'] = "ディスプレイ";
$lang['admin_topmenu_publish_status_show_indicate'] = "表示";
$lang['admin_topmenu_publish_status_show_hidden'] = "非表示";
$lang['admin_topmenu_publish_status_show_help'] = "表示するメニューがない場合は、非表示を選択します。";
$lang['admin_topmenu_publish_sort_order'] = "並び順";
$lang['admin_topmenu_publish_sort_order_help'] = "*小さい方の番号が最初に表示されます。<br/>
(例: 0-#1-#2-#3-#4)";
$lang['admin_topmenu_publish_button_type'] = "ボタン背景タイプ";
$lang['admin_topmenu_publish_button_type_image'] = "画像";
$lang['admin_topmenu_publish_button_type_color'] = "カラー";
$lang['admin_topmenu_publish_image_for_list'] = "リストボタンイメージ(jpeg,jpg,gif,png)";
$lang['admin_topmenu_publish_image_for_list_help'] = "画像を登録してください。 <span class='help-tab-gray'>推奨サイズ</span> 640*350<br/>
<span class='help-tab-gray'>容量</span> 300 KB 以下";
$lang['admin_topmenu_publish_overlay'] = "オーバーレイ";
$lang['admin_topmenu_publish_overlay_effectiveness'] = "有効";
$lang['admin_topmenu_publish_overlay_invalid'] = "無効";
$lang['admin_topmenu_publish_overlay_help'] = "ボタンイメージを黒くする。";
$lang['admin_topmenu_publish_title_disp_yn'] = "タイトル表示";
$lang['admin_topmenu_publish_title_disp_yn_indicate'] = "表示";
$lang['admin_topmenu_publish_title_disp_yn_hidden'] = "非表示";
$lang['admin_topmenu_publish_title_text_color'] = "タイトルテキスト色";
$lang['admin_topmenu_publish_title_text_color_help'] = "タイトルのテキストの色を設定できます。";
$lang['admin_topmenu_publish_button_type'] = 'ボタン背景タイプ';
$lang['admin_topmenu_publish_title_position'] = "タイトル位置";
$lang['admin_topmenu_publish_title_position_middle'] = "真ん中";
$lang['admin_topmenu_publish_title_position_up'] = "上";
$lang['admin_topmenu_publish_title_position_under'] = "下";
$lang['admin_topmenu_publish_title_position_button_back_color'] = "ボタン背景色";
$lang['admin_topmenu_publish_title_position_button_back_color_help'] = "";
$lang[''] = "画像が設定されていないときは背景色を設定できます。";

////////Admin/topmenu/list

$lang['admin_topmenu_list_description']="アプリのトップページに表示する各メニューの画像や順番、表示・非表示を設定、編集することができます。<br>
修正ボタンから詳細設定を行ってください。<br>
※メニューを表示させたくない場合は、非表示を選択して下さい。<br>
※番号の入れ替えで、並び順を変更することが可能です。<br>
※並び順の変更を行った場合は、右上の「一括更新」を押して下さい。大文字入力NG。
";
$lang['admin_topmenu_top_menu_setting']="トップメニュー設定";
$lang['admin_topmenu_setting_edit']="トップメニュー設定 / 編集";
$lang['admin_topmenu_edit_top_menu']="トップメニュー編集";
$lang['admin_topmenu_index_fix'] = "修正";
$lang['admin_topmenu_index_delete'] = "削除";
$lang['admin_topmenu_index_top_menu_name'] = "トップメニュー名";
$lang['admin_topmenu_index_display'] = "ディスプレイ";
$lang['admin_topmenu_index_order'] = "順序";

//admin/topmenu/confirm
$lang['confirm_admin_topmenu_publish_type'] = "タイプ";
$lang['confirm_admin_topmenu_publish_type_web_page'] = "WEBページ形式";
$lang['confirm_admin_topmenu_publish_type_list_format'] = "リスト形式";
$lang['confirm_admin_topmenu_publish_type_menu'] = "メニュー";
$lang['confirm_admin_topmenu_publish_type_images_only'] = "イメージのみ";
$lang['confirm_admin_topmenu_publish_menu_name_top_menu_name'] = "トップメニュー名";
$lang['confirm_admin_topmenu_publish_image_for_panel_image'] = "イメージ";
$lang['confirm_admin_topmenu_publish_image_for_panel_image_help'] = " 画像を登録してください。<span class='help-tab-gray'>推奨サイズ</span> 360*280<br/>
<span class='help-tab-gray'>容量</span> 300KB 以内";
$lang['confirm_admin_topmenu_publish_device'] = "デバイス";
$lang['confirm_admin_topmenu_publish_device_ios'] = "iOS";
$lang['confirm_admin_topmenu_publish_device_android'] = "Android";
$lang['confirm_admin_topmenu_publish_status_show'] = "ディスプレイ";
$lang['confirm_admin_topmenu_publish_status_show_indicate'] = "表示";
$lang['confirm_admin_topmenu_publish_status_show_hidden'] = "非表示";
$lang['confirm_admin_topmenu_publish_status_show_help'] = "表示するメニューがない場合は、プライベートを選択します。";
$lang['confirm_admin_topmenu_publish_sort_order'] = "順序";
$lang['confirm_admin_topmenu_publish_sort_order_help'] = "";
$lang[''] = "*小さい方の番号が最初に表示されます。<br/>
(例: 0-#1-#2-#3-#4)";
///////////success
$lang['admin_topmenu_edit_success_return_text'] = "会員証の色ページの設定/編集に戻る";


/////////////////////////////////////////////////////

//Admin/design/listoflinks
$lang['design_listoflink_edit_basic'] = '基本情報編集';
$lang['design_listoflink_space'] = '';
$lang['design_listoflink_list_of_links'] = 'リンクリスト';
$lang['design_listoflinks_layout_type'] = 'レイアウト形式';
$lang['design_listoflinks_panel_layout'] = 'パネル形式';
$lang['design_listoflinks_list_layout'] = 'リスト型レイアウト';
$lang['design_listoflinks_margin_between'] = 'メニュー間のマージン';
$lang['design_listoflinks_with_margins'] = 'マージンあり';
$lang['design_listoflinks_no_margin'] = 'マージンなし';
$lang['design_listoflinks_latest_news_back_color'] = '最新お知らせ背景色';
$lang['design_listoflinks_news_back_color_help'] = "最新お知らせの背景色を設定できます。";
$lang['design_listoflinks_news_font_color'] = '最新お知らせ文字色';
$lang['design_listoflinks_news_font_color_help'] = "最新の発表のテキストの色を設定できます。";
$lang['design_listoflinks_news_title_color'] = "最新お知らせ見出し色";
$lang['design_listoflinks_news_title_color_help'] = "最新お知らせの見出し色を設定できます。";
$lang['design_listoflinks_coupon_back_color'] = "最新お知らせ背景色";
$lang['design_listoflinks_coupon_back_color_help'] ="最新クーポンの背景色を設定できます。";
$lang['design_listoflinks_coupon_font_color'] = "最新お知らせ文字色";
$lang['design_listoflinks_coupon_font_color_help'] ="最新クーポンの背景色を設定できます。";
$lang['design_listoflinks_coupon_title_color'] = "最新お知らせ見出し色";
$lang['design_listoflinks_coupon_title_color_help'] = "最新クーポンの見出し色を設定できます。";
$lang['form_description']="ホームページ、ブログ、facebookページなどにリンクを投稿できます。<br>.※アプリは情報リンク一覧ページに表示されます。";
$lang['list_of_links']="リンク一覧";
$lang['success_return_text'] ="戻る";

//Admin/design/Photoinformation
$lang['design_photoinformation_page_title']="基本情報編集";
$lang['design_photoinformation_space']="";
$lang['design_photoinformation_panel_title'] = "イメージ情報";
$lang['design_photoinformation_media_link'] = "パブリックフォト";
$lang['design_photoinformation_form_description']="ショップの写真は60枚まで登録でき、コメント付きで掲載できます。<br>
<span class='red'>*下の画面で写真をクリックしてドラッグすると、表示順序を並べ替えることができます。</span><br>
<span class='help-tab-gray'>形式:</span>jpeg,jpg,gif,png<br>
<span class='help-tab-gray'>推奨サイズ:</span>640*430 画像を登録してください。<br>
<span class='help-tab-gray'>容量</span>300KB<br>
*コメントは最大 40 文字です。 絵文字は使えません。";
$lang['design_photoinformation_success_return_text'] = "戻る";

//Admin/design/privacypolicy

$lang['design_privacypolicy_page_title']="基本コンテンツ";
$lang['design_privacypolicy_page_subtitle']="";
$lang['design_privacypolicy_panel_title']="プライバシーポリシー";
$lang['design_privacypolicy_title'] = 'テキスト';
$lang['design_privacypolicy_content'] = '運用組織名';
$lang['design_privacypolicy_space'] = "";
$lang['design_privacypolicy_return'] = "戻る";
$lang['design_productmenu_page_title']="新規商品メニュー";
$lang['design_productmenu_page_subtitle']="メニュー制作";
$lang['design_productmenu_panel_title']="商品メニュー編集";

//admin/autopush/settings/
$lang['autopush_settings_page_title']="自動プッシュ通知 設定";
$lang['autopush_settings_page_subtitle']="編集";
$lang['autopush_settings_panel_title']="自動プッシュ通知 設定";
$lang['autopush_settings_content'] = "テキスト";
$lang['autopush_settings_status_show'] = "ディスプレイ";
$lang['autopush_settings_status_show_y'] = "表示";
$lang['autopush_settings_status_show_n'] = "非表示";
$lang['autopush_settings_form_description'] = "";
$lang['autopush_settings_success_return_text'] = "戻る";
//admin/autopush/welcome_push/
$lang['autopush_welcomepush_page_title']="ようこそプッシュ";
$lang['autopush_welcomepush_page_subtitle']="編集";
$lang['autopush_welcomepush_panel_title']="ようこそプッシュ";
$lang['autopush_welcomepush_content'] = "テキスト";
$lang['autopush_welcomepush_status_show'] = "ディスプレイ";
$lang['autopush_welcomepush_status_show_y'] = "表示";
$lang['autopush_welcomepush_status_show_n'] = "非表示";
$lang['autopush_welcomepush_form_description'] = "";
$lang['autopush_welcomepush_success_return_text'] = "戻る";
//admin/autopush/birthdaypush/
$lang['autopush_birthdaypush_page_title']="誕生日配信";
$lang['birthday_push_information']="Birthday Push Information";
$lang['autopush_birthdaypush_page_subtitle']="編集";
$lang['autopush_birthdaypush_panel_title']="誕生日配信";
$lang['autopush_birthdaypush_content'] = "テキスト";
$lang['autopush_birthdaypush_status_show'] = "ディスプレイ";
$lang['autopush_birthdaypush_status_show_y'] = "表示";
$lang['autopush_birthdaypush_status_show_n'] = "非表示";
$lang['autopush_birthdaypush_form_description'] = "";
$lang['autopush_birthdaypush_success_return_text'] = "戻る";

//Admin/design/productmenu

$lang['design_productmenu_page_title'] = "新規商品メニュー";
$lang['design_productmenu_page_subtitle']="新規メニュー";
$lang['design_productmenu_panel_title'] = "商品メニュー編集";
$lang['design_productmenu_menutitle'] = 'メニュータイトル';
$lang['design_productmenu_menutitle_help'] = "*25 文字以下<span class='help-tab-gray'>入力例</span> drink メニュー";
$lang['design_productmenu_sub_title'] = 'subtitle';
$lang['design_productmenu_sub_title_help'] ="*25 文字以下<span class='help-tab-gray'>入力例</span> drink メニュー";
$lang['design_productmenu_detail'] ="メニュー詳細";
$lang['design_productmenu_detail_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png<br>
<span class='help-tab-gray'>推奨サイズ</span> width 250<br>
<span class='help-tab-gray'>容量</span> 300 KB 未満の画像を登録してください。";
$lang['design_productmenu_detail'] = "メニュー詳細";
$lang['design_productmenu_detail_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png<br>
<span class='help-tab-gray'>推奨サイズ</span> width 250<br>
<span class='help-tab-gray'>容量</span> 300 KB 未満の画像を登録してください。";
$lang['design_productmenu_thumbnail_image'] = 'サムネイルイメージ(jpeg,jpg,gif,png)';
$lang['design_productmenu_thumbnail_image_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png  画像をに登録してください。<br>
<span class='help-tab-gray'>推奨サイズ</span> 600*600<br>
<span class='help-tab-gray'>容量</span> 300KB以下";
$lang['design_productmenu_menu_image'] = "メニューイメージ(jpeg,jpg,gif,png)";
$lang['design_productmenu_menu_image'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png  画像をに登録してください。<br>
<span class='help-tab-gray'>推奨サイズ</span> 600*600<br>
<span class='help-tab-gray'>容量</span> 300KB以下";
$lang['design_productmenu_status_show'] = "公開設定";
$lang['design_productmenu_status_show_indicate'] = "表示";
$lang['design_productmenu_status_show_hidden'] = "非表示";
$lang['design_productmenu_order'] = "メニュー表示順";
$lang['design_productmenu_order_help'] = "*小さい番号が最初に表示されます。<br>
(例:0->1->2->3->4)";
$lang['design_productmenu_form_description'] = "トップメニュー設定で開いている「メニュー」に表示する製品またはサービスの紹介ページを作成できます。";
$lang['design_productmenu_success_return_text'] = "戻る";
////admin/productmenu/success
$lang['admin_productmenu_success_return_text'] = "メニュー編集に戻る。";
 

//Admin/basic/store

$lang['design_storeinformation_page_title']="アプリケーションの作成と編集";
$lang['design_storeinformation_space']="";
$lang['design_storeinformation_panel_title']="基本情報";
$lang['design_storeinformation_form_description']="本社情報を入力してください。<br>
※本部からプッシュ通知が配信される際に、本社の名称及び住所が配信元情報として表示されます。";
$lang['design_storeinformation_success_return_text']="入力に戻る";
$lang['admin_basic_store_publish_name'] = "店舗名";
$lang['admin_basic_store_publish_name_help'] = "<b>店名を入力してください。</b>(最大 20 の 2 バイト文字)<br>
*絵文字を使うことはできません";
$lang['admin_basic_store_publish_name_kana'] = "店舗名(カナ)";
$lang['admin_basic_store_publish_name_kana_help'] = "<b>店名を入力してください。</b>(最大 20 の 2 バイト文字)<br>
*絵文字を使うことはできません";
$lang['admin_basic_store_publish_country'] = "都道府県";
$lang['admin_basic_store_publish_address1'] = "住所1(市区町村)";
$lang['admin_basic_store_publish_address1_help'] = "<span class='help-tab-gray'>入力例</span> 東京都千代田区千代田1-1";
$lang['admin_basic_store_publish_address2'] = "住所2(番地)";
$lang['admin_basic_store_publish_address2_help'] = "<span class='help-tab-gray'>入力例</span> MMビル8階";
$lang['admin_basic_store_publish_latitude'] = "緯度";
$lang['admin_basic_store_publish_longitude'] = "経度線";
$lang['admin_basic_store_publish_street_name'] = "町名";
$lang['admin_basic_store_publish_street_name_help'] = "*を設定すると、アドレスではなく、これが表示されます。";
$lang['admin_basic_store_publish_google_play_url'] = "Google PlayのダウンロードURL";
$lang['admin_basic_store_publish_google_play_url_help'] = "アプリがGoogle Playで公開されたら記入します。";
$lang['admin_basic_store_publish_app_store_url'] = "App StoreのダウンロードURL";
$lang['admin_basic_store_publish_app_store_url_help'] = "AppStoreでアプリを公開したら記入します。";
$lang['admin_basic_store_publish_email'] = "Eメールアドレス";
$lang['admin_basic_store_publish_email_help'] = "<span class='help-tab-gray'>例</span> xxx@xxx.xxx<br>
*お客様からのお問い合わせのメールアドレスです。<br>
*このアドレスは、管理画面のパスワードを再発行する場合にも必要です。";
$lang['admin_basic_store_publish_email_show'] = "Email ディスプレイ";
$lang['admin_basic_store_publish_email_show_help'] = "*情報の問い合わせの表示を切り替えます。";
$lang['admin_basic_store_publish_email_show_y'] = "表示";
$lang['admin_basic_store_publish_email_show_n'] = "非表示";
$lang['admin_basic_store_publish_status_show'] = "ステータス";
$lang['admin_basic_store_publish_status_show_y'] = "表示";
$lang['admin_basic_store_publish_status_show_n'] = "非表示";
 
///////////////////////
$lang['confirm_admin_basic_store_publish_name'] = "店舗名";
$lang['confirm_admin_basic_store_publish_name_help'] = "<b>店名を入力してください。</b>(最大 20 の 2 バイト文字)<br>
*絵文字を使うことはできません";
$lang['confirm_admin_basic_store_publish_name_kana'] = "店舗名(カナ)";
$lang['confirm_admin_basic_store_publish_name_kana_help'] = "<b>店名を入力してください。</b>(最大 20 の 2 バイト文字)<br>
*絵文字を使うことはできません";
$lang['confirm_admin_basic_store_publish_country'] = "Prefectures";
$lang['confirm_admin_basic_store_publish_address1'] = "住所 1";
$lang['confirm_admin_basic_store_publish_address1_help'] = "<span class='help-tab-gray'>入力例</span> 東京都千代田区千代田1-1";
$lang['confirm_admin_basic_store_publish_address2'] = "住所 2";
$lang['confirm_admin_basic_store_publish_address2_help'] = "<span class='help-tab-gray'>記入例</span> MMビル8階";
$lang['confirm_admin_basic_store_publish_latitude_longitude'] = "緯度";
$lang['confirm_admin_basic_store_publish_longitude'] = "経度線";
$lang['confirm_admin_basic_store_publish_street_name'] = "町名";
$lang['confirm_admin_basic_store_publish_street_name_help'] = "*を設定すると、アドレスではなく、これが表示されます。";
$lang['confirm_admin_basic_store_publish_google_play_url'] = "Google PlayのダウンロードURL";
$lang['confirm_admin_basic_store_publish_google_play_url_help'] = "アプリがGoogle Playで公開されたら記入します。";
$lang['confirm_admin_basic_store_publish_app_store_url'] = "App StoreのダウンロードURL";
$lang['confirm_admin_basic_store_publish_app_store_url_help'] = "AppStoreでアプリを公開したら記入します。";
$lang['confirm_admin_basic_store_publish_email'] = "Eメールアドレス";
$lang['confirm_admin_basic_store_publish_email_help'] = "<span class='help-tab-gray'>例</span> xxx@xxx.xxx<br>
*お客様からのお問い合わせのメールアドレスです。<br>
*このアドレスは、管理画面のパスワードを再発行する場合にも必要です。";
$lang['confirm_admin_basic_store_publish_email_show'] = "Email ディスプレイ";
$lang['confirm_admin_basic_store_publish_email_show_help'] = "*情報の問い合わせの表示を切り替えます。";
$lang['confirm_admin_basic_store_publish_email_show_y'] = "表示";
$lang['confirm_admin_basic_store_publish_email_show_n'] = "非表示";
$lang['confirm_admin_basic_store_publish_status_show'] = "ステータス";
$lang['confirm_admin_basic_store_publish_status_show_y'] = "表示";
$lang['confirm_admin_basic_store_publish_status_show_n'] = "非表示";
 
/////////////////////////////
//Admin/design/termssentence

$lang['design_termssentence_page_title']="基本コンテンツ";
$lang['design_termssentence_page_subtitle']="";
$lang['design_termssentence_panel_title']="利用規約";
$lang['design_termssentence_content'] = "テキスト";
$lang['design_termssentence_status_show'] = "ディスプレイ";
$lang['design_termssentence_status_show_y'] = "表示";
$lang['design_termssentence_status_show_n'] = "非表示";
$lang['design_termssentence_form_description'] = "";
$lang['design_termssentence_success_return_text'] = "戻る";

//Admin/design/videoinformation
$lang['design_videoinformation_page_title']="基本情報編集";
$lang['design_videoinformation_space']="";
$lang['design_videoinformation_panel_title']="ビデオ情報";
$lang['design_videoinformation_media_link'] = "パブリックフォト";
$lang['design_videoinformation_form_description'] ="お店の紹介ビデオをパブリックに設定することができます。 動画をYouTubeにアップロードしてURLを投稿してください。<br>
*最大10本の動画を登録し、コメント付きで公開できます。<br>
*動画ファイル自体は公開できません。<br>
*コメントは最大 100 文字です。";
$lang['design_videoinformation_success_return_text'] = "戻る";
$lang['design_privacypolicy_page_title'] = "基本コンテンツ";
$lang['design_privacypolicy_page_subtitle'] = "";
$lang['design_privacypolicy_panel_title'] = "プライバシーポリシー";

///admin/productmenu/publish
$lang['admin_productmenu_publish_menu_title'] = "メニュータイトル";
$lang['admin_productmenu_publish_menu_title_help'] = "*25 文字以下<span class='help-tab-gray'>入力例</span> drink メニュー";
$lang['admin_productmenu_publish_sub_title'] = "subtitle";
$lang['admin_productmenu_publish_sub_title_help'] = "*25 文字以下<span class='help-tab-gray'>入力例</span> drink メニュー";
$lang['admin_productmenu_publish_detail'] = "メニュー詳細";
$lang['admin_productmenu_publish_detail_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png<br>
<span class='help-tab-gray'>推奨サイズ</span> width 250<br>
<span class='help-tab-gray'>容量</span> 300 KB 未満の画像を登録してください。";
$lang['admin_productmenu_publish_thumbnail_image'] = "Thumbnailイメージ(jpeg,jpg,gif,png)";
$lang['admin_productmenu_publish_thumbnail_image_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png  画像をに登録してください。<br>
<span class='help-tab-gray'>推奨サイズ</span> 600*600<br>
<span class='help-tab-gray'>容量</span> 300KB以下";
$lang['admin_productmenu_publish_menu_image'] = "メニューイメージ(jpeg,jpg,gif,png)";
$lang['admin_productmenu_publish_menu_image_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png  画像をに登録してください。<br>
<span class='help-tab-gray'>推奨サイズ</span> 600*600<br>
<span class='help-tab-gray'>容量</span> 300KB以下";
$lang['admin_productmenu_publish_status_show'] = "公開設定";
$lang['admin_productmenu_publish_status_show_y'] = "表示";
$lang['admin_productmenu_publish_status_show_n'] = "非表示";
$lang['admin_productmenu_publish_order'] = "メニュー表示順";
$lang['admin_productmenu_publish_order_help'] = "*小さい番号が最初に表示されます。<br>
(例:0->1->2->3->4)";
$lang['admin_productmenu_design_settings']="デザイン設定";
$lang['admin_productmenu_space']="";
$lang['admin_productmenu_new_product_menu']="新製品メニュー";
$lang['admin_productmenu_product_menu']="製品メニュー";
$lang['admin_productmenu_sub_product_menu']="製品メニュー";
$lang["admin_productmenu_sub_menu_list"]="メニューリスト";    
$lang["admin_productmenu_create_new"]="新規作成";
$lang['admin_productmenu_menu_creation']="メニュー制作";

///admin/productmenu/confirm
$lang['confirm_admin_productmenu_publish_menu_title'] = "メニュータイトル";
$lang['confirm_admin_productmenu_publish_menu_title_help'] = "*25 文字以下<span class='help-tab-gray'>入力例</span> drink メニュー";
$lang['confirm_admin_productmenu_publish_sub_title'] = "subtitle";
$lang['confirm_admin_productmenu_publish_sub_title_help'] = "*25 文字以下<span class='help-tab-gray'>入力例</span> drink メニュー";
$lang['confirm_admin_productmenu_publish_detail'] = "メニュー詳細";
$lang['confirm_admin_productmenu_publish_detail_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png<br>
<span class='help-tab-gray'>推奨サイズ</span> width 250<br>
<span class='help-tab-gray'>容量</span> 300 KB 未満の画像を登録してください。";
$lang['confirm_admin_productmenu_publish_thumbnail_image'] = "Thumbnailイメージ";
$lang['confirm_admin_productmenu_publish_thumbnail_image_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png  画像をに登録してください。<br>
<span class='help-tab-gray'>推奨サイズ</span> 600*600<br>
<span class='help-tab-gray'>容量</span> 300KB以下";
$lang['confirm_admin_productmenu_publish_menu_image'] = "メニューイメージ";
$lang['confirm_admin_productmenu_publish_menu_image_help'] = "<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png  画像をに登録してください。<br>
<span class='help-tab-gray'>推奨サイズ</span> 600*600<br>
<span class='help-tab-gray'>容量</span> 300KB以下";
$lang['confirm_admin_productmenu_publish_status_show'] = "公開設定";
$lang['confirm_admin_productmenu_publish_status_show_y'] = "表示";
$lang['confirm_admin_productmenu_publish_status_show_n'] = "非表示";
$lang['confirm_admin_productmenu_publish_order'] = "順序";
$lang['confirm_admin_productmenu_publish_order_help'] = "*小さい番号が最初に表示されます。<br>
(例:0->1->2->3->4)";
//admin/productmenu/index
 $lang['admin_productmenu_index_fix'] = "修正";
 $lang['admin_productmenu_index_delete'] = "削除";
 $lang['admin_productmenu_index_transition_source'] = "Transition source top メニュー";
 $lang['admin_productmenu_index_title'] = "タイトル";
 $lang['admin_productmenu_index_thumbnail_image'] = "Thumbnailイメージ";
 $lang['admin_productmenu_index_status_show'] = "ステータス";
 $lang['admin_productmenu_index_order'] = "順序";
//admin/productmenu/search
$lang['admin_productmenu_search_menu_title'] = "タイトル";
$lang['admin_productmenu_search_status_show_status'] = "ステータス";
$lang['admin_productmenu_search_status_show_publishing'] = "発行時のみ";
// admin/basic/agency
$lang['agency_information'] = "支店基本情報";
// Catalog Production
$lang['catalog_production'] = "カタログ/フォトギャラリー";
// Reservation
$lang['reservation'] = "予約";
$lang['reservation_management'] = "予約一覧";
$lang['time_settings'] = "時間設定";
$lang['assignee_settings'] = "担当者設定";
$lang['time_week_setting'] = '曜日・時間選択';
// Catalog
$lang['catalog'] = 'カタログ';
$lang['design_cataloginformation_success_return_text'] = '戻る';

$lang['aggregate'] = "集約する";
$lang['aggregation_period'] = "集約期間";
$lang['you_can_check_the_number_of_survey'] = "アンケート回答者数は1日単位で確認できます。 以下の検索条件から、表示するアンケートを選択し、検索ボタンを押してください。";

//User Information
$lang["user_id"] = 'ユーザーID';
$lang["nick_name"] = 'ニックネーム';
$lang["os"] = 'OS';
$lang["user_information_search_description"] = '対応するユーザ ID を検索し、参照ボタンからスタンプを追加できます。
                                                <br>ニックネーム検索は部分数学でも実行できます。<br>
                                                <span class="red">ユーザ ID 検索は抽出一致のみ</span>';
$lang["branch_name"] = "支店名";
$lang["android"] = "Android";
$lang["ios"] = "iOS";
$lang["year_month"] = "年、月";
$lang["total"] = "合計";
$lang['stamp_benefit_daily'] = "印紙給付の使用回数(日単位)";
$lang['distribution_title'] = "配布タイトル";
$lang['send_date'] = "送信日";
$lang["unread"] = "未読";
$lang["already_read"] = "読み済";
$lang["delivery_number"] = "配送番号";
$lang["open_rate"] = "オープンレート";
$lang["date"] = "日にち";
$lang["coupon_name"] = "クーポン名";
$lang["user_information_search_description"] = 'ユーザーID 閲覧ボタンからIDとスタンプを追加します。';
//Birthday_push
$lang["30days_before_birthday"] = "誕生日の30日前";
$lang["7days_before_birthday"] = "誕生日の7日前";
$lang["1day_before_birthday"] = "誕生日の１日前";
$lang["Birthday"] = "誕生日";
$lang["published_until_midnight_30"] ="誕生日の30日後深夜まで公開";
$lang["published_until_midnight_10"] ="誕生日の10日後深夜まで公開";
$lang["release_until_midnight"] ="誕生日の3日後深夜まで公開";
$lang["published_only_on_birthdays"] ="誕生日限定で発行";
$lang["Birthay_Coupon"] ="<span class = 'help-tab-gray'>記入例</span> 誕生日クーポン";
$lang["Happy_birthday"] ="<span class = 'help-tab-gray'>記入例</span> お誕生日おめでとう。...";
$lang["DISCOUNT_CONTENT_HELP_birthday"] = "<span class='help-tab-gray'>記入例</span> デザート一品無料！";
$lang["coupon_delivery_date"] = "クーポン配送日";
$lang["30_minutes"] = "ダウンロード後30分";
$lang["2_hours"] = "ダウンロードから2時間";
$lang["1_day"] = "ダウンロードから1日";
$lang["2_day"] = "ダウンロードから２日";
$lang["After_setting_profile"] = "Aプロフィール設定後";
$lang["DELIVERY_DATE_HELP"] = "※「ダウンロード」はアプリを起動しなければ有効になりません。<br> 
※「プロフィール設定後」はプロフィールの全項目を登録しなければ有効になりません。";
$lang["coupon_validity_period"] = "クーポン有効期間";
$lang["one_year"] = "クーポン配布から1年";
$lang["6_months"] = "クーポン配布から半年";
$lang["3_months"] = "クーポン配布から3ヶ月";
$lang["2_months"] = "クーポン配布から２ヶ月";
$lang["1_months"] = "クーポン配布から１ヶ月";
$lang["coupon_title"] = "クーポンタイトル";
$lang["TITLE_HELP"] = "<span class = 'help-tab-gray'>例</span>* アプリケーションのダウンロードありがとうございます。*";
$lang["coupon_detail"] = "クーポン詳細";
$lang["DETAIL_HELP"] = "<span class = 'help-tab-gray'>例</span>* この度は公式アプリのダウンロードありがとうございます! 感謝の気持ちを込めて、クーポン付き無料ドリンクを差し上げます! ";
$lang["coupon_image"] = "クーポン画像（アップ）";
$lang["IMAGE_HELP"] = "<span class='help-tab-gray'>形式:</span>jpeg,jpg,gif,png<br>
<span class='help-tab-gray'>推奨サイズ:</span>640*430 で画像を登録してください。<br>
<span class='help-tab-gray'>容量</span>300KB<br>";
$lang["discount_content"] = "割引内容";
$lang["DISCOUNT_CONTENT_HELP"] = "*  <span class='help-tab-gray'>記入例</span> 20文字まで無料ドリンク !!";
$lang["coupon_notice"] = "クーポン注意書き";
$lang["NOTICE_1_YN_HELP"] = "※他特典割引券、他サービス併用不可";
$lang["NOTICE_2_YN_HELP"] = "※なくなり次第終了";
$lang["NOTICE_3_YN_HELP"] = "※お一人様1回限り有効";
$lang["USE_SETTING_YN_HELP"] = "* 有効にすると、クーポンには使用ボタンが表示され、使用時には表示されなくなります。";
//Notification_setting
$lang["automatic_push_cycle"] = "自動プッシュサイクル";
$lang["automatic_push_delivery_criteria"] = "自動プッシュ配信基準";
$lang["automatic_push_delivery_criteria_1"] = "アプリにアクセスしている時";
$lang["automatic_push_delivery_criteria_2"] = "アプリに最終アクセスした後";
$lang["message"] = "メッセージ";
$lang['auto_push_type'] = '自動プッシュ形式';
///////////////////////////////////////////////////////////////////////
$lang["uuid"]="UUID";
$lang["registrationid"]="登録ID";
$lang["pin_for_changing"]="モデル変更用 PIN";
$lang["user_agent"] = "ユーザーエージェント";
$lang["terminal_name"] = "端末名";
$lang["os"]="OS";
$lang["android"] = "Android";
$lang["ios"] = "iOS";
$lang["edit_top_menu"]="トップメニュー編集";
$lang["top_menu_set"]="トップ メニュー設定";
$lang["top_menu_edit"]="トップメニュー設定/編集";
$lang["fix"]="修正";
$lang["delete"]="削除";
$lang["top_menu_name"]="トップ メニュー名";
$lang["display"]="状態";
$lang["order"]="注文";
$lang["create_new_survey"]="新しい調査の作成";
$lang["create_a_new"]="新規作成";
$lang["questionnaire"]="アンケート";
$lang["period"]="ピリオド";
$lang["title"]="タイトル";
$lang["coupon_setting"]="クーポン設定";
$lang["COUPON"]="クーポン";
$lang["fav_store_reg"]="お気に入り店舗登録";
$lang["questionnare_explanation"]="アンケート説明";
$lang["survey_period"]="アンケート期間";
$lang["publishing_settings"]="公開設定";
$lang["question"]="質問";
$lang["question_number"]="質問番号";
$lang["question_content"]="質問内容";
$lang["option"]="選択肢";
$lang['option_add'] = "選択肢追加";
$lang["input_type"]="入力タイプ";
$lang["delivery_status"]="配送状況";
$lang["reserved"]="予約済み";
$lang["delivered"]="配送済み";
$lang["new_product_menu"]="新製品メニュー";
$lang["product_menu"]="製品メニュー";
$lang["menu_list"]="メニュー リスト";
$lang["create_new"]="新規作成";
$lang["menu_creation"]="メニュー作成";
$lang["menu_title"]="メニュータイトル";
$lang["subtitle"]="サブタイトル";
$lang["top_menu_button"]="トップメニューボタン";
$lang["menu_details"]="メニューの詳細";
$lang["productmenu_menutitle_help"]="*25 文字以下 <span class='help-tab-gray'>入力例</span> ドリンクメニュー";
$lang["productmenu_subtitle_help"]="*25 文字以下 <span class='help-tab-gray'>入力例</span> ドリンクメニュー";
$lang["productmenu_detail_help"]="<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png<br><span class='help-tab-gray'>推奨サイズ</span> width 250<br><span class='help-tab-gray'>容量</span> 300 KB 未満の画像を登録してください。";
$lang["productmenu_imagetype"]="サムネール画像(jpeg,jpg,gif,png)";
$lang["productmenu_image_help"]="D<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png 画像を に登録してください<br>
                    <span class='help-tab-gray'>推奨サイズ</span> 600*600<br>
                    <span class='help-tab-gray'>容量</span> 300KB 以下 ";
$lang["productmenu_menuimagetype"]="メニュー イメージ(jpeg,jpg,gif,png)";
$lang["productmenu_menuimagehelp"]="<span class='help-tab-gray'>フォーマット</span>jpeg,jpg,gif,png 画像登録<br>
                    <span class='help-tab-gray'>推奨サイズ</span> 600*600<br>
                    <span class='help-tab-gray'>容量</span> 300 KBで画像を登録してください。";
$lang["menu_display_order"]="メニューの表示順序";
$lang["productmenu_order_help"]="*小さい方の番号が最初に表示されます。<br>
                    (例:0->1->2->3->4)";
$lang["new_event"]="新しいイベント";
$lang["publication_date"]="公表日";
$lang["publishing_only"]="発行のみ";
$lang["release_period_setting"]="リリース期間設定";
$lang["effectiveness"]="有効";
$lang["invalid"]="無効";
$lang["published_date"]="公開日";
$lang["news_event_date"]="ニュース、イベント日";
$lang["date"]="*50% OFF<br><span class='help-tab-gray'></span> 20 文字未満の説明!";
$lang["event_title_help"]="日付";
//$lang["news_event"]="ニュース/イベント";
$lang["news_event"]="コンテンツ名";
$lang["news_setting_edit"]="編集/削除";
$lang["body"]="ボディ";
$lang["event_body_help"]="* 1行20文字程度の改行があると、見やすくなります。";
$lang["event_image_help"]="<span class='help-tab-gray'>フォーマット</span> jpeg,jpg,gif,png<br>
                    <span class='help-tab-gray'>推奨サイズ</span> 750*540 <br>
                    <span class='help-tab-gray'>容量</span>300 KBで画像を登録してください。";
$lang["hidden_only"]="非表示(個別プッシュ時のみ表示)";
$lang["sort_order"]="ソート順序";
$lang["event_order_help"]="*小さい方の番号が最初に表示されます。(例:0->1->2->3->4)";
$lang["expiration_date"]="有効期限";
$lang["only_when_publishing"]="発行時のみ";
$lang["coupon_expiration_date"]="クーポン有効期限";
$lang["coupon_title"]="クーポンタイトル";
$lang["coupon_titile_help"]="<span class='help-tab-gray'>入力例</span> 4名以上お越しいただくとドリンク1杯無料!";
$lang["coupon_details"]="クーポン詳細";
$lang["coupon_details_help"]="<span class='help-tab-gray'>入力例</span> 4人以上で1ドリンク無料!";
$lang["coupon_image_type"]="クーポン画像（アップ）";
$lang["coupon_image_help"]="<span class='help-tab-gray'>フォーマット</span> jpeg,jpg,gif,png<br>
                        <span class='help-tab-gray'>推奨サイズ</span>640*430 <br>
                        <span class='help-tab-gray'>容量</span>300 KBで画像を登録してください。";
$lang["discount_content"]="割引内容";
$lang["coupon_discount_help"]="<span class='help-tab-gray'>記入例</span> ハンド&フットオフ無料15,800円▶10,500円";
$lang["coupon_notice"]="クーポン注意書き";
$lang["single_use_setting"]="使いきり設定";
$lang["coupon_single_help"]="*有効にすると、クーポンには使用ボタンが表示され、使用時には表示されなくなります。";
$lang["countdown_display_setting"]="カウントダウン表示設定";
$lang["coupon_countdown_help"]="*有効にすると、クーポン有効期限のカウントダウンが表示されます。";
$lang["coupon_status_help"]="*非表示にするとユーザーが使用できなくなります。";
$lang["coupon_display_order"]="クーポン表示順番";
$lang["coupon_display_order_help"]="*小さい方の番号が最初に表示されます。<br>(例:0->1->2->3->4)";
$lang["for_questionnaire"]="アンケート用";
$lang["coupon_questionaire_help"]="*調査の報酬のクーポンとして設定されます。";
$lang["end"]="終わり";
$lang["waiting_for_release"]="開始待ち";
$lang["now_open"]="現在開始中";
$lang["pdf"]="PDF";
$lang["create_new_reservation"]="新規予約の作成";
$lang["reservation_management_space"]="予約一覧";
$lang["create_reservation"]="予約の作成";
$lang["user"]="ユーザー";
$lang["number_of_persons"]="人数";
$lang['reservation_date']="予約日付";
$lang['reservation_time']="予約時間";
$lang["on_going"]="継続中";
$lang["accepted"]="承認済み";
$lang["application_file_for_management"]="管理用アプリケーションファイル";
$lang["application_file_for_information"]="ストア登録";
$lang["application"]="ストア申請";
$lang["manual"]="マニュアル";
$lang["headerfooter_headerimg_help"]="<span class='help-tab-gray'>フォーマット</span> jpeg,jpg,gif,png</br>
                         <span class='help-tab-gray'>推奨サイズ</span>640*84</br>
                         <span class='help-tab-gray'>容量</span>イメージを 300 KB 以内に登録してください。";
$lang["headerfooter_footerimg_help"]="<span class='help-tab-gray'>フォーマット</span> jpeg,jpg,gif,png</br>
                         <span class='help-tab-gray'>推奨サイズ</span>640*100</br>
                         <span class='help-tab-gray'>容量</span>イメージを 300 KB 以内に登録してください。";
$lang["layout_form_discription"]="アプリトップページのレイアウトを編集する.";
$lang["layout_success_return_text"]="レイアウト設定の編集に戻る";
$lang["pagecolor_form_description"]="スタンプ・クーポンページの背景色やテキスト色などの設定を編集します。";
$lang["return"]="戻る";
$lang["pagecolor_succes_return_text"]="ページカラー設定/編集に戻る";
$lang["total"] = "合計";
$lang['stamp_benefit_daily'] = "スタンプ特典の使用回数(日単位)";
$lang['distribution_title'] = "配布タイトル";
$lang["number_of_analysis"]="分析";
$lang["stamp_by_month"]="スタンプ(月別)";
$lang["all_stores"]="全店舗";
$lang["branch_2"]="支店 2";
$lang["branch_3"]="支店 3";
$lang["graph"]="グラフ";
$lang["jan"]="Jan";
$lang["feb"]="Feb";
$lang["mar"]="Mar";
$lang["stamp_by_day"]="スタンプ数(日別)";
$lang["stamp_benefit_month"]="スタンプ特典利用数（月別）";
$lang["total"]="合計";
$lang["stamp_benefit_daily"]="スタンプ特典利用数（日別）";
$lang["push_notification_ctr"]="プッシュ通知CTR";
$lang["number_search_results_displayed"]="Number of search results displayed";
$lang["csv_name_jan"]="CSVダウンロード";
$lang["csvdownload"]="CSVダウンロード";
$lang["favorite_by_month"]="お気に入り（月別）";
$lang["favorite_by_day"]="お気に入り（日別）";
$lang["mon"]="月";
$lang["tue"]="火";
$lang['send_date'] = "送信日";
$lang["wed"]="水";
$lang["thu"]="木";
$lang["fri"]="金";
$lang["sat"]="土";
$lang["san"]="日";
$lang["please_select"]="選択してください";
$lang["coupon_1"]="クーポン 1";
$lang["coupon_2"]="クーポン 2";
$lang["coupon_3"]="クーポン 3";
$lang["number_search_results_displayed"]="表示された検索結果の数";
$lang["no_new_announcements_now"]="現在新しいお知らせはございません。";
$lang["how_to_use_storeapp"]=" アプリモの使い方";
$lang["how_to_create_apps(pdf)"]="アプリの作り方・更新の方法(PDF)";
$lang["analysis"]="分析";
$lang["total_number_app_downloads"]="アプリ総ダウンロード数";
$lang["uninstall_substraction_no_addition"]="（アンインストール減算、再ダウンロード加算なし）";
$lang["total_last_push_in_total"]="合計 ";
$lang["total_push_notification_ctr"]="前回のプッシュ通知CTR<br>合計 0%";
$lang["no_know_how_to_see_app"]="私が作ったアプリを携帯で見る方法が分からない。";
$lang["read_qr_code_by_app"]="アプリケーション管理でQRコードをお読みください。QR コードのダウンロード、ダウンロード、および端末へのインストール";
$lang["want_create_edit_app"]="アプリを作成·編集したい、どうしたらよいですか?";
$lang["read_qr_code_by_app_on_terminal"]="アプリケーション管理でQRコードをお読みください。QR コードのダウンロード、ダウンロード、および端末へのインストール";
$lang["want_make_coupon"]="クーポンを作成したい、どうすればいいですか?";
$lang["app_will_teminated_forcibly"]="アプリは強制終了となります。";
$lang["what_pust_notification"]="プッシュ通知とは?";
$lang["pust_notification_not_delivered"]="プッシュ通知は配信されません。";
$lang["displayed_cannot_install"]="同じアプリが終了するのでインストールできないと表示されます。";
$lang["need_reinstall_app_everytime"]="写真や情報を更新するたびにアプリを再インストールする必要がありますか?";
$lang["i_editted_photos_info"]="写真や情報を編集しましたが、表示は変わりません。";
$lang["can_change_name_app"]="アプリ名を変更できますか?";
$lang["post_twitter_link"]="Twitterリンク投稿";
$lang["not_post_twitter_link"]="Twitter を投稿しない";
$lang["twitter_account"]="Twitterアカウント";
$lang["enter_twitter_account_post"]="投稿するTwitterアカウントを入力してください。";
$lang["login_twitter_enter_number_setting"]="Twitterページにログインし、Settings*に説明されている番号を入力します。";
$lang["entry_example"]="エントリの例";
$lang["page_info_twitter_page"]="ページ情報*Twitter ページ ID。";
$lang["facebook_link"]="Facebook リンク";
$lang["facebook_account"]="Facebook アカウント";
$lang["enter_facebook_accout"]="投稿したいFacebookアカウントを入力する";
$lang["page_info_facebook_pageid"]="ページ情報>FacebookページID.";
$lang["can_post_link_blog_homepage"]="ブログまたはホームページへのリンクを投稿できます。.";
$lang["enter_url_press_addbutton"]="*URLを入力して[+上のボタンを追加]ボタンを押してください。";
$lang["please_select"]="選択してください";
$lang["blog"]="ブログ";
$lang["pc_homepage"]="PCホームページ";
$lang["smartphone_page"]="スマートフォンのページ";
$lang["hot_pepper"]="ホットペッパー";
$lang["gournavi"]="ぐるなび";
$lang["shopping"]="ショッピング";
$lang["reservation"]="予約";
$lang["job_information"]="求人情報";
$lang["add_above_site"]="上記のサイトを追加";
$lang["unread"] = "未読";
$lang["already_read"] = "既読";
$lang["delivery_number"] = "配送番号";
$lang["open_rate"] = "開封率";
$lang["valid"]="有効";
$lang["from_dec_last"]="12月 31, 9999から";
$lang["to_dec_last"]="12月 31, 9999まで";
$lang["preview_welcome_other_privilege"]="※ その他の特典割引チケットとその他のサービスは併用できません。";
$lang["will_end_as_soon_gone"]="※ なくなり次第終了とさせていただきます";
$lang["efftive_once_per_person"]="※ お一人様１回限り有効。";
$lang["date"] = "日付";
$lang["find_out_more"]="詳細を確認する";
$lang["obu_company_published_july"]="Company 2020年7月23日発行";
$lang["latest_announment_info_displayed"]="最新のアナウンス情報が表示されます。";
$lang["store_where_stamp_used"]="スタンプ取得数";
$lang["member_id"]="メンバー ID";
$lang["about_stamp"]="スタンプについて";
$lang["about_benefits"]="特典について";
$lang["full_days"]="365日";
$lang["coupon_name"] = "クーポン名";
$lang["customer_name"]="お客様の名前";
$lang["membership_id"]="メンバシップ ID";

$lang['basicsetting_webview_url']="Webビュー Url";

$lang['other_privilege_discount'] = "※他特典割引券、他サービス併用不可";
$lang['will_end_as_soon_as_it'] = "※なくなり次第終了とさせていただきます";
$lang['effective_once_per_person'] = "※お一人様１回限り有効。";
$lang['news_events_description'] = "セールやイベント情報など、お店からのお知らせを作成して公開し、プッシュ通知で配信することが出来ます。";
$lang['coupon_to_distribute'] = "配布するクーポンを選択して下さい。";
$lang['answering_the_questionnare'] = "アンケート回答前に、お気に入り店舗の登録を必須にするかの選択をします。";
$lang['valid_only'] = "有効のみ";
$lang['news_event_information'] = "お知らせ・イベント情報";
$lang['all_delivery'] = "全配信";
$lang['delivery_segment'] = "セグメント別配信";
$lang['create_new_announcement'] = "新しくお知らせを作成";
$lang['text'] = '内容';
$lang['language'] = '言語';
$lang['basicsetting_webview_head'] = 'ウエブビューアプリ';
$lang['expiration'] = '有効期限';
$lang['Holding'] = '開催';
$lang['Pending'] = '期間外';
$lang['Finished'] = '完成';
$lang['selection_type'] = '選択タイプ';
$lang['choice_type'] = '選択タイプ(複数回答)';
$lang['input_1line'] = '入力(１行)';
$lang['input_multiline'] = '入力(複数行)';
$lang['any'] = '任意';
$lang['to_list_question'] = '設問一覧へ';
$lang['return_to_repair'] = '戻る';
$lang['stampprivilege_page_space'] = 'スタンプと権限の設定';
$lang["Sun"] = "<span style = 'color:#F00'>(日)</span>";
$lang["Mon"] = "(月)";
$lang["Tue"] = "(火)";
$lang["Wed"] = "(水)";
$lang["Thu"] = "(木)";
$lang["Fri"] = "(金)";
$lang["Sat"] = "<span style = 'color:#00F'>(土)</span>";
$lang["January"] = "01月";
$lang["February"] = "02月";
$lang["March"] = "03月";
$lang["April"] = "04月";
$lang["May"] = "05月";
$lang["June"] = "06月";
$lang["July"] = "07月";
$lang["August"] = "08月";
$lang["September"] = "09月";
$lang["October"] = "10月";
$lang["November"] = "11月";
$lang["December"] = "12月";
$lang["staff_intro"] = "スタッフ紹介";
$lang["product_service"] = "商品・サービス";
$lang["company_info"] = "会社情報";
$lang["case"] = "事例";
$lang["news_blog"] = "ニュース・ブログ";
$lang['member_function'] = '会員機能';
$lang['on'] = 'ON';
$lang['off'] = "OFF";
$lang['password_function'] = 'パスワード';
$lang['id_password_function'] = 'IDとパスワード';
$lang['inquiry_form'] = '問い合わせフォーム';
$lang['inquiry_form_sub_menu'] = '問い合わせフォーム管理';
$lang['katakana'] = 'カタカナ';
$lang['depart_sign'] = '部署名';
$lang['email_address'] = 'メールアドレス';
$lang['office_name'] = '役職名';
$lang['inquiries'] = 'お問い合わせ内容';
$lang['information_security'] = '情報セキュリティ';
$lang['about_this_app'] = 'このアプリについて';
$lang['free_content'] = 'フリーコンテンツ';
$lang['list_reg'] = '一覧 / 登録';
$lang['new_reg'] = '新規登録';
$lang['post_content'] = '投稿コンテンツ';
$lang['category_reg'] = 'カテゴリー新規登録';
$lang['category_list'] = 'カテゴリー一覧 / 編集';
$lang['post_reg'] = '投稿新規登録';
$lang['post_list'] = '投稿一覧 / 編集';
$lang['intro_friend'] = '友達紹介';
$lang['step_send'] = 'ステップ配信';
$lang['user_search_list'] = '検索・一覧';
$lang['user_info_setting'] = 'ユーザー設定情報';
$lang['category_title'] = 'カテゴリー名';
$lang['post_template'] = 'テンプレ選択（4種類）';
$lang['post_content_start_end_date'] = '掲載開始、終了日設定';
$lang['post_content_category'] = 'カテゴリー選択';
$lang['stamp_per_day'] = '1日取得店舗上限数';
$lang['stamp'] = 'スタンプ';
$lang['link_select'] = 'リンク選択（内部・外部）';
$lang['link_in'] = '内部';
$lang['link_out'] = '外部';
$lang['url'] = "URL";
$lang['stamp_image_upload'] = 'スタンプ画像（アップ）';
$lang['stamp_image_help'] = 'スタンプ用の画像を登録できます。<br>
※最大3枚まで登録することが出来ます。';
$lang['inquiry_form_desc'] = "※設問は10個まで設定できます。<br>※列をドラッグして並べ替えできます。";
$lang['inquiry_no'] = '設問番号';
$lang['store_guide'] = '店舗案内';
$lang['pricing'] = '料金';
$lang['FAQ'] = 'FAQ';
$lang['notice_setting'] = '通知設定';
$lang['text_body'] = '本文';
$lang['stamp_per_day_desc'] = '1日に取得できる店舗数の上限です。(午前0時にリセット)';
$lang['pdf_max_size'] = 'サイズが5MB以下のファイルをご登録してください。';
$lang['fix1'] = '修正・確認';
$lang['survey1'] = '設問';
$lang['push_type_all'] = '全体';
$lang['push_type_individual'] = '個別';
$lang['push_type_segment'] = 'セグメント';
$lang['header_help_desc'] = '<span class="help-block help-down-area"><span class="help-tab-gray">形式</span>jpeg,jpg,gif,png<br><span class="help-tab-gray">推 奨サイズ</span>640×480
<br><span class="help-tab-gray">容 量</span>300KB以内の画像を登録してください<br></span>';
$lang['header_help_desc1'] = '<span class="help-block help-down-area" style="padding:15px;"><span class="help-tab-gray">形式</span>jpeg,jpg,gif,png<br><span class="help-tab-gray">推 奨サイズ</span>640×480
<br><span class="help-tab-gray">容 量</span>300KB以内の画像を登録してください<br>必須に1個以上は登録してください。</span>';
$lang['use'] = '使用する';
$lang['user_info_change'] = 'ユーザー情報を変更してください。';
$lang['list_edit'] = '一覧/編集';
$lang['no_select'] = '選択なし';
$lang['inquiry_address'] = '問合せ受信用アドレス';
$lang['min5'] = '5分';
$lang['10min'] = '10分';
$lang['15min'] = '15分';
$lang['30min'] = '30分';
$lang['1hour'] = '1時間';
$lang['2hour'] = '2時間';
$lang['1day'] = '1日';
$lang['book_weekdays'] = '予約可能日';
$lang['book_time_range'] = '時間範囲';
$lang['book_time'] = '予約可能時間';
$lang['book_input_setting'] = '予約入力設定';
$lang['phone'] = '電話番号';
$lang['rest_week'] = '休日';
$lang['work_hour'] = '勤務時間';
$lang['stamp_acquired'] = 'スタンプ取得数';
$lang['no_holiday'] = '休日なし';
$lang['app_build_failed'] = 'アプリの吐出しが失敗しました。確認後に再度行って下さい。';
$lang['survey_desc'] = 'アンケートの設問を作成します。';
$lang['inquiry_field_name'] = '項目名';
$lang['off_twofactor'] = '<span class="help-block help-area">2ファクタ認証を無効（オフ）にしてください。</span>';
$lang['input_2fa_code'] = '2ファクターコードを入力してください';
$lang['download'] = 'ダウンロード';
$lang['label_edit'] = '項目名を変更';
$lang['file_path'] = '結果ファイルのパス';
$lang['public'] = '公開';
$lang['non_public'] = '非公開';
$lang['inquiry_list'] = '問い合わせ内容一覧';
$lang['send_date'] = '伝送日付';
$lang['member_show'] = '会員ログインのみ表示';
$lang['on'] = "ON";
$lang['off'] = "OFF";
$lang['all_app'] = "アプリ全体";
$lang['part_app'] = "一部コンテンツのみ";
$lang['member_option'] = "オプション";
$lang['footer_menu_setting'] = 'フッターメニュー表示設定';
$lang['footer_image_icon'] = 'フッター画像登録';
$lang['admin_topmenu_footer_image_icon_help'] = " 画像を登録してください。<br><span class='help-tab-gray'>推奨サイズ</span> 30*30<br/>
<span class='help-tab-gray'>容量</span> 300 KBで画像を登録してください。";
$lang['header_image_icon'] = 'ヘッダーアイコン画像';
$lang['header_icon_setting'] = 'ヘッダーメニュー表示設定';
$lang['description'] = '説明文';
$lang['language_setting'] = '言語設定';
$lang['japanese'] = '日本語';
$lang['chinese_simple'] = '中国語(簡体)';
$lang['chinese_tradition'] = '中国語(繁体)';
