<?php
class Demo extends PREVIEW_Controller {
  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->load->view('demo');
  }
}