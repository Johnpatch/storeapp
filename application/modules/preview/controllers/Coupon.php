<?php
class Coupon extends PREVIEW_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Common_model');
    $this->load->model('Basicsetting_model');
  }

  public function index()
  {
    $branch_id = $this->input->post('BRANCH_ID');

    $common_setting = $this->Common_model->get_settings($branch_id);
    if ($common_setting === NULL) 
    {
      $common_setting = json_decode("{}");
    }

    $common_setting->COUPON_TITLE = $this->input->post('COUPON_TITLE');
    $common_setting->COUPON_DETAIL = $this->input->post('COUPON_DETAIL');
    $common_setting->COUPON_START_DATE = $this->input->post('COUPON_START_DATE');
    $common_setting->COUPON_END_DATE = $this->input->post('COUPON_END_DATE');
    $common_setting->COUPON_IMAGE = $this->input->post('COUPON_IMAGE');
    $common_setting->DISCOUNT = $this->input->post('DISCOUNT');
    $common_setting->COUPON_NOTICE_CHK1 = $this->input->post('COUPON_NOTICE_CHK1');
    $common_setting->COUPON_NOTICE_CHK2 = $this->input->post('COUPON_NOTICE_CHK2');
    $common_setting->COUPON_NOTICE_CHK3 = $this->input->post('COUPON_NOTICE_CHK3');
    $common_setting->USE_SETTING_YN = $this->input->post('USE_SETTING_YN');
    $common_setting->COUNTDOWN_DISP_YN = $this->input->post('COUNTDOWN_DISP_YN');
    $common_setting->STATUS_SHOW = $this->input->post('STATUS_SHOW');
    $common_setting->ORDER = $this->input->post('ORDER');
    $common_setting->REWARD_SURVEY_YN = $this->input->post('REWARD_SURVEY_YN');

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $common_setting->BASIC_SETTING = $basic_setting;

    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('coupon')]);
    $this->load->view('CouponLayout', $common_setting);
    $this->load->view('_parts/footer_t1.php');
  }
}