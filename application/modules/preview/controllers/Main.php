<?php

/**
 * Super Class
 *
 * @package     StoreApp
 * @subpackage  Preview
 * @category    preview
 * @author      storeapp team
 * @link        http://storeapp.com
 */
class Main extends PREVIEW_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Common_model');
    $this->load->model('Topmenu_model');
    $this->load->model('Slide_model');
    $this->load->model('Photovideo_model');
    $this->load->model('Postlinks_model');
    $this->load->model('Productmenu_model');
    $this->load->model('Stampinfo_model');
    $this->load->model('Stampimage_model');
    $this->load->model('Basicsetting_model');
  }

  public function index()
  {
    $this->load->view('demo');
  }

  /**
   * POST method for layout setting.
   */
  public function topmenu_layout() {
    $branch_id = $this->input->post('BRANCH_ID');
    $result = $this->db->select('LAYOUT_TYPE, LAYOUT_TEMPLATE_TYPE')->where('BRANCH_ID', $branch_id)->get('layout_settings')->row_array();
    $layout_type = $result['LAYOUT_TYPE'];
    $layout_template_type = $result['LAYOUT_TEMPLATE_TYPE'];
    
    if (!isset($branch_id)) 
    {
      show_404();
    }

    if (isset($layout_type) && $layout_type == 1) 
    {
      $this->panellayout($branch_id,$layout_template_type);
    } 
    else if (isset($layout_type) && $layout_type == 2)
    {
      $this->listlayout($branch_id);
    }
    else
    {
      show_404();
    }
  }

  public function layout(){
    $branch_id = $this->input->post('BRANCH_ID');
    $layout_type = $this->input->post('LAYOUT_TYPE');
    $layout_template_type = $this->input->post('LAYOUT_TEMPLATE_TYPE');
    if(empty($layout_type)){
      $result = $this->db->select('LAYOUT_TYPE')->where('BRANCH_ID', $branch_id)->get('layout_settings')->row_array();
      $layout_type = $result['LAYOUT_TYPE'];
    }
    if (!isset($branch_id)) 
    {
      show_404();
    }

    if (isset($layout_type) && $layout_type == 1) 
    {
      $this->panellayout($branch_id,$layout_template_type);
    } 
    else if (isset($layout_type) && $layout_type == 2)
    {
      $this->listlayout($branch_id);
    }
    else
    {
      show_404();
    }
  }
  /**
   * Process panel layout on layout setting.
   */
  public function panellayout($branch_id, $type) {
    $sql_result = $this->Common_model->get_settings($branch_id);

    if ($sql_result !== NULL) $params = $sql_result;

    if($type == 5 || $type == 3 || $type == 6 || $type == 11){
      $top_menu_settings = $this->Topmenu_model->get_rows($branch_id, 1);
      if ($sql_result !== NULL) {
        $params->TOP_MENU_SETTINGS = $top_menu_settings;
      } else {
        $params['TOP_MENU_SETTINGS'] = $top_menu_settings;
      }
    }
    
    if($type == 3 || $type == 4 || $type == 5 || $type == 6 || $type == 10 || $type == 11){
      $slide_settings = $this->Slide_model->get_rows($branch_id);
      $sql_result->SLIDE_SETTINGS = $slide_settings;
    }

    if($type == 3 || $type == 5){
      $header_menu = $this->Common_model->get_header_menu($branch_id);
      $sql_result->HEADER_MENU = $header_menu;
    }
    
    if($type == 11){
      //$languages = $this->Common_model->get_languages($branch_id);
      $lang_array = [];
      //foreach($languages as $key => $value){
        if($this->input->post('ENGLISH') == 'Y')
          array_push($lang_array, 'ENGLISH');
        if($this->input->post('JAPANESE') == 'Y')
          array_push($lang_array, lang('japanese'));
        if($this->input->post('CHINESE1') == 'Y')
          array_push($lang_array, lang('chinese_simple'));
        if($this->input->post('CHINESE2') == 'Y')
          array_push($lang_array, lang('chinese_tradition'));
        if($this->input->post('THAI') == 'Y')
          array_push($lang_array, 'THAI');
      //}
      $sql_result->LANGUAGES = $lang_array;
    }
    

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;

    if($type != 5 && $type != 6){
      $post_category = $this->Common_model->get_post_category($branch_id);
      $sql_result->POST_CATEGORY = $post_category;
      $posts = $this->Common_model->get_posts($branch_id);
      $sql_result->POSTS = $posts;
    }
    $sql_result->LAYOUT_TEMPLATE_NO = $type;
    $this->load->view('_parts/header.php', $params);
    $this->load->view('PanelLayout', $params);
    if($type == 1 || $type == 4 || $type == 5 || $type == 7 || $type == 9 || $type == 10){
      $footer->FOOTERS = $this->Common_model->get_footer_menu($branch_id);
      $this->load->view('_parts/footer_t1.php', $footer);
    }
    //$this->load->view('_parts/footer.php', $params);
  }
  /**
   * Process list layout on layout setting.
   */
  public function listlayout($branch_id) {
    $is_margin_between_menus = $this->input->post('IS_MARGIN_BETWEEN_MENUS');
    $news_back_color = $this->input->post('NEWS_BACK_COLOR');
    $news_font_color = $this->input->post('NEWS_FONT_COLOR');
    $news_title_color = $this->input->post('NEWS_TITLE_COLOR');
    $coupon_back_color = $this->input->post('COUPON_BACK_COLOR');
    $coupon_font_color = $this->input->post('COUPON_FONT_COLOR');
    $coupon_title_color = $this->input->post('COUPON_TITLE_COLOR');
    $is_active = $this->input->post('IS_ACTIVE');

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }
    if(!empty($is_margin_between_menus)) $sql_result->IS_MARGIN_BETWEEN_MENUS = $is_margin_between_menus;

    if (!empty($news_back_color)) $sql_result->NEWS_BACK_COLOR = $news_back_color;
    if (!empty($news_font_color)) $sql_result->NEWS_FONT_COLOR = $news_font_color;
    if (!empty($news_title_color)) $sql_result->NEWS_TITLE_COLOR = $news_title_color;
    if (!empty($coupon_back_color)) $sql_result->COUPON_BACK_COLOR = $coupon_back_color;
    if (!empty($coupon_font_color)) $sql_result->COUPON_FONT_COLOR = $coupon_font_color;
    if (!empty($coupon_title_color)) $sql_result->COUPON_TITLE_COLOR = $coupon_title_color;
    if (!empty($is_active)) $sql_result->IS_ACTIVE = $is_active;

    $top_menu_settings = $this->Topmenu_model->get_rows($branch_id);
    $sql_result->TOP_MENU_SETTINGS = $top_menu_settings;

    $slide_settings = $this->Slide_model->get_rows($branch_id);
    $sql_result->SLIDE_SETTINGS = $slide_settings;

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;

    $this->load->view('_parts/header.php', $sql_result);
    $this->load->view('ListLayout', $sql_result);
    $this->load->view('_parts/footer.php', $sql_result);
  }

  /**
   * POST method for Header and Footer settings.
   */
  public function headerandfooter() {
    $branch_id = $this->input->post('BRANCH_ID');
    $header_image = $this->input->post('HEADER_IMAGE');
    $footer_image = $this->input->post('FOOTER_IMAGE');
    $header_image_del_yn = $this->input->post('HEADER_IMAGE_DEL_YN');
    $footer_image_del_yn = $this->input->post('FOOTER_IMAGE_DEL_YN');

//    var_dump($this->input->post());exit;
    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

     if (isset($header_image) && !empty($header_image))
     {
        if($header_image_del_yn == 'Y')
          $sql_result->HEADER_IMAGE = '';
        else 
          $sql_result->HEADER_IMAGE = $header_image;
     }

    if (isset($footer_image)) 
    {
      if($footer_image_del_yn == 'Y')
        $sql_result->FOOTER_IMAGE = '';
      else
        $sql_result->FOOTER_IMAGE = $footer_image;
    }

    $back_url = $this->input->post("back_url");
    $is_topmenu = strstr($back_url, "topmenu");
    $is_slide = strstr($back_url, "design/slide");

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;

    //if (isset($sql_result->LAYOUT_TYPE) && $sql_result->LAYOUT_TYPE == 1 || $is_topmenu || $is_slide)
    $slide_settings = $this->Slide_model->get_rows($branch_id);
    $sql_result->SLIDE_SETTINGS = $slide_settings;

    if (isset($sql_result->LAYOUT_TYPE) && $sql_result->LAYOUT_TYPE == 1)
    {
      $top_menu_settings = $this->Topmenu_model->get_rows($branch_id, 1);
      $sql_result->TOP_MENU_SETTINGS = $top_menu_settings;

      
      $sql_result->LAYOUT_TEMPLATE_NO = 'slide';
      $this->load->view('_parts/header.php', $sql_result);
      $this->load->view('PanelLayout', $sql_result);
      $this->load->view('_parts/footer.php', $sql_result);
    }
    else 
    {
      $top_menu_settings = $this->Topmenu_model->get_rows($branch_id, 2);
      $sql_result->TOP_MENU_SETTINGS = $top_menu_settings;

      $this->load->view('_parts/header.php', $sql_result);
      $this->load->view('ListLayout', $sql_result);
      $this->load->view('_parts/footer.php', $sql_result);
    }
  }

  /**
   * POST method for Card Layout
   */
  public function cardlayout() {
    $branch_id = $this->input->post('BRANCH_ID');
    $BACK_COLOR = $this->input->post('BACK_COLOR');
    $FONT_COLOR = $this->input->post('FONT_COLOR');

    if (!isset($branch_id)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    if (isset($BACK_COLOR)) $sql_result->BACK_COLOR = $BACK_COLOR;
    if (isset($FONT_COLOR)) $sql_result->FONT_COLOR = $FONT_COLOR;

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;

    $this->load->view('CardLayout', $sql_result);
  }

  /**
   * POST method for Color pattern Layout
   */
  public function colorpattern() {
    $branch_id = $this->input->post('BRANCH_ID');
    $COLOR_PATTERN = $this->input->post('COLOR_PATTERN');
    $TITLE_BAR_BACK_COLOR = $this->input->post('TITLE_BAR_BACK_COLOR');
    $TITLE_BAR_FONT_COLOR = $this->input->post('TITLE_BAR_FONT_COLOR');
    $PAGE_BACK_COLOR = $this->input->post('PAGE_BACK_COLOR');
    $PAGE_BACK_IMAGE = $this->input->post('PAGE_BACK_IMAGE');
    $PAGE_FONT_COLOR = $this->input->post('PAGE_FONT_COLOR');
    $BUTTON_BACK_COLOR = $this->input->post('BUTTON_BACK_COLOR');

    if (!isset($branch_id)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    // $PAGE_BACK_IMAGE = base_url("assets/imgs/login-bg.png");

    if (isset($COLOR_PATTERN)) $sql_result->COLOR_PATTERN = $COLOR_PATTERN;
    if (isset($TITLE_BAR_BACK_COLOR)) $sql_result->TITLE_BAR_BACK_COLOR = $TITLE_BAR_BACK_COLOR;
    if (isset($TITLE_BAR_FONT_COLOR)) $sql_result->TITLE_BAR_FONT_COLOR = $TITLE_BAR_FONT_COLOR;
    if (isset($PAGE_BACK_COLOR)) $sql_result->PAGE_BACK_COLOR = $PAGE_BACK_COLOR;
    if (isset($PAGE_BACK_IMAGE)) $sql_result->PAGE_BACK_IMAGE = $PAGE_BACK_IMAGE;
    if (isset($PAGE_FONT_COLOR)) $sql_result->PAGE_FONT_COLOR = $PAGE_FONT_COLOR;
    if (isset($BUTTON_BACK_COLOR)) $sql_result->BUTTON_BACK_COLOR = $BUTTON_BACK_COLOR;

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;

    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('stamp')]);
    $this->load->view('ColorPatternLayout', $sql_result);
    $this->load->view('_parts/footer_t1.php');
  }

  /**
   * POST method for Stamp info
   */
  public function stampinfo() {
    $input = $this->input->post();

    $param = array();
    $branch_id = $input['BRANCH_ID'];
    $param = (array)$this->Common_model->get_settings($branch_id);
    $param['TITLE'] = $input['TITLE'];
    $param['DETAIL'] = $input['DETAIL'];
    $param['NUMBER_OF_STAMPS'] = $input['NUMBER_OF_STAMPS'];
    $param['MAXIMUM_STAMPS'] = $input['MAXIMUM_STAMPS'];
    $param['COMPLETE_NUMBER'] = $input['COMPLETE_NUMBER'];
    $param['STAMP_CARD_VALID_DAYS'] = $input['STAMP_CARD_VALID_DAYS'];
    $param['STAMP_IMAGE'] = $input['STAMP_IMAGE'];
    if(!empty($param['STAMP_IMAGE'])){
      $result = $this->db->select('URL')->where('ID', $param['STAMP_IMAGE'])->get('stamp_image')->row_array();
      $param['STAMP_IMAGE_URL'] = base_url().$result['URL'];
    }
    $param['PRIVILEGE_DETAIL'] = $input['PRIVILEGE_DETAIL'];
    $param['REWARD_EXPIRATION_DATE'] = $input['REWARD_EXPIRATION_DATE'];
    $param['TERMS_CONDITIONS'] = $input['TERMS_CONDITIONS'];
    $param['bonus_image_delete'] = $input['bonus_image_delete'];
    $param['BONUS_IMAGE'] = $input['BONUS_IMAGE'];
    /*if($param['bonus_image_delete'] == "1") {
      $param['BONUS_IMAGE'] = "";
    } else {
      if(array_key_exists('file', $_FILES)) {
        $filename = $_FILES['file']['name'];
        if($filename != "") {
          $location = "attachments/".$filename;
          $imageFileType = pathinfo($location, PATHINFO_EXTENSION);

          $valid_extensions = array("jpg","jpeg","png","gif");
          if( in_array(strtolower($imageFileType),$valid_extensions) ) {
              if(move_uploaded_file($_FILES['file']['tmp_name'],$location)) {
                  $param['BONUS_IMAGE'] = $location;
              }
          }
        }
      }
    }*/
    if (!isset($branch_id)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    // $PAGE_BACK_IMAGE = base_url("assets/imgs/login-bg.png");


    $param['TITLE_BAR_BACK_COLOR'] = $sql_result->TITLE_BAR_BACK_COLOR;
    $param['TITLE_BAR_FONT_COLOR'] = $sql_result->TITLE_BAR_FONT_COLOR;
    $param['PAGE_BACK_COLOR'] = $sql_result->PAGE_BACK_COLOR;
    $param['PAGE_BACK_IMAGE'] = $sql_result->PAGE_BACK_IMAGE;
    $param['PAGE_FONT_COLOR'] = $sql_result->PAGE_FONT_COLOR;
    $param['BUTTON_BACK_COLOR'] = $sql_result->BUTTON_BACK_COLOR;

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $param['BASIC_SETTING'] = $basic_setting;

    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('stamp')]);
    $this->load->view('StampInfoLayout', $param);
    $this->load->view('_parts/footer_t1.php');
  }

  /**
   * POST method for photo information layout
  */
  public function photoinfo() {
    $branch_id = $this->input->post('BRANCH_ID');
    $MEDIA_LINK = $this->input->post('MEDIA_LINK');
    $COMMENT = $this->input->post('COMMENT');

    if (!isset($branch_id)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    $photo_infos = $this->Photovideo_model->get_rows($branch_id);
    if ($photo_infos === NULL) $photo_infos = [];

    if (is_array($MEDIA_LINK) && !empty($MEDIA_LINK)) 
    {
      $sql_result->MEDIA_LINK = $MEDIA_LINK;
    } else {
      $sql_result->MEDIA_LINK = [];
    }

    if (is_array($COMMENT) && !empty($COMMENT)) 
    {
      $sql_result->COMMENT = $COMMENT;
    } else {
      $sql_result->COMMENT = [];
    }

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;
    
    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('mobile_header')]);
    $this->load->view('PhotoLayout', $sql_result);
    $this->load->view('_parts/footer_t1.php');
  }

  /**
   * POST method for catalog information layout
  */
  public function cataloginfo() {
    
    $branch_id = $this->input->post('BRANCH_ID');
    $TYPE = $this->input->post('TYPE');
    $MEDIA_LINK = $this->input->post('MEDIA_LINK');
    $PDF = $this->input->post('PDF');
    $TITLE = $this->input->post('TITLE');
    $THUMBNAIL = $this->input->post('THUMBNAIL');
    // var_dump($this->input->post());exit;

    if (!isset($branch_id)) 
    {
      show_404();
    }

    $catalog_data = json_decode("{}");
    $catalogPDF_data = json_decode("{}");
    $catalogPhoto_data = json_decode("{}");

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $catalog_data->BASIC_SETTING = $basic_setting;
    $catalogPDF_data->BASIC_SETTING = $basic_setting;
    $catalogPhoto_data->BASIC_SETTING = $basic_setting;

    $arrCatalog = [];
    $arrPhoto = [];
    $arrPDF = [];
    $arrThumbnail = [];
    $arrTitle = [];
    
    isset($TYPE) ? $is_type = $TYPE : $is_type = 0;
    if ($is_type == 0) {
      $query = $this->db->query('SELECT * FROM catalogs where branch_id="'.$branch_id.'" and title IS NOT NULL');
      foreach ($query->result() as $row)
      {
        array_push($arrCatalog, [$row->THUMBNAIL, $row->TITLE]);
      }

      $catalog_data->CATALOG = $arrCatalog;
    }
    
    
    if ($is_type == 2) 
    {
      $query = $this->db->query('SELECT * FROM catalogs where branch_id="'.$branch_id.'" and type=2 and title IS NOT NULL');
      foreach ($query->result() as $row)
      {
        array_push($arrPhoto, $row->THUMBNAIL);
      }
      $catalogPhoto_data->MEDIA_LINK = $arrPhoto;
      if(!empty($THUMBNAIL)){
        array_push($arrPhoto, $THUMBNAIL);
        $catalogPhoto_data->MEDIA_LINK = $arrPhoto;
      }
    } 

    if (is_array($PDF) && !empty($PDF)) 
    {
      $catalogPDF_data->PDF = $PDF;
    } 
    if($is_type == 1) {
      $query = $this->db->query('SELECT * FROM catalogs where branch_id="'.$branch_id.'" and type=1 and title IS NOT NULL');
      foreach ($query->result() as $row)
      {
        array_push($arrPDF, $row->PDF);
        array_push($arrThumbnail, $row->THUMBNAIL);
        array_push($arrTitle, $row->TITLE);
      }

      $catalogPDF_data->PDF = $arrPDF;
      $catalogPDF_data->THUMBNAIL = $arrThumbnail;
      $catalogPDF_data->TITTLE = $arrTitle;
      if(!empty($THUMBNAIL)){
        array_push($arrThumbnail, $THUMBNAIL);
        $catalogPDF_data->THUMBNAIL = $arrThumbnail;
      }
      if(!empty($TITLE)){
        array_push($arrTitle, $TITLE);
        $catalogPDF_data->TITTLE = $arrTitle;
      }
      if(!empty($PDF)){
        array_push($arrPDF, $PDF);
        $catalogPDF_data->PDF = $arrPDF;
      }
    }

    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('catalog_production')]);
    if ($is_type == 0) {
      $this->load->view('CatalogLayout', $catalog_data);
    } else if ($is_type == 1) {
      $this->load->view('CatalogPDFLayout', $catalogPDF_data);
    } else if ($is_type == 2) {
      $this->load->view('CatalogPhotoLayout', $catalogPhoto_data);
    }
    //$this->load->view('_parts/footer_t1.php');
  }

  /**
   * POST method for video information layout
  */
  public function videoinfo() {
    $branch_id = $this->input->post('BRANCH_ID');
    $MEDIA_LINK = $this->input->post('MEDIA_LINK');
    $COMMENT = $this->input->post('COMMENT');

    if (!isset($branch_id)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    $photo_infos = $this->Photovideo_model->get_rows($branch_id, 2);
    if ($photo_infos === NULL) $photo_infos = [];

    if (is_array($MEDIA_LINK) && !empty($MEDIA_LINK)) 
    {
      for($i=0; $i<count($MEDIA_LINK); $i++) {
        $index = strripos($MEDIA_LINK[$i], 'v=');
        $video_id = substr($MEDIA_LINK[$i], $index + 2);
        $MEDIA_LINK[$i] = "http://img.youtube.com/vi/".$video_id."/hqdefault.jpg";
      }
      $sql_result->MEDIA_LINK = $MEDIA_LINK;
    } else {
      $sql_result->MEDIA_LINK = [];
    }

    if (is_array($COMMENT) && !empty($COMMENT)) 
    {
      $sql_result->COMMENT = $COMMENT;
    } else {
      $sql_result->COMMENT = [];
    }
    
    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;

    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('mobile_header')]);
    $this->load->view('VideoLayout', $sql_result);
    $this->load->view('_parts/footer_t1.php');
  }

  /**
   * POST method for link information layout
  */
  public function linksinfo() {
    $branch_id = $this->input->post('BRANCH_ID');
    $TYPE = $this->input->post('TYPE');
    $LINK = $this->input->post('LINK');
    $TWITTER_ACTIVE_YN = $this->input->post('TWITTER_ACTIVE_YN');
    $FACEBOOK_ACTIVE_YN = $this->input->post('FACEBOOK_ACTIVE_YN');
    // print_r($this->input->post()); exit;
    
    if (!isset($branch_id)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    if (is_array($TYPE) && !empty($TYPE)) 
    {
      for($i = 0; $i < count($TYPE); $i++)
      {
        $sql_result->LINKS[] = ['TYPE' => $TYPE[$i], 'LINK' => $LINK[$i], 'TWITTER_ACTIVE_YN' => $TWITTER_ACTIVE_YN, 'FACEBOOK_ACTIVE_YN' => $FACEBOOK_ACTIVE_YN];
      }
    } else {
      $sql_result->LINKS[] = ['TYPE' => 'N', 'LINK' => 'N', 'TWITTER_ACTIVE_YN' => $TWITTER_ACTIVE_YN, 'FACEBOOK_ACTIVE_YN' => $FACEBOOK_ACTIVE_YN];
    }
    
    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;

    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('other')]);
    $this->load->view('LinksLayout', $sql_result);
    $this->load->view('_parts/footer_t1.php');
  }

  /**
   * POST method for link information layout
  */
  public function birthdayinfo() {
    $branch_id = $this->input->post('BRANCH_ID');
    
    if (!isset($branch_id)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    $sql_result->AUTO_PUSH_YN = $this->input->post('AUTO_PUSH_YN');
    $sql_result->DELIVERY_DATE = $this->input->post('DELIVERY_DATE');
    $sql_result->VALIDITY_PERIOD = $this->input->post('VALIDITY_PERIOD');
    $sql_result->TITLE = $this->input->post('TITLE');
    $sql_result->DETAIL = $this->input->post('DETAIL');
    $sql_result->IMAGE = $this->input->post('IMAGE');
    $sql_result->DISCOUNT_CONTENT = $this->input->post('DISCOUNT_CONTENT');
    $sql_result->NOTICE_1_YN = $this->input->post('NOTICE_1_YN');
    $sql_result->NOTICE_2_YN = $this->input->post('NOTICE_2_YN');
    $sql_result->NOTICE_3_YN = $this->input->post('NOTICE_3_YN');
    $sql_result->USE_SETTING_YN = $this->input->post('USE_SETTING_YN');
    $sql_result->PAGE_BACK_IMAGE = $this->input->post('PAGE_BACK_IMAGE');
    // print_r($sql_result); exit;

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;

    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('Birthday')]);
    $this->load->view('birthdayLayout', $sql_result);
    $this->load->view('_parts/footer_t1.php');
  }

  /**
   * POST method for link information layout
  */
  public function welcomeinfo() {
    $branch_id = $this->input->post('BRANCH_ID');
    
    if (!isset($branch_id)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    $sql_result->AUTO_PUSH_YN = $this->input->post('AUTO_PUSH_YN');
    $sql_result->DELIVERY_DATE = $this->input->post('DELIVERY_DATE');
    $sql_result->VALIDITY_PERIOD = $this->input->post('VALIDITY_PERIOD');
    $sql_result->TITLE = $this->input->post('TITLE');
    $sql_result->DETAIL = $this->input->post('DETAIL');
    $sql_result->IMAGE = $this->input->post('IMAGE');
    $sql_result->DISCOUNT_CONTENT = $this->input->post('DISCOUNT_CONTENT');
    $sql_result->NOTICE_1_YN = $this->input->post('NOTICE_1_YN');
    $sql_result->NOTICE_2_YN = $this->input->post('NOTICE_2_YN');
    $sql_result->NOTICE_3_YN = $this->input->post('NOTICE_3_YN');
    $sql_result->USE_SETTING_YN = $this->input->post('USE_SETTING_YN');
    $sql_result->PAGE_BACK_IMAGE = $this->input->post('PAGE_BACK_IMAGE');
    // print_r($sql_result); exit;
    
    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;

    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('other')]);
    $this->load->view('welcomeLayout', $sql_result);
    $this->load->view('_parts/footer_t1.php');
  }

  /**
   * POST method for link information layout
  */
  public function pushinfo() {
    $branch_id = $this->input->post('BRANCH_ID');
    
    if (!isset($branch_id)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    $DELIVERY_TYPE = $this->input->post('DELIVERY_TYPE');
    $REFER_ID = $this->input->post('REFER_ID');
    $sql_result->TITLE = $this->input->post('TITLE');
    $sql_result->TEXT = $this->input->post('MESSAGE');
    $DELIVERY_TIME_SETTING = $this->input->post('DELIVERY_TIME_SETTING');
    if($DELIVERY_TIME_SETTING == 1){
      $now = new DateTime();
      $sql_result->CENTER_TITLE = $now->format('Y').'年'.$now->format('m').'月'.$now->format('d').'日 '.$now->format('H').'時'.$now->format('i').'分';
      $sql_result->DATE = $now->format('Y').".".$now->format("m").".".$now->format("d");
    }else{
      $now = new DateTime($this->input->post('DELIVERY_DATETIME'));
      $sql_result->CENTER_TITLE = $now->format('Y').'年'.$now->format('m').'月'.$now->format('d').'日 '.$now->format('H').'時'.$now->format('i').'分';
      $sql_result->DATE = $now->format('Y').".".$now->format("m").".".$now->format("d");
    }
    
    /*if($DELIVERY_TYPE != NULL && $REFER_ID != NULL){
      switch ($DELIVERY_TYPE) {
        case '1':
          $query = $this->db->query('select title, detail from events where id='.$REFER_ID);
          foreach ($query->result() as $row)
          {
            $sql_result->TLTLE = $row->title;
            $sql_result->TEXT = $row->detail;
          }
          break;
  
        case '2':
          $query = $this->db->query('select coupon_title, coupon_detail from coupon where id='.$REFER_ID);
          foreach ($query->result() as $row)
          {
            $sql_result->TLTLE = $row->coupon_title;
            $sql_result->TEXT = $row->coupon_detail;
          }
          break;

        case '3':
          $query = $this->db->query('select title, detail from events where id='.$REFER_ID);
          foreach ($query->result() as $row)
          {
            $sql_result->TLTLE = $row->title;
            $sql_result->TEXT = $row->detail;
          }
          break;

        case '4':
          $query = $this->db->query('select title, description from survey where id='.$REFER_ID);
          foreach ($query->result() as $row)
          {
            $sql_result->TLTLE = $row->title;
            $sql_result->TEXT = $row->description;
          }
          break;
        
        default:
          # code...
          break;
      }
    }*/
    
    $sql_result->LEFT_TITLE = 'アプリモ';
    
    

    // print_r($REFER_ID); exit;
    
    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    $sql_result->BASIC_SETTING = $basic_setting;
    
    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('notice')]);
    $this->load->view('pushLayout', $sql_result);
    $this->load->view('_parts/footer_t1.php');
  }

  public function html($type) {
    $branch_id = $this->input->post('BRANCH_ID');
    $basic_setting->FIRST_HTML = $this->input->post('FIRST_HTML');
    $basic_setting->SECOND_HTML = $this->input->post('SECOND_HTML');
    $basic_setting->THRID_HTML = $this->input->post('THIRD_HTML');
	// $basic_setting = $this->Basicsetting_model->get_settings($branch_id);

    $sql_result = json_decode("{}");
    $sql_result->type = $type;
    $sql_result->BASIC_SETTING = $basic_setting;

    $this->load->view('_parts/header_t1.php');
    $this->load->view('HtmlLayout', $sql_result);
    $this->load->view('_parts/footer_t1.php');
  }
}