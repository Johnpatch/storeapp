<?php
class Basiccontent extends PREVIEW_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Common_model');
    $this->load->model('Basicsetting_model');

  }

  public function index()
  {
    $branch_id = $this->input->post('BRANCH_ID');

    $common_setting = $this->Common_model->get_settings($branch_id);
    if ($common_setting === NULL) 
    {
      $common_setting = json_decode("{}");
    }

    
    $common_setting->TYPE = $this->input->post('TYPE');
    $common_setting->TITLE = $this->input->post('TITLE');
    $common_setting->COMMENT = $this->input->post('COMMENT');
    $common_setting->IMAGE = $this->input->post('IMAGE');

    $basic_setting = $this->Basicsetting_model->get_settings($branch_id);
    //$common_setting->BASIC_SETTING = $basic_setting;
      
    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => lang('basic_information')]);
    $this->load->view('BasicLayout', $common_setting);
    $this->load->view('_parts/footer_t1.php');
  }
}