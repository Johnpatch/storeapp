<?php
class Productmenu extends PREVIEW_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Common_model');
    $this->load->model('Topmenu_model');
    $this->load->model('Productmenu_model');
  }

  public function index()
  {
    $branch_id = $this->input->post('BRANCH_ID');

    if (!isset($branch_id)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    $sql_result->MENU_TITLE = $this->input->post('MENU_TITLE');
    $sql_result->SUB_TITLE = $this->input->post('SUB_TITLE');
    $sql_result->PARENT_MENU_ID = $this->input->post('PARENT_MENU_ID');
    $sql_result->DETAIL = $this->input->post('DETAIL');
    $sql_result->THUMBNAIL_IMAGE = $this->input->post('THUMBNAIL_IMAGE');
    $sql_result->MENU_IMAGE = $this->input->post('MENU_IMAGE');
    $sql_result->STATUS_SHOW = $this->input->post('STATUS_SHOW');
    $sql_result->ORDER = $this->input->post('ORDER');

    $menu = $this->Topmenu_model->get_menu($branch_id, $sql_result->PARENT_MENU_ID);
    $menu === NULL ? $title_bar = "" : $title_bar = $menu->MENU_NAME;

    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => $title_bar]);
    $this->load->view('ProductMenuDetail', $sql_result);
    $this->load->view('_parts/footer_t1.php');
  }

  public function list() {
    $branch_id = $this->input->post('BRANCH_ID');
    $PARENT_MENU_ID = $this->input->post('PARENT_MENU_ID');

    if (!isset($branch_id) || !isset($PARENT_MENU_ID)) 
    {
      show_404();
    }

    $sql_result = $this->Common_model->get_settings($branch_id);
    if ($sql_result === NULL) 
    {
      $sql_result = json_decode("{}");
    }

    $menu = $this->Topmenu_model->get_menu($branch_id, $PARENT_MENU_ID);
    $menu === NULL ? show_404() : $sql_result->MENU = $menu;

    $product_menu = $this->Productmenu_model->get_product_menus($branch_id, $PARENT_MENU_ID);

    $sql_result->PRODUCT_MENU = $product_menu;

    $this->load->view('_parts/header_t1.php', ['TITLE_BAR' => $menu->MENU_NAME]);
    $this->load->view('ProductMenuList', $sql_result);
    $this->load->view('_parts/footer_t1.php');
  }
}