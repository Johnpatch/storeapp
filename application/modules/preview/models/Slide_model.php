<?php

class Slide_model extends MY_Model
{
    protected $table = 'slide_settings';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_rows($branch_id) {
      $sql = "SELECT * FROM slide_settings WHERE BRANCH_ID = ? AND STATUS_SHOW = 'Y'";
    
      $sql_result = $this->db->query($sql, array($branch_id));

      if (!$sql_result) 
      {
        $error = $this->db->error();
        log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
        return NULL;
      }

      $result = $sql_result->result_array();
      return $result;
    }
}