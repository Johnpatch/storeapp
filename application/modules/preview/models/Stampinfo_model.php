<?php

class Stampinfo_model extends MY_Model
{
    protected $table = 'stamp_info';
    public function __construct()
    {
        parent::__construct();
    }

    public function insert_row($params) {
        $this->db->insert($this->table, $params);
    }

    public function update_row($id, $params) {
    	$this->db->where('BRANCH_ID', $id);
        $this->db->update($this->table, $params);
    }

    public function get_row($params) {
        if ($params["BRANCH_ID"] != NULL) {
            $this->db->where('BRANCH_ID', $params["BRANCH_ID"]);
        }
        return $this->db->get($this->table)->row_array();
    }
}
