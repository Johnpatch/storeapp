<?php

class Photovideo_model extends MY_Model
{
    protected $table = 'photo_video_info';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_rows($branch_id, $media_type=1) {
      $sql = "SELECT * FROM post_links WHERE BRANCH_ID = ? AND ACTIVE_YN = 'Y' ORDER BY `CREATE_TIME` desc";
    
      $sql_result = $this->db->query($sql, array($branch_id));

      if (!$sql_result) 
      {
        $error = $this->db->error();
        log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
        return NULL;
      }

      $result = $sql_result->result_array();
      return $result;
    }
}