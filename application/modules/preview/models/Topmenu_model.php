<?php

class Topmenu_model extends MY_Model
{
    protected $table = 'topmenu_settings';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_rows($branch_id) {
      //$sql = "SELECT * FROM topmenu_settings WHERE BRANCH_ID = ? AND LAYOUT_TYPE = ? AND STATUS_SHOW = 'Y' ORDER BY `ORDER`";
      $sql = "SELECT * FROM topmenu_settings WHERE BRANCH_ID = ? AND STATUS_SHOW = 'Y' AND IS_FOOTER != 'Y' ORDER BY `ORDER`";
    
      $sql_result = $this->db->query($sql, array($branch_id));

      if (!$sql_result) 
      {
        $error = $this->db->error();
        log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
        return NULL;
      }

      $result = $sql_result->result_array();
      return $result;
    }

    public function get_menu($branch_id, $menu_id) {
      $sql = "SELECT * FROM topmenu_settings WHERE BRANCH_ID = ? AND ID = ?";
    
      $sql_result = $this->db->query($sql, array($branch_id, $menu_id));

      if (!$sql_result) 
      {
        $error = $this->db->error();
        log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
        return NULL;
      }

      $result = $sql_result->row();
      return $result;
    }
}