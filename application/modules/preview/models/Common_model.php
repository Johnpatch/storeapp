<?php

class Common_model extends CI_Model
{
  public function __construct()
  {
      parent::__construct();
  }

  public function get_settings($branch_id)
  {
    $sql = "SELECT *, header_footer_settings.*, membercard_settings.*, page_color_settings.* FROM layout_settings LEFT JOIN header_footer_settings ON header_footer_settings.BRANCH_ID = layout_settings.BRANCH_ID LEFT JOIN membercard_settings ON membercard_settings.BRANCH_ID = layout_settings.BRANCH_ID LEFT JOIN page_color_settings ON page_color_settings.BRANCH_ID = layout_settings.BRANCH_ID WHERE layout_settings.BRANCH_ID = ?";
    
    $sql_result = $this->db->query($sql, array($branch_id));
    if (!$sql_result) 
    {
      $error = $this->db->error();
      log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
      return NULL;
    }

    $result = $sql_result->row();
    if (isset($result->HEADER_IMAGE) && !empty($result->HEADER_IMAGE)) {
      $result->HEADER_IMAGE = $result->HEADER_IMAGE;
    } else if(isset($result->HEADER_IMAGE)) {
      $result->HEADER_IMAGE = NULL;
    }

    if (isset($result->FOOTER_IMAGE)&& !empty($result->FOOTER_IMAGE)) {
      $result->FOOTER_IMAGE = $result->FOOTER_IMAGE;
    }

    return $result;
  }

  public function get_page_color_settings($branch_id)
  {
    $sql = "SELECT * FROM page_color_settings WHERE BRANCH_ID = ?";
    
    $sql_result = $this->db->query($sql, array($branch_id));
    if (!$sql_result) 
    {
      $error = $this->db->error();
      log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
      return NULL;
    }

    $result = $sql_result->row();

    return $result;
  }

  public function get_post_category($branch_id){
    $sql = "SELECT * FROM post_category WHERE BRANCH_ID = ? ORDER BY CREATE_TIME DESC";
    $sql_result = $this->db->query($sql, array($branch_id));
    if (!$sql_result) 
    {
      $error = $this->db->error();
      log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
      return NULL;
    }

    $result = $sql_result->result_array();

    return $result;
  }

  public function get_posts($branch_id){
    $sql = "SELECT * FROM events WHERE BRANCH_ID = ? AND CURDATE() between START_DATE AND END_DATE ORDER BY CREATE_TIME DESC";
    $sql_result = $this->db->query($sql, array($branch_id));
    if (!$sql_result) 
    {
      $error = $this->db->error();
      log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
      return NULL;
    }

    $result = $sql_result->result_array();

    return $result;
  }

  public function get_branch_info($branch_id){
    $sql = "SELECT * FROM branches WHERE ID = ?";
    $sql_result = $this->db->query($sql, array($branch_id));
    if (!$sql_result) 
    {
      $error = $this->db->error();
      log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
      return NULL;
    }

    $result = $sql_result->row_array();

    return $result;
  }

  public function get_footer_menu($branch_id){
    $sql = "SELECT * FROM topmenu_settings WHERE BRANCH_ID = ? and IS_FOOTER = 'Y'";
    $sql_result = $this->db->query($sql, array($branch_id));
    if (!$sql_result) 
    {
      $error = $this->db->error();
      log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
      return NULL;
    }

    $result = $sql_result->result_array();

    return $result;
  }

  public function get_header_menu($branch_id){
    $sql = "SELECT * FROM topmenu_settings WHERE BRANCH_ID = ? and IS_HEADER = 'Y'";
    $sql_result = $this->db->query($sql, array($branch_id));
    if (!$sql_result) 
    {
      $error = $this->db->error();
      log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
      return NULL;
    }

    $result = $sql_result->result_array();

    return $result;
  }

  public function get_languages($branch_id){
    $sql = "SELECT ENGLISH, JAPANESE, CHINESE1, CHINESE2, THAI FROM layout_settings WHERE BRANCH_ID = ?";
    $sql_result = $this->db->query($sql, array($branch_id));
    if (!$sql_result) 
    {
      $error = $this->db->error();
      log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
      return NULL;
    }

    $result = $sql_result->result_array();

    return $result;
  }
}
