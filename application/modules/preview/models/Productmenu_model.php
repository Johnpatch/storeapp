<?php

class Productmenu_model extends MY_Model
{
    protected $table = 'product_menu';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_product_menus($branch_id, $menu_id) {
      $sql = "SELECT * FROM product_menu WHERE BRANCH_ID = ? AND PARENT_MENU_ID = ? ORDER BY `ORDER`";
    
      $sql_result = $this->db->query($sql, array($branch_id, $menu_id));

      if (!$sql_result) 
      {
        $error = $this->db->error();
        log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
        return NULL;
      }

      $result = $sql_result->result_array();
      return $result;
    }

    public function get_product_menu_detail($product_id) {
      $sql = "SELECT * FROM product_menu WHERE ID = ?";
    
      $sql_result = $this->db->query($sql, array($product_id));

      if (!$sql_result) 
      {
        $error = $this->db->error();
        log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
        return NULL;
      }

      $result = $sql_result->row();
      return $result;
    }
}