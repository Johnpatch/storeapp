<?php

class Postlinks_model extends MY_Model
{
    protected $table = 'post_links';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_rows($branch_id, $media_type) {
      $sql = "SELECT * FROM photo_video_info WHERE BRANCH_ID = ? AND TYPE = ? ORDER BY `CREATE_TIME` desc";
    
      $sql_result = $this->db->query($sql, array($branch_id, $media_type));

      if (!$sql_result) 
      {
        $error = $this->db->error();
        log_message('error', 'Get DB Setting From BRANCH_ID: ' . $error->message);
        return NULL;
      }

      $result = $sql_result->result_array();
      return $result;
    }
}