
<div class="news-events">
  <div class="news-event-pan card-type" style="margin: 0;">
    <?php if( !empty($IMAGE)) { ?>
    <div class="coupon-item" style="margin-top: 0px;">
      <img src="<?= chooseURL($IMAGE) ?>">
    </div>
    <?php } ?>
    <div style="height: 15px"></div>
    <div class="coupon-item" style="margin-top: 0px;word-break: break-all;"><?= $DETAIL? $DETAIL: '' ?></div>
    
    <div style="height: 15px"></div>

    <?php include('preview_html.php'); ?>
    
  </div>
</div>

<div style="height: 50px"></div>

<script>
  function changeInputs() {
    $('body').css("background-color", "white");
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");

    $('.ne-title').css("color", "#BF0000");
    $('.ne-title').css("padding", "7px 5px 7px 5px");
    $('.news-event-pan').css("background-color", "#ffffff");
  }

  changeInputs();
</script>