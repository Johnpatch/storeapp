<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Mobile preview panel layout page">
    <title><?= lang('mobile_preview_page') ?></title>
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bootstrap-select-1.12.1/bootstrap-select.min.css') ?>">
    <link href="<?= base_url('assets/css/preview.css') ?>" rel="stylesheet">
    <!--<link href='https://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>-->
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="scrollbar" style="height:526px;overflow-x: hidden;">
      <header class="header-layout" style="display: inline-flex;align-items: center;justify-content: space-between;">
        <?php
        if($LAYOUT_TEMPLATE_NO != 1){
          echo "<div style='width: 40px;'><i class='fa fa-bars'></i></div>";
        }
        ?>
        <?php 
        if($LAYOUT_TEMPLATE_NO == 1){
          echo '<img src="'.chooseURL($HEADER_IMAGE).'" width="100%" height="70px"/>';
        }
        else if($LAYOUT_TEMPLATE_NO != 3 && $LAYOUT_TEMPLATE_NO != 5 && $LAYOUT_TEMPLATE_NO != 9 && $LAYOUT_TEMPLATE_NO != 11 && isset($HEADER_IMAGE) && !empty($HEADER_IMAGE)) { ?>
          <img src="<?= chooseURL($HEADER_IMAGE)?>" style="height: 60px; width: calc(100% - 40px);"/>
        <?php }else if($LAYOUT_TEMPLATE_NO == 11){
          echo '<img src="'.chooseURL($HEADER_IMAGE).'" style="height: 60px; width: calc(100% - 141px);"/>';
          echo '<select style="width: 102px;border:none;">';
          
          foreach($LANGUAGES as $key => $value){
            echo '<option>'.$value.'</option>';
          }
          echo '</select>';
        }else if($LAYOUT_TEMPLATE_NO == 3 || $LAYOUT_TEMPLATE_NO == 5){ 
          echo '<div style="margin-right: 15px;">';
          foreach($HEADER_MENU as $key => $value) { ?>
            <img src="<?= chooseURL($value['HEADER_IMAGE_ICON'])?>" style="width: 40px;height: 40px;border-radius: 20px;" />
        <?php } echo '</div>'; } else if($LAYOUT_TEMPLATE_NO == 9){ ?>
          <input type="text" style="margin-right: 10px;" /><i class="fa fa-search" style="position: absolute; right: 16px;"></i>
        <?php } else { ?>
          <div style="height: 70px;"></div>
        <?php } ?>
      </header>