<?php
  $total_cnt = count($FOOTERS)+1;
  $width = 100/$total_cnt  .'%';
?>
        <div class="cp-menu-bar">
          <div class="line"></div>
          <a class="cp-menu-item" style="width: <?php echo $width; ?>">
            <span class="glyphicon glyphicon-home"></span><br/>
            <?= lang('cpn') ?>
          </a>
          <?php
          foreach($FOOTERS as $key => $value){
            echo '<a class="cp-menu-item" style="width: '.$width.'">
              <img src="'.base_url($value['FOOTER_IMAGE_ICON']).'" style="width: 14px;height: 14px;"/><br/>
              '.$value['MENU_NAME'].'
            </a>';
          }
          ?>
        </div>
      </div>
    </div>
    
  </body>
</html>