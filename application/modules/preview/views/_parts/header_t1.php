<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Mobile preview panel layout page">
    <title><?= lang('mobile_preview_page') ?></title>
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/font-awesome/css/font-awesome.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bootstrap-select-1.12.1/bootstrap-select.min.css') ?>">
    <link href="<?= base_url('assets/css/preview.css') ?>" rel="stylesheet">
    <!--<link href='https://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>-->
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-color: #eeeeee">
    <div class="scrollbar" style="height:526px;">
      <header class="header-card">
        <div class="row">
          <div class="col-xs-2" style="padding-left: 5px">
            <a class="glyphicon glyphicon-chevron-left card-header-icon"></a>
          </div>
          <div class="col-xs-8 center" style="padding:0px;">
            <div class="card-header-title"><strong><?= $TITLE_BAR ?></strong></div>
          </div>
          <div class="col-xs-2 right"  style="padding-right: 5px">
            <a class="glyphicon glyphicon-pushpin card-header-icon"></a>
          </div>
        </div>
      </header>
      <div class="cp-body">