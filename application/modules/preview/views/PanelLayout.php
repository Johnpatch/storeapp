<?php if($LAYOUT_TEMPLATE_NO != 7 && $LAYOUT_TEMPLATE_NO != 9) {?>

<div style="display: inline-flex;width: 100%;background: white; position: fixed; top: 60px;z-index: 99;">
  <?php
    foreach($POST_CATEGORY as $key => $value){
      echo '<div style="padding: 10px 15px;cursor:pointer;" onclick="clickTab('.$value["ID"].')">'.$value['TITLE'].'</div>';
    }
  ?>
</div>

<?php 
}
if(!empty($SLIDE_SETTINGS)) { ?>
<div class="slider" <?php if($LAYOUT_TEMPLATE_NO != 5 && $LAYOUT_TEMPLATE_NO != 6 && $LAYOUT_TEMPLATE_NO != 'slide'){ echo 'style="margin-top: 100px;"'; } else { echo 'style="margin-top: 60px;"';} ?>>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <?php
      for($i = 0; $i < count($SLIDE_SETTINGS); $i++) {
        if($i == 0){
          echo '<li data-target="#myCarousel" data-slide-to="'.$i.'" class="active"></li>';
        } else {
          echo '<li data-target="#myCarousel" data-slide-to="'.$i.'"></li>'; 
        }
      }?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <?php for($i = 0; $i < count($SLIDE_SETTINGS); $i++) {?>
        <div class="item <?=$i == 0 ? 'active' : ''?>">
          <img src="<?= chooseURL($SLIDE_SETTINGS[$i]['IMAGE']) ?>" style="width:100%; height:180.5px">
        </div>
      <?php }?>
    </div>
  </div>
</div>
<?php } ?>

    <?php
    if($LAYOUT_TEMPLATE_NO == 1){
      echo '<div style="margin: 100px 0 50px 0;">';
      foreach($POSTS as $key => $value){
        
        if(!empty($value['IMAGE']))
          echo '<img src="'.base_url($value['IMAGE']).'" style="width: 100%;height: 72vw;" />';
        else
          echo '<img src="'.base_url('assets/imgs/noimage1.jpg').'" style="width: 100%;height: 72vw;" />';
        echo '<div style="padding: 20px;">
          <div>'.date('yy.m.d', $value['CREATE_TIME']).'</div>
          <div style="font-weight: bold;font-size: 18px;">'.$value['TITLE'].'</div>
          <div style="white-space: pre-wrap;overflow: hidden;text-overflow: ellipsis;width: 100%;height: 24px;">'.$value['DETAIL'].'</div>
        </div>';
      }
    }
    else if($LAYOUT_TEMPLATE_NO == 2){
      echo '<div style="margin: 100px 0 0px 0;">';
      foreach($POSTS as $key => $value){
        
        if(!empty($value['IMAGE']))
          echo '<img src="'.base_url($value['IMAGE']).'" style="width: 100%;height: 72vw;" />';
        else
          echo '<img src="'.base_url('assets/imgs/noimage1.jpg').'" style="width: 100%;height: 72vw;" />';
        echo '<div style="width: 100%;height: 3px;background-color: white;"></div>';
      }
    }else if($LAYOUT_TEMPLATE_NO == 3 || $LAYOUT_TEMPLATE_NO == 11){
        ?>
        <div class="slider" style="margin-top: 25px;background-color: #c1c1c1;">
          <div id="myCarousel1" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <?php
              for($i = 0; $i < count($POSTS); $i++) {
                if($i == 0){
                  echo '<li data-target="#myCarousel1" data-slide-to="'.$i.'" class="active"></li>';
                } else {
                  echo '<li data-target="#myCarousel1" data-slide-to="'.$i.'"></li>'; 
                }
              }?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <?php for($i = 0; $i < count($POSTS); $i++) {?>
                
                <div class="item <?=$i == 0 ? 'active' : ''?>" style="height: 75px;">
                  <div style="display: inline-flex;align-items: center;">
                    <?php if(!empty($POSTS[$i]['IMAGE'])) { ?>
                      <img src="<?= chooseURL($POSTS[$i]['IMAGE']) ?>" style="width:80px; height:75px">
                    <?php } else { ?>
                      <img src="<?= base_url('assets/imgs/noimage3.jpg') ?>" style="width:80px; height:75px">
                    <?php } ?>
                    <div>
                      <div style="padding-left: 10px;"><?php echo date('yy.m.d', $POSTS[$i]['CREATE_TIME']) ?></div>
                      <div style="padding-left: 10px;font-weight: 'bold';font-size: 18px;"><?php echo $POSTS[$i]['TITLE']; ?></div>
                      <div style="padding-left: 10px;text-overflow:ellipsis;white-space: pre-wrap; overflow: hidden;height: 20px;"><?php echo $POSTS[$i]['DETAIL']; ?></div>
                    </div>
                  </div>
                </div>
                
              <?php }?>
            </div>
          </div>
        </div>
    <?php }
    else if($LAYOUT_TEMPLATE_NO == 4 || $LAYOUT_TEMPLATE_NO == 7){
      if($LAYOUT_TEMPLATE_NO == 4)
        echo '<div class="menu" style="margin-top: 32px;margin-bottom: 50px;">';
      else if($LAYOUT_TEMPLATE_NO == 7)
        echo '<div class="menu" style="margin-top: 60px;margin-bottom: 50px;">';
      for($i = 0; $i <= count($POSTS) / 2; $i++) {
      ?>
      <div class="row" style="margin-right: 0px; margin-left: 0px;">
        <?php
          for($j = 0; $j < 2 && $i * 2 + $j < count($POSTS); $j++) {
        ?>
          <span class="menu-btn col-xs-6" style="padding: 10px 20px;height: auto;" role="button">
            <?php if(!empty($POSTS[$i * 2 + $j]["IMAGE"])) {?>
              <img class="top-menu-img" style="width: 100%;height: 65px;" src="<?= chooseURL($POSTS[$i * 2 + $j]["IMAGE"])?>"><br/>
            <?php } else { ?>
              <img class="top-menu-img" style="width: 100%;height: 65px;" src="<?= base_url('assets/imgs/noimage1.jpg')?>"><br/>
            <?php } ?>
            <div style="text-align: left; font-weight: 'bold';font-size: 18px;"><?php echo $POSTS[$i*2 + $j]['TITLE'] ?></div>
            <div style="text-align: left;text-overflow: ellipsis;overflow: hidden;height: 40px;"><?php echo $POSTS[$i*2 + $j]['DETAIL'] ?></div>
          </span>
        <?php
          }
        ?>
      </div>
      <?php }
      echo '</div>';
    } else if($LAYOUT_TEMPLATE_NO == 8){
      echo '<div class="menu" style="margin-top: 100px;">';
      echo '<div class="row" style="margin-right: 0px;margin-left: 0px;">';
       for($i = 0;$i< 4;$i++){ ?>
          <span class="menu-btn col-xs-3">
            <?php if(!empty($POSTS[$i]["IMAGE"])) {?>
              <img class="top-menu-img" style="width: 100%; padding: 2px;" src="<?= chooseURL($POSTS[$i]["IMAGE"])?>"><br/>
            <?php } else { ?>
              <img class="top-menu-img" style="width: 100%; padding: 2px;" src="<?= base_url('assets/imgs/noimage3.jpg');?>" />
            <?php } ?>
          </span>
    <?php }
    
      if(count($POSTS) > 4)
        echo '<div><img class="top-menu-img" style="width: 100%; padding: 2px;" src="'.chooseURL($POSTS[4]["IMAGE"]).'" /></div>';
      if(count($POSTS) > 5){
        echo '<div class="menu" style="margin-top: 10px;">';
        for($i = 5; $i < (count($POSTS)-5) / 2 + 5; $i++) {
          ?>
          <div class="row" style="margin-right: 0px; margin-left: 0px;">
            <?php
              for($j = 0; $j< 2 && $i + $j < count($POSTS); $j++) {
            ?>
              <span class="menu-btn col-xs-6" style="padding: 10px 20px;height: auto;" role="button">
                <?php if(!empty($POSTS[$i * 2 + $j]["IMAGE"])) {?>
                  <img class="top-menu-img" style="width: 100%;height: 65px;" src="<?= chooseURL($POSTS[$i+ $j]["IMAGE"])?>"><br/>
                <?php } else { ?>
                  <img class="top-menu-img" style="width: 100%;height: 65px;" src="<?= base_url('assets/imgs/noimage1.jpg')?>"><br/>
                <?php } ?>
                <div style="text-align: left; font-weight: 'bold';font-size: 18px;"><?php echo $POSTS[$i+ $j]['TITLE'] ?></div>
                <div style="text-align: left;text-overflow: ellipsis;overflow: hidden;height: 40px;"><?php echo $POSTS[$i + $j]['DETAIL'] ?></div>
              </span>
            <?php
              }
            ?>
          </div>
        <?php }
        echo '</div>';
      }
      echo '</div>';
    } else if($LAYOUT_TEMPLATE_NO == 10){
      echo "<div style='margin-top: 30px;margin-bottom: 50px;'>";
      for($i = 0;$i < 6;$i++){ ?>
        <div style="width: 32%;margin-top: 1px;display: inline-flex;">
          <?php if(!empty($POSTS[$i]['IMAGE'])){ ?>
            <img style="width: 100%;height: 60px;" src="<?= chooseURL($POSTS[$i]["IMAGE"])?>">
          <?php } else { ?>
            <img style="width: 100%;height: 60px;" src="<?= base_url('assets/imgs/noimage1.jpg')?>">
          <?php } ?>
        </div>
    <?php }
      
      
      for($i = 6;$i < count($POSTS);$i++){ ?>
        <div style="display: inline-flex;align-items: center;width: 100%;background: #ececec;">
          <?php if(!empty($POSTS[$i]['IMAGE'])) { ?>
            <img src="<?= chooseURL($POSTS[$i]['IMAGE']) ?>" style="width:80px; height:75px">
          <?php } else { ?>
            <img src="<?= base_url('assets/imgs/noimage3.jpg') ?>" style="width:80px; height:75px">
          <?php } ?>
          <div>
            <div style="padding-left: 10px;"><?php echo date('yy.m.d', $POSTS[$i]['CREATE_TIME']) ?></div>
            <div style="padding-left: 10px;font-weight: 'bold';font-size: 18px;"><?php echo $POSTS[$i]['TITLE']; ?></div>
            <div style="padding-left: 10px;text-overflow:ellipsis;white-space: pre-wrap; overflow: hidden;height: 20px;"><?php echo $POSTS[$i]['DETAIL']; ?></div>
          </div>
        </div>
    <?php  }
    echo "</div>";
    } else if($LAYOUT_TEMPLATE_NO == 9){ 
    ?>
    <div style="margin-top: 60px;margin-bottom: 50px;">
      <div class="slider" style="background-color: #c1c1c1;">
        <div id="myCarousel1" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <?php
            for($i = 0; $i < 3; $i++) {
              if($i == 0){
                echo '<li data-target="#myCarousel1" data-slide-to="'.$i.'" class="active"></li>';
              } else {
                echo '<li data-target="#myCarousel1" data-slide-to="'.$i.'"></li>'; 
              }
            }?>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <?php for($i = 0; $i < 3; $i++) {?>
              
              <div class="item <?=$i == 0 ? 'active' : ''?>" style="height: 210px;">
                <div style="text-align: center;">
                  <?php if(!empty($POSTS[$i]['IMAGE'])) { ?>
                    <img src="<?= chooseURL($POSTS[$i]['IMAGE']) ?>" style="width:100%; height:180px">
                  <?php } else { ?>
                    <img src="<?= base_url('assets/imgs/noimage1.jpg') ?>"  style="width:100%; height:180px">
                  <?php } ?>
                  <div>
                    <div style="padding-left: 10px;font-weight: 'bold';font-size: 18px;"><?php echo $POSTS[$i]['TITLE']; ?></div>
                  </div>
                </div>
              </div>
              
            <?php }?>
          </div>
        </div>
      </div>
      <?php
      if(count($POSTS) > 4)
        echo '<div style="margin-top: 30px;"><img class="top-menu-img" style="width: 100%; padding: 2px;" src="'.chooseURL($POSTS[4]["IMAGE"]).'" /></div>';
      if(count($POSTS) > 5){
        for($i = 5;$i < count($POSTS);$i++){ ?>
          <div style="width: 32%;margin-top: 1px;display: inline-flex;flex-direction: column;">
            <?php if(!empty($POSTS[$i]['IMAGE'])){ ?>
              <img style="width: 100%;height: 60px;" src="<?= chooseURL($POSTS[$i]["IMAGE"])?>">
            <?php } else { ?>
              <img style="width: 100%;height: 60px;" src="<?= base_url('assets/imgs/noimage1.jpg')?>">
            <?php } ?>
            <div style="font-weight: bold;font-size:18px;text-overflow:ellipsis; overflow: hidden"><?= $POSTS[$i]['TITLE'] ?></div>
          </div>
      <?php }
     }
     echo "</div>";
    }
    ?>



  <?php
  if($LAYOUT_TEMPLATE_NO == 5){
    echo '<div class="menu" style="margin-top: 32px;border-top: 1px solid #e0e0e0;margin-bottom: 50px;">';
    for($i = 0; $i <= count($TOP_MENU_SETTINGS) / 2; $i++) {
    ?>
    <div class="row" style="margin-right: 0px; margin-left: 0px;">
      <?php
        for($j = 0; $j < 2 && $i * 2 + $j < count($TOP_MENU_SETTINGS); $j++) {
      ?>
        <span class="menu-btn col-xs-6" style="border-bottom: 1px solid #e0e0e0; border-right: 1px solid #e0e0e0;"  data-pm-have="<?= $TOP_MENU_SETTINGS[$i * 2 + $j]['PRODUCT_MENU_YN']?>" data-id="<?= $TOP_MENU_SETTINGS[$i * 2 + $j]['ID']?>" role="button">
          <?php if(!empty($TOP_MENU_SETTINGS[$i * 2 + $j]["IMAGE_FOR_PANEL"])) {?>
            <img class="top-menu-img" src="<?= chooseURL($TOP_MENU_SETTINGS[$i * 2 + $j]["IMAGE_FOR_PANEL"])?>"><br/>
          <?php } else { ?>
            <div style="width: 100%;height: 100%; line-height: 63px;"><?= $TOP_MENU_SETTINGS[$i * 2 + $j]['MENU_NAME'] ?></div>
          <?php } ?>
        </span>
      <?php
        }
      ?>
    </div>
    <?php }
    echo '</div>'; }
  else if($LAYOUT_TEMPLATE_NO == 3 || $LAYOUT_TEMPLATE_NO == 6 || $LAYOUT_TEMPLATE_NO == 11){
    echo '<div class="menu" style="margin-top: 32px;border-top: 1px solid #e0e0e0;">';
    for($i = 0;$i<3;$i++){?>
      <div style="display: inline-flex;width: 100%;justify-content: space-between;background: #cacaca; padding: 10px;margin-top: 1px;">
        <div><?= $TOP_MENU_SETTINGS[$i]['MENU_NAME'] ?></div>
        <div><i class="fa fa-long-arrow-right"></i></div>
      </div>
    <?php }
    if($LAYOUT_TEMPLATE_NO == 3 || $LAYOUT_TEMPLATE_NO == 11){
      for($i = 3;$i < count($TOP_MENU_SETTINGS);$i++){ ?>
        <div style="width: 100%;margin-top: 1px;">
          <?php if(!empty($TOP_MENU_SETTINGS[$i]['IMAGE_FOR_PANEL'])){ ?>
            <img style="width: 100%;" src="<?= chooseURL($TOP_MENU_SETTINGS[$i]["IMAGE_FOR_PANEL"])?>">
          <?php } else { ?>
            <img style="width: 100%;" src="<?= base_url('assets/imgs/noimage1.jpg')?>">
          <?php } ?>
        </div>
  <?php }} else if($LAYOUT_TEMPLATE_NO == 6){ 
    for($i = 3;$i < count($TOP_MENU_SETTINGS);$i++){ ?>
      <div style="width: 32%;margin-top: 1px;display: inline-flex;">
        <?php if(!empty($TOP_MENU_SETTINGS[$i]['IMAGE_FOR_PANEL'])){ ?>
          <img style="width: 100%;height: 60px;" src="<?= chooseURL($TOP_MENU_SETTINGS[$i]["IMAGE_FOR_PANEL"])?>">
        <?php } else { ?>
          <img style="width: 100%;height: 60px;" src="<?= base_url('assets/imgs/noimage1.jpg')?>">
        <?php } ?>
      </div>

  <?php } }
    echo '</div>';
  } ?>

  <?php include('preview_html.php'); ?>



</div>
<form id="form-preview-link" style="display:none" method="POST" action="<?= site_url('preview/productmenu/list')?>">
  <input type="hidden" name="BRANCH_ID" value="<?= $BRANCH_ID?>">
  <input id="parent-menu-id" type="hidden" name="PARENT_MENU_ID">
</form>
<script>
  $('.menu-btn').click(function() {
    /*pm_yn = $(this).attr("data-pm-have");
    product_id = $(this).attr("data-id");
    
    if (pm_yn == 'Y') {
      $('#parent-menu-id').attr("value", product_id);
      $('#form-preview-link').submit();
    }*/
  })
</script>
<script>
  $('.scrollbar').slimScroll({
    
  });
</script>