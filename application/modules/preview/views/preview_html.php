<?php if (isset($type)): ?>
  <div class="list-item lim preview-html1 hidden">
    <?= $BASIC_SETTING->FIRST_HTML ;?>
  </div>

  <div class="list-item lim preview-html2 hidden">
    <?= $BASIC_SETTING->SECOND_HTML ;?>
  </div>

  <div class="list-item lim preview-html3 hidden">
    <?= $BASIC_SETTING->THIRD_HTML ;?>
  </div>
<?php endif; ?>

<script>
	var id = '<?=$type?>';
	$('.preview-html' + id).removeClass('hidden');
</script>