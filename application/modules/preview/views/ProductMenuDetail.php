<div class="product-detail">
  <div class="product-detail-pan">
    <?php if(!empty($MENU_TITLE)) { ?>
    <div class="product-detail-title">
      <h4><strong><?= $MENU_TITLE?></strong></h4>
    </div>
    <?php } ?>
    <div class="product-detail-body">
      <?= $DETAIL?>
    </div>

    <?php if(!empty($MENU_IMAGE)) { ?>
    <div class="product-detail-img">
      <img src="<?= chooseURL($MENU_IMAGE) ?>">
    </div>
    <?php } ?>
  </div>
</div>
<script>
  function changeInputs() {
    $('body').css("background-color", "<?= $PAGE_BACK_COLOR ?>");
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");

    $('.product-detail-pan').css("background-color", "<?= $BACK_COLOR ?>");
    $('.product-detail-pan').css("color", "<?= $FONT_COLOR ?>");
  }

  changeInputs();
</script>