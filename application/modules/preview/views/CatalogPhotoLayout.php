<div style="margin-top: 15px;"></div>
  <div class="row" style="margin-right: 0px; margin-left: 0px;">
  <?php
    for($i = 0; $i <= count($MEDIA_LINK) / 3; $i++) {
      for($j = 0; $j < 3 && $i * 3 + $j < count($MEDIA_LINK); $j++) {   ?>
        <div class="col-xs-4" style="padding: 0px;">
          <div class="catalog-info-img">
          <img style="width: 100%;" src="<?= chooseURL($MEDIA_LINK[$i * 3 + $j]) ?>">
          </div>
        </div>
      <?php
        }
    }
  ?>
  </div>
</div>
<script>
  function changeInputs() {
    // $('body').css("background-color", "#e0c49f");
    <?php if (isset($PAGE_BACK_IMAGE)):?>
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    <?php endif;?>
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");
  }

  changeInputs();
</script>