
<div class="center cp-dash pg-txt">
  <?php echo $TITLE;?>
</div>
<div class="cp-panel-stamp-stores">
  <div class="cp-pss-header center">
    <span><?= lang('stamp_acquired') ?></span>
  </div>
</div>
<div class="cp-stamp-pic">
  <div class="row stamp-row">
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
        <img class="stamp-img" src="<?php echo $STAMP_IMAGE_URL;?>">
      </div>
    </div>
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
  </div>
  <div class="row stamp-row">
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
  </div>
  <div class="row stamp-row">
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
    <div class="col-xs-3 stamp-item">
      <div class="stamp-pic center">
      </div>
    </div>
  </div>
</div>

<a class="cp-btn"><?=lang('btn_receive_stamp')?></a>

<div class="cp-member-pan pg-txt">
  <h4 class="center">会員ID</h4>
  <div class="cp-member-body center">
    <span>999</span>
  </div>
</div>

<div class="cp-about-stamp-pan">
  <div class="cp-about-header">
    <span><strong>スタンプ有効期限</strong></span>
  </div>
  <p class="cp-about-txt-body pg-txt"> 初回スタンプを獲得してから<?php echo $STAMP_CARD_VALID_DAYS;?>日間</p>
</div>

<div class="cp-about-stamp-pan">
  <div class="cp-about-header">
    <span><strong>スタンプについて</strong></span>
  </div>
  <p class="cp-about-txt-body pg-txt" style="white-space: pre-wrap;"><?php echo $DETAIL;?></p>
</div>

<div class="cp-about-stamp-pan">
  <div class="cp-about-header">
    <span><strong>特典について</strong></span>
  </div>
  <?php if($BONUS_IMAGE != "") { 
    if(strstr($BONUS_IMAGE, 'attachments/slide_images')){
  ?>
    <img class="cp-benefit-img" src="<?php echo base_url($BONUS_IMAGE); ?>">
  <?php } else { ?>
    <img class="cp-benefit-img" src="<?php echo $BONUS_IMAGE; ?>">
  <?php } } ?>
  <p class="cp-about-txt-body pg-txt" style="white-space: pre-wrap;"><?php echo $PRIVILEGE_DETAIL;?></p>
  
  <div class="line"></div>

  
  <p class="cp-about-txt-body pg-txt" style="white-space: pre-wrap;"><?php echo $TERMS_CONDITIONS ?></p>
  

  <!--div class="row" style="margin-top: 15px; padding-right: 0px; margin-right:0px;">
    <div class="col-xs-3">
      <span class="cp-about-txt-body pg-txt"></span>
    </div>
    <div class="col-xs-9" style="padding-right: 0px;">
      <span class="cp-about-txt-body pg-txt"><?php echo $REWARD_EXPIRATION_DATE;?> </span>
      <div class="line"></div>
    </div>
  </div>
  <div class="row" style="margin-bottom: 15px; padding-right: 0px; margin-right: 0px;">
    <div class="col-xs-3">
      <span class="cp-about-txt-body pg-txt"></span>
    </div>
    <div class="col-xs-9 pg-txt" style="padding-right: 0px;">
    </div>
  </div-->
</div>

<div style="height: 50px"></div>
<script>
  function changeInputs() {
    $('.cp-about-header').css("background-color", "<?= $TITLE_BAR_BACK_COLOR?>");
    $('.cp-about-header').css("color", "<?= $TITLE_BAR_FONT_COLOR?>");
    $('body').css("background-color", "<?= $PAGE_BACK_COLOR ?>");
    $('body').css("background-image", "url(<?= $PAGE_BACK_IMAGE ?>)");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");
    $('.pg-txt').css("color", "<?= $PAGE_FONT_COLOR?>");
    $('.cp-btn').css("background-color", "<?= $BUTTON_BACK_COLOR?>");
  }

  changeInputs();
</script>