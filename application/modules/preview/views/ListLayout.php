<div class="slider" style="margin-top: 60px;">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <?php
      for($i = 0; $i < count($SLIDE_SETTINGS); $i++) {
        if($i == 0){
          echo '<li data-target="#myCarousel" data-slide-to="'.$i.'" class="active"></li>';
        } else {
          echo '<li data-target="#myCarousel" data-slide-to="'.$i.'"></li>'; 
        }
      }?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <?php for($i = 0; $i < count($SLIDE_SETTINGS); $i++) {?>
        <div class="item <?=$i == 0 ? 'active' : ''?>">
          <img src="<?= chooseURL($SLIDE_SETTINGS[$i]['IMAGE']) ?>" style="width:100%; height:180.5px">
        </div>
      <?php }?>
    </div>

    <!-- Left and right controls -->
    <!--a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only"></span>
    </a-->
  </div>
</div>
<div class="list-group" style="margin-top: 20px;">
  <div class="list-item item-news">
    <div class="list-item-header">
      <span class="list-item-header-tag header-news"><?=lang('notice')?></span><span class="list-item-header-title"><strong><?=lang('obu_company_published_july')?></strong></span>
    </div>
    <div class="list-item-body">
      <?=lang("latest_announment_info_displayed")?>
    </div>
  </div>

  <!--div class="list-item lim item-coupon">
    <div class="list-item-header">
      <span class="list-item-header-tag header-coupon"><?=lang('COUPON')?></span><span class="list-item-header-title"><strong><?=lang('obu_company_published_july')?></strong></span>
    </div>
    <div class="list-item-body">
      <?=lang("latest_announment_info_displayed")?>
    </div>
  </div-->

  <?php
    for($i = 0; $i < count($TOP_MENU_SETTINGS); $i++) {
  ?>
    <?php
      if($TOP_MENU_SETTINGS[$i]['BUTTON_TYPE'] == 1){
        if(!empty($TOP_MENU_SETTINGS[$i]["IMAGE_FOR_LIST"])){
    ?>
      <a class="list-item lim" style="padding: 0;">
        <img class="list-item-only-img" style="width: 100%;height: 60px;" src="<?= chooseURL($TOP_MENU_SETTINGS[$i]["IMAGE_FOR_LIST"]) ?>">
      </a>
    <?php
      }else{ ?>
      <div class="list-item lim other-menu btn-menu">
        <span class="other-menu-title" data-pos="<?= $TOP_MENU_SETTINGS[$i]["TITLE_POSITION"] ?>"><strong><?= $TOP_MENU_SETTINGS[$i]["MENU_NAME"] ?></strong></span>
      </div>
    <?php }
      } else{
        if($TOP_MENU_SETTINGS[$i]['TITLE_DISP_YN'] == 'Y'){
          ?>
          <div class="list-item lim other-menu btn-menu" data-pm-have="<?= $TOP_MENU_SETTINGS[$i]['PRODUCT_MENU_YN'] ?>" data-id="<?= $TOP_MENU_SETTINGS[$i]['ID'] ?>" style="background:<?= $TOP_MENU_SETTINGS[$i]["BUTTON_BACK_COLOR"] ?>; color:<?= $TOP_MENU_SETTINGS[$i]["TITLE_TEXT_COLOR"] ?>;">
            <span class="other-menu-title" data-pos="<?= $TOP_MENU_SETTINGS[$i]["TITLE_POSITION"] ?>"><strong><?= $TOP_MENU_SETTINGS[$i]["MENU_NAME"] ?></strong></span>
          </div>
          <?php
        }
      }
    ?>
    
      <?php
        /*if($TOP_MENU_SETTINGS[$i]["BUTTON_TYPE"] == 1) {
      ?>
        <div class="list-item lim img-menu btn-menu" data-pm-have="<?= $TOP_MENU_SETTINGS[$i]['PRODUCT_MENU_YN'] ?>" data-id="<?= $TOP_MENU_SETTINGS[$i]['ID'] ?>">
          <img class="img-menu" src="<?= chooseURL($TOP_MENU_SETTINGS[$i]["IMAGE_FOR_LIST"]) ?>">
          <span class="other-menu-title" data-pos="<?= $TOP_MENU_SETTINGS[$i]["TITLE_POSITION"] ?>">
            <?php
              if($TOP_MENU_SETTINGS[$i]["TITLE_DISP_YN"] == 'Y') {
            ?>
                <strong><?= $TOP_MENU_SETTINGS[$i]["MENU_NAME"] ?></strong>
            <?php
              }
            ?>
          </span>
        </div>
      <?php
        } else if($TOP_MENU_SETTINGS[$i]["BUTTON_TYPE"] == 2) {
      ?>
        <div class="list-item lim other-menu btn-menu" data-pm-have="<?= $TOP_MENU_SETTINGS[$i]['PRODUCT_MENU_YN'] ?>" data-id="<?= $TOP_MENU_SETTINGS[$i]['ID'] ?>" style="background:<?= $TOP_MENU_SETTINGS[$i]["BUTTON_BACK_COLOR"] ?>; color:<?= $TOP_MENU_SETTINGS[$i]["BUTTON_TEXT_COLOR"] ?>;">
          <span class="other-menu-title" data-pos="<?= $TOP_MENU_SETTINGS[$i]["TITLE_POSITION"] ?>"><strong><?= $TOP_MENU_SETTINGS[$i]["MENU_NAME"] ?></strong></span>
        </div>
      <?php
        } */
      ?>

  <?php
    }
  ?>

  <?php include('preview_html.php'); ?>

</div>
<form id="form-preview-link" style="display:none" method="POST" action="<?= site_url('preview/productmenu/list')?>">
  <input type="hidden" name="BRANCH_ID" value="<?= $BRANCH_ID?>">
  <input id="parent-menu-id" type="hidden" name="PARENT_MENU_ID">
</form>
<script>
  function changeInput() {
    var isMargin = "<?= $IS_MARGIN_BETWEEN_MENUS ?>";
    
    if (isMargin === "Y") {
      $(".list-group").addClass("list-padding");
      $(".lim").addClass("list-item-margin");
    } else {
      $(".list-group").removeClass("list-padding");
      $(".lim").removeClass("list-item-margin");
    }

    $('.item-coupon').css('background-color', '<?= $COUPON_BACK_COLOR?>');
    $('.header-coupon').css('background-color', '<?= $COUPON_TITLE_COLOR?>');
    $('.header-coupon').css('color', '<?= $COUPON_FONT_COLOR?>');

    $('.item-news').css('background-color', '<?= $NEWS_BACK_COLOR?>');
    $('.header-news').css('background-color', '<?= $NEWS_TITLE_COLOR?>');
    $('.header-news').css('color', '<?= $NEWS_FONT_COLOR?>');

    $('.other-menu-title').each(function(index, item){
      var pos = $(item).attr("data-pos");
      if (pos == '1') {
        $(item).addClass("vertical-center");
      } else if (pos == '2') {
        $(item).addClass("vertical-top");
      } else if (pos == '3') {
        $(item).addClass("vertical-bottom");
      }
    })
  }

  $('.btn-menu').click(function() {
    pm_yn = $(this).attr("data-pm-have");
    product_id = $(this).attr("data-id");
    
    if (pm_yn == 'Y') {
      $('#parent-menu-id').attr("value", product_id);
      $('#form-preview-link').submit();
    }
  })

  changeInput();
</script>