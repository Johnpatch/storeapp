<div style="margin-top: 15px;"></div>
  <div class="row" style="margin-right: 0px; margin-left: 0px;">
  <?php
  for($i = 0; $i <= count($PDF) / 3; $i++) {
      for($j = 0; $j < 3 && $i * 3 + $j < count($PDF); $j++) {   ?>
      <div class="col-xs-4" style="padding: 0px;">
        <a href="<?= chooseURL($PDF[$i * 3 + $j]) ?>">
          <div class="catalog-info-img" style="padding: 0px;">
            <img style="width: 100%;" src="<?= chooseURL($THUMBNAIL[$i * 3 + $j]) ?>">
          </div>
        </a>
        
        
        <span class="help-block help-area" style="text-overflow:ellipsis;white-space: nowrap;overflow:hidden;text-align:center;"><?= $TITTLE[$i * 3 + $j] ?></span>
      </div>
    <?php
      }
  } ?>
  </div>
</div>
<script>
  function changeInputs() {
    // $('body').css("background-color", "#e0c49f");
    <?php if (isset($PAGE_BACK_IMAGE)):?>
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    <?php endif;?>
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");
  }

  changeInputs();
</script>