<div>
  <div style="margin-top: 15px;"></div>
  <div class="row" style="margin-right: 0px; margin-left: 0px;">
  <?php
  for($i = 0; $i <= count($MEDIA_LINK) / 3; $i++) {
  ?>
    <?php
      for($j = 0; $j < 3 && $i * 3 + $j < count($MEDIA_LINK); $j++) {
    ?>
      <div class="col-xs-4 photo-info-img ">
        <img src="<?= chooseURL($MEDIA_LINK[$i * 3 + $j]) ?>">
      </div>
    <?php
      }
    ?>
  <?php } ?>
  </div>
  <?php include('preview_html.php'); ?>
</div>
<script>
  function changeInputs() {
    $('body').css("background-color", "<?= $PAGE_BACK_COLOR ?>");
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");
  }

  changeInputs();
</script>