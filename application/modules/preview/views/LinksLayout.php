<div>
<?php
  if($LINKS[0]['TWITTER_ACTIVE_YN'] === 'Y'){ ?>
    <a class="links-item">
      <span class="links-data-item" data-type="1"></span>
    </a>
<?php }
  if($LINKS[0]['FACEBOOK_ACTIVE_YN'] === 'Y'){ ?>
    <a class="links-item">
      <span class="links-data-item" data-type="2"></span>
    </a>
<?php } ?>
<?php
  if($LINKS[0]['TYPE'] != 'N'){
    for($i = 0; $i < count($LINKS); $i++) { ?>
      <a class="links-item">
        <span class="links-data-item" data-type="<?= $LINKS[$i]['TYPE'] ?>" ></span>
      </a>
<?php }
  } ?>
</div>
<?php include('preview_html.php'); ?>
<script>
  function changeInputs() {
    $('body').css("background-color", "#0e4c61");
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");
    $('.pg-txt').css("color", "#addbd3");
    $('.links-item').css("color", "#addbd3");

    $('.links-data-item').each(function(index, item) {
      type = $(item).attr("data-type");
      link = $(item).attr("data-link");
      
      $(item).parent().attr("href", link);
      switch(type) {
        case '1': // Twitter
          $(item).addClass("icon-twitter");
          $(item).html("&nbsp;Twitter");
          break;
        case '2': // Facebook
          $(item).addClass("icon-facebook");
          $(item).html("&nbsp;Facebook");
          break;
        case '3': // blog
          $(item).addClass("icon-tumblr");
          $(item).html("&nbsp;ブログ");
          break;
        case '4': // Pc Homepage
          $(item).addClass("glyphicon");
          $(item).addClass("glyphicon-home");
          $(item).html("&nbsp;PCホームページ");
          break;
        case '5': // Smartphone page
          $(item).addClass("glyphicon");
          $(item).addClass("glyphicon-phone");
          $(item).html("&nbsp;スマートフォンのページ");
          break;
        case '6': // Hot pepper
          $(item).addClass("glyphicon");
          $(item).addClass("glyphicon-screenshot");
          $(item).html("&nbsp;ホットペッパー");
          break;
        case '7': // GourNavi
          $(item).addClass("icon-tumblr");
          $(item).html("&nbsp;ぐるなび");
          break;
        case '8': // Shopping
          $(item).addClass("glyphicon");
          $(item).addClass("glyphicon-shopping-cart");
          $(item).html("&nbsp;ショッピング");
          break;
        case '9': // Reservation
          $(item).addClass("icon-tumblr");
          $(item).html("&nbsp;予約");
          break;
        case '10': // Job information
          $(item).addClass("icon-tumblr");
          $(item).html("&nbsp;求人情報");
          break;
        default:
          break;
      }
    })
  }

  changeInputs();
</script>