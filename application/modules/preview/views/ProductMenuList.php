<div>
  <?php for($i = 0; $i < count($PRODUCT_MENU); $i++) {?>
    <div class="product-menu-item rows">
      <div class="col-xs-3 no-padding">
        <img class="pmi-img" src="<?= chooseURL($PRODUCT_MENU[$i]['THUMBNAIL_IMAGE']) ?>">
      </div>
      <div class="col-xs-7 product-menu-body no-padding">
        <div class="pmb-title"><strong><?= $PRODUCT_MENU[$i]['MENU_TITLE']?></strong></div>
        <div class="pmb-text"><?= $PRODUCT_MENU[$i]['DETAIL']?></div>
      </div>
      <div class="col-xs-2 product-menu-detail-icon no-padding">
        <a class="pmdi-link"><span class="icon-chevron-right" style="font-size:18px;"></span></a>
      </div>
    </div>
    <div class="pm-line"></div>
  <?php } ?>
</div>

<script>
  function changeInputs() {
    $('body').css("background-color", "<?= $PAGE_BACK_COLOR ?>");
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");

    $('.pmb-title').css("color", "<?= $PAGE_FONT_COLOR ?>");
    $('.pmb-text').css("color", "<?= $PAGE_FONT_COLOR ?>");
    $('.pmdi-link').css("color", "<?= $PAGE_FONT_COLOR ?>");

    $('.pmb-text').css("height", "15px");
    $('.pmb-text').css("overflow", "hidden");
  }

  changeInputs();
</script>