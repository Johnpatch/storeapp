<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Mobile preview panel layout page">
    <title>Mobile Preview Page</title>
    <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('assets/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/bootstrap-select-1.12.1/bootstrap-select.min.css') ?>">
    <link href="<?= base_url('assets/css/preview.css') ?>" rel="stylesheet">
    <!--<link href='https://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>-->
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background-color: black;">
    <div style="height: 526px;">
      <header class="header-card">
        <div class="row">
          <div class="col-xs-3" style="padding-left: 5px">
            <a class="glyphicon glyphicon-chevron-left card-header-icon"></a>
          </div>
          <div class="col-xs-6 center">
            <div class="card-header-title"><strong>会員ID</strong></div>
          </div>
          <div class="col-xs-3 right"  style="padding-right: 5px">
            <a class="glyphicon glyphicon-pushpin card-header-icon"></a>
          </div>
        </div>
      </header>
      <div class="card-body-pan">
        <div class="card-title center">
          <h4><strong><?=lang('o_b_u_company')?></strong></h4>
        </div>
        <div class="card-body">
          <h5>NAME:</h5>
          <h4><strong><?=lang('customer_name')?></strong></h4>
          <br/>
          <h5>Date of birth:</h5>
          <h4><strong>2002/09/24</strong></h4>
          <br/><br/><br/>
          <h5><?=lang('membership_id')?>:</h5>
          <h4><strong>999</strong></h4><br/>
        </div>
      </div>
    </div>
    <script>
      $('.card-body-pan').css("background-color", "<?= $BACK_COLOR ?>");
      $('.card-title').css("color", "<?= $FONT_COLOR ?>");
      $('.card-body').css("color", "<?= $FONT_COLOR ?>");
      $('body').css("background-color", "<?= $PAGE_BACK_COLOR ?>");
      $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
      $('body').css("background-repeat", "repeat-y");
      $('body').css("background-size", "100%");
    </script>
  </body>
</html>