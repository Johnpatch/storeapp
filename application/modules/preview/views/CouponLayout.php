<div class="coupon card-type">
  <div class="coupon-pan">
    <?php if( !empty($COUPON_TITLE)) { ?>
    <div class="coupon-title">
      <h4><?= $COUPON_TITLE ?></h4>
    </div>
    <?php } else {?>
    <div style="height: 5px"></div>
    <?php } ?>

    <?php if( !empty($DISCOUNT)) { ?>
    <div class="coupon-discount center">
      <p><?= $DISCOUNT ?></p>
    </div>
    <?php } ?>

    <?php if( !empty($COUPON_DETAIL)) { ?>
    <div class="coupon-item">
      <p style="white-space: pre-wrap;"><?= $COUPON_DETAIL ?></p>
    </div>
    <?php } ?>

    <?php if( !empty($COUPON_IMAGE)) { ?>
    <div class="coupon-item">
      <img src="<?= chooseURL($COUPON_IMAGE) ?>">
    </div>
    <?php } ?>

    <?php if(!empty($COUPON_END_DATE)) { ?>
    <div class="coupon-section">
      <p class="center">有効期限</p>
      <?php $date = date_create($COUPON_END_DATE); ?>
      <p class="center"><?php echo date_format($date, 'Y')."年".date_format($date, 'm')."月".date_format($date, 'd')."日".date_format($date, 'H')."時".date_format($date, 'i')."分"; ?>まで</p>
    </div>
    <?php } ?>
    <?php if($COUPON_NOTICE_CHK1 == "Y" || $COUPON_NOTICE_CHK2 == "Y" || $COUPON_NOTICE_CHK3 == "Y") { ?>
    <div class="coupon-item coupon-notice">
      <?php if($COUPON_NOTICE_CHK1 == "Y") {?>
      <p><?=lang('other_privilege_discount')?></p>
      <?php }?>
      <?php if($COUPON_NOTICE_CHK2 == "Y") {?>
      <p><?=lang('will_end_as_soon_gone')?></p>
      <?php }?>
      <?php if($COUPON_NOTICE_CHK3 == "Y") {?>
      <p><?=lang('efftive_once_per_person')?></p>
      <?php }?>
    </div>
    <?php } ?>

    <?php if($USE_SETTING_YN == "Y" || $COUNTDOWN_DISP_YN == "Y") {?>
    <div class="line"></div>
    <?php } ?>

    <?php if($COUNTDOWN_DISP_YN == "Y") {
      $now = date("Y-m-d H:i:s");
      $end_date = date("Y-m-d H:i:s", strtotime($COUPON_END_DATE));
      
      $diff = dateDifference($now, $end_date);
      $diff = str_replace(' years', '年', $diff);
      $diff = str_replace(' months', '月', $diff);
      $diff = str_replace(' days', '日', $diff);
      $diff = str_replace(' hours', '時間', $diff);
      $diff = str_replace(' minutes', '分', $diff);
    ?>
      <div class="coupon-countdown center">
        <h4><strong><?= $diff == ''? lang('Finished') : $diff?></strong></h4>
      </div>
    <?php } ?>

    <?php if($USE_SETTING_YN == "Y") {?>
      <a class="cp-btn"><?= lang('use') ?></a>
    <?php } ?>

    <div style="height: 5px"></div>
  </div>

  <?php include('preview_html.php'); ?>

</div>

<div style="height: 50px"></div>

<script>
  function changeInputs() {
    $('body').css("background-color", "<?= $PAGE_BACK_COLOR ?>");
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");

    $('.coupon-pan').css("background-color", "#ffffff");
    $('.coupon-pan').css("color", "#000000");

    $('.title-bar, .coupon-section').css("background-color", "#dddddd");
    $('.title-bar, .coupon-section').css("color", "#000000");

    $('.coupon-item').css("background-color", "#ffffff");
    $('.coupon-item').css("color", "#000000");
    $('.coupon-notice').css("color", "#505050");
    $('.coupon-countdown').css("color", "#E00000");

    $('.coupon-discount').css("background-color", "#ffffff");
    $('.coupon-discount').css("color", "#A60000");

    $('.cp-btn').css("background-color", "<?= $BUTTON_BACK_COLOR ?>");
  }

  changeInputs();
</script>