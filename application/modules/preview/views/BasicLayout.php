<div class="news-events">
  <?php foreach($TITLE as $key => $value){ ?>
    <div class="news-event-pan card-type" style="margin: 0;">
      <div style="height: 15px"></div>
      <?php if( !empty($IMAGE[$key])) { ?>
      <div class="coupon-item" style="margin-top: 0px;">
        <img src="<?= chooseURL($IMAGE[$key]) ?>">
      </div>
      <?php } ?>
      <div style="height: 15px"></div>
      <div class="coupon-item" style="margin-top: 0px;font-weight: 'bold'; font-size: 18px;"><?= $TITLE[$key]? $TITLE[$key]: '' ?></div>
      <div class="coupon-item" style="margin-top: 0px;white-space: pre-wrap;"><?= $COMMENT[$key]? $COMMENT[$key]: '' ?></div>
      <div style="height: 15px"></div>

      <?php include('preview_html.php'); ?>
      
    </div>
  <?php } ?>
</div>

<div style="height: 50px"></div>

<script>
  function changeInputs() {
    $('body').css("background-color", "white");
    //$('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");

    $('.ne-title').css("color", "#BF0000");
    $('.ne-title').css("padding", "7px 5px 7px 5px");
    $('.news-event-pan').css("background-color", "#ffffff");
  }

  changeInputs();
</script>