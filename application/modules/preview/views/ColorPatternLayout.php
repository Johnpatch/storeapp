
<div class="center cp-dash pg-txt">
  <span><?= lang('cps_dash_1')?></span><br/>
  <span>スタンプ4個でお会計1,000円引き！</span>
</div>
<div class="cp-panel-stamp-stores">
  <div class="cp-pss-header center">
    <span><?=lang('store_where_stamp_used')?></span>
  </div>
</div>
<div class="cp-stamp-pic">
  <?php
  for($i=0; $i<3; $i++) {
  ?>
    <div class="row stamp-row">
      <div class="col-xs-3 stamp-item">
        <div class="stamp-pic center">
          <img class="stamp-img" src="<?= base_url('assets/imgs/discount.png')?>">
        </div>
      </div>
      <div class="col-xs-3 stamp-item">
        <div class="stamp-pic center">
        </div>
      </div>
      <div class="col-xs-3 stamp-item">
        <div class="stamp-pic center">
        </div>
      </div>
      <div class="col-xs-3 stamp-item">
        <div class="stamp-pic center">
        </div>
      </div>
    </div>
  <?php } ?>
</div>

<a class="cp-btn"><?=lang('btn_receive_stamp')?></a>

<div class="cp-member-pan pg-txt">
  <h4 class="center"><?=lang('member_id')?></h4>
  <div class="cp-member-body center">
    <span>999</span>
  </div>
</div>

<div class="cp-about-stamp-pan">
  <div class="cp-about-header">
    <span><strong><?=lang('about_stamp')?></strong></span>
  </div>
  <p class="cp-about-txt-body pg-txt"><?= lang('cp_about_body')?></p>
</div>

<div class="cp-about-stamp-pan">
  <div class="cp-about-header">
    <span><strong><?=lang('about_benefits')?></strong></span>
  </div>
  <img class="cp-benefit-img" src="<?= base_url('assets/imgs/cp_benefits.png')?>">
  <p class="cp-about-txt-body pg-txt"><?= lang('cp_benefit_discount')?></p>
  
  <div class="line"></div>

  <!--div class="row" style="margin: 15px 0;">
    <div class="col-xs-5">
      <span class="cp-about-txt-body pg-txt"><?=lang('expiration')?></span>
    </div>
    <div class="col-xs-7" style="padding-right: 0px;">
      <span class="cp-about-txt-body pg-txt"><?=lang('full_days')?></span>
      <div class="line"></div>
    </div>
  </div-->
  <!--div class="row" style="margin-bottom: 15px; padding-right: 0px; margin-right: 0px;">
    <div class="col-xs-3">
      <span class="cp-about-txt-body pg-txt"><?=lang('date')?></span>
    </div>
    <div class="col-xs-9 pg-txt" style="padding-right: 0px;">
    </div>
  </div-->
</div>

<?php include('preview_html.php'); ?>

<div style="height: 50px"></div>
<script>
  function changeInputs() {
    $('.cp-about-header').css("background-color", "<?= $TITLE_BAR_BACK_COLOR ?>");
    $('.cp-about-header').css("color", "<?= $TITLE_BAR_FONT_COLOR ?>");
    $('body').css("background-color", "<?= $PAGE_BACK_COLOR ?>");
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");
    $('.pg-txt').css("color", "<?= $PAGE_FONT_COLOR ?>");
    $('.cp-btn').css("background-color", "<?= $BUTTON_BACK_COLOR ?>");
  }

  changeInputs();
</script>