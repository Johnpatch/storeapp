
  <div class="row" style="margin-right: 0px; margin-left: 0px;">
  <?php
    for($i = 0; $i <= count($CATALOG) / 3; $i++) {
  ?>
  
  <?php
      for($j = 0; $j < 3 && $i * 3 + $j < count($CATALOG); $j++) {   ?>
        <div class="col-xs-4" style="padding: 0px;">
          <div class=" catalog-info-img">
            <img src="<?= chooseURL($CATALOG[$i * 3 + $j][0]) ?>" style="width: 100%;height: 100%;">
            <p style="padding-top: 10px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><?= $CATALOG[$i * 3 + $j][1] ?></p>
          </div>
        </div>
      <?php
        }
    }
  ?>
  </div>

  <?php include('preview_html.php'); ?>
  
</div>
<script>
  function changeInputs() {
    // $('body').css("background-color", "#e0c49f");
    <?php if (isset($PAGE_BACK_IMAGE)):?>
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    <?php endif;?>
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");
  }

  changeInputs();
</script>