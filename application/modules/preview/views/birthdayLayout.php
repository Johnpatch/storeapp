<div class="coupon card-type">
  <div class="coupon-pan">
    <?php if($AUTO_PUSH_YN == 1){ ?>
      <?php if( !empty($TITLE)) { ?>
      <div class="coupon-title">
        <h4><?= $TITLE ?></h4>
      </div>
      <?php } else {?>
      <div style="height: 5px"></div>
      <?php } ?>

      <?php if( !empty($DISCOUNT_CONTENT)) { ?>
      <div class="coupon-discount center">
        <p><?= $DISCOUNT_CONTENT ?></p>
      </div>
      <?php } ?>

      <?php if( !empty($DETAIL)) { ?>
      <div class="coupon-item">
        <p style="color:#4DB6AC; white-space: pre-wrap;">&nbsp;<?= $DETAIL ?></p>
      </div>
      <?php } ?>

      <?php if( !empty($IMAGE)) { ?>
      <div class="coupon-item">
        <img src="<?= chooseURL($IMAGE) ?>">
      </div>
      <?php } ?>

      <div class="coupon-section">
        <p class="center"><?=lang('valid')?></p>
        <p class="center"><?=lang('from_dec_last')?></p>
        <p class="center"><?=lang('to_dec_last')?></p>
      </div>

      <?php if($NOTICE_1_YN == "Y" || $NOTICE_2_YN == "Y" || $NOTICE_3_YN == "Y") { ?>
      <div class="coupon-item coupon-notice">
        <?php if($NOTICE_1_YN == "Y") {?>
        <p style="color:#4DB6AC">&nbsp;<?=lang('preview_welcome_other_privilege')?></p>
        <?php }?>
        <?php if($NOTICE_2_YN == "Y") {?>
        <p style="color:#4DB6AC">&nbsp;<?=lang('will_end_as_soon_gone')?></p>
        <?php }?>
        <?php if($NOTICE_3_YN == "Y") {?>
        <p style="color:#4DB6AC">&nbsp;<?=lang('efftive_once_per_person')?></p>
        <?php }?>
      </div>
      <?php } ?>

      <?php if($USE_SETTING_YN == "Y") {?>
        <a class="cp-btn"><?=lang('use')?></a>
      <?php } ?>

      <div style="height: 5px"></div>
    <?php } ?>
  </div>

  <?php include('preview_html.php'); ?>
  
</div>

<div style="height: 50px"></div>

<script>
  function changeInputs() {
    $('body').css("background-color", "#0e4c61");
    <?php if (isset($PAGE_BACK_IMAGE)):?>
    $('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    <?php endif;?>
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");

    $('.coupon-pan').css("background-color", "#ffffff");
    $('.coupon-pan').css("color", "#000000");

    $('.title-bar, .coupon-section').css("background-color", "#dddddd");
    $('.title-bar, .coupon-section').css("color", "#000000");

    $('.coupon-item').css("background-color", "#ffffff");
    $('.coupon-item').css("color", "#000000");
    $('.coupon-notice').css("color", "#505050");
    $('.coupon-countdown').css("color", "#E00000");

    $('.coupon-discount').css("background-color", "#ffffff");
    $('.coupon-discount').css("color", "#A60000");

    $('.cp-btn').css("background-color", "#AD1457");
  }

  changeInputs();
</script>