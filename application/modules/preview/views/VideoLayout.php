<div>
  <div style="margin-top: 15px;"></div>
  <?php
  for($i = 0; $i < count($MEDIA_LINK); $i++) {
  ?>
  <div class="video-info-bg" style="text-align: center;">
    <div class="video-pan">
      <img src="<?= $MEDIA_LINK[$i]?>">
      <div class="overlay">
        <div class="overlay-warp">
          <a class="img-play">
            <span class="glyphicon glyphicon-play"></span>
          </a>
        </div>
      </div>
    </div>
    <div style="height: 10px;"></div>
    <span class="pg-txt" style="margin: 10px 0px;"><?= $COMMENT[$i]?></span>
  </div>
  <?php } ?>
</div>
<?php include('preview_html.php'); ?>
<script>
  function changeInputs() {
    //$('body').css("background-color", "<?= $PAGE_BACK_COLOR ?>");
    //$('body').css("background-image", "url(<?= chooseURL($PAGE_BACK_IMAGE) ?>)");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");
    $('.pg-txt').css("color", "<?= $PAGE_FONT_COLOR?>");
  }

  changeInputs();
</script>