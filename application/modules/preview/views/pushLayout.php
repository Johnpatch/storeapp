<div class="coupon card-type">
  <div class="coupon-pan" style="margin-top: 50px; padding-left: 5px; padding-right: 5px;">
    <?php if( !empty($LEFT_TITLE)) { ?>
    <div style="border-bottom: 1px solid #AAAAAA;">
      <div style="padding-top: 5px; padding-bottom: 5px;">
        <table>
          <tr>
            <td style="text-align: center; background: #777777; padding-top: 3px; padding-bottom: 3px; padding-right: 3px; padding-left: 3px; border-radius: 3px;"><span style="color:#FFFFFF; font-size: 12px;"><?= $LEFT_TITLE ?></span></td>
            <td style="text-align: center; padding-left: 5px;"><?= $CENTER_TITLE ?></td>
          </tr>
        </table>
      </div>
    </div>
    <?php } else {?>
    <div style="height: 5px"></div>
    <?php } ?>

    <?php if( !empty($DATE)) { ?>
    <div class="coupon-item">
      <p style="color:#888888;">&nbsp;<?= $DATE ?></p>
    </div>
    <?php } ?>

    <div style="border: 1px solid #DDDDDD;">
      <?php if( !empty($TITLE)) { ?>
        <p><h4 class="center" style="color:#EC407A;"><strong>&nbsp;<?= $TITLE ?></strong></h4></p>
      <?php } ?>
      <?php if( !empty($TEXT)) { ?>
        <div style="padding-left: 10px">
          <p><?= $TEXT ?></p>
        </div>
      <?php } ?>
    </div>

    <div style="border-top: 1px solid #AAAAAA; margin-top: 10px;">
      <table>
        <tr>
          <td width="50%"><a class="cp-btn" style="font-size: 12px;"><?=lang('close')?></a></td>
          <td width="50%"><a class="cp-btn" style="font-size: 12px;"><?=lang('find_out_more')?></a></td>
        </tr>
      </table>
    </div>

    <?php include('preview_html.php'); ?>
    
    <div style="height: 80px"></div>
  </div>
</div>


<script>
  function changeInputs() {
    $('body').css("background-color", "#0e4c61");
    $('body').css("background-repeat", "repeat-y");
    $('body').css("background-size", "100%");

    $('.coupon-pan').css("background-color", "#ffffff");
    $('.coupon-pan').css("color", "#000000");

    $('.title-bar, .coupon-section').css("background-color", "#dddddd");
    $('.title-bar, .coupon-section').css("color", "#000000");

    $('.coupon-item').css("background-color", "#ffffff");
    $('.coupon-item').css("color", "#000000");
    $('.coupon-notice').css("color", "#505050");
    $('.coupon-countdown').css("color", "#E00000");

    $('.coupon-discount').css("background-color", "#ffffff");
    $('.coupon-discount').css("color", "#A60000");

    $('.cp-btn').css("background-color", "#00BCD4");
  }

  changeInputs();
</script>