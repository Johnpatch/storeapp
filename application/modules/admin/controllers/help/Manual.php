<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Manual extends ADMIN_Controller
{
    protected $table='manual';
    protected $route_prefix = 'admin/help/manual/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
    protected $publish_has_preview = false;
    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize()
    {

        $this->fields = [
            [
                "label" => lang("manual"),//lang('Manual'),
                "name" => "URL",
                "type" => "pdf",
                "required"=>true,
                "rules"=>"required"
            ],
        ];

        $this->page_title = lang('help');//"Edit basic information";
        $this->page_subtitle = lang("manual");//"";
        $this->panel_title = lang("manual");//"catalog information";

        $this->form_description = lang('design_cataloginformation_form_description');
        //Success settings
        $this->success_return_text = lang('design_cataloginformation_success_return_text');// "Return";

        $this->head['title'] = lang("app_creation_manual");
        $this->head['bread_title'] = lang('help');
        $this->head["description"] = "";
    }


}
