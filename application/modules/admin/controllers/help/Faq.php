<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Faq extends ADMIN_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->login_check();
    }

    public function index($page = 0)
    {
        
        $data = array();
        $head = array();
        $head = $this->head;
        $head['description'] = '';
        $head['keywords'] = '';
        $head['title'] = lang("basic_information");
        $data['bread_title'] = lang("basic_information");
        $data['page_title'] = lang('basic_information');
        $data['page_subtitle'] = lang('FAQ');
        $data['panel_title'] = $this->panel_title;
        $head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $head);
        $this->load->view('help/faq', $data);
        $this->load->view('_parts/footer');
    }
    
    protected function initialize() {
        $this->page_title = lang('Help');//"Basic information";
        $this->panel_title = lang('home');//"";
    
        //Success settings
        $this->success_return_text = lang('home');//"Return";
        //language setting ended

        $this->head['title'] = lang("frequently_asked_quetions");
        $this->head["description"] = "";
    }

}
