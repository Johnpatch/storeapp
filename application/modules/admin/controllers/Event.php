<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Event extends ADMIN_Controller
{
    protected $table = "events";
    protected $route_prefix = 'admin/event/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $has_order = false;
    protected $has_add = true;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $publish_script_url = "event/layout";
    protected $confirm_script_url = "event/confirm";
    protected $index_has_preview = false;
    protected $edit_title = '';
    /*
         * Input Fields Description
         * name: input name----Must be exactly equal to corresponding DB column name
         * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
         * default: default value if initial value is not set
         * label: input label description
         * help: help block description
         * required: whether this field is requried or not
         * showIf: array(key=>value). shows this element when (key=value)
         *          value can take array values.
         * rules: validation settings
         */
    protected $fields;
    protected $preview_url;

    protected $search_fields=[];
    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    
    //Publish settings
    public $form_description= "";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        // $this->load->model('Event_model', 'model');
        $this->form_description = '';
        $this->head['title'] = lang("post_content");
        $this->head["description"] = "";
        $this->page_title = lang("post_content");
        $this->page_subtitle = lang("post_list");
        $this->panel_title = lang("post_list");
        $this->head['bread_title'] = lang("post_content");
        $this->head['page_subtitle'] = lang("post_list");
        $this->edit_title = lang("post_list");

        $events = $this->db
            ->select("ID, TITLE")
            ->where("BRANCH_ID", $this->auth['branch_id'])
            ->get("post_category")
            ->result_array();
        $parent_options = array();
        $search_list = array();
        $search_list[''] = lang('no_select');
        foreach ($events as $event) {
            $search_list[$event["ID"]] = $event["TITLE"];
            $parent_options[$event["ID"]] = $event["TITLE"];
        }
        
        $this->search_fields = [
            [
                "name"=>'TITLE',
                "type"=>'text',
                "label"=>lang("title")
            ],
            [
                "name"=>"TYPE",
                "type"=>"dropdown",
                "label"=>lang("category"),
                "default" => '',
                "options" => $search_list,
            ],
            [
                "name"=>'USE_SETTING_YN',
                "type"=>'checkbox',
                "checked"=>'Y',
                "unchecked"=>'N',
                "default" => 'Y',
                "text"=>lang('publishing_settings'),
                "label"=>lang('publishing_settings')
            ]
        ]; //used only when this controller has index page.(i.e $this->has_index_page = true)


        $this->fields = [
            [
                "group"=>true,
                "label"=>lang("post_content_start_end_date"),
                "between"=>"<span class='between'>~</span>",
                "input"=>[
                    [
                        "name" => "START_DATE",
                        "type" => "date",
                        "picker_type"=>"from",
                        "default"=>date("Y-m-d")
                    ],
                    [
                        "name" => "END_DATE",
                        "type" => "date",
                        "picker_type"=>"to",
                        "default" => date("Y-m-d")
                    ]
                ]
            ],
            [
                "name"=>"TYPE",
                "type"=>"dropdown",
                "label"=>lang("post_content_category"),
                "options" => $parent_options,
            ],
            [
                "name"=>'TITLE',
                "type"=>'text',
                "label"=>lang("title"),
                "required"=>true,
                "rules"=>"required",
            ],
            [
                "label"=>lang("text_body"),
                "name"=>"DETAIL",
                "type"=>"ckeditor",
                "help"=>lang("event_body_help")
            ],
            [
                "label"=>lang("image_jpeg"),
                "name"=>"IMAGE",
                "type"=>"file",
                "help"=>lang("event_image_help")
            ],
            [
                "label"=>lang("publishing_settings"),
                "name"=>"USE_SETTING_YN",
                "type"=>"radio",
                "default"=>"Y",
                "options"=>[
                    "Y"=>lang("public"),
                    "N"=>lang("non_public"),
                ]
            ],
        ];

        $this->table_fields = [
            [
                "label"=>lang("fix"),
                "type"=>"edit",
                "width" => 150
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete",
                "width" => 150
            ],
            [
                "label"=>lang("title"),
                "type"=>"text",
                "name"=>"TITLE"
            ],
            [
                "name"=>"TYPE",
                "type"=>"dropdown",
                "label"=>lang("category"),
                "options" => $parent_options,
            ],
            [
                "group"=>true,
                "label"=>lang("post_content_start_end_date"),
                "between"=>"~",
                "input"=>[
                    [
                        "type"=>"date",
                        "name"=>"START_DATE"
                    ],
                    [
                        "type"=>"date",
                        "name"=>"END_DATE"
                    ],
                ]
            ],
        ];
        $this->preview_url = site_url("preview/newsevents/");
    }

    protected function build_query($params) {
        $this->db->where('branch_id', $this->auth['branch_id']);
        if (element("USE_SETTING_YN", $params) == "Y") {
            $this->db->where($this->table.'.USE_SETTING_YN', 'Y');
        }
        if (element("USE_SETTING_YN", $params) == "N") {
            $this->db->where($this->table.'.USE_SETTING_YN', 'N');
        }
        $category = element("TYPE", $params);
        
        if(!empty($category)){
            $this->db->where($this->table.'.TYPE', $category);
        }

        $title = element("TITLE", $params);
        if ($title != NULL) {
            $this->db->like($this->table.'.TITLE', $title);
        }

        $order = element("ORDER", $params);
        $order_str = "";
        if($order != NULL){
            if($order == 'ASC'){
                $order_str = "ORDER ASC";
            } else {
                $order_str = "ORDER DESC";
            }
        }

        if ($order_str != "") $this->db->order_by($order_str);
    }
}
