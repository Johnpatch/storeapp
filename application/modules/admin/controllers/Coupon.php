<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Coupon extends ADMIN_Controller
{
    protected $table = "coupon";
    protected $route_prefix = 'admin/coupon/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $has_order = true;
    protected $has_add = true;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $index_has_preview = false;
    protected $edit_title = "";
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $fields;
    protected $preview_url;

    protected $search_fields = [];
    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */


    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->load->model('Coupon_model', 'model');
        $this->head['title'] = lang("list_reg");
        $this->head["description"] = "";
        $this->page_title = lang("coupon");
        $this->page_subtitle = lang("list_reg");
        $this->panel_title = lang("list_reg");
        $this->form_description=lang("coupon_description");
        $this->head['bread_title'] = lang("coupon");
        $this->head['page_subtitle'] = lang("coupon_setting_edit");
        $this->edit_title = lang("list_reg");
        $this->search_fields = [
        [
            "group" => true,
            "label" => lang("expiration_date"),
            "between"=>"<span class='between'>~</span>",
            "input" => [
                [
                    "name" => "FROM",
                    "type" => "date",
                    "class" => "search-start-date",
                    "picker_type"=>"from"
                ],
                [
                    "name" => "TO",
                    "type" => "date",
                    "class" => "search-end-date",
                    "picker_type"=>"to"
                ]
            ]
        ],
        [
            "name"=>'COUPON_TITLE',
            "type"=>'text',
            "label"=>lang("title")
        ],
        [
            "name"=>'STATUS_SHOW',
            "checked"=>'Y',
            "type"=>'checkbox',
            "unchecked"=>'N',
            "default"=>'Y',
            "text"=>lang('only_when_publishing'),
            "label"=>lang('publishing_settings')
        ]
    ]; //used only when this controller has index page.(i.e $this->has_index_page = true)


        $this->fields = [
            [
                "group"=>true,
                "label"=>lang("coupon_expiration_date"),
                "between"=>"<span class='between'>~</span>",
                "input"=>[
                    [
                        "name" => "COUPON_START_DATE",
                        "type" => "datetime",
                        "picker_type"=>"from",
                        "default"=>date("Y-m-d H:i")
                    ],
                    [
                        "name" => "COUPON_END_DATE",
                        "type" => "datetime",
                        "picker_type"=>"to",
                        "default" => date("Y-m-d")." 23:59"
                    ]
                ]
            ],
            [
                "label"=>lang("coupon_title"),
                "name"=>"COUPON_TITLE",
                "type"=>"text",
                "help"=>lang("coupon_titile_help")
            ],
            [
                "label"=>lang("coupon_details"),
                "name"=>"COUPON_DETAIL",
                "type"=>"textarea",
                "help"=>lang("coupon_details_help")
            ],
            [
                "label"=>lang("coupon_image_type"),
                "name"=>"COUPON_IMAGE",
                "type"=>"file",
                "help"=>lang("coupon_image_help")
            ],
            [
                "label"=>lang("discount_content"),
                "name"=>"DISCOUNT",
                "type"=>"text",
                "help"=>lang("coupon_discount_help")
            ],
            [
                "group"=>true,
                "label"=>lang("coupon_notice"),
                "input"=>[
                    [
                        "type"=>"checkbox",
                        "text"=>lang("other_privilege_discount"),
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"N",
                        "name"=>"COUPON_NOTICE_CHK1"
                    ],
                    [
                        "type"=>"checkbox",
                        "text"=>lang("will_end_as_soon_as_it"),
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"N",
                        "name"=>"COUPON_NOTICE_CHK2"
                    ],
                    [
                        "type"=>"checkbox",
                        "text"=>lang("effective_once_per_person"),
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"N",
                        "name"=>"COUPON_NOTICE_CHK3"
                    ],
                ]
            ],
            [
                "label"=>lang("single_use_setting"),
                "name"=>"USE_SETTING_YN",
                "type"=>"radio",
                "default"=>"N",
                "options"=>[
                    "Y"=>lang("effectiveness"),
                    "N"=>lang("invalid"),
                ],
                "help"=>lang("coupon_single_help")
            ],
            [
                "label"=>lang("countdown_display_setting"),
                "name"=>"COUNTDOWN_DISP_YN",
                "type"=>"radio",
                "default"=>"N",
                "options"=>[
                    "Y"=>lang("effectiveness"),
                    "N"=>lang("invalid"),
                ],
                "help"=>lang("coupon_countdown_help")
            ],
            [
                "label"=>lang("publishing_settings"),
                "name"=>"STATUS_SHOW",
                "type"=>"radio",
                "default"=>"Y",
                "options"=>[
                    "Y"=>lang("indicate"),
                    "N"=>lang("hidden"),
                ],
                "help"=>lang("coupon_status_help")
            ],
            [
                "label"=>lang("coupon_display_order"),
                "name"=>"ORDER",
                "type"=>"text",
                "required"=>true,
                "rules"=>"required",
                "default"=>0,
                "coupon_display_order_help"=>"*The smaller number is displayed first.<br>(Example:0->1->2->3->4)"
            ],
            [
                "label"=>lang("for_questionnaire"),
                "name"=>"REWARD_SURVEY_YN",
                "type"=>"checkbox",
                "text"=>lang('indicate'),
                "default"=>"N",
                "checked"=>"Y",
                "unchecked"=>"N",
                "help"=>lang("coupon_questionaire_help"),
                "show_text" => lang('indicate'),
                "hide_text" => lang('hidden')
            ]
        ];

        $this->table_fields = [
            [
                "label"=>lang('fix'),
                "type"=>"edit",
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete"
            ],
            [
                "group"=>true,
                "label"=>lang("expiration_date"),
                "between"=>"~",
                "input"=>[
                    [
                        "type"=>"datetime",
                        "name"=>"COUPON_START_DATE"
                    ],
                    [
                        "type"=>"datetime",
                        "name"=>"COUPON_END_DATE"
                    ],
                ]
            ],

            [
                "label"=>lang("title"),
                "type"=>"text",
                "name"=>"COUPON_TITLE"
            ],
            [
                "label" => lang("status"),
                "type" => "radio",
                "name" => "HOLDING_STATUS",
                "options"=>[
                    1=>lang("end"),
                    2=>lang("waiting_for_release"),
                    3=>lang("now_open"),
                ],
                "class"=>[
                    '1'=>"status-inactive",
                    '2'=>"",
                    '3'=>"status-active",
                ]
            ],
            [
                "name" => "ORDER",
                "type" => "order",
                "label" => lang("admin_staff_index_order"),
                "sortable"=>true
            ],
        ];
        $this->preview_url = site_url("preview/coupon");
    }

    protected function build_query($params) {
        $this->db->where('branch_id', $this->auth['branch_id']);
        $datetime = date("Y-m-d H:i:s");
        $this->db->select("*, (('$datetime' >= COUPON_START_DATE) + ('$datetime' <= COUPON_END_DATE) * 2) as HOLDING_STATUS");
        if (element("STATUS_SHOW", $params) == "Y") {
            $this->db->where('STATUS_SHOW', 'Y');
        }
        if (element("STATUS_SHOW", $params) == "N") {
            $this->db->where('STATUS_SHOW', 'N');
        }

        $title = element("COUPON_TITLE", $params);
        if ($title != NULL) {
            $this->db->like('COUPON_TITLE', $title);
        }
        
        $from = element("FROM", $params);
        $to = element("TO", $params);
        if ($from != NULL) {
            $this->db->where("DATE(COUPON_START_DATE) >=", $from);
            $this->db->where("DATE(COUPON_END_DATE) >=", $from);
        }
        if($to != NULL){
            $this->db->where("DATE(COUPON_START_DATE) <=", $to);
            $this->db->where("DATE(COUPON_END_DATE) <=", $to);
        }

        $order = element("ORDER", $params);
        if($order != NULL){
            if($order == 'ASC'){
                $this->db->order_by("ORDER ASC");
            } elseif ($order == 'DESC') {
                $this->db->order_by("ORDER DESC");
            }
        }
    }
}
