<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Fileinformation extends ADMIN_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('phpqrcode/qrlib');
        $this->load->helper('url');
    }

    public function index($page = 0)
    {
        $this->login_check();
        $data = array();
        $head = array();
        $head['description'] = '';
        $head['keywords'] = '';
        $head['title'] = lang('application_file_for_information');

	    $head = $this->head;
        $data['page_title'] = $this->page_title;
        $data['page_subtitle'] = $this->page_subtitle;
        $data['panel_title'] = $this->panel_title;
        $data['bread_title'] = lang('management');
        $data['form_description'] = $this->form_description;
        $this->db->where('BRANCH_ID', $this->auth['branch_id']);
        //$this->db->where('STATUS', 2);
        $result = $this->db->get('applications')->result_array();
        $qr_code = array();
        if(count($result) > 0){
			foreach($result as $key => $value){
                if($value['STATUS'] == 2){
                    $text = $value['URL'];
                    $SERVERFILEPATH = $_SERVER['DOCUMENT_ROOT'].'/qrcode';
                    $file_name = $SERVERFILEPATH."/".$this->auth['branch_id']."-".$value['DEVICE_TYPE']."-QRCode".".png";
                    QRcode::png($text,$file_name);
                    if($value['DEVICE_TYPE'] == 1){
                        $qr_code['android']['qrcode'] = $this->auth['branch_id']."-".$value['DEVICE_TYPE']."-QRCode".".png";
                        $qr_code['android']['app_url'] = $value['URL'];	
                    }else{
                        //$qr_code['ios']['qrcode'] = $this->auth['branch_id']."-".$value['DEVICE_TYPE']."-QRCode".".png";
                        //$qr_code['ios']['app_url'] = $value['URL'];
                        $qr_code['ios']['STATUS'] = $value['STATUS'];
                        $qr_code['ios']['branch_id'] = $this->auth['branch_id'];
                    }
                }else if($value['STATUS'] == 3){
                    if($value['DEVICE_TYPE'] == 1)
                        $qr_code['android']['STATUS'] = 'fail';
                    else
                        $qr_code['ios']['STATUS'] = 'fail';
                }
			}
		}
        $data['qr_code'] = $qr_code;
        $head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $head);
        $this->load->view('manage/fileinformation', $data);
        $this->load->view('_parts/footer');
    }
    
    protected function initialize() {
        $this->page_title = lang("management");
        $this->panel_title = lang("application_file_for_information");
        $this->page_subtitle = lang("application_file_for_information");
        $this->success_return_text = "Return";
        $this->form_description = "GooglePlay申請用APKファイルダウンロード<br>
        GooglePlayへの申請を行う際に下記よりAPKファイルをダウンロードしてご利用ください。<br>
        AppStore申請用ReactNativeファイルダウンロード<br>AppStoreへの申請を行う際に下記よりReactNativeファイルをダウンロードしてご利用ください<br>
        <span class='red'>※ストア申請ご依頼以降にご利用いただけます。</span>";
    }

}
