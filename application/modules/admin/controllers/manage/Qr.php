<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Qr extends ADMIN_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index($page = 0)
    {
        $this->login_check();
        $data = array();
        $this->head['description'] = '';
        $this->head['keywords'] = '';
        $data['page_title'] = $this->page_title;
        $data['page_subtitle'] = $this->page_subtitle;
        $data['panel_title'] = $this->panel_title;
        $data['bread_title'] = lang('management');
        $data['form_description'] = $this->form_description;
	    $head = $this->head;
        $head['title'] = lang('qr_code');
        $head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $head);
        $this->load->view('manage/qr', $data);
        $this->load->view('_parts/footer');
    }
    
    protected function initialize() {
        $this->page_title = lang("management");//lang('management_qrcode');
        $this->panel_title = lang("qr_code"); //lang('management');
        $this->page_subtitle = lang("qr_code");//lang('Qr code');
        $this->success_return_text = "Return"; //lang('home');
        $this->form_description = "GooglePlayまたはAppstoreのアプリ紹介インストールページへ遷移先リンクです。<br>
                                   <span class='red'>※ストア公開後からご利用いただけます。</span>";
    }

}
