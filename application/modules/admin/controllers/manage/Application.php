<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Application extends ADMIN_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index($page = 0)
    {
        $this->login_check();
        $data = array();
        $head = array();
	    $head = $this->head;
        $head['title'] = '';
        $head['description'] = '';
        $head['title'] = lang('application');
        $head['keywords'] = '';
        $data['page_title'] = $this->page_title;
        $data['page_subtitle'] = $this->page_subtitle;
        $data['panel_title'] = $this->panel_title;
        $data['bread_title'] = lang('management');
        $data['form_description'] = $this->form_description;
        
        $head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $data['android'] = $this->db->where('BRANCH_ID', $this->auth['branch_id'])->where('DEVICE_TYPE', 1)->get('applications')->result_array();
        $data['ios'] = $this->db->where('BRANCH_ID', $this->auth['branch_id'])->where('DEVICE_TYPE', 2)->get('applications')->result_array();
        $this->load->view('_parts/header', $head);
        $this->load->view('manage/application', $data);
        $this->load->view('_parts/footer');
    }
    
    protected function initialize() {
        $this->page_title = lang("management");
        $this->panel_title = lang("application");
        $this->page_subtitle = lang("application");
        $this->success_return_text = lang("return");
        $this->form_description = "申請からストア登録用のQR等の発行には時間がかかります。";
    }

    function ddo_upload($type, $filename){
        $config = [];
        $config['upload_path'] = './ipa/';
        $config['allowed_types'] = '*';
        if($type == 1){
            $config['file_name'] = $this->auth['branch_id']."_p12".".p12";
        }if($type == 2){
            $config['file_name'] = $this->auth['branch_id']."_provision".".mobileprovision";
        }
        if($type == 3){
            $config['file_name'] = $this->auth['branch_id']."_push".".p8";
        }
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload($filename)) {
            $error = array('error' => $this->upload->display_errors());
            return $config;
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $config;
        }
    }

    public function request_app(){
        $input = $this->input->post();
        /*if($input['type'] == 2 && $input['subtype'] == 1){
            if(isset($_FILES['p12_file']) && $_FILES['p12_file']['name'] != ''){
                $file1 = $this->ddo_upload(1, 'p12_file');
            }
        }
        if($input['type'] == 2 && $input['subtype'] == 2){
            if(isset($_FILES['provision_file']) && $_FILES['provision_file']['name'] != ''){
                $file2 = $this->ddo_upload(2, 'provision_file');
            }
        }
        if($input['type'] == 2 && $input['subtype'] == 3){
            if(isset($_FILES['provision_file']) && $_FILES['provision_file']['name'] != ''){
                $file2 = $this->ddo_upload(3, 'push_key_file');
            }
        }*/
       

        $result = $this->db->where('BRANCH_ID', $this->auth['branch_id'])->where('DEVICE_TYPE', $input['type'])->get('applications')->row_array();
        
        if(count($result) > 0){
            $data = array();
            if($input['type'] == 2){
                $data['BUNDLE_ID'] = $input['bundle_id'];
                $data['APPLE_ID'] = $input['apple_id'];
                $data['APPLE_PASSWORD'] = $input['apple_password'];
                $data['APP_NAME'] = $input['app_name'];
                //$data['CONTACT'] = $input['contact'];
                /*$data['P12_PASSWORD'] = $input['p12_password'];
                $data['PUSH_KEY'] = $input['push_key'];
                $data['TEAM_ID'] = $input['team_id'];
                $data['P12'] = $this->auth['branch_id']."_p12".".p12";
                $data['PROVISION'] = $this->auth['branch_id']."_provision".".mobileprovision";
                $data['PUSH'] = $this->auth['branch_id']."_push".".p8";*/
            }
            if($input['type'] == 1){
                $data['APP_NAME'] = $input['app_name'];
                $data['BUNDLE_ID'] = $input['bundleId'];
            }
            //if($result['FA_STATUS'] == 4)
                $data['STATUS'] = 1;
            //else
            //    $data['STATUS'] = 0;
            $this->db->set($data);
            $this->db->where('BRANCH_ID', $this->auth['branch_id']);
            $this->db->where('DEVICE_TYPE', $input['type']);
            $this->db->update('applications');
        }else{
            $insert_dat = array(
                'DEVICE_TYPE' => $input['type'],
                'BRANCH_ID' => $this->auth['branch_id']
            );
            if($input['type'] == 2){
                $insert_dat['BUNDLE_ID'] = $input['bundle_id'];
                $insert_dat['APPLE_ID'] = $input['apple_id'];
                $insert_dat['APPLE_PASSWORD'] = $input['apple_password'];
                $insert_dat['APP_NAME'] = $input['app_name'];
                //$insert_dat['CONTACT'] = $input['contact'];
                /*$insert_dat['P12_PASSWORD'] = $input['p12_password'];
                $insert_dat['PUSH_KEY'] = $input['push_key'];
                $insert_dat['P12'] = $this->auth['branch_id']."_p12".".p12";
                $insert_dat['PROVISION'] = $this->auth['branch_id']."_provision".".mobileprovision";
                $insert_dat['PUSH'] = $this->auth['branch_id']."_push".".p8";
                $insert_dat['TEAM_ID'] = $input['team_id'];*/
                
            }
            if($input['type'] == 1){
                $insert_dat['APP_NAME'] = $input['app_name'];
                $insert_dat['BUNDLE_ID'] = $input['bundleId'];
            }
            //$insert_dat['FA_STATUS'] = 1;
            $insert_data['STATUS'] = 1;
            $this->db->insert('applications',$insert_dat);
        }
        $data = array();
        $data['state'] = 'success';
        $data['STATUS'] = $result['FA_STATUS'];
        echo json_encode($data);
    }

}
