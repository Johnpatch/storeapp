<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Stampsday extends ADMIN_Controller
{
    protected $table = "events";


    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }

    public function index($page = 0)
    {
        $user = $this->auth;
        $data = array();

        $input = $this->input->get();
        $data["branch_id"] = $this->auth['branch_id'];
        $data['params'] = $input;

        $data["bread_title"] =  lang("analysis");
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        /////////////////////////////title with index
        $data["form_description"] = $this->form_description;
        
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/analysis_list', $data);
        $this->load->view('_parts/footer');
    }
}
