<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Favorite_month extends ADMIN_Controller
{
    public $android_array = array();
    public $ios_array = array();
    public $shop_data = array();
    public $search_table = array();

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }
    protected function initialize() {
        $this->head['title'] = lang("analysis");

        $year = $this->input->get("selected_year");
        $branch_id = ($this->input->get("branch_id") == NULL) ? 1 : $this->input->get("branch_id");
        if ($year > 0 );
        else
        {
            $year = date('Y');
        }

        $todaytime = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        
        $this->android_array = array();
        for( $month = 1; $month <= 12 ;$month ++){
            $starttime = mktime(0, 0, 0, $month, 1, $year);
            $endtime = strtotime("+1 month", $starttime);
            if ($starttime > $todaytime)
            break;
            $this->android_array[$month - 1] = $this->db
                ->where("CREATE_TIME >=", $starttime)
                ->where("CREATE_TIME <=", $endtime)
                ->where('BRANCH_ID', $branch_id)
                ->where("DEVICE_TYPE", 1)->count_all_results("favorite_data");
        }
        $this->ios_array = array();
        for( $month = 1; $month <= 12 ;$month ++){
            $starttime = mktime(0, 0, 0, $month, 1, $year);
            $endtime = strtotime("+1 month", $starttime);
            if ($starttime > $todaytime)
                 break;
            $this->ios_array[$month - 1] = $this->db
                ->where("CREATE_TIME >=", $starttime)
                ->where("CREATE_TIME <=", $endtime)
                ->where('BRANCH_ID', $branch_id)
                ->where("DEVICE_TYPE", 2)->count_all_results("favorite_data");
        }
        $this->search_table = array();
        
        for( $month = 1; $month <= 12 ;$month ++){
            $starttime = mktime(0, 0, 0, $month, 1, $year);
            $endtime = strtotime("+1 month", $starttime);
            
            if ($starttime > $todaytime)
            {
                break;
            }

            $row = array();
            $row[0] = lang(date("F", mktime(0,0,0, $month, 1, $year)));
            $row[1] = $this->db
                ->where("CREATE_TIME >=", $starttime)
                ->where("CREATE_TIME <=", $endtime)
                ->where('BRANCH_ID', $branch_id)
                ->where("DEVICE_TYPE", 1)->count_all_results("favorite_data");
            $row[2] = $this->db
                ->where("CREATE_TIME >=", $starttime)
                ->where("CREATE_TIME <=", $endtime)
                ->where('BRANCH_ID', $branch_id)
                ->where("DEVICE_TYPE", 2)->count_all_results("favorite_data");
            $row[3] = $row[1] + $row[2];
            array_push($this->search_table, $row); 
        }
        $this->shop_data = $this->db->get('branches')->result_array();
    }
    
    public function index($page = 0) {
        $this->initialize();

        $data = array();
        $data['start_year'] = 2016;
        $data["last_year"] = 2030;

        $data["bread_title"] =  lang("analysis");
        $data['android_array'] =  json_encode($this->android_array);
        $data['ios_array'] = json_encode( $this->ios_array);
        $data['shop_data'] = $this->shop_data;
        $data['search_table'] = $this->search_table;
	    $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('analysis/favorite_month', $data);
        $this->load->view('_parts/footer');

    }

    public function CSVDownload()
    {
        $this->load->helper('download');
        $name = lang("favorite_by_month").date('YmdHis').'.csv';
        $shop_info = $this->db->where('ID', $this->auth['branch_id'])->get('branches')->row_array();
        $start_date = $shop_info['CREATE_TIME'];
        $start_year = date("Y", $start_date);
        $start_month = date("m", $start_date);
        $csvFileHeader = chr(0xef) . chr(0xBB) . chr(0xBF).'年月,Android,iOS,'.lang('total')."\n";
        $csv_content = $csvFileHeader;
        for($y = $start_year; $y <= date("Y"); $y ++) {
            for($m = $start_month; $m <= 12; $m ++) {
                if(strtotime($y."-".$m) > strtotime(date("Y-m"))) break;
                $starttime = mktime(0, 0, 0, $m, 1, $y);
                $endtime = strtotime("+1 month", $starttime);
                $row[0] = date("Ym", mktime(0, 0, 0, $m, 1, $y));
                $row[1] = $this->db
                    ->where("CREATE_TIME >=", $starttime)
                    ->where("CREATE_TIME <=", $endtime)
                    ->where('BRANCH_ID', $shop_info['ID'])
                    ->where("DEVICE_TYPE", 1)->count_all_results("favorite_data");
                $row[2] = $this->db
                    ->where("CREATE_TIME >=", $starttime)
                    ->where("CREATE_TIME <=", $endtime)
                    ->where('BRANCH_ID', $shop_info['ID'])
                    ->where("DEVICE_TYPE", 2)->count_all_results("favorite_data");
                $row[3] = $row[1] + $row[2];
                $csv_content .= $row[0].','.$row[1].','.$row[2].','.$row[3]."\n";
            }
            $start_month = 1;
        }
        force_download($name, $csv_content);
    }
}
