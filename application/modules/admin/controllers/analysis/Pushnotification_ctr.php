<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pushnotification_ctr extends ADMIN_Controller
{
    //Indicates whether index page has preview
    protected $index_has_preview = false;
    protected $route_prefix = 'admin/analysis/pushnotification_ctr/';

    //Indicates whether publish page has preview
    protected $publish_has_preview = false;
    protected $perpage = 5;
    protected $perpage_options = [5, 10, 20, 50];
    protected $branch_id = 0;

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }

    public $search_table = array();
    public $branch_name = array();
    public $table = "push_notification_ctr";
   
    
    protected function initialize() {

        $this->head['title'] = lang("analysis");
        $this->branch_id = $this->input->get("selected_branch");
        $year = $this->input->get("selected_year");
        $month = $this->input->get("selected_month");
        if ($year > 0 && $month >0 );
        else
        {
            $year = date('Y');
            $month = date('m');
        }

        if(!$this->branch_id){
            $this->branch_id = 1;
        }

        $query_b = $this->db->query("SELECT NAME, ID FROM branches");
		foreach ($query_b->result() as $row_b)
		{
            $row__ = array();
            $row__[0] = $row_b->ID;
            $row__[1] = $row_b->NAME;
            array_push($this->branch_name, $row_b);
        }
        
        $nUnRead_A = 0;
        $nAlreadyRead_A = 0;
        $nDeliveryNumber_A = 0;
        $nOpenRate_A = 0;
        $nUnRead_I = 0;
        $nAlreadyRead_I = 0;
        $nDeliveryNumber_I = 0;
        $nOpenRate_I = 0;
        $nRows = 0;

        $query_t = $this->db
            ->query("SELECT DISTRIBUTION_TITLE, SEND_DATE, ID FROM push_notification_ctr WHERE BRANCH_ID = '".$this->branch_id."' GROUP BY DISTRIBUTION_TITLE ORDER BY ID");
		foreach ($query_t->result() as $row_t)
		{
            $year_ = substr($row_t->SEND_DATE, 0, 4); 
            $month_ = substr($row_t->SEND_DATE, 5, 2); 
            $day_ = substr($row_t->SEND_DATE, -2); 

            if($year_ == $year){
                if($month_ == $month){
                    $query = $this->db
                        ->where('BRANCH_ID', $this->branch_id)
                        ->where('DISTRIBUTION_TITLE', $row_t->DISTRIBUTION_TITLE)
                        ->get('push_notification_ctr');
                    $nRows = count($query->result());
                    $i = 0;
                    foreach ($query->result() as $row_)
                    {
                        if($row_->DEVICE_TYPE == 1){
                            $nDeliveryNumber_A++;   
                            if($row_->IS_READ == 'N'){
                                $nUnRead_A++;
                            }else{
                                $nAlreadyRead_A++;
                            }
                            $nOpenRate_A = ($nAlreadyRead_A / $nDeliveryNumber_A) * 100;
                        }else{
                            $nDeliveryNumber_I++;
                            if($row_->IS_READ == 'N'){
                                $nUnRead_I++;
                            }else{
                                $nAlreadyRead_I++;
                            }
                            $nOpenRate_I = ($nAlreadyRead_I / $nDeliveryNumber_I) * 100;
                        }

                        $i++;
                        if($i == $nRows){
                            $row = array();
                            $date = date("D", mktime(0, 0, 0, $month, $day_, $year));
                            $row[0] = $row_->DISTRIBUTION_TITLE;
                            $row[1] = $day_.lang($date);
                            $row[2] = $nUnRead_A;
                            $row[3] = $nAlreadyRead_A;
                            $row[4] = $nDeliveryNumber_A;
                            $row[5] = $nOpenRate_A."%";
                            $row[6] = $nUnRead_I;
                            $row[7] = $nAlreadyRead_I;
                            $row[8] = $nDeliveryNumber_I;
                            $row[9] = $nOpenRate_I."%";
                            array_push($this->search_table, $row);
                        }
                    }
                }
            }
            $nUnRead_A = 0;
            $nAlreadyRead_A = 0;
            $nDeliveryNumber_A = 0;
            $nOpenRate_A = 0;
            $nUnRead_I = 0;
            $nAlreadyRead_I = 0;
            $nDeliveryNumber_I = 0;
            $nOpenRate_I = 0;
        }
    }

    public function index($page = 0) {
        $this->publish_has_preview = false;
        $this->index_has_preview = false;

        $data = array();
        $data["bread_title"] =  lang("analysis");
        $data["page_subtitle"] =  lang("edit");

        
        $input = $this->input->get();
        $perpage = $this->input->get('perpage');
        if ($perpage == 0) {
            $perpage = $this->perpage;
        }
        if ($this->index_has_search) {
            $this->set_field_values($this->search_fields, $input);
        }
        $input["perpage"] = $perpage;
        $rowscount = $this->count_rows($input);
        $data['perpage_options'] = $this->perpage_options;
        $data["perpage"] = $perpage;
        $data['start_year'] = 2016;
        $data["last_year"] = 2030;
        $data['search_table'] = $this->search_table;
        $data['branch_name'] = $this->branch_name;
        $data['links_pagination'] = pagination($this->redirect_url('index'), $rowscount, $perpage, count(explode("/", $this->route_prefix)) + 1);
        $this->head['title'] = lang('push_notification_ctr');
        
        $this->load->view('_parts/header', $this->head);
        $this->load->view('pushnotification_ctr', $data);
        $this->load->view('_parts/footer');
    }

    public function CSVDownload()
    {
        $this->load->helper('download');
        $name = lang("push_notification_ctr").date('YmdHis').'.csv';
        $shop_info = $this->db->where('ID', $this->auth['branch_id'])->get('branches')->row_array();
        $start_date = $shop_info['CREATE_TIME'];
        $start_year = date("Y", $start_date);
        $start_month = date("m", $start_date);
        $start_day = date("d", $start_date);
        $csvFileHeader = chr(0xef) . chr(0xBB) . chr(0xBF).'配布タイトル,送信日,Android,,,,ios,,,'."\n".',,未読,既読,配送番号,開封率,未読,既読,配送番号,開封率'."\n";
        $csv_content = $csvFileHeader;

        for($year = $start_year; $year <= date("Y"); $year ++) {
            for ($month = $start_month; $month <= 12; $month++) {
                for ($day = $start_day; $day <= 31; $day++) {
                    if (checkdate($month, $day, $year) == false) break;
                    $starttime = mktime(0, 0, 0, $month, $day, $year);
                    $endtime = strtotime("+1 day", $starttime);
                    if ($endtime > mktime(0, 0, 0, date('m'), date('d'), date('y'))) break;
                    if ($year > 0 && $month > 0) ;
                    else {
                        $year = date('Y');
                        $month = date('m');
                    }

                    if (!$this->branch_id) {
                        $this->branch_id = 1;
                    }

                    $query_b = $this->db->query("SELECT NAME, ID FROM branches");
                    foreach ($query_b->result() as $row_b) {
                        $row__ = array();
                        $row__[0] = $row_b->ID;
                        $row__[1] = $row_b->NAME;
                        array_push($this->branch_name, $row_b);
                    }

                    $nUnRead_A = 0;
                    $nAlreadyRead_A = 0;
                    $nDeliveryNumber_A = 0;
                    $nOpenRate_A = 0;
                    $nUnRead_I = 0;
                    $nAlreadyRead_I = 0;
                    $nDeliveryNumber_I = 0;
                    $nOpenRate_I = 0;
                    $nRows = 0;

                    $query_t = $this->db
                        ->query("SELECT DISTRIBUTION_TITLE, SEND_DATE, ID FROM push_notification_ctr WHERE BRANCH_ID = '" . $this->branch_id . "' GROUP BY DISTRIBUTION_TITLE ORDER BY ID");
                    foreach ($query_t->result() as $row_t) {
                        $year_ = substr($row_t->SEND_DATE, 0, 4);
                        $month_ = substr($row_t->SEND_DATE, 5, 2);
                        $day_ = substr($row_t->SEND_DATE, -2);

                        if ($year_ == $year) {
                            if ($month_ == $month) {
                                $query = $this->db
                                    ->where('BRANCH_ID', $this->branch_id)
                                    ->where('DISTRIBUTION_TITLE', $row_t->DISTRIBUTION_TITLE)
                                    ->get('push_notification_ctr');
                                $nRows = count($query->result());
                                $i = 0;
                                foreach ($query->result() as $row_) {
                                    if ($row_->DEVICE_TYPE == 1) {
                                        $nDeliveryNumber_A++;
                                        if ($row_->IS_READ == 'N') {
                                            $nUnRead_A++;
                                        } else {
                                            $nAlreadyRead_A++;
                                        }
                                        $nOpenRate_A = ($nAlreadyRead_A / $nDeliveryNumber_A) * 100;
                                    } else {
                                        $nDeliveryNumber_I++;
                                        if ($row_->IS_READ == 'N') {
                                            $nUnRead_I++;
                                        } else {
                                            $nAlreadyRead_I++;
                                        }
                                        $nOpenRate_I = ($nAlreadyRead_I / $nDeliveryNumber_I) * 100;
                                    }

                                    $i++;
                                    if ($i == $nRows) {
                                        $row = array();
//                                        $date = date("D", mktime(0, 0, 0, $month, $day_, $year));
                                        $row[0] = $row_->DISTRIBUTION_TITLE;
                                        $row[1] = date("Ymd", mktime(0, 0, 0, $month, $day, $year));
                                        $row[2] = $nUnRead_A;
                                        $row[3] = $nAlreadyRead_A;
                                        $row[4] = $nDeliveryNumber_A;
                                        $row[5] = $nOpenRate_A . "%";
                                        $row[6] = $nUnRead_I;
                                        $row[7] = $nAlreadyRead_I;
                                        $row[8] = $nDeliveryNumber_I;
                                        $row[9] = $nOpenRate_I . "%";
                                        $csv_content .= $row[0].','.$row[1].','.$row[2].','.$row[3].','.$row[4].','.$row[5].','.$row[6].','.$row[7].','.$row[8].','.$row[9]."\n";
                                    }
                                }
                            }
                        }
                        $nUnRead_A = 0;
                        $nAlreadyRead_A = 0;
                        $nDeliveryNumber_A = 0;
                        $nOpenRate_A = 0;
                        $nUnRead_I = 0;
                        $nAlreadyRead_I = 0;
                        $nDeliveryNumber_I = 0;
                        $nOpenRate_I = 0;
                    }
                }
                $start_day = 1;
            }
            $start_month = 1;
        }
        force_download($name, $csv_content);
    }
}
