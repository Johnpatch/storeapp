<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Stamp_benefit extends ADMIN_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }
    
    public $android_array = array();
    public $ios_array = array();
    public $search_table = array();
    protected function initialize() {

        $this->head['title'] = lang("analysis");
        $year = $this->input->get("selected_year");
        $month = $this->input->get("selected_month");
        if ($year > 0 && $month >0 );
        else
        {
            $year = date('y');
            $month = date('m');
        }
        
        $startmonthtime = mktime(0, 0, 0, $month, 1, $year);
        $endmonthtime = strtotime("+1 month", $startmonthtime); 
        $todaytime = mktime(0, 0, 0, date('m'), date('d'), date('y')); 

        //  echo date('Y-m-d', $todaytime);
        $this->android_array = array();
        for( $day = 1; $day <= 31 ;$day ++){
            if  (checkdate($month, $day, $year) == false) break;
            $starttime = mktime(0, 0, 0, $month, $day, $year);
            $endtime = strtotime("+1 day", $starttime);
            if ($endtime > $todaytime)
                break;
            
            $this->android_array[$day - 1] = $this->db
                ->where("CREATE_TIME >=", $starttime)
                ->where("CREATE_TIME <=", $endtime)
                ->where("DEVICE_TYPE", 1)
                ->count_all_results("stamp_uses");
        }
        $this->ios_array = array();
        for( $day = 1; $day <= 31 ;$day ++){
            if  (checkdate($month, $day, $year) == false) break;
            $starttime = mktime(0, 0, 0, $month, $day, $year);
            $endtime = strtotime("+1 day", $starttime);
            if ($endtime > $todaytime)
                break;
            
            $this->ios_array[$day - 1] = $this->db
                ->where("CREATE_TIME >=", $starttime)
                ->where("CREATE_TIME <=", $endtime)
                ->where("DEVICE_TYPE", 2)->count_all_results("stamp_uses");
        }

        $startmonthtime = mktime(0,0,0, $month, 1, $year);
        $endmonthtime = strtotime("+1 month", $startmonthtime); 
        $this->search_table = array();
        for( $day = 1; $day <= 31 ;$day ++){
            if  (checkdate($month, $day, $year) == false) break;
            $starttime = mktime(0, 0, 0, $month, $day, $year);
            $endtime = strtotime("+1 day", $starttime);
            if ($endtime > $todaytime)
            {
                break;
            }
            $row = array();
            $date = date("D", mktime(0, 0, 0, $month, $day, $year));
            $row[0] = $day."日".lang($date);
            
            $row[1] = $this->db->where("CREATE_TIME >=", $starttime)
                ->where("CREATE_TIME <=", $endtime)
                ->where("DEVICE_TYPE", 1)
                ->count_all_results("stamp_uses");
            $row[2] = $this->db->where("CREATE_TIME >=", $starttime)
                ->where("CREATE_TIME <=", $endtime)
                ->where("DEVICE_TYPE", 2)
                ->count_all_results("stamp_uses");
            $row[3] =   $row[1] + $row[2];
            array_push($this->search_table, $row);
        }
//        array_push($this->search_table, [lang('total'), 0, 0]);

    }
  
    public function index($page = 0) {
        $this->initialize();

        $data = array();
        $data['start_year'] = 2016;
        $data["last_year"] = 2030;

        $data["bread_title"] =  lang("analysis");
        $data['android_array'] =  json_encode($this->android_array);
        $data['ios_array'] = json_encode( $this->ios_array);
        $data['search_table'] = $this->search_table;
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('analysis/stamp_benefit', $data);
        $this->load->view('_parts/footer');
    }

    public function CSVDownload()
    {
        $this->load->helper('download');
        $name = lang("stamp_benefit_daily").date('YmdHis').'.csv';
        $shop_info = $this->db->where('ID', $this->auth['branch_id'])->get('branches')->row_array();
        $start_date = $shop_info['CREATE_TIME'];
        $start_year = date("Y", $start_date);
        $start_month = date("m", $start_date);
        $start_day = date("d", $start_date);
        $csvFileHeader = chr(0xef) . chr(0xBB) . chr(0xBF).'年月,Android,iOS,'.lang('total')."\n";
        $csv_content = $csvFileHeader;

        for($year = $start_year; $year <= date("Y"); $year ++) {
            for($month = $start_month; $month <= 12; $month ++) {
                for ($day = $start_day; $day <= 31; $day++) {
                    if (checkdate($month, $day, $year) == false) break;
                    $starttime = mktime(0, 0, 0, $month, $day, $year);
                    $endtime = strtotime("+1 day", $starttime);
                    if ($endtime > mktime(0, 0, 0, date('m'), date('d'), date('y'))) {
                        break;
                    }
                    $row = array();
                    $row[0] = date("Ymd", mktime(0, 0, 0, $month, $day, $year));
                    $row[1] = $this->db
                        ->where("CREATE_TIME >=", $starttime)
                        ->where("CREATE_TIME <=", $endtime)
                        ->where("DEVICE_TYPE", 1)
                        ->count_all_results("stamp_uses");
                    $row[2] = $this->db->where("CREATE_TIME >=", $starttime)
                        ->where("CREATE_TIME <=", $endtime)
                        ->where("DEVICE_TYPE", 2)
                        ->count_all_results("stamp_uses");
                    $row[3] = $row[1] + $row[2];
                    $csv_content .= $row[0].','.$row[1].','.$row[2].','.$row[3]."\n";
                }
                $start_day = 1;
            }
            $start_month = 1;
        }
        force_download($name, $csv_content);
    }
}
