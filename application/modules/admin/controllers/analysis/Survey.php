<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Survey extends ADMIN_Controller
{
    public $android_array = array();
    public $ios_array = array();
    public $search_table = array();

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }
    protected function initialize() {
        $this->head['title'] = lang("analysis");

        $year = $this->input->get("selected_year");
        if ($year > 0 );
        else
        {
            $year = date('Y');
        }

        $todaytime = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        
        $this->android_array = array();
        for( $month = 1; $month <= 12 ;$month ++){
            $starttime = mktime(0, 0, 0, $month, 1, $year);
            $endtime = strtotime("+1 month", $starttime);
            if ($starttime > $todaytime)
            break;
            $this->android_array[$month - 1] = $this->db->where("CREATE_TIME >=", $starttime)->where("CREATE_TIME <=", $endtime)->where("DEVICE_ID", 1)->count_all_results("users");
        }
        $this->ios_array = array();
        for( $month = 1; $month <= 12 ;$month ++){
            $starttime = mktime(0, 0, 0, $month, 1, $year);
            $endtime = strtotime("+1 month", $starttime);
            if ($starttime > $todaytime)
                 break;
            $this->ios_array[$month - 1] = $this->db->where("CREATE_TIME >=", $starttime)->where("CREATE_TIME <=", $endtime)->where("DEVICE_ID", 2)->count_all_results("users");
        }
        $this->search_table = array();
        
        for( $month = 1; $month <= 12 ;$month ++){
            $starttime = mktime(0, 0, 0, $month, 1, $year);
            $endtime = strtotime("+1 month", $starttime);
            
            if ($starttime > $todaytime)
            {
                break;
            }

            $row = array();
            $row[0] = lang(date("F", mktime(0,0,0, $month, 1, $year)));
            $row[1] = $this->db->where("CREATE_TIME >=", $starttime)->where("CREATE_TIME <=", $endtime)->where("DEVICE_ID", 1)->count_all_results("users");
            $row[2] = $this->db->where("CREATE_TIME >=", $starttime)->where("CREATE_TIME <=", $endtime)->where("DEVICE_ID", 2)->count_all_results("users");
           
            array_push($this->search_table, $row); 
        }
        array_push($this->search_table, ["合計", 0, 0]); 
        
    }
    
    public function index($page = 0) {
        $this->initialize();

        $data = array();
        $data['start_year'] = 2016;
        $data["last_year"] = 2030;

        $data["bread_title"] =  lang("analysis");
        $data['android_array'] =  json_encode($this->android_array);
        $data['ios_array'] = json_encode( $this->ios_array);
        $data['search_table'] = $this->search_table;
	    $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('analysis/survey', $data);
        $this->load->view('_parts/footer');

    }
}
