<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Segment_statistics extends ADMIN_Controller
{
    protected $has_index = true;//true if this controller has list pages
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";

    //series for sex
    public $sex_chart;
    public $sex_fields;

    public $os_chart;
    public $os_table;
    public $download_count;
    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }
    public $base_conditions = [
        "ID >= "=> 0 
    ];
    public function db_search(){
        $year = $this->input->get("selected_year");
        $month = $this->input->get("selected_month");
        $use = $this->input->get("USE");
       
        $starttime = mktime(0,0,0, $month, 1, $year);
        $endtime = strtotime("+1 month", $starttime);
        if ($use == 'N')
        {
            $this->base_conditions = [
                "ID >= "=> 0 
            ];
            return;
        }
        if($year >0 && $month >0 ){
            $this->base_conditions = [
                "CREATE_TIME >=" => $starttime."000",
                "CREATE_TIME <= " => $endtime."000"
            ];        
    
        }
    }
    protected function initialize() {

        $this->db_search();

        $this->page_title = lang("analysis");
        $this->page_subtitle = lang("segment_statistics");
        $this->panel_title = lang('notice');

        // $this->load->model('Event_model', 'model');
        $this->head['title'] = lang("segment_statistics");
        $this->head["description"] = "";
        $this->head["bread_title"] =  lang("analysis");

        $rows = $this->db->where($this->base_conditions)->where('BRANCH_ID', $this->auth['branch_id'])->get("users")->result_array();
        
        // sex
        $cnt_total = count($rows);
        
        $cnt_man = $this->db->where($this->base_conditions)->where("SEX",'M')->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
        $cnt_woman = $this->db->where($this->base_conditions)->where("SEX",'F')->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
        $cnt_not_set = $this->db->where($this->base_conditions)->where("SEX",NULL)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
        if($cnt_total == 0) $cnt_total = 1;
        $this->sex_chart = [
            [
                "name"=> "男性",
                "y"=> $cnt_man * 100 / $cnt_total,
            ],
            [
                "name"=> '女性',
                "y"=> $cnt_woman * 100 / $cnt_total,
            ], 
        ];
        $this->sex_table = [
            [
                "男性", $cnt_man,
            ],
            [
                "女性", $cnt_woman,
            ],
            [
                "未設定", $cnt_not_set,
            ]
        ];

        //os
     
        $cnt_total = $this->db->where($this->base_conditions)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
        if($cnt_total == 0) $cnt_total = 1;

        $cnt_android = $this->db->where($this->base_conditions)->where("DEVICE_TYPE",1)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
        $cnt_ios  = $this->db->where($this->base_conditions)->where("DEVICE_TYPE",2)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
        $cnt_not_set  = $this->db->where($this->base_conditions)->where("DEVICE_TYPE",NULL)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
        $this->download_count = $cnt_android + $cnt_ios;
        $this->os_chart = [
            [
                "name"=> "Android",
                "y"=> $cnt_android * 100 / $cnt_total,
            ],
            [
                "name"=> 'IOS',
                "y"=> $cnt_ios * 100 / $cnt_total,
            ], 
            [
                "name"=> '未設定',
                "y"=> $cnt_not_set * 100 / $cnt_total,
            ], 
        ];
        $this->os_table = [
            [
                "Android", $cnt_android." 人",
            ],
            [
                "iOS", $cnt_ios." 人",
            ],
        ];

        //age
        $cnt_total = $this->db->where($this->base_conditions)->count_all_results("users");
        if($cnt_total == 0) $cnt_total = 1;

        $theday = date("Y-m-d", strtotime("-20 years"));
        $cnt_19 = $this->db
                    ->where($this->base_conditions)
                    ->where("BIRTHDAY >", $theday)
                    ->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
       
        $cnt_29 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-30 years")))
                            ->where('BRANCH_ID', $this->auth['branch_id'])
                            ->where("BIRTHDAY <=", date("Y-m-d", strtotime("-20 years")))
                            ->count_all_results("users");
        $cnt_39 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-40 years")))
                            ->where('BRANCH_ID', $this->auth['branch_id'])
                            ->where("BIRTHDAY <=", date("Y-m-d", strtotime("-30 years")))
                            ->count_all_results("users");
        $cnt_49 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-50 years")))
                            ->where('BRANCH_ID', $this->auth['branch_id'])
                            ->where("BIRTHDAY <=", date("Y-m-d", strtotime("-40 years")))
                            ->count_all_results("users");   
        $cnt_59 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-60 years")))
                            ->where('BRANCH_ID', $this->auth['branch_id'])
                            ->where("BIRTHDAY <=", date("Y-m-d", strtotime("-50 years")))
                            ->count_all_results("users");
        $cnt_69 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-70 years")))
                            ->where('BRANCH_ID', $this->auth['branch_id'])
                            ->where("BIRTHDAY <=", date("Y-m-d", strtotime("-60 years")))
                            ->count_all_results("users");
        $cnt_79 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-80 years")))
                            ->where('BRANCH_ID', $this->auth['branch_id'])
                            ->where("BIRTHDAY <=", date("Y-m-d", strtotime("-70 years")))
                            ->count_all_results("users");
        $cnt_89 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-90 years")))
                            ->where('BRANCH_ID', $this->auth['branch_id'])
                            ->where("BIRTHDAY <=", date("Y-m-d", strtotime("-80 years")))
                            ->count_all_results("users");
        $cnt_end = $this->db->where($this->base_conditions)->where("BIRTHDAY <=", date("Y-m-d", strtotime("-90 years")))
                            ->where('BRANCH_ID', $this->auth['branch_id'])
                            ->count_all_results("users");
        $cnt_not_set = $this->db->where($this->base_conditions)->where("BIRTHDAY",NULL)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");

        $this->age_chart = [
            [
                "name"=> "~19 歳",
                "y"=> $cnt_19 * 100 / $cnt_total,
            ],
            [
                "name"=> "20-29 歳",
                "y"=> $cnt_29 * 100 / $cnt_total,
            ],
            [
                "name"=> "30-39 歳",
                "y"=> $cnt_39 * 100 / $cnt_total,
            ],
            [
                "name"=> "40-49 歳",
                "y"=> $cnt_49 * 100 / $cnt_total,
            ],
            [
                "name"=> "50-59 歳",
                "y"=> $cnt_59 * 100 / $cnt_total,
            ],
            [
                "name"=> "60-69 歳",
                "y"=> $cnt_69 * 100 / $cnt_total,
            ],
            [
                "name"=> "70-79 歳",
                "y"=> $cnt_79 * 100 / $cnt_total,
            ],
            [
                "name"=> "80-89 歳",
                "y"=> $cnt_89 * 100 / $cnt_total,
            ],
            [
                "name"=> "90 歳~",
                "y"=> $cnt_end * 100 / $cnt_total,
            ],
            [
                "name"=> "未設定",
                "y"=> $cnt_not_set * 100 / $cnt_total,
            ],
        ];
        $this->age_table = [
            [
                "~19 歳", $cnt_19." 人",
            ],
            [
                "20~29 歳", $cnt_29." 人",
            ],
            [
                "30~39 歳", $cnt_39." 人",
            ],
            [
                "40~49 歳", $cnt_49." 人",
            ],
            [
                "50~59 歳", $cnt_59." 人",
            ],
            [
                "60~69 歳", $cnt_69." 人",
            ],
            [
                "70~79 歳", $cnt_79." 人",
            ],
            [
                "80~89 歳", $cnt_89." 人",
            ],
            [
                "90 歳~", $cnt_end." 人",
            ],
        ];


    }
    public function index($page = 0)
    {
        $this->initialize();
        
        $user = $this->auth;

        $data = array();

        $input = $this->input->get();
        $perpage = $this->input->get('perpage');
        if ($perpage == 0) {
            $perpage = $this->perpage;
        }
        if ($this->index_has_search) {
            $this->set_field_values($this->search_fields, $input);
        }
        
        $data['search_fields'] = $this->search_fields;

        $data["has_order"] = $this->has_order;
        $data["has_add"] = $this->has_add;
//        $data['has_preview'] = true;
        $data['params'] = $input;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        /////////////////////////////title with index
        $data['page_title1'] = $this->page_title1;
        $data['page_subtitle1'] = $this->page_subtitle1;
        $data['panel_title1'] = $this->panel_title1;
        $data['list_description1'] = $this->list_description1;
        $data['title_index'] = $this->title_index;
        //
        $data["form_description"] = $this->form_description;
        $data["list_description"] = $this->list_description;
        $data["page_icon"] = $this->page_icon;
        $data["perpage_options"] = $this->perpage_options;
        $data["perpage"] = $perpage;
        $data["has_preview"] = $this->index_has_preview;
        $data['has_search'] = $this->index_has_search;


        //analysis
        $data['start_year'] = 2016;
        $data["last_year"] = 2030;
        
        $data['sex_chart'] = json_encode($this->sex_chart);
        $data['sex_table'] = $this->sex_table;

        $data['download_count'] = $this->download_count;

        $data['os_chart'] = json_encode($this->os_chart);
        $data['os_table'] = $this->os_table;
        
        $data['age_chart'] = json_encode($this->age_chart);
        $data['age_table'] = $this->age_table;
        
        $shop_info = $this->db->where('ID', $this->auth['branch_id'])->get('branches')->row_array();
        $start_date = $shop_info['CREATE_TIME'];
        $start_year = date("Y", $start_date);
        $start_month = date("m", $start_date);
        $data['start_year'] = $start_year;
        $data['start_month'] = $start_month;
        $this->load->view('_parts/header', $this->head);
        $this->load->view('analysis/segment_statics', $data);
        $this->load->view('_parts/footer');
    }

    public function CSVDownload()
    {
        $this->load->helper('download');
        $name = lang("segment_statistics").date('YmdHis').'.csv';
        $shop_info = $this->db->where('ID', $this->auth['branch_id'])->get('branches')->row_array();
        $start_date = $shop_info['CREATE_TIME'];
        $start_year = date("Y", $start_date);
        $start_month = date("m", $start_date);
        $csvFileHeader = chr(0xef) . chr(0xBB) . chr(0xBF).'年月,ダウンロード数,Android,iOS,男性,女性,性別未設定,～19歳,20歳～29歳,30歳～39歳,40歳～49歳,50歳～59歳,60歳～69歳,70歳～79歳,80歳～89歳,90歳～,年齢未設定'."\n";
        $csv_content = $csvFileHeader;
        for($year = $start_year; $year <= date("Y"); $year ++) {
            for($month = $start_month; $month <= 12; $month ++) {
                if(strtotime($year."-".$month) > strtotime(date("Y-m"))) break;
                $starttime = mktime(0, 0, 0, $month, 1, $year);
                $endtime = strtotime("+1 month", $starttime);
                if($year >0 && $month >0 ){
                    $this->base_conditions = [
                        "CREATE_TIME >=" => $starttime."000",
                        "CREATE_TIME <= " => $endtime."000"
                    ];

                }
                $cnt_man = $this->db->where($this->base_conditions)->where("SEX",'M')->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
                $cnt_woman = $this->db->where($this->base_conditions)->where("SEX",'F')->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
                $cnt_gender_not_set = $this->db->where($this->base_conditions)->where("SEX",NULL)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
                $cnt_android = $this->db->where($this->base_conditions)->where("DEVICE_TYPE",1)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
                $cnt_ios  = $this->db->where($this->base_conditions)->where("DEVICE_TYPE",2)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
                //age
                $theday = date("Y-m-d", strtotime("-20 years"));
                $cnt_19 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", $theday)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
                $cnt_29 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-30 years")))->where('BRANCH_ID', $this->auth['branch_id'])->where("BIRTHDAY <=", date("Y-m-d", strtotime("-20 years")))->count_all_results("users");
                $cnt_39 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-40 years")))->where('BRANCH_ID', $this->auth['branch_id'])->where("BIRTHDAY <=", date("Y-m-d", strtotime("-30 years")))->count_all_results("users");
                $cnt_49 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-50 years")))->where('BRANCH_ID', $this->auth['branch_id'])->where("BIRTHDAY <=", date("Y-m-d", strtotime("-40 years")))->count_all_results("users");
                $cnt_59 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-60 years")))->where('BRANCH_ID', $this->auth['branch_id'])->where("BIRTHDAY <=", date("Y-m-d", strtotime("-50 years")))->count_all_results("users");
                $cnt_69 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-70 years")))->where('BRANCH_ID', $this->auth['branch_id'])->where("BIRTHDAY <=", date("Y-m-d", strtotime("-60 years")))->count_all_results("users");
                $cnt_79 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-80 years")))->where('BRANCH_ID', $this->auth['branch_id'])->where("BIRTHDAY <=", date("Y-m-d", strtotime("-70 years")))->count_all_results("users");
                $cnt_89 = $this->db->where($this->base_conditions)->where("BIRTHDAY >", date("Y-m-d", strtotime("-90 years")))->where('BRANCH_ID', $this->auth['branch_id'])->where("BIRTHDAY <=", date("Y-m-d", strtotime("-80 years")))->count_all_results("users");
                $cnt_end = $this->db->where($this->base_conditions)->where("BIRTHDAY <=", date("Y-m-d", strtotime("-90 years")))->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
                $cnt_age_not_set = $this->db->where($this->base_conditions)->where("BIRTHDAY",NULL)->where('BRANCH_ID', $this->auth['branch_id'])->count_all_results("users");
                $row[0] = date("Ym", mktime(0, 0, 0, $month, 1, $year));
                $row[1] = $cnt_android + $cnt_ios;
                $row[2] = $cnt_android;
                $row[3] = $cnt_ios;
                $row[4] = $cnt_man;
                $row[5] = $cnt_woman;
                $row[6] = $cnt_gender_not_set;
                $row[7] = $cnt_19;
                $row[8] = $cnt_29;
                $row[9] = $cnt_39;
                $row[10] = $cnt_49;
                $row[11] = $cnt_59;
                $row[12] = $cnt_69;
                $row[13] = $cnt_79;
                $row[14] = $cnt_89;
                $row[15] = $cnt_end;
                $row[16] = $cnt_age_not_set;
                $csv_content .= $row[0].','.$row[1].','.$row[2].','.$row[3].','.$row[4].','.$row[5].','.$row[6].','.$row[7].','.$row[8].','.$row[9].','.$row[10].','.$row[11].','.$row[12].','.$row[13].','.$row[14].','.$row[15].','.$row[16]."\n";
            }
            $start_month = 1;
        }
        force_download($name, $csv_content);
    }
}
