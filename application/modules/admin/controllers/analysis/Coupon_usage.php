<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Coupon_usage extends ADMIN_Controller
{
    protected $route_prefix = 'admin/analysis/coupon_usage/'; //Dont't forget to attach '/' to the end of the string

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }

    public $android_array = array();
    public $ios_array = array();
    public $search_table = array();
    public $coupon_array = array();
    public $coupon_id;
    protected function initialize() {
        
        $this->head['title'] = lang("analysis");
        $year = $this->input->get("selected_year");
        $month = $this->input->get("selected_month");
        $this->coupon_id = $this->input->get("selected_coupon_id");

        if ($year > 0 && $month >0 );
        else
        {
            $year = date('y');
            $month = date('m');
        }
      
        
        $this->coupon_array = array();
        
        $this->coupon_array = $this->db->select("COUPON_TITLE, ID")
        ->where('BRANCH_ID', $this->auth['branch_id'])
        ->where('USE_SETTING_YN', 'Y')
        ->get("coupon")->result_array();
        $coupon_where = '';
        if ($this->coupon_id > 0){
            $coupon_where = ' coupon_usage.COUPON_ID ';
        }
        else
        {
            $coupon_where = ' coupon_usage.COUPON_ID > ';
            $this->coupon_id = 0;
        }

        $startmonthtime = mktime(0, 0, 0, $month, 1, $year);
        $endmonthtime = strtotime("+1 month", $startmonthtime); 
        $todaytime = mktime(0, 0, 0, date('m'), date('d'), date('y')).'000'; 

        //  echo date('Y-m-d', $todaytime);
        $this->android_array = array();
        for( $day = 1; $day <= 31 ;$day ++){
            if  (checkdate($month, $day, $year) == false) break;
            $starttime = mktime(0, 0, 0, $month, $day, $year);
            $endtime = strtotime("+1 day", $starttime);
            $starttime .= '000';
            $endtime .= '000';
            if ($endtime > $todaytime)
                break;
            
            $this->android_array[$day - 1] =
                count($this->db
                ->join("users", "users.USER_ID = coupon_usage.USER_ID", 'left')
                ->where("coupon_usage.CREATE_TIME >=", $starttime)
                ->where("coupon_usage.CREATE_TIME <=", $endtime)
                ->where("users.DEVICE_TYPE", 1)
                ->where("coupon_usage.IS_USE", 'Y')
                ->where($coupon_where, $this->coupon_id)
                ->where('coupon_usage.BRANCH_ID', $this->auth['branch_id'])
                ->get("coupon_usage")->result_array());         
        }

        $this->ios_array = array();
        for( $day = 1; $day <= 31 ;$day ++){
            if  (checkdate($month, $day, $year) == false) break;
            $starttime = mktime(0, 0, 0, $month, $day, $year);
            $endtime = strtotime("+1 day", $starttime);
            $starttime .= '000';
            $endtime .= '000';
            if ($endtime > $todaytime)
                break;
            
            $this->ios_array[$day - 1] =  
            count($this->db
            ->join("users", "users.USER_ID = coupon_usage.USER_ID", 'left')
            ->where("coupon_usage.CREATE_TIME >=", $starttime)
            ->where("coupon_usage.CREATE_TIME <=", $endtime)
            ->where("users.DEVICE_TYPE", 2)
            ->where("coupon_usage.IS_USE", 'Y')
            ->where($coupon_where, $this->coupon_id)
            ->where('coupon_usage.BRANCH_ID', $this->auth['branch_id'])
            ->get("coupon_usage")->result_array());
        }

        $startmonthtime = mktime(0,0,0, $month, 1, $year);
        $endmonthtime = strtotime("+1 month", $startmonthtime); 
        $this->search_table = array();
        for( $day = 1; $day <= 31 ;$day ++){
            if  (checkdate($month, $day, $year) == false) break;
            $starttime = mktime(0, 0, 0, $month, $day, $year);
            $endtime = strtotime("+1 day", $starttime);
            $starttime .= '000';
            $endtime .= '000';
            if ($endtime > $todaytime)
            {
                break;
            }
            $row = array();
            $date = date("D", mktime(0, 0, 0, $month, $day, $year));
            $row[0] = $day."日".lang($date);
            $coupon_name = '';
            if($this->coupon_id > 0){
                $coupon_name =  $this->db
                ->where('ID', $this->coupon_id)
                ->get("coupon")->result_array()[0]['COUPON_TITLE'];
            }else{
                $couponRes = $this->db->select('coupon.COUPON_TITLE')
                ->join("coupon", "coupon.ID = coupon_usage.COUPON_ID", 'left')
                ->where("coupon_usage.CREATE_TIME >=", $starttime)
                ->where("coupon_usage.CREATE_TIME <=", $endtime)
                ->where("coupon_usage.IS_USE", 'Y')
                ->where($coupon_where, $this->coupon_id)
                ->where('coupon_usage.BRANCH_ID', $this->auth['branch_id'])
                ->group_by('COUPON_TITLE')
                ->get("coupon_usage")->result_array();
                foreach($couponRes as $key => $val){
                    if($key > 0){
                        $coupon_name .= ',';
                    }
                    $coupon_name .= $val['COUPON_TITLE'];
                }
            }
            
            
            $row[1] = $coupon_name;

            $company_name = $this->db
            ->where('ID', $this->auth['branch_id'])
            ->get("branches")->result_array()[0]['NAME'];

            $row[2] = $company_name;
            
            $total = count($this->db
            ->where("coupon_usage.CREATE_TIME >=", $starttime)
            ->where("coupon_usage.CREATE_TIME <=", $endtime)
            ->where("coupon_usage.IS_USE", 'Y')
            ->where($coupon_where, $this->coupon_id)
            ->where('coupon_usage.BRANCH_ID', $this->auth['branch_id'])
            ->get("coupon_usage")->result_array());

            $row[3] = $total;
            array_push($this->search_table, $row); 
        }

    }
  
    public function index($page = 0) {
        $data = array();
        $data['start_year'] = 2016;
        $data["last_year"] = 2030;
        $data["bread_title"] =  lang("analysis");
        $data["page_subtitle"] =  lang("edit");
        $input = $this->input->get();
        $perpage = $this->input->get('perpage');
        if ($perpage == 0) {
            $perpage = $this->perpage;
        }
        $input["perpage"] = $perpage;
        $data['perpage_options'] = $this->perpage_options;
        $data["perpage"] = $perpage;

        $rowscount = count($this->search_table);
        $data['links_pagination'] = pagination($this->redirect_url('index'), $rowscount, $perpage, count(explode("/", $this->route_prefix)) + 1);

        $data["table_fields"] = $this->table_fields;
        $data['search_fields'] = $this->search_fields;

        $data["bread_title"] =  lang("analysis");
        
        $data['coupon_array'] = $this->coupon_array;
        $data['android_array'] =  json_encode($this->android_array);
        $data['ios_array'] = json_encode( $this->ios_array);
        $data['search_table'] = $this->search_table;

        $this->load->view('_parts/header', $this->head);
        $this->load->view('analysis/coupon_usage', $data);
        $this->load->view('_parts/footer');
    }
    public function CSVDownload()
    {
        $this->load->helper('download');
        $name = lang("coupon_usage_daily").date('YmdHis').'.csv';
        $shop_info = $this->db->where('ID', $this->auth['branch_id'])->get('branches')->row_array();
        $start_date = $shop_info['CREATE_TIME'];
        $start_year = date("Y", $start_date);
        $start_month = date("m", $start_date);
        $start_day = date("d", $start_date);
        $csvFileHeader = chr(0xef) . chr(0xBB) . chr(0xBF).'年月,クーポン名,店舗名,'.lang('total')."\n";
        $csv_content = $csvFileHeader;

        for($year = $start_year; $year <= date("Y"); $year ++) {
            for($month = $start_month; $month <= 12; $month ++) {
                for ($day = $start_day; $day <= 31; $day++) {
                    if (checkdate($month, $day, $year) == false) break;
                    $starttime = mktime(0, 0, 0, $month, $day, $year);
                    $endtime = strtotime("+1 day", $starttime);
                    if ($endtime > mktime(0, 0, 0, date('m'), date('d'), date('y'))) {
                        break;
                    }
                    $row = array();
                    
                    $row[0] = date("Ymd", mktime(0, 0, 0, $month, $day, $year));
                    
                    $coupon_name =  $this->db
                    ->where('ID', $this->coupon_id)
                    ->get("coupon")->result_array()[0]['COUPON_TITLE'];
        
                    $row[1] = $coupon_name;
                    
                    $company_name = $this->db
                    ->where('ID', $this->auth['branch_id'])
                    ->get("branches")->result_array()[0]['NAME'];

                    $row[2] = $company_name;
                    
                    $total = count($this->db
                    ->where("coupon_usage.CREATE_TIME >=", $starttime)
                    ->where("coupon_usage.CREATE_TIME <=", $endtime)
                    ->where("coupon_usage.COUPON_ID", $this->coupon_id)
                    ->where('coupon_usage.BRANCH_ID', $this->auth['branch_id'])
                    ->get("coupon_usage")->result_array());

                    $row[3] = $total;

                    $csv_content .= $row[0].','.$row[1].','.$row[2].','.$row[3]."\n";
                }
                $start_day = 1;
            }
            $start_month = 1;
        }
        force_download($name, $csv_content);
    }
}
