<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Stamp_day extends ADMIN_Controller
{
    //Indicates whether index page has preview
    protected $index_has_preview = false;

    //Indicates whether publish page has preview
    protected $publish_has_preview = false;

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }
    public $android_array = array();
    public $ios_array = array();
    public $shop_data = array();
    public $search_table = array();
    protected function initialize() {

        $this->head['title'] = lang("analysis");
        $year = $this->input->get("selected_year");
        $month = $this->input->get("selected_month");
        $branch_id = $this->auth['branch_id'];
        if ($year > 0 && $month >0 );
        else
        {
            $year = date('Y');
            $month = date('m');
        }
        
        $startmonthtime = mktime(0, 0, 0, $month, 1, $year);
        $endmonthtime = strtotime("+1 month", $startmonthtime); 
        $todaytime = mktime(0, 0, 0, date('m'), date('d'), date('Y')).'000';

        $this->android_array = array();
        for( $day = 1; $day <= 31 ;$day ++){
            if  (checkdate($month, $day, $year) == false) break;
            $starttime = mktime(0, 0, 0, $month, $day, $year);
            $endtime = strtotime("+1 day", $starttime);
            $starttime .= '000';
            $endtime .= '000';
            if ($endtime > $todaytime)
                break;
            $this->android_array[$day - 1] = count(
                $this->db
                ->join("users", "users.USER_ID = stamp_log.USER_ID", 'left')
                ->where("stamp_log.CREATE_TIME >=", $starttime)
                ->where("stamp_log.CREATE_TIME <=", $endtime)
                ->where("users.DEVICE_TYPE", 1)
                ->where('stamp_log.BRANCH_ID', $branch_id)
                ->get("stamp_log")->result_array());
        }
        $this->ios_array = array();
        for( $day = 1; $day <= 31 ;$day ++){
            if  (checkdate($month, $day, $year) == false) break;
            $starttime = mktime(0, 0, 0, $month, $day, $year);
            $endtime = strtotime("+1 day", $starttime);
            $starttime .= '000';
            $endtime .= '000';
            if ($endtime > $todaytime)
                break;       
                $this->ios_array[$day - 1] = count(
                    $this->db
                    ->join("users", "users.USER_ID = stamp_log.USER_ID", 'left')
                    ->where("stamp_log.CREATE_TIME >=", $starttime)
                    ->where("stamp_log.CREATE_TIME <=", $endtime)
                    ->where("users.DEVICE_TYPE", 2)
                    ->where('stamp_log.BRANCH_ID', $branch_id)
                    ->get("stamp_log")->result_array());        
        }

        $startmonthtime = mktime(0,0,0, $month, 1, $year);
        $endmonthtime = strtotime("+1 month", $startmonthtime); 
        $this->search_table = array();
      
        for( $day = 1; $day <= 31 ;$day ++){
            if  (checkdate($month, $day, $year) == false) break;
            $starttime = mktime(0, 0, 0, $month, $day, $year);
            $endtime = strtotime("+1 day", $starttime);
            $starttime .= '000';
            $endtime .= '000';
            if ($endtime > $todaytime)
            {
                break;
            }
            $row = array();
            $date = date("D", mktime(0, 0, 0, $month, $day, $year));
            $row[0] = $day."日".lang($date);
            $row[1] =  count(
                $this->db
                ->join("users", "users.USER_ID = stamp_log.USER_ID", 'left')
                ->where("stamp_log.CREATE_TIME >=", $starttime)
                ->where("stamp_log.CREATE_TIME <=", $endtime)
                ->where("users.DEVICE_TYPE", 1)
                ->where('stamp_log.BRANCH_ID', $branch_id)
                ->get("stamp_log")->result_array());
            $row[2] = count(
                $this->db
                ->join("users", "users.USER_ID = stamp_log.USER_ID", 'left')
                ->where("stamp_log.CREATE_TIME >=", $starttime)
                ->where("stamp_log.CREATE_TIME <=", $endtime)
                ->where("users.DEVICE_TYPE", 2)
                ->where('stamp_log.BRANCH_ID', $branch_id)
                ->get("stamp_log")->result_array());
 
            // $this->db->where("CREATE_TIME >=", $starttime)->where("CREATE_TIME <=", $endtime)->where("DEVICE_TYPE", 2)->where('BRANCH_ID', $branch_id)->count_all_results("stamp_data");
            $row[3] =   $row[1] + $row[2];
            array_push($this->search_table, $row); 
        }
        $this->shop_data = $this->db->get('branches')->result_array();
    }
  
    public function index($page = 0) {
        $this->initialize();

        $data = array();
        $data['start_year'] = 2020;
        $data["last_year"] = 2040;

        $data["bread_title"] =  lang("analysis");
        $data['android_array'] =  json_encode($this->android_array);
        $data['ios_array'] = json_encode( $this->ios_array);
        $data['shop_data'] = $this->shop_data;
        $data['search_table'] = $this->search_table;
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('analysis/stamp_day', $data);
        $this->load->view('_parts/footer');
    }

    public function CSVDownload()
    {
        $this->load->helper('download');
        $name = lang("stamp_by_day").date('YmdHis').'.csv';
        $shop_info = $this->db->where('ID', $this->auth['branch_id'])->get('branches')->row_array();
        $start_date = $shop_info['CREATE_TIME'];
        $start_year = date("Y", $start_date);
        $start_month = date("m", $start_date);
        $start_day = date("d", $start_date);
        $csvFileHeader = chr(0xef) . chr(0xBB) . chr(0xBF).'年月,Android,iOS,'.lang('total')."\n";
        $csv_content = $csvFileHeader;

        for($year = $start_year; $year <= date("Y"); $year ++) {
            for($month = $start_month; $month <= 12; $month ++) {
                for ($day = $start_day; $day <= 31; $day++) {
                    if (checkdate($month, $day, $year) == false) break;
                    $starttime = mktime(0, 0, 0, $month, $day, $year);
                    $endtime = strtotime("+1 day", $starttime);
                    if ($endtime > mktime(0, 0, 0, date('m'), date('d'), date('y'))) {
                        break;
                    }
                    $row = array();
                    $row[0] = date("Ymd", mktime(0, 0, 0, $month, $day, $year));
                    $row[1] =  count(
                        $this->db
                            ->join("users", "users.USER_ID = stamp_data.USER_ID", 'left')
                            ->where("stamp_data.CREATE_TIME >=", $starttime)
                            ->where("stamp_data.CREATE_TIME <=", $endtime)
                            ->where("users.DEVICE_TYPE", 1)
                            ->get("stamp_data")->result_array());
                    $row[2] = count(
                        $this->db
                            ->join("users", "users.USER_ID = stamp_data.USER_ID", 'left')
                            ->where("stamp_data.CREATE_TIME >=", $starttime)
                            ->where("stamp_data.CREATE_TIME <=", $endtime)
                            ->where("users.DEVICE_TYPE", 2)
                            ->get("stamp_data")->result_array());
                    $row[3] = $row[1] + $row[2];
                    $csv_content .= $row[0].','.$row[1].','.$row[2].','.$row[3]."\n";
                }
                $start_day = 1;
            }
            $start_month = 1;
        }
        force_download($name, $csv_content);
    }
}
