<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Webview extends ADMIN_Controller
{
    protected $table = "webview";

    protected $route_prefix = 'admin/webview/';//Don't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $index_has_search = true;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $publish_script_url = "";
    protected $confirm_script_url = "";
    protected $has_add = true;
    protected $has_order = false;
   
    //Indicates whether index page has preview
    protected $index_has_preview = false;

    //Indicates whether publish page has preview
    protected $publish_has_preview = false;
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $fields = [
        
    ];

    protected $preview_url;

    protected $search_fields = array();//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields = [
      
    ];

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->page_title = lang('basic_information');//"Basic information";
        $this->page_subtitle = lang('web_view_settings');//"";
        $this->panel_title = lang('web_view_settings');//"";
        $this->bread_title = lang('basic_information');
        $this->fields = [
            [
                "name"=>"TITLE",
                "type"=>"text",
                "label"=>lang("title"),
            ],
            [
                "name"=>"LINK",
                "type"=>"text",
                "label"=>lang("url"),
            ],
            [
                "name"=>"TYPE",
                "type"=>"radio",
                "label"=>lang('link_select'),//"Email inquiry display",
                "default"=>"OUT",
                "options"=>[
                    "OUT"=>lang('link_out'),
                    "IN"=>lang('link_in')
                ]
            ],
        ];

        $this->table_fields = [
            [
                "label"=>lang('fix'),//"Fix",
                "type"=>"edit",
            ],
            [
                "label"=>lang('delete'),//"Delete",
                "type"=>"delete"
            ],
            [
                "label" => lang('title'),
                "name" => "TITLE",
                "type" => "text",
                "required"=>true,
                "rules"=>"required"
            ],
            [
                "name"=>"TYPE",
                "type"=>"radio",
                "label"=>lang('link_select'),//"Email inquiry display",
                "default"=>"OUT",
                "options"=>[
                    "OUT"=>lang('link_out'),
                    "IN"=>lang('link_in')
                ]
            ],
        ];

        $this->search_fields = [
            [
                "label" => lang('title'),
                "name" => "TITLE",
                "type" => "text",
            ],
        ];

        $this->form_description='';//"";

        // $this->load->model('facebook_model', 'model');
        $this->head['title'] = lang("web_view_settings");
        $this->head['bread_title'] = lang('basic_information');
        $this->head['page_subtitle'] = lang('web_view_settings');
        $this->head["description"] = "";
    }

    protected function build_query($params) {
        $TITLE = element("TITLE", $params);
        $this->db->where('BRANCH_ID', $this->auth["branch_id"]);
        $this->db->like("TITLE", $TITLE);
    }
}
