<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Welcome_push extends ADMIN_Controller
{
    protected $table = "birthday_welcome_push";

    protected $route_prefix = 'admin/autopush/welcome_push/';//Don't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
    protected $index_has_search = false;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $confirm_script_url = "autopush/confirm";
    protected $publish_script_url = "autopush/publish";
    //Indicates whether index page has preview
    protected $index_has_preview = false;

    //Indicates whether publish page has preview
    protected $publish_has_preview = true;
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $additional_conditions = [
        "PUSH_TYPE" => 2
    ];
    protected $fields = [
     
    ];

    protected $preview_url;

    protected $search_fields = array();//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields = [
      
    ];

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
    }
    public function confirm($id = 0)
    {
        $input = $this->input->post();

        $this->session->set_flashdata('old', $input);
        $data = $input;
        $back_url = $input["back_url"];
        $this->set_field_values($this->fields, $input);
        if($input["AUTO_PUSH_YN"] == "1")
            $this->set_validation_rules($this->fields);

        if ($this->form_validation->run() == FALSE) {
            $error = array();
            $this->set_errors($this->fields, $error);

            if (count($error)  > 0) {
                $this->session->set_flashdata("errors", $error);
                redirect($back_url);
            }
        }

        $data["fields"] = $this->fields;
//        $data["row"] = $row;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        $data["form_description"] = $this->form_description;
        $data["page_icon"] = $this->page_icon;
        $data['script_url'] = $this->confirm_script_url;
        $data["save_url"] = $this->redirect_url('save' . ($id > 0 ? "/$id" : ""));
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/confirm', $data);
        $this->load->view('_parts/footer');
    }
    protected function initialize() {
        $this->page_title = lang('auto_push_notification');//"Basic information";
        $this->page_subtitle = lang('welcome_push');//"";
        $this->panel_title = lang('autopush_welcomepush_panel_title');//"";
        $this->head["bread_title"] =  lang("auto_push_notification");
        $this->head["page_subtitle"] =  lang("autopush_welcomepush_page_title");
        $this->fields = [
            [
                "name"=>"AUTO_PUSH_YN",
                "type"=>"radio",
                "inline"=>true,
                "label"=>lang("automatic_push"),
                "default"=> 2,
                "options"=>[
                    1 => lang("design_topmenu_button_type_effectiveness"),
                    2 => lang("design_topmenu_button_type_invalid"),
                ]
            ],
            [
                "name"=>"DELIVERY_DATE",
                "type"=>"radio",
                "label"=>lang("coupon_delivery_date"),
                "default"=> 1,
                "options"=>[
                    1 => lang("30_minutes"),
                    2 => lang("2_hours"),
                    3 => lang("1_day"),
                    4 => lang("2_day"),
                    5 => lang("After_setting_profile"),
                ],
                "help"=>lang("DELIVERY_DATE_HELP"),
            ],
            [
                "name"=>"VALIDITY_PERIOD",
                "type"=>"radio",
                "label"=>lang("coupon_validity_period"),
                "default"=> 1,
                "options"=>[
                    1 => lang("one_year"),
                    2 => lang("6_months"),
                    3 => lang("3_months"),
                    4 => lang("2_months"),
                    5 => lang("1_months"),
                ],
            ],
            [
                "name"=>"TITLE",
                "type"=>"text",
                "label"=>lang("coupon_title"),
                "required"=> true,
                "rules" => 'required',
                "help"=> lang("TITLE_HELP")              
            ],

            [
                "name"=>"DETAIL",
                "type"=>"textarea",
                "label"=>lang("coupon_detail"),
                "help"=> lang("DETAIL_HELP")                   
            ],
            [
                "name"=>"IMAGE",
                "type" => "file",
                "label"=>lang("coupon_image"),
                "help"=> lang("IMAGE_HELP")            
            ],
            [
                "name"=>"DISCOUNT_CONTENT",
                "type" => "text",
                "label"=>lang("discount_content"),
                "help"=> lang("DISCOUNT_CONTENT_HELP"),
            ],
            [
                "group" => true,
                "label" => lang("coupon_notice"),
                "input" => [
                    [
                        "name" => "NOTICE_1_YN",
                        "type" => "checkbox",
                        "checked"=>'Y',
                        "unchecked"=>'N',
                        "text"=>lang("NOTICE_1_YN_HELP") 
                    ],
                    [
                        "name" => "NOTICE_2_YN",
                        "type" => "checkbox",
                        "checked"=>'Y',
                        "unchecked"=>'N',
                        "text"=>lang("NOTICE_2_YN_HELP") 
                    ],
                    [
                        "name" => 'NOTICE_3_YN',
                        "type" => "checkbox",
                        "checked"=>'Y',
                        "unchecked"=>'N',
                        "text"=>lang("NOTICE_3_YN_HELP") 
                    ],
                ]
            ],
            [
                "name"=>"USE_SETTING_YN",
                "type"=>"radio",
                "label"=>lang("single_use_setting"),
                "default"=> "Y",
                "options"=>[
                    "Y" => lang("design_topmenu_button_type_effectiveness"),
                    "N" => lang("design_topmenu_button_type_invalid"),
                ],
                "help" => lang("USE_SETTING_YN_HELP")
            ],
            
            
        ];

        $this->form_description = lang('autopush_welcomepush_form_description');//"";
        //Success settings
        $this->success_return_text = lang('autopush_welcomepush_success_return_text');//"Return";
        //language setting ended

        // $this->load->model('Webview_app_model', 'model');
        $this->head['title'] = lang("welcome_push");
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/welcomeinfo");
    }
}
