<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Notification_setting extends ADMIN_Controller
{
    protected $table = "auto_push";

    protected $route_prefix = 'admin/autopush/notification_setting/';//Don't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $index_has_search = false;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $confirm_script_url = "";
    protected $has_order = false;
    protected $has_add = true;
    
   
    //Indicates whether index page has preview
    protected $index_has_preview = false;

    //Indicates whether publish page has preview
    protected $publish_has_preview = false;
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $fields = [
     
    ];

    protected $preview_url;

    protected $search_fields = array();//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields = [
      
    ];

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->page_title = lang('autopush_settings_page_title');//"Basic information";
        $this->page_subtitle = lang('step_send');//"";
        $this->panel_title = lang('step_send');//"";
        $this->head["bread_title"] =  lang("auto_push_notification");
        $this->head["page_subtitle"] =  lang("step_send");
        $this->fields = [
            [
                "label" => lang('auto_push_type'),
                "type" => "radio",
                "name" => "AUTO_PUSH_TYPE",
                "default" => 1,
                "options" => [
                    1 => lang("automatic_push_cycle"),
                    2 => lang("automatic_push_delivery_criteria")
                ]
            ],
            [
                "label" => lang("automatic_push_cycle"),//"Public photos",
                "group" => true,
                "input" => [
                    [
                        "name" => 'START_PUSH',
                        "type" => "dropdown",
                        "class" => "auto-push-period",
                        "options" => [
                            "ダウンロードから","最終ログイン日から"
                        ]
                    ],
                    [
                        "name" => 'PERIOD',
                        "type" => "dropdown",
                        "class" => "auto-push-period",
                        "options" => [
                            1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30
                        ]
                    ],
                    [
                        "name" => 'UNIT',
                        "type" => "dropdown",
                        "class" => "auto-push-unit",
                        "options" => [
                            "年","月", "日", "ウィーク"
                        ]
                    ]
                ]
            ],
            [
                "label" => lang("automatic_push_delivery_criteria"),//"Public photos",
                "type"=>"radio",
                "name"=>"TYPE",
                "options" =>[
                    1 =>lang("automatic_push_delivery_criteria_1"),
                    2 =>lang("automatic_push_delivery_criteria_2"),
                ]
            ],
            [
                "name"=>"TITLE",
                "type"=>"text",
                "label"=>lang("title"),
            ],
            [
                "name"=>"MESSAGE",
                "type"=>"textarea",
                "label"=>lang("message"),
            ],
         

        ];

        $this->table_fields = [
            [
                "label" => lang("correction_confirmation"),
                "type" => "edit",
            ],
            [
                "label" => lang("delete"),
                "type" => "delete"
            ],
            [
                "label" => lang("title"),
                "name" => "TITLE",
                "type" => "text",
            ],
        ];

        $this->form_description = lang('autopush_settings_form_description');//"";
        //Success settings
        $this->success_return_text = lang('autopush_settings_success_return_text');//"Return";
        //language setting ended

        // $this->load->model('Webview_app_model', 'model');
        $this->head['title'] = lang("auto_push_notification");
        $this->head["description"] = "";

        // $this->preview_url = site_url("preview/main/layout");
    }
}
