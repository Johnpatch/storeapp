<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Login extends ADMIN_Controller
{
    public function generateToken($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function token_login() {
        $this->load->model(array('History_model'));
        header("Access-Control-Allow-Origin: *");
        $info = $this->input->post();
        $result = $this->Home_admin_model->loginCheck($info);
        if (!empty($result)) {
            /* $_SESSION['last_login'] = $result['last_login'];
            $result['branch_id'] = $result['ID'];
            $this->session->set_userdata('logged_in', $result['LOGIN_ID']);
            $this->session->set_userdata('user', $result); */
            $token = $this->generateToken();
            
            $data = array();
            $data['logged_in'] = $result['LOGIN_ID'];
            $result['branch_id'] = $result['ID'];
            $data['user'] = $result;

            $query = "select * from token_table where BRANCH_ID=".$result['ID'];
            $ret = $this->db->query($query);
            $ret = $ret->result_array();
            if(count($ret) < 1) {
                $query = "insert into token_table set TOKEN='".$token."' , BRANCH_ID = ".$result['ID'].", DATA = '".json_encode($data)."'";
            }
            else {
                $query = "update token_table set TOKEN='".$token."', DATA='".json_encode($data)."' where BRANCH_ID=".$result['ID'];
            }
            $this->db->query($query);
            echo json_encode(array("status" => "success" , "token"=>$token, "branch_id" => $result['ID']));
        } else {
            echo json_encode(array("status" => "failed"));
        }

    }

}
