<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Push extends ADMIN_Controller
{
    protected $table = "push_notification";
    protected $route_prefix = 'admin/push/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $has_order = false;
    protected $index_has_preview = false;
    protected $publish_has_preview = true;
    protected $has_add = false;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $publish_script_url = "push/publish";
    protected $confirm_script_url = "push/confirm";
    protected $edit_title = '';
    /*
         * Input Fields Description
         * name: input name----Must be exactly equal to corresponding DB column name
         * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
         * default: default value if initial value is not set
         * label: input label description
         * help: help block description
         * required: whether this field is requried or not
         * showIf: array(key=>value). shows this element when (key=value)
         *          value can take array values.
         * rules: validation settings
         */
    protected $fields;
    protected $preview_url;

    protected $search_fields = [
        /*[
            "label"=>"Publication date",
            "name"=>"SEARCH_PUBLIC_DATE",
            "picker_type"=>"single",
            "type"=>"date",
        ],
        [
            "name"=>'TITLE',
            "type"=>'text',
            "label"=>"title"
        ],
        [
            "name"=>'PUBLIC_TYPE',
            "type"=>'checkbox',
            "checked"=>'Y',
            "unchecked"=>'N',
            "text"=>'Publishing only'
        ]*/
    ]; //used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */

    //Publish settings
    protected $form_description = "";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize()
    {
        $this->form_description = lang("push_form_description");
        $this->head['title'] = lang("push_notification");
        $this->page_title = lang("push_notification");
        $this->panel_title = lang("list_reg");
        $this->page_subtitle = lang("list_reg");
        $this->success_return_text = lang("success_return_text");
        $this->head["bread_title"] =  lang("push_notification");
        $this->head["page_subtitle"] =  lang("edit");
        $this->head["description"] = "";
        $this->edit_title = lang('list_reg');

        $users = $this->db
            ->select("ID,USER_ID")
            ->where("BRANCH_ID", $this->auth['branch_id'])
            ->get("users")
            ->result_array();
        $users_options = array();
        foreach ($users as $user) {
            $users_options[$user["ID"]] = $user["USER_ID"];
        }

        $surveys = $this->db
            ->select("ID,TITLE")
            ->where("BRANCH_ID", $this->auth['branch_id'])
            ->get("survey")
            ->result_array();
        $surveys_options = array();
        $surveys_options[''] = lang('please_select');
        foreach ($surveys as $survey) {
            $surveys_options[$survey["ID"]] = $survey["TITLE"];
        }

        $this->fields = [
            [
                "label" => lang("delivery_type"),
                "header" => lang('delivery_type_help'),
                "name" => 'DELIVERY_TYPE',
                "type" => 'radio',
                "default" => 1,
                "options" => [
                    1 => lang("push_type_all"),
                    2 => lang("push_type_individual"),
                    3 => lang("push_type_segment"),
                ]
            ],
            [
                "label" => lang("delivery_target"),
                "name" => "DELIVERY_USER",
                "type" => 'dropdown',
                "options" => $users_options
            ],
            [
                "label" => lang("delivery_target"),
                "group" => true,
                "name"=>"DELIVERY_SURVEY_SETTING",
                "input" => [
                    [
                        "name" => "DELIVERY_SURVEY",
                        "type"=>'dropdown',
                        "options" => $surveys_options
                    ],
                    [
                        "name"=>"DELIVERY_SURVEY_QUESTION",
                        "type"=> 'dropdown',
                        "options"=> []
                    ],
                    [
                        "name"=>"DELIVERY_SURVEY_OPTION",
                        "type"=>"dropdown",
                        "options"=> []
                        
                    ],
                    [
                        "name"=>"DELIVERY_SURVEY_ANSWER",
                        "type"=>"text",
                        "class"=>"push_survey_input hide"
                    ]
                ]
                
            ],
            [
                "label" => lang("delivery_time_setting"),
                "type" => "radio",
                "name" => "DELIVERY_TIME_SETTING",
                "class" => "push_delivery_time_setting",
                "default" => 1,
                "options" => [
                    1 => lang("delivery_10"),
                    2 => lang("make_reservation")
                ]
            ],
            [
                "label"=>lang('delivery_date'),
                "name" => "DELIVERY_DATETIME",
                "class"=>"push_delivery_datetime",
                "picker_type"=>"single",
                "type"=>"datetime",
                "default"=>date("Y-m-d H:i:s"),
            ],
            [
                "name"=>'TITLE',
                "type"=>'text',
                "label"=>lang("title")
            ],
            [
                "name"=>'MESSAGE',
                "type"=>'textarea',
                "label"=>lang("message")
            ]
        ];

        $this->table_fields = [
            [
                "label" => lang("correction_confirmation"),
                "type" => "edit",
            ],
            [
                "label" => lang("delete"),
                "type" => "delete"
            ],
            [
                "label" => lang("title"),
                "name" => "TITLE",
                "type" => "text",
            ],
            [
                "label" => lang("delivery_datetime"),
                "name" => "DELIVERY_DATETIME",
                "type" => "datetime",
                "picker_type" => "single",
            ],
            [
                "label" => lang("status"),
                "type" => "radio",
                "name" => "DELIVERY_STATUS",
                "options" => [
                    0 => lang("reserved"),
                    1 => lang("delivered")
                ]
            ],
        ];

        $this->search_fields = [

            [
                "label" => lang("delivery_status"),
                "type" => "radio",
                "name" => "DELIVERY_STATUS",
                "default"=>2,
                "options" => [
                    0 => lang("reserved"),
                    1 => lang("delivered")
                ]
            ],
            [
                "group" => true,
                "label" => lang('delivery_datetime'),//"Publication date",
                "between" => "<span class='between'>~</span>",
                "input" => [
                    [
                        "name" => "FROM",
                        "type" => "date",
                        "class"=>"search-start-date",
                        "picker_type" => "from",
                    ],
                    [
                        "name" => "TO",
                        "type" => "date",
                        "class"=>"search-end-date",
                        "picker_type" => "to",
                    ]
                ]
            ],
            [
                "label" => lang("title"),
                "name" => "EVENT_TITLE",
                "type" => "text",
            ],
            [
                "label" => lang('status'),
                "name" => 'STATUS_SHOW',
                "type" => 'checkbox',
                "checked" => 'Y',
                "unchecked" => 'N',
                "text" => lang("valid_only"), //lang('valid_only'),//'Only when displayed'
            ]
        ];
        $this->preview_url = site_url("preview/main/pushinfo");
    }

    protected function build_query($params)
    {
        $date = date("Y-m-d H:i:s");
        $this->db->select($this->table . ".*, " .
            "events.TITLE AS EVENT_TITLE, coupon.COUPON_TITLE, survey.TITLE AS SURVEY_TITLE, " .
            "CONCAT(IF((push_notification.DELIVERY_TYPE = 1), events.TITLE, ''), " .
            "IF((push_notification.DELIVERY_TYPE = 2), coupon.COUPON_TITLE, ''), " .
            "IF((push_notification.DELIVERY_TYPE = 3), events.TITLE, ''), " .
            "IF((push_notification.DELIVERY_TYPE = 4), survey.TITLE, '')) AS EVENT_TITLE1, " .
            "CONCAT(IF((push_notification.DELIVERY_TYPE = 1), events.PUBLIC_PERIOD_SETTING, ''), " .
            "IF((push_notification.DELIVERY_TYPE = 2), coupon.STATUS_SHOW, ''), " .
            "IF((push_notification.DELIVERY_TYPE = 3), events.PUBLIC_PERIOD_SETTING, ''), " .
            "IF((push_notification.DELIVERY_TYPE = 4), survey.VIEW_YN, '')) AS STATUS_SHOW, " .
            "('$date' > DELIVERY_DATETIME) AS DELIVERY_STATUS");
        $this->db->join("events", "events.ID = push_notification.REFER_ID", "left");
        $this->db->join("coupon", "coupon.ID = push_notification.REFER_ID", "left");
        $this->db->join("survey", "survey.ID = push_notification.REFER_ID", "left");

        $this->db->where('push_notification.BRANCH_ID', $this->auth['branch_id']);

        $TITLE = element("TITLE", $params);
        if ($TITLE != NULL) {
            $this->db->having("TITLE", "%$TITLE%");
        }

        $DELIVERY_STATUS = element("DELIVERY_STATUS", $params);

        if ($DELIVERY_STATUS != NULL) {
            $this->db->having('DELIVERY_STATUS', $DELIVERY_STATUS);
        }

        $FROM = element("FROM", $params);
        if ($FROM != NULL) {
            $this->db->where("DELIVERY_DATETIME >=", $FROM);
        }

        $TO = element("TO", $params);
        if ($TO != NULL) {
            $this->db->where("DELIVERY_DATETIME <=", $TO);
        }

        $STATUS_SHOW = element("STATUS_SHOW", $params);
        if ($STATUS_SHOW == "Y") {
            $this->db->having("STATUS_SHOW", $STATUS_SHOW);
        }
    }

    protected function count_rows($params) {
        $this->build_query($params);
        $this->db->select("COUNT(*) AS cnt");
        $result = $this->db->get($this->table)->row_array();
        if ($result != NULL) element("cnt", $result);
        return 0;
    }
}
