<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cardcolor extends ADMIN_Controller
{
    protected $table = "membercard_settings";
    protected $route_prefix = 'admin/design/cardcolor/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
//    protected $page_title = "Design Settings";
//    protected $page_subtitle = "Card color settings";
//    protected $panel_title = "Membership card page color settings";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
//    protected $fields = [
//        [
//            "name"=>"BACK_COLOR",
//            "type"=>"color",
//            "label"=>"Background color",
//            "help"=>"You can set the background color of the membership card.",
//        ],
//        [
//            "name"=>"FONT_COLOR",
//            "type"=>"color",
//            "label"=>"Letter color",
//            "help"=>"You can set the text color of the membership card.",
//        ]
//    ];
    protected $preview_url;
    
    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)
    
    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;
    
    //Publish settings
//    protected $form_description="You can choose the text color and background color to use for your membership card.";
    //Success settings
//    protected $success_return_text = "戻る";
    
    public function __construct()
    {
        parent::__construct();
        
        $this->login_check();
    }
    
    protected function initialize() {
        $this->load->model('Cardcolor_model', 'model');
        $this->head['title'] = "Design Settings";
        $this->head["description"] = "";
        $this->head['bread_title'] = lang("design_setting");

        //Language setting
        $this->page_title = lang('cardcolor_design_settings');//"Design Settings";
        $this->page_subtitle = lang('cardcolor_settings');//"Card color settings";
        $this->panel_title = lang('cardcolor_membership');//"Membership card page color settings";
        $this->fields = [
            [
                "name"=>"BACK_COLOR",
                "type"=>"color",
                "label"=>lang('cardcolor_background_color'),//"Background color",
                "help"=>lang('cardcolor_help_background_color')//"You can set the background color of the membership card.",
            ],
            [
                "name"=>"FONT_COLOR",
                "type"=>"color",
                "label"=>lang('cardcolor_font_color_letter_color'),//"Letter color",
                "help"=>lang('cardcolor_help_font_color_letter_color')//"You can set the text color of the membership card.",
            ]
        ];
        ///////confirm page
        $this->confirm_fields = [
            [
                "name"=>"BACK_COLOR",
                "type"=>"color",
                "label"=>lang('cardcolor_background_color'),//"Background color",
                "help"=>lang('cardcolor_help_background_color')//"You can set the background color of the membership card.",
            ],
            [
                "name"=>"FONT_COLOR",
                "type"=>"color",
                "label"=>lang('cardcolor_font_color_letter_color'),//"Letter color",
                "help"=>lang('cardcolor_help_font_color_letter_color')//"You can set the text color of the membership card.",
            ]
        ];
        $this->form_description=lang('cardcolor_form_description');//"You can choose the text color and background color to use for your membership card.";
        //Success settings
        $this->success_return_text =lang('cardcolor_return');// "Return";
        //language setting ended
        $this->load->model('Cardcolor_model', 'model');
        $this->head['title'] = "Design Settings";
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/cardlayout");
    }
}
