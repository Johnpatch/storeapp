<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Agency extends ADMIN_Controller
{
    protected $table = "branches";

    protected $route_prefix = 'admin/basicsetting/agency/';//Don't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
    protected $index_has_search = false;
    protected $page_title = "";
    protected $panel_title = "";
    protected $confirm_script_url = "";
   
    //Indicates whether index page has preview
    protected $index_has_preview = false;

    //Indicates whether publish page has preview
    protected $publish_has_preview = false;
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $fields = [
        
    ];

    protected $preview_url;

    protected $search_fields = array();//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields = [
      
    ];

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->head['bread_title'] = lang("user_info_setting");
        $this->page_title = lang('user_info_setting');//"Basic information";
        $this->panel_title = lang('user_info_setting');//"";
        $this->fields = [
            [
                "name"=>"LOGIN_ID",
                "type"=>"text_readonly",
                "label"=>"ID",
                "required"=> true,
                "rules" => 'required'
            ],
            [
                "name"=>"PASSWORD",
                "type"=>"password",
                "label"=>lang("password")
            ],
            [
                "name"=>"MEMBER_FUNCTION",
                "label"=>lang("member_function"),
                "type"=>"radio",
                "default"=>"OFF",
                "inline"=>true,
                "options"=>[
                    "ON"=>lang("on"),
                    "OFF"=>lang("off")
                ]
            ],
            [
                "label"=>lang('member_option'),
                "group"=>true,
                "name"=>"MEMBER_DETAIL",
                "input"=>[
                    [
                        "name"=>"MEMBER_DETAIL",
                        "type"=>"radio",
                        "inline"=>true,
                        "options"=>[
                            "1"=>lang('id_password_function'),
                            "2"=>lang('password_function')
                        ]
                    ],
                    [
                        "name"=>"MEMBER_PASSWORD",
                        "label"=>lang('password_function'),
                        "type"=>"text"
                    ]
                ] 
            ],
            [
                "name"=>"ALL_APP",
                "label"=>lang('member_option'),
                "type"=>"radio",
                "default"=>1,
                "inline"=>true,
                "options"=>[
                    1=>lang("all_app"),
                    2=>lang("part_app")
                ]
            ]
            /*[
                "name"=>"EMAIL",
                "type"=>"text",
                "label"=>lang("email"),
            ],
            [
                "name"=>"LANGUAGE",
                "type"=>"dropdown",
                "label"=>lang("language"),
                "default"=>"jp",
                "options"=>[
                    "jp"=>lang("lang_jp"),
                    "en"=>lang("lang_en"),
                    "cn"=>lang("lang_cn"),
                    "thai"=>lang("lang_thai")
                ]
            ],*/
        
        ];
        $this->form_description= lang('user_info_change');
        //Success settings
        $this->success_return_text = lang('basicsetting_agency_success_return_text');//"Return";
        //language setting ended

        // $this->load->model('Agency_setting_model', 'model');
        $this->head['title'] = lang("basicsetting_agency_panel_title");
        $this->head["description"] = "";
        // $this->preview_url = site_url("preview/main/layout");
    }
}
