<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Question extends ADMIN_Controller
{
    protected $table = "survey_question";
    protected $route_prefix = 'admin/question/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $index_has_preview = false;
    protected $publish_has_preview = false;

    // protected $publish_script_url = "survey/layout";
    // protected $confirm_script_url = "survey/confirm";
    /*
         * Input Fields Description
         * name: input name----Must be exactly equal to corresponding DB column name
         * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
         * default: default value if initial value is not set
         * label: input label description
         * help: help block description
         * required: whether this field is requried or not
         * showIf: array(key=>value). shows this element when (key=value)
         *          value can take array values.
         * rules: validation settings
         */
    protected $fields;
    protected $preview_url;

    protected $search_fields;
     //used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function get_coupon_options() {
        $coupon_array = array();
        $coupon_array["0"] = "No Coupon";

        $rows = $this->db->where("BRANCH_ID", $this->auth["branch_id"])->get("coupon")->result_array();
        foreach ($rows as $row) {
            $coupon_array[$row["ID"]] = $row["COUPON_TITLE"];
        }

        return $coupon_array;
    }

    protected function initialize() {
        // $this->load->model('Survey_model', 'model');
        $this->head['title'] = "Design Settings";
        $this->head["description"] = "";
     $this->page_title = lang("create_new_survey");
     $this->page_subtitle = lang("create_a_new");
     $this->panel_title = lang("questionnaire");


        $this->table_fields= [
            [
                "label"=>lang("fix"),
                "type"=>"edit",
                "width" => 150
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete",
                "width" => 150
            ],
            [
                "label"=>lang("title"),
                "type"=>"text",
                "name"=>"TITLE"
            ],
            [
                "label"=>lang("coupon_setting"),
                "type"=>"dropdown",
                "name"=>"COUPON_ID",
                "options"=> $coupon_array,
            ],
            [
                "group"=>true,
                "label"=>lang("period"),
                "between"=>"<br>-<br>",
                "input"=>[
                    [
                        "name"=>"START_DATE",
                        "type"=>"date",
                    ],

                    [
                        "name"=>"END_DATE",
                        "type"=>"date",
                    ]
                ]    
            ],
            [
                "label"=>lang("status"),
                "type"=>"text",
                "name"=>"VIEW_YN"
            ],
        ];


        // $this->preview_url = site_url("preview/newsevents/");
    }

    public function publish($id) {

    }

    public function aggregate($page = 0) {
        $coupon_array = $this->get_coupon_options();
        $search_fields = [
            [
                "name"=>"COUPON_ID",
                "type"=>"dropdown",
                "label"=>lang("COUPON"),
                "options"=> $coupon_array,
                "help"=>"Please select a coupon to distribute.",
            ],
            [
                "group" => true,
                "label" => lang('aggregation_period'),//"Publication date",
                "between"=>"<span class='between'>~</span>",
                "input" => [
                    [
                        "name" => "FROM",
                        "type" => "date",
                        "picker_type" => "from",
                    ],
                    [
                        "name" => "TO",
                        "type" => "date",
                        "picker_type" => "to",
                    ]
                ]
            ],
        ];
        $user = $this->auth;
        if ($this->has_index == false) {
            $this->redirect('publish');
        }
        $data = array();

        $input = $this->input->get();
        $perpage = $this->input->get('perpage');
        if ($perpage == 0) {
            $perpage = $this->perpage;
        }
        if ($this->index_has_search) {
            $this->set_field_values($search_fields, $input);
        }
        $input["perpage"] = $perpage;
        $rowscount = $this->count_rows($input);
        $data['branch_id'] = $user['branch_id'];
        $data['add_url'] = $this->redirect_url('publish/');

        $data["has_order"] = false;
        $data["has_add"] = false;
        $data["delete_url"] = $this->redirect_url('delete/');
        $data["order_url"] = $this->redirect_url('order/');
        $data['rows'] = $this->get_rows($input, $perpage, $page);
        $data['links_pagination'] = pagination($this->redirect_url('aggregate'), $rowscount, $perpage, count(explode("/", $this->route_prefix)) + 1);
        $data["table_fields"] = $this->table_fields;
        $data['search_fields'] = $search_fields;

        $data['has_preview'] = false;
        $data['params'] = $input;
        $data["page_title"] = lang('survey')    ;
        $data["page_subtitle"] = lang('aggregate');//'Aggregate'
        /////////////////////////////title with index
        $data['title_index'] = $this->title_index;
        //
        $data["form_description"] = lang('you_can_check_the_number_of_survey');
        $data["list_description"] = "";
        $data["perpage_options"] = $this->perpage_options;
        $data["perpage"] = $perpage;
        $data['has_search'] = true;
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/list', $data);
        $this->load->view('_parts/footer');
    }
}
