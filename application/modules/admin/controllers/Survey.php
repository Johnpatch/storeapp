<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Survey extends ADMIN_Controller
{
    protected $table = "survey";
    protected $route_prefix = 'admin/survey/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $index_has_preview = false;
    protected $publish_has_preview = false;
    protected $has_questions = true;
    protected $has_add = true;
    protected $edit_title = '';
    // protected $publish_script_url = "survey/layout";
    // protected $confirm_script_url = "survey/confirm";
    /*
         * Input Fields Description
         * name: input name----Must be exactly equal to corresponding DB column name
         * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
         * default: default value if initial value is not set
         * label: input label description
         * help: help block description
         * required: whether this field is requried or not
         * showIf: array(key=>value). shows this element when (key=value)
         *          value can take array values.
         * rules: validation settings
         */
    protected $fields;
    protected $preview_url;

    protected $search_fields;
    //used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;

    //Publish settings
    protected $form_description = "";
    //Success settings
    protected $success_return_text = "戻る";

    protected $question_fields;

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }

    protected function get_coupon_options()
    {
        $coupon_array = array();
        $coupon_array["0"] = "No Coupon";

        $rows = $this->db->where("BRANCH_ID", $this->auth["branch_id"])->get("coupon")->result_array();
        foreach ($rows as $row) {
            $coupon_array[$row["ID"]] = $row["COUPON_TITLE"];
        }

        return $coupon_array;
    }

    protected function initialize()
    {

        $this->page_title = lang("survey");
        $this->page_subtitle = lang("list_reg");
        $this->panel_title = lang("list_reg");
        $this->head['title'] = lang("survey");
        $this->head["description"] = "";
        $this->head['bread_title'] = lang("survey");
        $this->head['page_subtitle'] = lang("survey_setting");
        $this->edit_title = lang("list_reg");

        $question_array = array();
        $rows = $this->db->where("BRANCH_ID", $this->auth["branch_id"])->get("survey")->result_array();
        foreach ($rows as $row) {
            $question_array[$row["ID"]] = $row["TITLE"];
        }
        $coupon_array = $this->get_coupon_options();
        $this->search_fields = [
            /*[
                "group" => true,
                "label" => lang("period"),
                "between" => "<span class='between'>~</span>",
                "input" => [
                    [
                        "name" => "START_DATE",
                        "type" => "date",
                        "class" => "search-start-date",
                        "picker_type" => "from",
                    ],

                    [
                        "name" => "END_DATE",
                        "type" => "date",
                        "class" => "search-end-date",
                        "picker_type" => "to",
                    ]
                ]
            ],*/
            [
                "label" => lang("title"),
                "type" => "text",
                "name" => "TITLE"
            ],
            [
                "label"=>lang('status'),
                "name"=>'VIEW_YN',
                "type"=>'checkbox',
                "checked"=>'Y',
                "unchecked"=>'N',
                "text"=>lang("valid_only"),//'Only when displayed'
            ]
        ];
        $this->table_fields = [
            [
                "label" => lang("fix"),
                "type" => "edit",
            ],
            [
                "label" => lang("delete"),
                "type" => "delete"
            ],
            [
                "label" => lang("title"),
                "type" => "text",
                "name" => "TITLE"
            ],
            /*[
                "label" => lang("coupon_setting"),
                "type" => "dropdown",
                "name" => "COUPON_ID",
                "options" => $coupon_array,
            ],
            [
                "group" => true,
                "label" => lang("period"),
                "between" => "<br>-<br>",
                "input" => [
                    [
                        "name" => "START_DATE",
                        "type" => "date",
                    ],

                    [
                        "name" => "END_DATE",
                        "type" => "date",
                    ]
                ]
            ],*/
            [
                "name" => "VIEW_YN",
                "type" => "radio",
                "label" => lang("publishing_settings"),
                "default" => "Y",
                "options" => [
                    "Y" => lang('indicate'),
                    'N' => lang('hidden')
                ],
            ],
        ];

        $this->fields = [
            /*[
                "name" => "COUPON_ID",
                "type" => "dropdown",
                "label" => lang("COUPON"),
                "options" => $coupon_array,
                "help" => lang("coupon_to_distribute"),
            ],
            [
                "name" => "FAVORITE_STORE_YN",
                "type" => "radio",
                "default" => 'Y',
                "options" => [
                    "Y" => lang('indicate'),
                    'N' => lang('hidden')
                ],
                "label" => lang("fav_store_reg"),
                "help" => lang("answering_the_questionnare")
            ],*/
            [
                "name" => "TITLE",
                "type" => "text",
                "label" => lang("title"),
                "required" => true,
                "rules" => "required",
            ],
            [
                "name" => "DESCRIPTION",
                "type" => "textarea",
                "label" => lang("questionnare_explanation"),
                "required" => true,
                "rules" => "required",
            ],
            [
                "group" => true,
                "label" => lang("survey_period"),
                "required" => true,
                "between" => "<span class=''>~</span>",
                "input" => [
                    [
                        "name" => "START_DATE",
                        "rules" => "required",
                        "type" => "date",
                        "class" => "search-start-date",
                        "default" => date("Y-m-d"),
                        "picker_type" => "from",
                    ],

                    [
                        "name" => "END_DATE",
                        "rules" => "required",
                        "type" => "date",
                        "class" => "search-end-date",
                        "default" => date("Y-m-d", strtotime(date(date('Y') . 'm', strtotime('+1 month')) . date('d'))),
                        "picker_type" => "to",
                    ]
                ]
            ],
            [
                "name" => "VIEW_YN",
                "type" => "radio",
                "label" => lang("publishing_settings"),
                "default" => "Y",
                "options" => [
                    "Y" => lang('indicate'),
                    'N' => lang('hidden')
                ],

            ],
        ];

        $this->question_fields = [
            [
                "group" => true,
                "label" => lang("question_number"),
                "input" => [
                    [
                        "name" => "SURVEY_NO",
                        "type" => "static",
//                        "default" => $no
                    ],
                    [
                        "name" => "SURVEY_NO",
                        "type" => "hidden",
//                        "default"=> $no
                    ],
                ]
            ],
            [
                "group" => true,
                "label" => lang("publishing_settings"),
                "input" => [
                    [
                        "type" => "static",
                        "name" => 'STATUS_SHOW',
                        "default" => 'Y',
                        "options" => [
                            'Y' => lang("indicate"),
                            "N" => lang("hidden")
                        ]
                    ],
                    [
                        "type" => "hidden",
                        "name" => 'STATUS_SHOW',
                        "default" => 'Y'
                    ]
                ]
            ],
            [
                "label" => lang("type"),
                "name" => "ANSWER_TYPE",
                "type" => "radio",
                "default" => 1,
                "options" => [
                    1 => lang("selection_type"),
                    2 => lang("choice_type"),
                    3 => lang("input_1line"),
                    4 => lang("input_multiline")
                ]
            ],
            [
                "label" => lang("question_content"),
                "name" => "QUESTION",
                "type" => "textarea",
                "required" => true,
                "rules" => "required"
            ],
            [
                "label" => lang("input_type"),
                "name" => "INPUT_TYPE",
                "type" => "radio",
                "default" => 1,
                "options" => [
                    1 => lang("required"),
                    2 => lang("any")
                ]
            ],
        ];
        // $this->preview_url = site_url("preview/newsevents/");
    }

    public function success($id = 0) {
        $data['page_title'] = $this->page_title;
        $data['back_text'] = $this->success_return_text;
        $data['back_url'] = site_url($this->has_index == true ? $this->route_prefix : $this->route_prefix . "publish");
        $data["ID"] = $id;
        $data["edit_url"] = $this->redirect_url("publish/$id");
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->load->view('_parts/header', $this->head);
        $this->load->view('survey/success', $data);
        $this->load->view('_parts/footer');
    }

    protected function build_query($params)
    {
        $this->db->where('branch_id', $this->auth['branch_id']);
        $date = date("Y-m-d");
        $this->db->select("*, (('$date' >= START_DATE) + ('$date' <= END_DATE) * 2) as HOLDING_STATUS");

        $VIEW_YN = element("VIEW_YN", $params);
        if ($VIEW_YN == 'Y') {
            $this->db->where("VIEW_YN", "Y");
        }

        $TITLE = element("TITLE", $params);
        if ($TITLE != NULL) {
            $this->db->like("TITLE", $TITLE);
        }

        $START_DATE = element("START_DATE", $params);
        if ($START_DATE != NULL) {
            $this->db->where("START_DATE >=", $START_DATE);
        }

        $END_DATE = element("END_DATE", $params);
        if ($END_DATE != NULL) {
            $this->db->where("END_DATE <=", $END_DATE);
        }
    }

    public function aggregate($page = 0)
    {
        $coupon_array = $this->get_coupon_options();
        $search_fields = [
            [
                "name" => "COUPON_ID",
                "type" => "dropdown",
                "label" => lang("COUPON"),
                "options" => $coupon_array,
                "help" => lang("coupon_to_distribute"),
            ],
            [
                "group" => true,
                "label" => lang('aggregation_period'),//"Publication date",
                "between" => "<span class='between'>~</span>",
                "input" => [
                    [
                        "name" => "FROM",
                        "type" => "date",
                        "class" => "search-start-date",
                        "picker_type" => "from",
                    ],
                    [
                        "name" => "TO",
                        "type" => "date",
                        "class" => "search-end-date",
                        "picker_type" => "to",
                    ]
                ]
            ],
        ];
        $user = $this->auth;
        if ($this->has_index == false) {
            $this->redirect('publish');
        }
        $data = array();

        $input = $this->input->get();
        $perpage = $this->input->get('perpage');
        if ($perpage == 0) {
            $perpage = $this->perpage;
        }
        if ($this->index_has_search) {
            $this->set_field_values($search_fields, $input);
        }
        $input["perpage"] = $perpage;
        $rowscount = $this->count_rows($input);
        $data["branch_id"] = $this->auth['branch_id'];
        $data['add_url'] = $this->redirect_url('publish/');

        $data["has_order"] = false;
        $data["has_add"] = true;
        $data["delete_url"] = $this->redirect_url('delete/');
        $data["order_url"] = $this->redirect_url('order/');
        $data['rows'] = $this->get_rows($input, $perpage, $page);
        $data['links_pagination'] = pagination($this->redirect_url('aggregate'), $rowscount, $perpage, count(explode("/", $this->route_prefix)) + 1);
        $data["table_fields"] = $this->table_fields;
        $data['search_fields'] = $search_fields;

        $data['has_preview'] = false;
        $data['params'] = $input;
        $data["page_title"] = lang('survey');
        $data["page_subtitle"] = lang('aggregate');//'Aggregate'
        /////////////////////////////title with index
        $data['title_index'] = $this->title_index;
        //
        $data["form_description"] = lang('you_can_check_the_number_of_survey');
        $data["list_description"] = "";
        $data["perpage_options"] = $this->perpage_options;
        $data["perpage"] = $perpage;
        $data['has_search'] = true;
        
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/list', $data);
        $this->load->view('_parts/footer');
    }

    public function publish_question($survey_id, $no = 0)
    {
        $user = $this->auth;
        $table = "survey_questions";
        $old = $this->session->flashdata('old');
        $row = $this->db->where("SURVEY_ID", $survey_id)->where("SURVEY_NO", $no)->get($table)->row_array();
        if (is_array($old)) $row = $old;
        if ($row == NULL) $row = array();        
        $is_exist = 1;
        if($no == 0)
            $is_exist = 0;
        $no = $no > 0 ? $no : $this->db->where("SURVEY_ID", $survey_id)->count_all_results($table) + 1;
        $fields = $this->question_fields;
        $fields[0]["input"][0]["default"] = $no;
        $fields[0]["input"][1]["default"] = $no;

        $options = array();
        if($no != 0 && $is_exist != 0){
            $question_id = $row['ID'];
            $options = $this->db->where("SURVEY_QUESTION_ID", $question_id)->where("SURVEY_NO", $no)->get('survey_options')->result_array();
        }

        $this->set_field_values($fields, $row);
        $errors = $this->session->flashdata("errors");
        if ($errors) {
            $this->get_errors($fields, $errors);
        }

        $data["fields"] = $fields;
        $data["confirm_url"] = $this->redirect_url("confirm_question/$survey_id") . ($no > 0 ? "/$no" : "");
        $data["page_title"] = lang("survey");
        $data["page_subtitle"] = lang('stampimageadd_page_space');
        $data["panel_title"] = lang("survey");
        $data["form_description"] = lang("survey_desc");
        $data['script_url'] = $this->publish_script_url;
        $data['has_preview'] = false;
        $data['has_questions'] = false;
        $data["errors"] = $this->session->flashdata("errors");
        $data['survey_options'] = $options;
        $data['is_survey_option'] = true;
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/publish', $data);
        $this->load->view('_parts/footer');
    }

    public function confirm_question($survey_id, $no = 0)
    {
        $input = $this->input->post();

        $this->session->set_flashdata('old', $input);
        $data = $input;
        $back_url = $input["back_url"];
        $this->set_field_values($this->question_fields, $input);
        $this->set_validation_rules($this->question_fields);

        if ($this->form_validation->run() == FALSE) {
            $error = array();
            $this->set_errors($this->question_fields, $error);

            if (count($error) > 0) {
                $this->session->set_flashdata("errors", $error);
                redirect($back_url);
            }
        }

        $data["fields"] = $this->question_fields;
//        $data["row"] = $row;
        $data["page_title"] = "Survey";
        $data["page_subtitle"] = 'Create new';
        $data["panel_title"] = "Questionnaire setting";
        $data["form_description"] = "Create a new questionnaire for survey";
        $data['script_url'] = $this->publish_script_url;
        $data["save_url"] = $this->redirect_url("save_question/$survey_id" . ($no > 0 ? "/$no" : ""));
        $data['survey_options'] = $input['option_value'];
        $data['is_survey_option'] = true;
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/confirm', $data);
        $this->load->view('_parts/footer');
    }

    public function save_question($survey_id, $no = 0)
    {
        $input = $this->input->post();

        $this->get_non_array_inputs_by_fields($this->question_fields, $non_array_input, $input);
        $non_array_input["SURVEY_ID"] = $survey_id;
        $non_array_input["SURVEY_NO"] = $no;
        $table = "survey_questions";
        $question_id = 0;
        if ($this->db->where("SURVEY_ID", $survey_id)->where("SURVEY_NO", $no)->count_all_results($table) > 0) {
            $this->db->set($non_array_input)->where("SURVEY_ID", $survey_id)->where("SURVEY_NO", $no)->update($table);
            $result = $this->db->where("SURVEY_ID", $survey_id)->where("SURVEY_NO", $no)->get($table)->row_array();
            $question_id = $result['ID'];
        } else {
            $this->db->insert($table, $non_array_input);
            $question_id = $this->db->insert_id();
        }
        $option_value = $input['option_value'];
        $this->db->where('SURVEY_NO', $no);
        $this->db->where('SURVEY_QUESTION_ID', $question_id);
        $this->db->delete('survey_options');
        foreach($option_value as $option){
            if(!empty($option)){
                $data = array(
                    'SURVEY_QUESTION_ID' => $question_id,
                    'OPTION_VALUE' => $option,
                    'SURVEY_NO' => $no
                );
                $this->db->insert('survey_options', $data);
            }
            
        }
        $this->get_flush_session_data($this->token, 'old');
        $this->redirect("success_question/$survey_id");
    }

    public function success_question($survey_id)
    {

        $data['page_title'] = lang('Survey');
        $data['back_text'] = lang("to_list_question");
        $data['back_url'] = $this->redirect_url("publish/$survey_id?token=".$this->token);
        $this->load->view('_parts/header', $this->head);
        $this->load->view('_parts/success', $data);
        $this->load->view('_parts/footer');
    }

    public function order_question($survey_id)
    {
        $input = $this->input->post();
        $orders = json_decode($input["orders"]);
        $len = count($orders);
//        var_dump($input);
        $this->db->trans_start();
        $table = "survey_questions";
        for ($i = 0; $i < $len; $i++) {
            $id = $orders[$i];
            $no = $i + 1;
            $status = $input["STATUS_SHOW$id"];
            $this->db->query("UPDATE $table SET `SURVEY_NO` = $no, `STATUS_SHOW`='$status' WHERE `ID`=$id");
        }
        $this->db->trans_complete();
        $this->redirect("publish/$survey_id");
    }
}
