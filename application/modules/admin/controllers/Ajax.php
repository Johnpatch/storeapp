<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Ajax extends ADMIN_Controller
{
    public function get_page_options()
    {
        $type = $this->input->post("LINK");
        $branch_id = $this->input->post("BRANCH_ID");
        $ret = array();

        if ($type == 1) {//Event Page
            $rows = $this->db
                ->where("BRANCH_ID", $branch_id)
                ->get("events")
                ->result_array();
            foreach ($rows as $row) {
                $ret[$row["ID"]] = $row["TITLE"];
            }
        } else if ($type == 2) {//Coupon Page
            $rows = $this->db
                ->where("BRANCH_ID", $branch_id)
                ->get("coupon")
                ->result_array();
            foreach ($rows as $row) {
                $ret[$row["ID"]] = $row["COUPON_TITLE"];
            }
        } else if ($type == 4) {//Survey page
            $rows = $this->db
                ->where("BRANCH_ID", $branch_id)
                ->get("survey")
                ->result_array();
            foreach ($rows as $row) {
                $ret[$row["ID"]] = $row["TITLE"];
            }
        } else if($type == 'top_post_content_category'){
            $rows = $this->db
                ->where("BRANCH_ID", $branch_id)
                ->get("post_category")
                ->result_array();
            foreach ($rows as $row) {
                $ret[$row["ID"]] = $row["TITLE"];
            }
        }
        echo json_encode($ret);
    }

    public function get_freecontent()
    {
        $branch_id = $this->input->post("BRANCH_ID");
        $ret = array();

        $rows = $this->db
            ->where("BRANCH_ID", $branch_id)
            ->get("basic_html")
            ->result_array();
        foreach ($rows as $row) {
            $ret[$row["ID"]] = $row["TITLE"];
        }
        echo json_encode($ret);
    }

    public function get_webviewlist()
    {
        $branch_id = $this->input->post("BRANCH_ID");
        $ret = array();

        $rows = $this->db
            ->where("BRANCH_ID", $branch_id)
            ->get("webview")
            ->result_array();
        foreach ($rows as $row) {
            $ret[$row["ID"]] = $row["TITLE"];
        }
        echo json_encode($ret);
    }

    public function get_survey_question_list()
    {
        $branch_id = $this->input->post("BRANCH_ID");
        $id = $this->input->post("ID");
        $ret = array();
        $rows = $this->db
            ->where("SURVEY_ID", $id)
            ->where("STATUS_SHOW", 'Y')
            ->order_by('SURVEY_NO', 'ASC')
            ->get("survey_questions")
            ->result_array();
        $ret['TYPE'] = $rows[0]['ANSWER_TYPE'];
        foreach ($rows as $row) {
            $ret['list'][$row["ID"]] = $row["QUESTION"];
        }
        echo json_encode($ret);
    }

    public function get_survey_option_list()
    {
        $id = $this->input->post("ID");
        $ret = array();
        $rows = $this->db
            ->where("SURVEY_QUESTION_ID", $id)
            ->order_by('ID', 'ASC')
            ->get("survey_options")
            ->result_array();
        foreach ($rows as $row) {
            $ret[$row["ID"]] = $row["OPTION_VALUE"];
        }
        echo json_encode($ret);
    }
   
}
