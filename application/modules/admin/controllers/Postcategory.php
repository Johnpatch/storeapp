<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Postcategory extends ADMIN_Controller
{
    protected $table = "post_category";
    protected $route_prefix = 'admin/postcategory/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $has_order = false;
    protected $has_add = true;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $index_has_preview = false;
    protected $publish_has_preview = false;
    protected $edit_title = "";
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $fields;
    protected $preview_url;

    protected $search_fields = [];
    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */


    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->load->model('Coupon_model', 'model');
        $this->head['title'] = lang("category_reg");
        $this->head["description"] = "";
        $this->page_title = lang("post_content");
        $this->page_subtitle = lang("category_reg");
        $this->panel_title = lang("category_reg");
        $this->form_description='';
        $this->head['bread_title'] = lang("post_content");
        $this->head['page_subtitle'] = '';
        $this->edit_title = lang("category_list");

        $this->search_fields = [
            [
                "name"=>'TITLE',
                "type"=>'text',
                "label"=>lang("category_title")
            ],
        ]; //used only when this controller has index page.(i.e $this->has_index_page = true)


        $this->fields = [
            [
                "name"=>'TITLE',
                "type"=>'text',
                "label"=>lang("category_title")
            ],
            [
                "name"=>"POST_TYPE",
                "type"=>"radio_image",
                "inline"=>true,
                "label"=>lang('post_template'),//"Layout type",
                "default" => 1,
                "options" => [
                    "1"=>["attachments/layout_images/template_1.png", false],
                    "2"=>["attachments/layout_images/template_2.png", false],
                    "3"=>["attachments/layout_images/template_3.png", false],
                    "4"=>["attachments/layout_images/template_4.png", false],
                ],
            ],
            [
                "label" => lang('member_show'),//"device",
                "name" => "IS_MEMBER",
                "type" => "checkbox",
                "text" => '',
                "checked" => "Y",
                "unchecked" => "N",
                "default" => "N",
                "show_text" => lang('indicate'),
                "hide_text" => lang('hidden')
            ],
        ];

        $this->table_fields = [
            [
                "label"=>lang('fix'),
                "type"=>"edit",
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete"
            ],
            [
                "name"=>'TITLE',
                "type"=>'text',
                "label"=>lang("category_title")
            ],
        ];
        
    }

    protected function build_query($params) {
        $this->db->where('branch_id', $this->auth['branch_id']);
        
        $title = element("TITLE", $params);
        if ($title != NULL) {
            $this->db->like('TITLE', $title);
        }

        $order = element("ORDER", $params);
        $order_str = "";
        if($order != NULL){
            if($order == 'ASC'){
                $order_str = "ORDER ASC";
            } else {
                $order_str = "ORDER DESC";
            }
        }

        if ($order_str != "") $this->db->order_by($order_str);
    }
}
