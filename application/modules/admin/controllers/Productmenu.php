<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Productmenu extends ADMIN_Controller
{
    protected $route_prefix = 'admin/productmenu/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $has_order = true;//true if this controller has list pages
    protected $page_title = "New product menu";
    //admin/productmenu/index
    protected $title_index = true;
    protected $page_title1 = "Product menu";
    protected $page_subtitle1 = "Product menu";
    protected $panel_title1 = "Menu list";
    //
    protected $page_subtitle = "Create New";
    protected $panel_title = "Menu creation";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */

    protected $preview_url;
    protected $fields;

    protected $search_fields = [
        [
            "name"=>'MENU_TITLE',
            "type"=>'text',
            "label"=>"title"
        ],
        [
            "name"=>'STATUS_SHOW',
            "checked"=>'Y',
            "type"=>'checkbox',
            "unchecked"=>'N',
            "label"=>'Status',
            "text"=>'Only when publishing'
        ]
    ]; //used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields = [
        [
            "label"=>"Fix",
            "type"=>"edit",
        ],
        [
            "label"=>"Delete",
            "type"=>"delete"
        ],
        [
            "label"=>"Transition source top menu",
            "type"=>"text",
            "name"=>"MENU_TITLE"
        ],
        [
            "label"=>"Title",
            "type"=>"text",
            "name"=>"MENU_TITLE"
        ],
        [
            "label"=>"Thumbnail Image",
            "type"=>"image",
            "name"=>"THUMBNAIL_IMAGE"
        ],
        [
            "label"=>"Status",
            "type"=>"text",
            "name"=>"STATUS_SHOW"
        ],
        [
            "name" => "ORDER",
            "type" => "order",
            "label" => "order",
        ],
    ];

    //Publish settings
    protected $form_description="You can create a product or service introduction page to be displayed in the 'Menu' that is open to the top menu settings.";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->load->model('Productmenu_model', 'model');

            $this->page_title = lang("new_product_menu");
    //admin/productmenu/index
     $this->title_index = true;
     $this->page_title1 = lang("product_menu");
     $this->page_subtitle1 = lang("Product menu");
     $this->panel_title1 = lang("menu_list");
    //
     $this->page_subtitle = lang("create_new");
     $this->panel_title = lang("menu_creation");

        //lang setting
        $this->success_return_text = lang('admin_productmenu_success_return_text');//"Return to menu editing";
        $this->fields = [
            [
                "name"=>"MENU_TITLE",
                "type"=>"text",
                "label"=>lang('admin_productmenu_publish_menu_title'),//"Menu title",
                "required"=>true,
                "rules"=>"required",
                "help"=>lang('admin_productmenu_publish_menu_title_help')//"*25 characters or less <span class='help-tab-gray'>entry example</span> drink menu"
            ],
            [
                "name"=>"SUB_TITLE",
                "type"=>"text",
                "label"=>lang('admin_productmenu_publish_sub_title'),//"subtitle",
                "help"=>lang('admin_productmenu_publish_sub_title_help')//"*25 characters or less <span class='help-tab-gray'>entry example</span> drink menu"
            ],
            [
                "name"=>"DETAIL",
                "type"=>"ckeditor",
                "label"=>lang('admin_productmenu_publish_detail'),//"Menu details",
                "required"=>"true",
                "role"=>"required",
                "help"=>lang('admin_productmenu_publish_detail_help')//"<span class='help-tab-gray'>Format</span>jpeg,jpg,gif,png<br>
                        // <span class='help-tab-gray'>recommended size</span> width 250<br>
                        // <span class='help-tab-gray'>capacity</span> please register the image of less than 300KB"
            ],
            [
                "name"=>"THUMBNAIL_IMAGE",
                "type"=>"file",
                "label"=>lang('admin_productmenu_publish_thumbnail_image'),//"Thumbnail image(jpeg,jpg,gif,png)",
                "help"=>lang('admin_productmenu_publish_thumbnail_image_help')//"<span class='help-tab-gray'>Format</span>jpeg,jpg,gif,png Please register an image with<br>
                        // <span class='help-tab-gray'>recommended size</span> 600*600<br>
                        // <span class='help-tab-gray'>capacity</span> 300KB or less"
            ],
            [
                "name"=>"MENU_IMAGE",
                "type"=>"file",
                "label"=>lang('admin_productmenu_publish_menu_image'),//"Menu image(jpeg,jpg,gif,png)",
                "help"=>lang('admin_productmenu_publish_menu_image_help')//"<span class='help-tab-gray'>Format</span>jpeg,jpg,gif,png Please register an image with<br>
                        // <span class='help-tab-gray'>recommended size</span> 600*600<br>
                        // <span class='help-tab-gray'>capacity</span> 300KB or less"
            ],
            [
                "name"=>"STATUS_SHOW",
                "type"=>"radio",
                "label"=>lang('admin_productmenu_publish_status_show'),//"Publishing Settings",
                "default"=>"Y",
                "options"=>[
                    "Y"=>lang('admin_productmenu_publish_status_show_y'),//"Indicate",
                    "N"=>lang('admin_productmenu_publish_status_show_n')//"Hidden"
                ]
            ],
            [
                "name"=>"ORDER",
                "type"=>"text",
                "label"=>lang('admin_productmenu_publish_order'),//"Menu display order",
                "required"=>true,
                "role"=>"required",
                "help"=>lang('admin_productmenu_publish_order_help')//"*The smaller number is displayed first.<br>
                        //(Example:0->1->2->3->4)"
            ],
        ];
        $this->confirm_fields = [
            [
                "name"=>"MENU_TITLE",
                "type"=>"text",
                "label"=>lang('confirm_admin_productmenu_publish_menu_title'),//"Menu title",
                "required"=>false,
                "rules"=>"required",
                "help"=>lang('confirm_admin_productmenu_publish_menu_title_help')//"*25 characters or less <span class='help-tab-gray'>entry example</span> drink menu"
            ],
            [
                "name"=>"SUB_TITLE",
                "type"=>"text",
                "label"=>lang('confirm_admin_productmenu_publish_sub_title'),//"subtitle",
                "help"=>lang('confirm_admin_productmenu_publish_sub_title_help')//"*25 characters or less <span class='help-tab-gray'>entry example</span> drink menu"
            ],
            [
                "name"=>"DETAIL",
                "type"=>"ckeditor",
                "label"=>lang('confirm_admin_productmenu_publish_detail'),//"Menu details",
                "required"=>false,
                "role"=>"required",
                "help"=>lang('confirm_admin_productmenu_publish_detail_help')//"<span class='help-tab-gray'>Format</span>jpeg,jpg,gif,png<br>
                        // <span class='help-tab-gray'>recommended size</span> width 250<br>
                        // <span class='help-tab-gray'>capacity</span> please register the image of less than 300KB"
            ],
            [
                "name"=>"THUMBNAIL_IMAGE",
                "type"=>"file",
                "label"=>lang('confirm_admin_productmenu_publish_thumbnail_image'),//"Thumbnail image",
                "help"=>lang('confirm_admin_productmenu_publish_thumbnail_image_help')//"<span class='help-tab-gray'>Format</span>jpeg,jpg,gif,png Please register an image with<br>
                        // <span class='help-tab-gray'>recommended size</span> 600*600<br>
                        // <span class='help-tab-gray'>capacity</span> 300KB or less"
            ],
            [
                "name"=>"MENU_IMAGE",
                "type"=>"file",
                "label"=>lang('confirm_admin_productmenu_publish_menu_image'),//"Menu image",
                "help"=>lang('confirm_admin_productmenu_publish_menu_image_help')//"<span class='help-tab-gray'>Format</span>jpeg,jpg,gif,png Please register an image with<br>
                        // <span class='help-tab-gray'>recommended size</span> 600*600<br>
                        // <span class='help-tab-gray'>capacity</span> 300KB or less"
            ],
            [
                "name"=>"STATUS_SHOW",
                "type"=>"radio",
                "label"=>lang('confirm_admin_productmenu_publish_status_show'),//"Publishing Settings",
                "default"=>"Y",
                "options"=>[
                    "Y"=>lang('confirm_admin_productmenu_publish_status_show_y'),//"Indicate",
                    "N"=>lang('confirm_admin_productmenu_publish_status_show_n')//"Hidden"
                ]
            ],
            [
                "name"=>"ORDER",
                "type"=>"text",
                "label"=>lang('confirm_admin_productmenu_publish_order'),//"Order",
                "required"=>false,
                "role"=>"required",
                "help"=>lang('confirm_admin_productmenu_publish_order_help')//"*The smaller number is displayed first.<br>
                        //(Example:0->1->2->3->4)"
            ],
        ];
        
        $this->table_fields = [
            [
                "label"=>lang('admin_productmenu_index_fix'),//"Fix",
                "type"=>"edit",
            ],
            [
                "label"=>lang('admin_productmenu_index_delete'),//"Delete",
                "type"=>"delete"
            ],
            [
                "label"=>lang('admin_productmenu_index_transition_source'),//"Transition source top menu",
                "type"=>"text",
                "name"=>"MENU_TITLE"
            ],
            [
                "label"=>lang('admin_productmenu_index_title'),//"Title",
                "type"=>"text",
                "name"=>"MENU_TITLE"
            ],
            [
                "label"=>lang('admin_productmenu_index_thumbnail_image'),//"Thumbnail Image",
                "type"=>"image",
                "name"=>"THUMBNAIL_IMAGE"
            ],
            [
                "label"=>lang('admin_productmenu_index_status_show'),//"Status",
                "type"=>"text",
                "name"=>"STATUS_SHOW"
            ],
            [
                "name" => "ORDER",
                "type" => "order",
                "label" => lang('admin_productmenu_index_order'),//"order",
            ],
        ];
        $this->search_fields = [
            [
                "name"=>'MENU_TITLE',
                "type"=>'text',
                "label"=>lang('admin_productmenu_search_menu_title')//"title"
            ],
            [
                "name"=>'STATUS_SHOW',
                "checked"=>'Y',
                "type"=>'checkbox',
                "unchecked"=>'N',
                "label"=>lang('admin_productmenu_search_status_show_status'),//'Status',
                "text"=>lang('admin_productmenu_search_status_show_publishing')//'Only when publishing'
            ]
        ];
        $this->head['title'] = lang('admin_productmenu_design_settings');//"Design Settings";
        $this->head["description"] = lang('admin_productmenu_space');//"";
        $this->page_title = lang('admin_productmenu_new_product_menu');//"New product menu";
        $this->page_title1 = lang('admin_productmenu_product_menu');//"Product menu";
        $this->page_subtitle1 = lang('admin_productmenu_sub_product_menu');"Product menu";
        $this->panel_title1 = lang("admin_productmenu_sub_menu_list");//"Menu list";
    
        $this->page_subtitle = lang("admin_productmenu_create_new");//"Create New";
        $this->panel_title = lang('admin_productmenu_menu_creation');//"Menu creation";
        $menus = $this->db
            ->select("ID, MENU_NAME")
            ->where("PRODUCT_MENU_YN", "Y")
            ->get("topmenu_settings")
            ->result_array();
        $parent_options = array();
        foreach ($menus as $menu) {
            $parent_options[$menu["ID"]] = $menu["MENU_NAME"];
        }

        $this->fields = [
            [
                "name"=>"MENU_TITLE",
                "type"=>"text",
                "label"=>lang("menu_title"),
                "required"=>true,
                "rules"=>"required",
                "help"=>lang("productmenu_menutitle_help")
            ],
            [
                "name"=>"SUB_TITLE",
                "type"=>"text",
                "label"=>lang("subtitle"),
                "help"=>lang("productmenu_subtitle_help")
            ],
            [
                "name"=>"PARENT_MENU_ID",
                "type"=>"dropdown",
                "label"=>lang("top_menu_button"),
                "options" => $parent_options,
            ],
            [
                "name"=>"DETAIL",
                "type"=>"ckeditor",
                "label"=>lang("menu_details"),
                "required"=>true,
                "rules"=>"required",
                "help"=>lang("productmenu_detail_help")
            ],
            [
                "name"=>"THUMBNAIL_IMAGE",
                "type"=>"file",
                "label"=>lang("productmenu_imagetype"),
                "help"=>lang("productmenu_image_help")
            ],
            [
                "name"=>"MENU_IMAGE",
                "type"=>"file",
                "label"=>lang("productmenu_menuimagetype"),
                "help"=>lang("productmenu_menuimagehelp")
            ],
            [
                "name"=>"STATUS_SHOW",
                "type"=>"radio",
                "label"=>lang("publishing_settings"),
                "default"=>"Y",
                "options"=>[
                    "Y"=>lang("indicate"),
                    "N"=>lang("hidden")
                ]
            ],
            [
                "name"=>"ORDER",
                "type"=>"text",
                "label"=>lang("menu_display_order"),
                "required"=>true,
                "rules"=>"required",
                "help"=>lang("productmenu_order_help")
            ],
        ];

        $this->preview_url = site_url("preview/productmenu");
        $this->index_preview_url = site_url("preview/main/headerandfooter");
        $this->publish_preview_url = site_url("preview/productmenu");
    }
}
