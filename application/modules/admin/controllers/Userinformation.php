<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Userinformation extends ADMIN_Controller
{

    protected $table = "users";
    protected $route_prefix = 'admin/userinformation/'; //Dont't forget to attach '/' to the end of the string
    protected $has_index = true; //true if this controller has list pages
    protected $publish_has_preview = false;
    protected $index_has_preview = false;

    protected $page_icon = "<i class='fa fa-sitemap'></i>";

    protected $has_add = false;
    protected $has_order = false;
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is required or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $list_description;
    protected $confirm_fields;
    protected $preview_url;


    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }

    protected function initialize()
    {
        $this->head['title'] = lang('user_search_list');
        $this->head["description"] = "";
        $this->page_title = lang('user_information');
        $this->page_subtitle = lang('user_search_list');
        $this->panel_title = lang('user_search_list');
        $this->head["bread_title"] =  lang("user_information");
        $this->fields = [
            [
                "label" => lang('user_id'),
                "type" => "text",
                "name" => "USER_ID"
            ],
            [
                "label" => lang("os"),
                "type" => "radio",
                "name" => "DEVICE_TYPE",

                "options" => [
                    1 => lang("android"),
                    2 => lang("ios")
                ]
            ],

        ];
        $this->table_fields = [
            [
                "label" => lang('refer'),
                "type" => "search",
            ],
            [
                "label" => lang('user_id'),
                "type" => "text",
                "sortable"=>true,
                "name" => "USER_ID"
            ],
            [
                "name" => "DEVICE_TYPE",
                "type" => "radio",
                "label" => lang('os'),

                "options" => [
                    1 => lang("android"),
                    2 => lang("ios")
                ]
            ]
        ];

        $this->search_fields = [
            [
                "name" => 'SEARCH_USER_ID',
                "type" => 'text',
                "label" => lang('user_id'),
            ],
        ];
        $this->form_description = lang('user_information_search_description');
    }

    public function detail($id = 0)
    {
        $back_url = base_url() . $this->route_prefix;

        $old = $this->session->flashdata('old');
        $row = $this->db_get_row($id);
        if (is_array($old)) $row = $old;
        if ($row == NULL) $row = array();

        $this->set_field_values($this->fields, $row);

        $data['back_url'] = $back_url;
        $data["fields"] = $this->fields;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        $data["form_description"] = $this->form_description;

        $this->load->view('_parts/header', $this->head);
        $this->load->view('userinformation/detail', $data);
        $this->load->view('_parts/footer');
    }

    public function index($page = 0)
    {
        $user = $this->auth;
        if ($this->has_index == false) {
            $this->redirect('publish');
        }
        $data = array();

        $input = $this->input->get();
        $perpage = $this->input->get('perpage');
        if ($perpage == 0) {
            $perpage = $this->perpage;
        }
        if ($this->index_has_search) {
            $this->set_field_values($this->search_fields, $input);
        }
        $input["perpage"] = $perpage;
        $rowscount = $this->count_rows($input);
        $data["branch_id"] = $this->auth['branch_id'];
        $data['add_url'] = $this->redirect_url('publish/');
        $data['preview_url'] = $this->index_preview_url ? $this->index_preview_url : $this->preview_url;
        $data["delete_url"] = $this->redirect_url('delete/');
        $data["order_url"] = $this->redirect_url('order/');
        $data['rows'] = $this->get_rows($input, $perpage, $page);
        $data['links_pagination'] = pagination($this->redirect_url('index'), $rowscount, $perpage, count(explode("/", $this->route_prefix)) + 1);
        $data["table_fields"] = $this->table_fields;
        $data['search_fields'] = $this->search_fields;

        $data["has_order"] = $this->has_order;
        $data["has_add"] = $this->has_add;
        $data['params'] = $input;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        /////////////////////////////title with index
        $data['page_title1'] = $this->page_title1;
        $data['page_subtitle1'] = $this->page_subtitle1;
        $data['panel_title1'] = $this->panel_title1;
        $data['list_description1'] = $this->list_description1;
        $data['title_index'] = $this->title_index;
        //
        $data["form_description"] = $this->form_description;
        $data["list_description"] = $this->list_description;
        $data["page_icon"] = $this->page_icon;
        $data["perpage_options"] = $this->perpage_options;
        $data["perpage"] = $perpage;
        $data["has_preview"] = $this->index_has_preview;
        $data['has_search'] = $this->index_has_search;
        $this->load->view('_parts/header', $this->head);
        $this->load->view('userinformation/list', $data);
        $this->load->view('_parts/footer');
    }

    protected function build_query($params)
    {
        $this->db->where('branch_id', $this->auth['branch_id']);
        $this->db->where('USER_TYPE', 2);
        $USER_ID = element("SEARCH_USER_ID", $params);
        if ($USER_ID != NULL) {
            $this->db->where("USER_ID", $USER_ID);
        }
        $NICK_NAME = element("NICK_NAME", $params);
        if ($NICK_NAME != NULL) $this->db->like("NICK_NAME", $NICK_NAME);

        $DIRECTION = element("USER_ID", $params);
        if ($DIRECTION != NULL) {
            if ($DIRECTION == "ASC") $this->db->order_by("USER_ID","ASC");
            if ($DIRECTION == "DESC") $this->db->order_by("USER_ID","DESC");
        }else{
            $this->db->order_by("ID","DESC");
        }

    }
}
