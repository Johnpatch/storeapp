<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Store extends ADMIN_Controller
{
    protected $table = "branches";

    protected $route_prefix = 'admin/basic/store/';//Don't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
    protected $branch_id_field = 'id';
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";

    //Indicates whether index page has preview
    protected $index_has_preview = false;

    //Indicates whether publish page has preview
    protected $publish_has_preview = false;
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is required or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $fields;

    protected $preview_url;

    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->page_title = lang('basic_information');//"Create and edit apps";
        $this->page_subtitle = lang('store_information');//"";
        $this->panel_title = lang('store_information');//"Basic information";
        $this->fields = [
            [
                "name"=>"NAME",
                "type"=>"text",
                "label"=>lang('admin_basic_store_publish_name'),//"Store name",
                "required"=>true,
                "rules"=>"required",
                "help"=>lang('admin_basic_store_publish_name_help'),//"<b>Please enter the name of the store.</b>(Up to 20 double-byte characters)<br>
                        //*Emoji cannot be used",
            ],
            [
                "name"=>"NAME_KANA",
                "type"=>"text",
                "label"=>lang('admin_basic_store_publish_name_kana'),//"Store name(Kana)",
                "help"=>lang('admin_basic_store_publish_name_kana_help')//"<b>Please enter the name of the store.</b>(Up to 20 double-byte characters)<br>
                        // *Emoji cannot be used",
            ],
            [
                "name"=>"COUNTRY",
                "type"=>"city_dropdown",
                "label"=>lang('admin_basic_store_publish_country'),//"Prefecture",
                "required"=>true,
                "rules"=>"required",
                "options" => [
                    ""=>"",
                ]
            ],
            [
                "name"=>"ADDRESS1",
                "type"=>"text",
                "label"=>lang('admin_basic_store_publish_address1'),//"Address 1(city)",
                "required"=>true,
                "rules"=>"required",
                "help"=>lang('admin_basic_store_publish_address1_help')//"<span class='help-tab-gray'>Entry example</span> 1-1, Chiyoda, Chiyoda-ku, Tokyo"
            ],
            [
                "name"=>"ADDRESS2",
                "type"=>"text",
                "label"=>lang('admin_basic_store_publish_address2'),//"Address 2(building name, etc.)",
                "help"=>lang('admin_basic_store_publish_address2_help')//"<span class='help-tab-gray'>Entry example</span> MM Building 8F"
            ],
            [
                "name"=>"LAT",
                "type"=>"text",
                "label"=>lang('admin_basic_store_publish_latitude')//"latitude",
            ],
            [
                "name"=>"LNG",
                "type"=>"text",
                "label"=>lang('admin_basic_store_publish_longitude')//"longitude",
            ],
            [
                "name"=>"STREET_NAME",
                "type"=>"text",
                "label"=>lang('admin_basic_store_publish_street_name'),//"Street name",
                "help"=>lang('admin_basic_store_publish_street_name_help')//"*If you set a street name, this will be displayed instead of the address."
            ],
            [
                "name"=>"GOOGLE_PLAY_URL",
                "type"=>"text",
                "label"=>lang('admin_basic_store_publish_google_play_url'),//"Google Play download URL",
                "help"=>lang('admin_basic_store_publish_google_play_url_help')//"Fill out the app when it is published on GooglePlay."
            ],
            [
                "name"=>"APP_STORE_URL",
                "type"=>"text",
                "label"=>lang('admin_basic_store_publish_app_store_url'),//"App Store download URL",
                "help"=>lang('admin_basic_store_publish_app_store_url_help')//"Fill in the app when it is published on the Appstore."
            ],
            [
                "name"=>"EMAIL",
                "type"=>"email",
                "label"=>lang('admin_basic_store_publish_email'),//"Email address",
                "required"=>true,
                "rules"=>"required",
                "help"=>lang('admin_basic_store_publish_email_help')//"<span class='help-tab-gray'>Example of entry</span> xxx@xxx.xxx<br>
                        // *This is the e-mail address for customer inquires.<br>
                        // *This address is also required when re-issuing the management screen password."
            ],
            [
                "name"=>"EMAIL_SHOW",
                "type"=>"radio",
                "label"=>lang('admin_basic_store_publish_email_show'),//"Email inquiry display",
                "default"=>"Y",
                "help"=>lang('admin_basic_store_publish_email_show_help'),//"*Switch the display of inquiries in the information.",
                "options"=>[
                    "Y"=>lang('admin_basic_store_publish_email_show_y'),//"Indicate",
                    "N"=>lang('admin_basic_store_publish_email_show_n')//"Hidden"
                ]
            ],
            
            [
                "group" => true,    
                "label"=>lang('member_function'),//"Email inquiry display",
                "input" => [
                    [
                        "name"=>"MEMBER_FUNCTION",
                        "type"=>"dropdown",
                        "default"=>"ON",
                        "options"=>[
                            "ON"=>lang('on'),//"Indicate",
                            "OFF"=>lang('off')//"Hidden"
                        ]
                    ],
                    [
                        "name"=>"MEMBER_DETAIL",
                        "type"=>"dropdown",
                        "label"=>'',//"Email inquiry display",
                        "default"=>1,
                        "options"=>[
                            "1"=>lang('password_function'),//"Indicate",
                            "2"=>lang('id_password_function')//"Hidden"
                        ],
                    ],
                ]
                
            ],
            
            [
                "name"=>"STATUS_SHOW",
                "type"=>"radio",
                "label"=>lang('admin_basic_store_publish_status_show'),//"Status",
                "default"=>"Y",
                "options"=>[
                    "Y"=>lang('admin_basic_store_publish_status_show_y'),//"Indicate",
                    "N"=>lang('admin_basic_store_publish_status_show_n')//"Hidden"
                ]
            ],
        ];
        $this->confirm_fields = [
            [
                "name"=>"NAME",
                "type"=>"text",
                "label"=>lang('confirm_admin_basic_store_publish_name'),//"Store name",
                "required"=>false,
                "rules"=>"required",
                "help"=>lang('confirm_admin_basic_store_publish_name_help'),//"<b>Please enter the name of the store.</b>(Up to 20 double-byte characters)<br>
                        //*Emoji cannot be used",
            ],
            [
                "name"=>"NAME_KANA",
                "type"=>"text",
                "label"=>lang('confirm_admin_basic_store_publish_name_kana'),//"Store name(Kana)",
                "help"=>lang('confirm_admin_basic_store_publish_name_kana_help')//"<b>Please enter the name of the store.</b>(Up to 20 double-byte characters)<br>
                        // *Emoji cannot be used",
            ],
            [
                "name"=>"COUNTRY",
                "type"=>"city_dropdown",
                "label"=>lang('confirm_admin_basic_store_publish_country'),//"Prefectures",
                "required"=>false,
                "rules"=>"required",
                "options" => [
                    ""=>"",
                ]
            ],
            [
                "name"=>"ADDRESS1",
                "type"=>"text",
                "label"=>lang('confirm_admin_basic_store_publish_address1'),//"Address 1",
                "required"=>false,
                "rules"=>"required",
                "help"=>lang('confirm_admin_basic_store_publish_address1_help')//"<span class='help-tab-gray'>Entry example</span> 1-1, Chiyoda, Chiyoda-ku, Tokyo"
            ],
            [
                "name"=>"ADDRESS2",
                "type"=>"text",
                "label"=>lang('confirm_admin_basic_store_publish_address2'),//"Address 2",
                "help"=>lang('confirm_admin_basic_store_publish_address2_help')//"<span class='help-tab-gray'>Entry example</span> MM Building 8F"
            ],
            [
                "name"=>"STREET_NAME",
                "type"=>"text",
                "label"=>lang('confirm_admin_basic_store_publish_street_name'),//"Street name",
                "help"=>lang('confirm_admin_basic_store_publish_street_name_help')//"*If you set a street name, this will be displayed instead of the address."
            ],
            [
                "name"=>"LAT",
                "type"=>"text",
                "label"=>lang('confirm_admin_basic_store_publish_latitude_longitude')//"longitude latitude",
            ],
           
            [
                "name"=>"GOOGLE_PLAY_URL",
                "type"=>"text",
                "label"=>lang('confirm_admin_basic_store_publish_google_play_url'),//"Google Play download URL",
                "help"=>lang('confirm_admin_basic_store_publish_google_play_url_help')//"Fill out the app when it is published on GooglePlay."
            ],
            [
                "name"=>"APP_STORE_URL",
                "type"=>"text",
                "label"=>lang('confirm_admin_basic_store_publish_app_store_url'),//"App Store download URL",
                "help"=>lang('confirm_admin_basic_store_publish_app_store_url_help')//"Fill in the app when it is published on the Appstore."
            ],
            [
                "name"=>"EMAIL",
                "type"=>"email",
                "label"=>lang('confirm_admin_basic_store_publish_email'),//"mail address",
                "required"=>false,
                "rules"=>"required",
                "help"=>lang('confirm_admin_basic_store_publish_email_help')//"<span class='help-tab-gray'>Example of entry</span> xxx@xxx.xxx<br>
                        // *This is the e-mail address for customer inquires.<br>
                        // *This address is also required when re-issuing the management screen password."
            ],
            [
                "name"=>"EMAIL_SHOW",
                "type"=>"radio",
                "label"=>lang('confirm_admin_basic_store_publish_email_show'),//"Email inquiry display",
                "default"=>"Y",
                "help"=>lang('confirm_admin_basic_store_publish_email_show_help'),//"*Switch the display of inquiries in the information.",
                "options"=>[
                    "Y"=>lang('confirm_admin_basic_store_publish_email_show_y'),//"Indicate",
                    "N"=>lang('confirm_admin_basic_store_publish_email_show_n')//"Hidden"
                ]
            ],
            [
                "name"=>"MEMBER_FUNCTION",
                "type"=>"dropdown",
                "label"=>lang('member_function'),//"Email inquiry display",
                "default"=>"ON",
                "options"=>[
                    "ON"=>lang('on'),//"Indicate",
                    "OFF"=>lang('off')//"Hidden"
                ]
            ],
            [
                "name"=>"MEMBER_DETAIL",
                "type"=>"dropdown",
                "label"=>'',
                "default"=>1,
                "options"=>[
                    1=>lang('password_function'),//"Indicate",
                    2=>lang('id_password_function')//"Hidden"
                ]
            ],
            [
                "name"=>"STATUS_SHOW",
                "type"=>"radio",
                "label"=>lang('confirm_admin_basic_store_publish_status_show'),//"Status",
                "default"=>"Y",
                "options"=>[
                    "Y"=>lang('confirm_admin_basic_store_publish_status_show_y'),//"Indicate",
                    "N"=>lang('confirm_admin_basic_store_publish_status_show_n')//"Hidden"
                ]
            ],
        ];
        $this->form_description=lang('design_storeinformation_form_description');//"Please enter the headquarters information.<br>
                                    //* The name and address of the headquarters will be displayed as distribution source information when the push notification is distributed from the headquarters.";
        //Success settings
        $this->success_return_text = lang('design_storeinformation_success_return_text');//"Return";
        //language setting ended

        // $this->load->model('Store_model', 'model');
        $this->head['title'] = lang("store_information");
        $this->head['bread_title'] = lang('basic_information');
        $this->head['page_subtitle'] = lang('store_information');
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/layout");
    }
}
