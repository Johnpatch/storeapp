<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Temp extends CI_Controller
{
     //Indicates whether index page has preview
     protected $index_has_preview = false;

     //Indicates whether publish page has preview
     protected $publish_has_preview = false;
     
    public function index() {
        $this->publish_has_preview = false;
        $this->index_has_preview = false;
        $head['bread_title'] = lang('basic_information');
        $head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $this->load->view('_parts/header', $head);
        $this->load->view('publish');
        $this->load->view('_parts/footer');
    }
}
