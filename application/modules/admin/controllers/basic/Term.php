<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Term extends ADMIN_Controller
{
    protected $table = "main_info";

    protected $route_prefix = 'admin/basic/term/';//Don't forget to attach '/' to the end of the string
//    protected $has_index = true;//true if this controller has list pages
    protected $index_has_search = false;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $has_add = true;
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $confirm_script_url = "";
    protected $additional_conditions = [
        "TYPE"=>1
    ];

    //Indicates whether index page has preview
    protected $index_has_preview = false;

    //Indicates whether publish page has preview
    protected $publish_has_preview = false;
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $fields; 

    protected $preview_url;

    protected $search_fields = array();//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields = [
        [
            "label"=>"Fix",
            "type"=>"edit",
        ],
        [
            "label"=>"Delete",
            "type"=>"delete"
        ],
        [
            "label"=>"Text",
            "type"=>"text",
            "name"=>"CONTENT"
        ],
    ];

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->page_title = lang('design_termssentence_page_title');//"Basic information";
        $this->page_subtitle = lang('design_termssentence_panel_title');//"";
        $this->panel_title = lang('design_termssentence_panel_title');//"Terms of service";
        $this->fields = [
            [
                "name"=>"CONTENT",
                "type"=>"textarea",
                "label"=>lang("text"),
            ],
        ];

        $this->table_fields = [
            [
                "label"=>lang("fix"),
                "type"=>"edit",
                "width" => 150
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete",
                "width" => 150
            ],
            [
                "label"=>lang("photo"),
                "type"=>"image",
                "name"=>"IMAGE",
            ],
            [
                "label"=>lang("name"),
                "type"=>"text",
                "name"=>"NAME"
            ],
            [
                "label"=>lang("birthday"),
                "type"=>"text",
                "name"=>"BIRTHDAY"
            ],          
        ];
        $this->form_description=lang('design_termssentence_form_description');//"";
        //Success settings
        $this->success_return_text = lang('design_termssentence_success_return_text');//"Return";
        //language setting ended

        // $this->load->model('Term_model', 'model');
        $this->head['title'] = lang("design_termssentence_panel_title");
        $this->head['bread_title'] = lang('basic_information');
        $this->head['page_subtitle'] = lang('design_termssentence_panel_title');
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/layout");
    }
}
