<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pdf extends ADMIN_Controller
{
    protected $table='pdf_info';
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $route_prefix = 'admin/basic/pdf/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $index_has_preview = false;
    protected $publish_has_preview = false;
    protected $index_has_search = false;
    protected $has_add = true;

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize()
    {

        $this->fields = [
            [
                "label"=>"コメント",
                "name"=>"COMMENT",
                "type"=>"text"
            ],
            [
                "label" => lang("pdf"),//lang('Manual'),
                "name" => "MEDIA_LINK",
                "type" => "pdf",
                "required"=>true,
                "rules"=>"required"
            ],
        ];

        $this->table_fields = [
            [
                "label" => lang('admin_topmenu_index_fix'),//"Fix",
                "type" => "edit",
            ],
            [
                "label" => lang('admin_topmenu_index_delete'),//"Delete"
                "type" => "delete"
            ],
            [
                "label" => lang('comment'),//"Top menu name",
                "type" => "text",
                "name" => "COMMENT"
            ],
        ];

        $this->page_title = lang('basic_information');//"Edit basic information";
        $this->page_subtitle = lang('pdf_information');//"";
        $this->panel_title = lang('pdf_information');//"catalog information";

        $this->form_description = lang('design_cataloginformation_form_description');
        //Success settings
        $this->success_return_text = lang('design_cataloginformation_success_return_text');// "Return";

        $this->head['title'] = lang("pdf_information");
        $this->head['bread_title'] = lang('basic_information');
        $this->head['page_subtitle'] = lang('video_information');
        $this->head["description"] = "";
    }


}
