<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class First_html extends ADMIN_Controller
{
    protected $table = "basic_html";

    protected $route_prefix = 'admin/basic/first_html/';//Don't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";

    //Indicates whether index page has preview
    protected $index_has_preview = true;

    //Indicates whether publish page has preview
    protected $publish_has_preview = true;

    protected $preview_url;

    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";


    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }
    public $type = 1;
    protected function initialize() {
        $this->page_title = lang('design_privacypolicy_page_title');
        $this->page_subtitle = lang('html');
        $this->panel_title = lang('html');

        $this->fields = [
            [
                "name"=>"FIRST_HTML",
                "type"=>"ckeditor",
                "label"=>lang("html"),
                "start_mode"=>"source",
            ]

        ];
        $this->success_return_text = lang('return');

        $this->head['title'] = lang("html");
        $this->head['bread_title'] = lang('basic_information');
        $this->head['page_subtitle'] = lang('html');
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/html/1");

    }
}