<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Content extends ADMIN_Controller
{
    protected $table = "company_profile";
    protected $route_prefix = 'admin/basic/content/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $has_order = false;
    protected $has_add = true;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $publish_script_url = "basic/content";
    protected $confirm_script_url = "basic/confirm";
    protected $index_has_preview = true;
    protected $publish_has_preview = true;
    protected $edit_title = '';
    protected $is_basic_add = true;
    protected $index_has_search = true;
    /*
         * Input Fields Description
         * name: input name----Must be exactly equal to corresponding DB column name
         * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
         * default: default value if initial value is not set
         * label: input label description
         * help: help block description
         * required: whether this field is requried or not
         * showIf: array(key=>value). shows this element when (key=value)
         *          value can take array values.
         * rules: validation settings
         */
    protected $fields;
    protected $preview_url;

    protected $search_fields=[];
    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    
    //Publish settings
    public $form_description= "";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        // $this->load->model('Event_model', 'model');
        $this->form_description = '';
        $this->head['title'] = lang("basic_information");
        $this->head["description"] = "";
        $this->page_title = lang("basic_information");
        $this->page_subtitle = lang("list_reg");
        $this->panel_title = lang("list_reg");
        $this->head['bread_title'] = lang("basic_information");
        $this->head['page_subtitle'] = lang("list_reg");
        $this->edit_title = lang("list_reg");

        $events = $this->db
            ->select("ID, TITLE")
            ->where("BRANCH_ID", $this->auth['branch_id'])
            ->get("post_category")
            ->result_array();
        $parent_options = array();
        foreach ($events as $event) {
            $parent_options[$event["ID"]] = $event["TITLE"];
        }
        $this->is_basic_add = true;
        $this->search_fields = [
            [
                "name"=>"TYPE",
                "type"=>"dropdown",
                "label"=>lang("category"),
                "options" => [
                    '' => lang('all'),
                    '1' => lang('company_profile'),
                    '2' => lang('store_guide'),
                    '3' => lang('pricing'),
                    '4' => lang('FAQ'),
                    '5' => lang('notice_setting'),
                    '6' => lang('terms_service'),
                    '7' => lang('privacy_policy'),
                    '8' => lang('information_security'),
                    '9' => lang('about_this_app'),
                ],
            ],
        ]; //used only when this controller has index page.(i.e $this->has_index_page = true)


        $this->fields = [
            [
                "name"=>"TYPE",
                "type"=>"dropdown",
                "label"=>lang("category"),
                "options" => [
                    '1' => lang('company_profile'),
                    '2' => lang('store_guide'),
                    '3' => lang('pricing'),
                    '4' => lang('FAQ'),
                    '5' => lang('notice_setting'),
                    '6' => lang('terms_service'),
                    '7' => lang('privacy_policy'),
                    '8' => lang('information_security'),
                    '9' => lang('about_this_app'),
                ],
            ],
            [
                "label"=>lang("staff_photo"),
                "type"=>"file",
                "name"=>"IMAGE[]",
                "help"=>lang("staff_photo_help")
            ],
            [
                "label"=>lang("title"),
                "type"=>"text",
                "name"=>"TITLE[]",
                "function"=>"updatePreview"
            ],
            [
                "label"=>lang("comment"),
                "type"=>"textarea",
                "name"=>"COMMENT[]",
                "function"=>"updatePreview"
            ]
        ];

        $this->table_fields = [
            [
                "label"=>lang("fix"),
                "type"=>"edit",
                "width" => 150
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete",
                "width" => 150
            ],
            [
                "name"=>"TYPE",
                "type"=>"dropdown",
                "label"=>lang("category"),
                "options" => [
                    '1' => lang('company_profile'),
                    '2' => lang('store_guide'),
                    '3' => lang('pricing'),
                    '4' => lang('FAQ'),
                    '5' => lang('notice_setting'),
                    '6' => lang('terms_service'),
                    '7' => lang('privacy_policy'),
                    '8' => lang('information_security'),
                    '9' => lang('about_this_app'),
                ],
            ]
        ];
        $this->preview_url = site_url("preview/basiccontent/");
    }

    protected function build_query($params) {
        $TYPE = element("TYPE", $params);
        if ($TYPE != "") $this->db->where("TYPE", $TYPE);
        $this->db->where('BRANCH_ID', $this->auth["branch_id"]);
        $this->db->group_by('CREATE_TIME');
    }
}
