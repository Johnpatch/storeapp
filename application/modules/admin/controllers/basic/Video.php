<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Video extends ADMIN_Controller
{
    protected $table="photo_video_info";
    protected $route_prefix = 'admin/basic/video/';//Dont't forget to attach '/' to the end of the string
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $additional_conditions = [
        "TYPE"=>'2'
    ];
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */


    protected $preview_url;

    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;

    //Publish settings

    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->page_title = lang('movie');//"Edit basic information";
        $this->page_subtitle = lang('list_reg');//"";
        $this->panel_title = lang('list_reg');//"Video information";
        $this->fields = [
            [
                "label"=>lang("video_registration"),
                "type"=>"bloghp",
                "group"=>true,
                "array"=>true,
                "button"=>lang("add_this_video"),
                "input"=>[
                    [
                        "name"=>'MEDIA_LINK',
                        'placeholder'=>lang('video_url'),
                        "default"=>"https://www.youtobe.com/watch?v="
                    ],
                    [
                        "name"=>'COMMENT',
                        'placeholder'=>lang('comment')
                    ]
                ]
            ],
        ];

        $this->form_description=lang('design_videoinformation_form_description');//"You can set your shop's introduction video public. Please upload the video to youtube and post the URL.<br>
        //                         *Up to 10 videos can be registered and published with comments.<br>
        //                         *You cannot publish the video file itself.<br>
        //                         *Comments can be up to 100 characters.
        //                         ";
        //Success settings
        $this->success_return_text = lang('design_videoinformation_success_return_text');//"Return";
        //language setting ended
         $this->load->model('Photo_model', 'model');
        $this->head['title'] = lang("movie");
        $this->head['page_subtitle'] = lang("list_reg");
        $this->head['bread_title'] = lang('movie');
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/videoinfo");
    }

    protected function db_get_row($id)
    {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);


        $fields = array('MEDIA_LINK', 'COMMENT');
        foreach ($fields as $key) {
            $row[$key] = array();
        }
        $rows = $this->db->select($fields)->get($this->table)->result_array();
        foreach ($rows as $item) {
            foreach ($item as $key => $value) {
                $row[$key][] = $value;
            }
        }
        return $row;
    }

    public function save($id = 0) {
        $input = $this->input->post();
        $input[$this->branch_id_field] = $this->auth['branch_id'];
        $input = array_merge($input, $this->additional_conditions);
        $conditions = $this->additional_conditions;
        $conditions[$this->branch_id_field] = $this->auth['branch_id'];
//        print_r($input);
        $this->model->delete_where($conditions);
        if (is_array(element('MEDIA_LINK', $input))) {
            $media_links = $input['MEDIA_LINK'];
            $comments = $input['COMMENT'];
            $len = count($media_links);
            $input_array = array();
            for ($i = 0; $i < $len; $i++) {
                $input = $conditions;
                $value = $media_links[$i];
                if (strstr($value, ",")) {
                    $url = "attachments/slide_images/".random_string().".png";
                    file_put_contents($url, base64_decode(explode(",", $value)[1]));
                    $input['MEDIA_LINK'] = $url;
                }else {
                    $input['MEDIA_LINK'] = $value;
                }
                $input['COMMENT'] = $comments[$i];
                $input_array[] = $input;
            }
            $this->model->insert_batch($input_array);
        }
        redirect($this->route_prefix."success" . "?token=".$this->token);
    }
}
