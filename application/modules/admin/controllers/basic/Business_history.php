<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Business_history extends ADMIN_Controller
{
    protected $table = "business_history";

    protected $route_prefix = 'admin/basic/business_history/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $index_has_search = false;
    protected $has_add = true;
    protected $has_order = false;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    // protected $publish_script_url = "staff/staff";
    protected $preview_url;
    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */

    //Publish settings
    protected $form_description="";
    ///List setting
    protected $list_description="
    ";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        // $this->load->model('Staff_model', 'model');
        $this->head['title'] = lang("history");
        $this->head['bread_title'] = lang('basic_information');
        $this->head['page_subtitle'] = lang('history');
        $this->head["description"] = "";
        $this->index_has_preview = false;
        $this->publish_has_preview = false;
        $this->preview_url = site_url("preview/main/headerandfooter");

        $this->table_fields = [
            [
                "label"=>lang("fix"),
                "type"=>"edit",
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete"
            ],
            [
                "label"=>lang("photo"),
                "type"=>"image",
                "name"=>"IMAGE",
            ],
            [
                "label"=>lang("title"),
                "type"=>"text",
                "name"=>"NAME"
            ],
            [
                "label"=>lang("comment"),
                "type"=>"text",
                "name"=>"COMMENT"
            ],          
        ];
    
        $this->fields = [
            [
                "label"=>lang("staff_photo"),
                "type"=>"file",
                "name"=>"IMAGE",
                "help"=>lang("staff_photo_help")
            ],
            [
                "label"=>lang("title"),
                "type"=>"text",
                "name"=>"NAME"
            ],
            [
                "label"=>lang("comment"),
                "type"=>"text",
                "name"=>"COMMENT"
            ],
        ];

        $this->list_description='';//"<li style='list-style-type:none;padding-left:0px;'>You can set and edit the image and order of each menu displayed on the top
        // page of the application, display/non-display.</li>
        // <li style='list-style-type:none;padding-left:0px;'>Click the Modify button to make detailed settings.</li>
        // <li>if you do not want to display the menu, select Private</li>
        // <li>If you change the order, please press \"Batch Update\" at the top right. Input capital NG</li>
        // ";
        $this->page_title = lang('basic_information');//"staff setting";
        $this->page_subtitle = lang('history');//"staff setting / edit";
        $this->panel_title = lang('history');//"Edit top menu";
        $this->success_return_text = lang('return');//Return to membership card color page setting / editing
       
    }
}
