<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Link extends ADMIN_Controller
{
    protected $route_prefix = 'admin/basic/link/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
    protected $table = 'post_links';
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $publish_script_url = "link/publish";
    protected $confirm_script_url = "link/confirm";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize()
    {
        $this->fields = [
            [
                "label" => lang('basic_twitter_page_link'),//twitter_link,
                'show' => [],
                'group' => false,//indicates whether this is an element or an input
                'array' => false,
                'name' => 'TWITTER_ACTIVE_YN',
                'type' => 'radio',
                'default' => 'Y',
                'options' => [
                    'Y' => lang('post_a_twitter_link'),
                    'N' => lang('do_not_post_a_twitter_link')
                ]
            ],
            [
                "label" => lang('twitter_account'),
                'show' => [
                    'TWITTER_ACTIVE_YN' => 'Y'
                ],
                'group' => false,//indicates whether this is an element or an input
                'array' => false,
                'name' => 'TWITTER_LINK',
                'type' => 'text',
                'addon' => 'http://www.twitter.com/',
                'header' => lang('twitter_account_header'),
                'help' => lang('twitter_account_help')
            ],
            [
                "label" => lang('facebook_link'),//facebook_link,
                'show' => [],
                'group' => false,//indicates whether this is an element or an input
                'array' => false,
                'name' => 'FACEBOOK_ACTIVE_YN',
                'type' => 'radio',
                'default' => 'Y',
                'options' => [
                    'Y' => lang('post_a_facebook_link'),
                    'N' => lang('do_not_post_a_facebook_link')
                ]
            ],
            [
                "label" => lang('facebook_account'),
                'show' => [
                    'FACEBOOK_ACTIVE_YN' => 'Y'
                ],
                'group' => false,//indicates whether this is an element or an input
                'array' => false,
                'name' => 'FACEBOOK_LINK',
                'type' => 'text',
                'addon' => 'http://www.facebook.com/',
                'header' => lang('facebook_account_header'),
                'help' => lang('facebook_account_help')
            ],

            [
                "label" => lang('blog_and_hp_link_settings'),//"blog_hp_link",
                "group" => true,
                "array" => true,
                "type" => "bloghp_dropdown",
                "input" => [
                    [
                        "group" => false,
                        "array" => false,
                        "name" => "TYPE",
                        "type" => "dropdown",
                        "options" => [
                            "0" => lang("please_select"),
                            "3" => lang("blog"),
                            "4" => lang("pc_homepage"),
                            "5" => lang("smartphone_page"),
                            "6" => lang("hot_pepper"),
                            "7" => lang("gournavi"),
                            "8" => lang("shopping"),
                            "9" => lang("reservation"),
                            "10" => lang("job_information")
                        ]
                    ],
                    [
                        "group" => false,
                        "array" => false,
                        "name" => "LINK",
                        "type" => "text",
                    ]
                ]
            ],
        ];

        $this->page_title = lang('basic_information');//"Edit basic information";
        $this->page_subtitle = lang('list_of_links');//"";
        $this->panel_title = lang('list_of_links');//"Photo information";

        $this->form_description = lang('list_of_link_description');//"Up to 60 photos of the shop can be registered and published with comments.<br>
        //Success settings
        $this->success_return_text = lang('design_photoinformation_success_return_text');// "Return";

        //language setting
        $this->head['title'] = lang("list_of_links");
        $this->head['bread_title'] = lang('basic_information');
        $this->head['page_subtitle'] = lang('list_of_links');
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/linksinfo");
    }

    protected function db_get_row($id)
    {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);

        $external_fields = array('TYPE', 'LINK');
        foreach ($external_fields as $key) {
            $row[$key] = array();
        }

        $externals = $this->db->where("BRANCH_ID", $this->auth['branch_id'])->get($this->table)->result_array();
        foreach ($externals as $external) {
            if ($external["TYPE"] == 1) {
                $row["TWITTER_ACTIVE_YN"] = $external["ACTIVE_YN"];
                $row["TWITTER_LINK"] = $external["LINK"];
            } elseif ($external["TYPE"] == 2) {
                $row["FACEBOOK_ACTIVE_YN"] = $external["ACTIVE_YN"];
                $row["FACEBOOK_LINK"] = $external["LINK"];
            } else {
                foreach ($external as $key => $value) {
                    $row[$key][] = $value;
                }
            }
        }
        return $row;
    }

    public function save($id = 0)
    {
        $input = $this->input->post();
        $branch_id = $this->auth["branch_id"];
        //clear rows
        $this->db->where("branch_id", $branch_id)->delete($this->table);

        //Now insert twitter
        $row["TYPE"] = 1;
        $row["LINK"] = $input["TWITTER_LINK"];
        $row["ACTIVE_YN"] = $input["TWITTER_ACTIVE_YN"];
        $row["BRANCH_ID"] = $branch_id;
        $this->db->insert($this->table, $row);
        //Insert facebook
        $row["TYPE"] = 2;
        $row["LINK"] = $input["FACEBOOK_LINK"];
        $row["ACTIVE_YN"] = $input["FACEBOOK_ACTIVE_YN"];
        $row["BRANCH_ID"] = $branch_id;
        $this->db->insert($this->table, $row);
        //Batch operation
        $rows = array();
        $links = $input["LINK"];
        $types = $input["TYPE"];
        $len = count($links);

        for ($i = 0; $i < $len; $i++) {
            $row = array();
            $row["TYPE"] = $types[$i];
            $row["LINK"] = $links[$i];
            $row["BRANCH_ID"] = $branch_id;
            $rows[] = $row;
        }
        if (count($rows) > 0) $this->db->insert_batch($this->table, $rows);
        redirect($this->route_prefix . "success" . "?token=".$this->token);
    }
}
