<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Stampprivilege extends ADMIN_Controller
{
    protected $table = "stamp_info";
    protected $route_prefix = 'admin/stamp/stampprivilege/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $has_order = false;
    protected $has_add = true;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $publish_script_url = "event/layout";
    protected $confirm_script_url = "event/confirm";
    protected $index_has_preview = false;
    protected $publish_has_preview = true;
    protected $edit_title = '';
    /*
         * Input Fields Description
         * name: input name----Must be exactly equal to corresponding DB column name
         * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
         * default: default value if initial value is not set
         * label: input label description
         * help: help block description
         * required: whether this field is requried or not
         * showIf: array(key=>value). shows this element when (key=value)
         *          value can take array values.
         * rules: validation settings
         */
    protected $fields;
    protected $preview_url;

    protected $search_fields=[];
    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    
    //Publish settings
    public $form_description= "";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        // $this->load->model('Event_model', 'model');
        $this->form_description = '';
        $this->head['title'] = lang("store_stamp_information");
        $this->head["description"] = "";
        $this->page_title = lang("store_stamp_information");
        $this->page_subtitle = lang("list_reg");
        $this->panel_title = lang("list_reg");
        $this->head['bread_title'] = lang("store_stamp_information");
        $this->edit_title = lang("list_reg");

        $stamp_marks = $this->db
            ->select("ID,URL")
            ->where("BRANCH_ID", $this->auth['branch_id'])
            ->get("stamp_image")
            ->result_array();
        $parent_options = array();
        foreach ($stamp_marks as $mark) {
            $parent_options[$mark["ID"]] = [$mark["URL"], false];
        }

        $coupons = $this->db->select("ID, COUPON_TITLE")->where('BRANCH_ID', $this->auth['branch_id'])->get('coupon')->result_array();
        $coupon_options = array();
        $coupon_options[''] = lang('please_select');
        foreach($coupons as $coupon){
            $coupon_options[$coupon['ID']] = $coupon['COUPON_TITLE'];
        }

        $this->search_fields = [
            [
                "name"=>'TITLE',
                "type"=>'text',
                "label"=>lang("title")
            ]
        ]; //used only when this controller has index page.(i.e $this->has_index_page = true)


        $this->fields = [
            [
                "label"=>lang("stamp_title"),
                "type"=>"text",
                "name"=>"TITLE",
                "required"=>true,
                "rules"=>"required",
                "help"=>lang("stamp_title_desc")
            ],
            [
                "name"=>"DETAIL",
                "type"=>"textarea",
                "label"=>lang("stamp_details"),
                "required"=>true,
                "rules"=>"required",
                "help"=>lang("stamp_detail_desc")
            ],
            [
                "name"=>'NUMBER_OF_STAMPS',
                "type"=>'text',
                "label"=>lang("stamp_number"),
                "required"=>true,
                "rules"=>"required",
                "help"=>lang("stamp_number_desc")
            ],
            [
                "label"=>lang("stamp_max_number"),
                "name"=>"MAXIMUM_STAMPS",
                "type"=>"text",
                "help"=>lang("stamp_max_number_desc")
            ],
            [
                "label"=>lang("stamp_once_a_day"),
                "name"=>"STAMP_ONCE_PER_DAY",
                "type"=>"radio",
                "default"=>"N",
                "help"=>lang("stamp_once_a_day_desc"),
                "options"=>[
                    "Y"=>lang("effectiveness"),
                    "N"=>lang("invalid")
                ]
            ],
            [
                "label"=>lang("stamp_per_day"),
                "name"=>"STAMP_PER_DAY",
                "type"=>"text",
                "help"=>lang("stamp_per_day_desc"),
            ],
            [
                "label"=>lang("stamp_complete_number"),
                "name"=>"COMPLETE_NUMBER",
                "type"=>"text",
                "help"=>lang("stamp_complete_number_desc")
            ],
            [
                "label"=>lang("COUPON"),
                "name"=>"COUPON_ID",
                "type"=>"dropdown",
                "options" => $coupon_options
            ],
            [
                "label"=>lang("stamp_card_valid_days"),
                "name"=>"STAMP_CARD_VALID_DAYS",
                "type"=>"number",
                "help"=>lang("stamp_card_valid_days_desc"),
                "default"=>"60"
            ],
            [
                "label"=>lang("stamp_mark"),
                "name"=>"STAMP_IMAGE",
                "type"=>"radio_image",
                "inline"=>true,
                "options" => $parent_options
            ],
            [
                "label"=>lang("privilege_details"),
                "name"=>"PRIVILEGE_DETAIL",
                "type"=>"textarea",
                "help"=>lang("privilege_details_desc")
            ],
            [
                "label"=>lang("bonus_image"),
                "name"=>"BONUS_IMAGE",
                "type"=>"file",
                "help"=>lang("bonus_image_desc")
            ],
            [
                "label"=>lang("reward_expiration_date"),
                "name"=>"REWARD_EXPIRATION_DATE",
                "type"=>"number",
                "help"=>lang("reward_expiration_date_desc")
            ],
            [
                "label"=>lang("terms_and_conditions"),
                "name"=>"TERMS_CONDITIONS",
                "type"=>"textarea"
            ],
            [
                "label"=>lang("qr_code"),
                "name"=>"QR_CODE",
                "type"=>"qr_code"
            ],
            /*[
                "label"=>lang("qr_code"),
                "type"=>"qr_code"
            ]*/
        ];

        $this->table_fields = [
            [
                "label"=>lang("fix"),
                "type"=>"edit",
                "width" => 150
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete",
                "width" => 150
            ],
            [
                "label"=>lang("title"),
                "type"=>"text",
                "name"=>"TITLE"
            ],
        ];
        $this->preview_url = site_url("preview/main/stampinfo");
    }

    protected function build_query($params) {
        $TITLE = element("TITLE", $params);
        $this->db->where('BRANCH_ID', $this->auth["branch_id"]);
        $this->db->like("TITLE", $TITLE);
    }

}
