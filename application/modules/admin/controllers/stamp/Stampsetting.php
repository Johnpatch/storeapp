<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Stampsetting extends ADMIN_Controller
{
    protected $route_prefix = 'admin/stamp/stampsetting/';//Dont't forget to attach '/' to the end of the string
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $additional_conditions = [
        "TYPE"=>'2'
    ];
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */


    protected $preview_url;

    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;

    //Publish settings

    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }

    protected function initialize() {
        $this->page_title = lang('stampsetting_page_title');
        $this->page_subtitle = lang('stampsetting_page_title');
        $this->panel_title = lang('stampsetting_panel_title');
        $this->fields = [
            [
                "label"=>"Stamp Settings",
                "type" => "video",
                "name" => "MEDIA_LINK",
                "comment" => "COMMENT",
            ],
        ];

        $this->form_description=lang('stampsetting_form_description');
        //Success settings
        $this->success_return_text = lang('design_videoinformation_success_return_text');//"Return";
        //language setting ended
        $this->load->model('Settings_model', 'model');
        $this->head['title'] = "Stamp Settings";
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/videoinfo");
    }


    public function publish($id = 0) {
        $user = $this->auth;
        $old = $this->session->flashdata('old');
        $data["branch_id"] = $user['branch_id'];
        $data["fields"] = $this->fields;
        $data["confirm_url"] = $this->redirect_url('confirm').($id > 0 ? "/$id" : "");
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        $data["form_description"] = $this->form_description;
        $data["page_icon"] = $this->page_icon;
        $data['preview_url'] = $this->publish_preview_url ? $this->publish_preview_url : $this->preview_url;
        $data['script_url'] = $this->publish_script_url;
        $data['has_preview'] = $this->publish_has_preview;
        $param['BRANCH_ID'] = $data["branch_id"];
        $param['VARIABLE'] = 'useofstamp';
        $param['VALUE'] = "0";
        $stamp_status = $this->model->get_row($param);
        if($stamp_status == NULL) {
            $this->model->insert_row($param);
        } else {
            $param['VALUE'] = $stamp_status['VALUE'];
        }
        $data['stamp_status'] = $param['VALUE'];
        $data["errors"] = $this->session->flashdata("errors");
        $this->head['bread_title'] = lang('store_stamp_information');
        $this->head['title'] = lang('stamp_usage_settings');
        $this->load->view('_parts/header', $this->head);
        $this->load->view('stamp/setting', $data);
        $this->load->view('_parts/footer');
    }

    public function confirm($id = 0) {
        $input = $this->input->post();

        $this->session->set_flashdata('old', $input);
        $back_url = $input["back_url"];
        $data["back_url"] = $back_url;
        $row = $input;
        $data["fields"] = $this->fields;
        $data["row"] = $row;
        $data["page_title"] = $this->page_title;
        $data["page_subtitle"] = $this->page_subtitle;
        $data["panel_title"] = $this->panel_title;
        $data["form_description"] = $this->form_description;
        $data["page_icon"] = $this->page_icon;
        $data["save_url"] = $this->redirect_url('save'.($id > 0 ? "/$id" : ""));
        $this->load->view('_parts/header', $this->head);
        $this->load->view('basic/video/confirm', $data);
        $this->load->view('_parts/footer');
    }

    public function updateSetting() {
        $input = $this->input->post();
        $input['BRANCH_ID'] = $this->auth['branch_id'];
        $input['VARIABLE'] = 'useofstamp';
        $this->model->update_value($input);

        $data = array();
        $data['state'] = 'success';
        echo json_encode($data);
        return;
    }
}
