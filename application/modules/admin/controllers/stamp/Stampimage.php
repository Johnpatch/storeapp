<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Stampimage extends ADMIN_Controller
{
    protected $table='stamp_image';
    protected $route_prefix = 'admin/stamp/stampimage/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $index_has_search = false;

    protected $has_add = true;
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */

    protected $preview_url;
    protected $publish_script_url = "stamp/publish";
    protected $confirm_script_url = "stamp/confirm";
    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;
    protected $index_has_preview = false;
    protected $publish_has_preview = false;

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize()
    {
        $this->fields = [
            [
                "label" => lang('stamp_image_registration'),
                "name" => "URL",
                "type" => "file",
                "required"=>true,
                "rules"=>"required",
                "help" => lang("stamp_mark_image_desc")
            ],
        ];

        $this->table_fields = [
            [
                "label"=>lang('fix'),//"Fix",
                "type"=>"edit",
            ],
            [
                "label"=>lang('delete'),//"Delete",
                "type"=>"delete"
            ],
            [
                "label" => lang('stamp_image_registration'),
                "name" => "URL",
                "type" => "image",
            ],
        ];

        $this->page_title = lang('store_stamp_information');//"Edit basic information";
        $this->page_subtitle = lang('stamp_image_registration');//"";
        $this->panel_title = lang('stamp_image_registration');//"catalog information";
        $this->head['bread_title'] = lang("store_stamp_information");
        $this->head['page_subtitle'] = lang("store_stamp_information");

        $this->form_description = lang('stamp_image_help');
        //Success settings
        $this->success_return_text = lang('return');// "Return";

        
        $this->head['title'] = lang("stamp_image_registration");
        $this->head["description"] = "";

    }

    
}
