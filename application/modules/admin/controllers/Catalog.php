<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Catalog extends ADMIN_Controller
{
    protected $table='catalogs';
    protected $route_prefix = 'admin/catalog/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $index_has_search = true;

    protected $has_add = true;
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is requried or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */

    protected $preview_url;
    protected $publish_script_url = "catalog/publish";
    protected $confirm_script_url = "catalog/confirm";
    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)

    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields;

    //Publish settings

    //Success settings


    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize()
    {
        $this->fields = [
            [
                "label" => lang('category'),
                "name" => "TYPE",
                "type" => "radio",
                "default" => 1,
                "options" => [
                    "1" => lang("pdf"),
                    "2" => lang('photo_gallery')
                ]
            ],
            [
                "label" => lang('title'),
                "name" => "TITLE",
                "type" => "text",
                "required"=>true,
                "rules"=>"required"
            ],
            /*[
                "label" => lang('instruction'),
                "name" => "CONTENT",
                "type" => "textarea"
            ],*/ 
            [
                "label" => lang("pdf"),
                "name" => "PDF",
                "type" => "pdf",
                "help" => lang("pdf_max_size"),
                "required"=>true,
                "rules"=>"required",
            ],
            [
                "label" => lang('thumbnail'),
                "name" => "THUMBNAIL",
                "type" => "file",
                "required"=>true,
                "rules"=>"required",
                "help"=>lang("header_help_desc")
            ],
            [
                "label" => lang('photo_gallery'),
                "header"=>lang("header_help_desc1"),
                "type" => "photo",
                "group" => true,
                "array" => true,
                "required"=>true,
                "button" => lang("add_photo"),
                "input" => [
                    [
                        "name" => 'MEDIA_LINK',
                    ],
                    [
                        "name" => 'COMMENT',
                        'placeholder' => lang('comment')
                    ]
                ],
                
            ],
        ];

        $this->table_fields = [
            [
                "label"=>lang('fix'),//"Fix",
                "type"=>"edit",
            ],
            [
                "label"=>lang('delete'),//"Delete",
                "type"=>"delete"
            ],
            [
                "label" => lang('category'),
                "name" => "TYPE",
                "type" => "radio",
                "default" => 1,
                "options" => [
                    "1" => lang("pdf"),
                    "2" => lang('photo_gallery')
                ]
            ],
            [
                "label" => lang('title'),
                "name" => "TITLE",
                "type" => "text",
                "required"=>true,
                "rules"=>"required"
            ],

            [
                "label" => lang('thumbnail'),
                "name" => "THUMBNAIL",
                "type" => "image",
                "required"=>true,
                "rules"=>"required"
            ],
            [
                "label" => lang('pdf'),
                "name" => "PDF",
                "type" => "download",
            ]
        ];

        $this->search_fields = [
            [
                "label" => lang('category'),
                "name" => "TYPE",
                "type" => "radio",
                "default" => "",
                "inline"=>true,
                "options" => [
                    ""=>lang("all"),
                    "1" => lang('pdf'),
                    "2" => lang('photo_gallery')
                ]
            ],
            [
                "label" => lang('title'),
                "name" => "TITLE",
                "type" => "text",
            ],
        ];

        $this->page_title = lang('catalog_production');//"Edit basic information";
        $this->page_subtitle = '';//"";
        $this->panel_title = lang('design_cataloginformation_panel_title');//"catalog information";
        $this->head['bread_title'] = lang("catalog_production");
        $this->head['page_subtitle'] = '';

        $this->form_description = lang('design_cataloginformation_form_description');//"Up to 60 catalogs of the shop can be registered and published with comments.<br>
       
        $this->success_return_text = lang('design_cataloginformation_success_return_text');// "Return";

        //language setting
        $this->load->model('catalog_model', 'model');
        $this->head['title'] = lang("catalog_production");
        $this->head["description"] = "";

        $this->preview_url = site_url("preview/main/cataloginfo");
    }

    protected function db_get_row($id)
    {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
        $row = $this->db->get($this->table)->row_array();

        $external_fields = array('MEDIA_LINK', 'COMMENT');
        foreach ($external_fields as $key) {
            $row[$key] = array();
        }
        $externals = $this->db->select($external_fields)->where("CATALOG_ID", $id)->get("catalog_photos")->result_array();
        foreach ($externals as $external) {
            foreach ($external as $key => $value) {
                $row[$key][] = $value;
            }
        }
        return $row;
    }

    protected function db_after_save($id, $data)
    {
        $links = element("MEDIA_LINK", $data, array());
        $comments = element("COMMENT", $data, array());

        $this->db->where("CATALOG_ID", $id)->delete('catalog_photos');
        $len = count($links);
        $rows = array();
        for ($i = 0; $i < $len; $i++) {
            $row = array();
            $row["CATALOG_ID"] = $id;
            $row["MEDIA_LINK"] = $links[$i];
            $value = $links[$i];
            if (strstr($value, ",")) {
                $url = "attachments/slide_images/" . random_string() . ".png";
                file_put_contents($url, base64_decode(explode(",", $value)[1]));
                $row['MEDIA_LINK'] = $url;
            }
            $row["COMMENT"] = $comments[$i];
            $row["CREATE_TIME"] = time();
            $row["UPDATE_TIME"] = time();
            $rows[] = $row;
        }
        if (count($rows) > 0)
            $this->db->insert_batch('catalog_photos', $rows);
    }

    protected function build_query($params) {
        $TYPE = element("TYPE", $params);
        $TITLE = element("TITLE", $params);
        if ($TYPE != "") $this->db->where("TYPE", $TYPE);
        $this->db->where('BRANCH_ID', $this->auth["branch_id"]);
        $this->db->like("TITLE", $TITLE);
    }
}
