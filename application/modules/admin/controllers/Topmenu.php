<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Topmenu extends ADMIN_Controller
{
    protected $table = "topmenu_settings";
    protected $route_prefix = 'admin/topmenu/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $index_has_search = false;
    protected $has_add = true;
    protected $has_order = true;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $page_icon = "<i class='fa fa-sitemap'></i>";
    protected $publish_script_url = "topmenu/topmenu";
    protected $confirm_script_url = "topmenu/confirm";
    protected $preview_url;
    protected $bread_title;
    protected $publish_has_preview = false;
    protected $search_fields;//used only when this controller has index page.(i.e $this->has_index_page = true)
    protected $perpage = 1000;
    /* table_fields
     * label: field label description
     * name: db field name
     * align: cell align type [left|center|right]
     * type: field type
     */
    protected $table_fields = [];

    //Publish settings
    protected $form_description = "";
    ///List setting
    protected $list_description = "<li style='list-style-type:none;padding-left:0px;'>You can set and edit the image and order of each menu displayed on the top
    page of the application, display/non-display.</li>
    <li style='list-style-type:none;padding-left:0px;'>Click the Modify button to make detailed settings.</li>
    <li>if you do not want to display the menu, select Private</li>
    <li>If you change the order, please press \"Batch Update\" at the top right. Input capital NG</li>
    ";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize()
    {
        $this->panel_title = lang('edit_top_menu');
        $this->page_title = lang("top_menu_settings");
        $this->page_subtitle = lang("edit_top_menu");

        $this->table_fields = [
        [
            "label" => lang("fix"),
            "type" => "edit",
        ],
        [
            "label" => lang("delete"),
            "type" => "delete"
        ],
        [
            "label" => lang("top_menu_name"),
            "type" => "text",
            "name" => "MENU_NAME"
        ],
        [
            "label" => lang("display"),
            "type" => "text",
            "name" => "STATUS_SHOW"
        ],
        [
            "name" => "ORDER",
            "type" => "order",
            "label" => lang("order"),
        ],
    ];

        // $this->load->model('Topmenu_model', 'model');
        $this->head['title'] = lang("edit_top_menu");
        $this->head['bread_title'] = lang("top_menu_settings");
        $this->head['page_subtitle'] = lang("edit_top_menu");
        $this->head["description"] = "";

        $layout = $this->db->select('LAYOUT_TYPE')->where('BRANCH_ID', $this->auth['branch_id'])->get('layout_settings')->row_array();
        
        if ($layout == NULL) {
            $layout_type = 1;
        } else {
            $layout_type = $layout["LAYOUT_TYPE"];
        }
        
        ////////Language setting;
        $this->fields = [
            [
                "group" => true,
                "label" => lang('admin_topmenu_publish_top_layout_types'),//"Top layout types",
                "input" => [
                    [
                        "name" => "LAYOUT_TYPE",
                        "type" => "static",
                        "default" => $layout_type,
                        "options" => [
                            "1" => lang('admin_topmenu_publish_list_layout'),//'Panel layout',
                            "2" => lang('admin_topmenu_publish_list_layout')//'List layout'
                        ]
                    ],
                    [
                        "name" => "LAYOUT_TYPE",
                        "type" => "hidden",
                        "default" => $layout_type
                    ]
                ]

            ],
            [
                "required" => true,
                "rules" => "required",
                "group" => true,    
                "label"=>lang('admin_topmenu_publish_type'),
                "input" => [
                    [
                        "name"=>"TYPE",
                        "type"=>"dropdown",
                        "default"=>1,
                        "options"=>[
                            "1"=>lang('catalog_production'),
                            "2"=>lang('movie'),
                            "3"=>lang('coupon'),
                            "4"=>lang('post_content'),
                            "5"=>lang('store_stamp_information'),
                            "6"=>lang('basic_information'),
                            "7"=>lang('free_content'),
                            "8"=>lang('inquiry_form'),
                            "9"=>lang('reservation'),
                            "10"=> lang('questionnaire'),
                            "11" => lang('web_view_settings')
                        ]
                    ],
                    [
                        "name"=>"SUBTYPE",
                        "type"=>"dropdown",
                        "default"=>1,
                        "options"=>[],
                    ],
                ]
            ],
            [
                "name" => "MENU_NAME",
                "type" => "text",
                "label" => lang('admin_topmenu_publish_menu_name_top_menu_name'),//"Top menu name",
                "required" => true,
                "rules" => "required",
            ],
            [
                "name" => "IMAGE_FOR_PANEL",
                "type" => "file",
                "label" => lang('admin_topmenu_publish_image_for_panel_image'),//"Image(jpeg,jpg,gif,png)",
                "help" => lang('admin_topmenu_publish_image_for_panel_image_help')//"Please register the image of <span class='help-tab-gray'>Recommended size</span> 360*280<br/>
                //<span class='help-tab-gray'>capacity</span> 300KB or less"
            ],
            /*[
                "group" => true,
                "array" => true,
                "type" => "bloghp",
                "label" => lang("blog_and_hp_link_settings"),
                "button" => lang("add_this_site"),
                "input" => [
                    [
                        "placeholder" => lang('title'),
                        "name" => 'LINK_NAME'
                    ],
                    [
                        "placeholder" => lang('external_site_url'),
                        "name" => 'LINK'
                    ]
                ]
            ],
            [
                "name" => "VIEW_PAGE_URL",
                "type" => "text",
                "label" => lang('admin_topmenu_publish_view_page_url'),//"Display page url",
                "help" => lang('admin_topmenu_publish_view_page_url_help')//"*Depending on the content of the set page, it may not be displayed property.<br/>
                //  <span class='help-tab-red'>For a PDF file</span><br/>
                //  http://docs.google.com/viewer?url=[Specify the URL of the PDF here] To<br/>
                //  <span class='help-tab-red'>open it with an external browser</span><br/>
                //  [URL to link]#target =_blank<br/>
                //  Please set in the format above."
            ],*/
            [
                "group" => true,
                "label" => lang('admin_topmenu_publish_device'),//"device",
                "input" => [[
                    "name" => "IOS_YN",
                    "type" => "checkbox",
                    "text" => lang('admin_topmenu_publish_device_ios'),//"iOS",
                    "checked" => "Y",
                    "unchecked" => "N",
                    "default" => "Y",
                ],
                    [
                        "name" => "ANDROID_YN",
                        "type" => "checkbox",
                        "text" => lang('admin_topmenu_publish_device_android'),//"Android",
                        "checked" => "Y",
                        "unchecked" => "N",
                        "default" => "Y",
                    ]],
            ],
            [
                "name" => "STATUS_SHOW",
                "type" => "radio",
                "label" => lang('admin_topmenu_publish_status_show'),//"display",
                "default" => "Y",
                "options" => [
                    "Y" => lang('admin_topmenu_publish_status_show_indicate'),//"Indicate",
                    "N" => lang('admin_topmenu_publish_status_show_hidden')//"Hidden"
                ],
                "help" => lang('admin_topmenu_publish_status_show_help')//"If you don't menu to appear, select Private"
            ],
            [
                "name" => "ORDER",
                "type" => "text",
                "label" => lang('admin_topmenu_publish_sort_order'),//"Sort order",
                "default" => 0,
                "help" => lang('admin_topmenu_publish_sort_order_help')//"*The smaller number is displayed first.<br/>
                //(Example: 0->1->2->3->4)"
            ],
            [
                "name" => "BUTTON_TYPE",
                "type" => "radio",
                "label" => lang('admin_topmenu_publish_button_type'),//"Button background type",
                "default" => "1",
                "options" => [
                    "1" => lang('admin_topmenu_publish_button_type_image'),//"Image",
                    "2" => lang('admin_topmenu_publish_button_type_color')//"Color"
                ],
            ],
            [
                "group"=>true,
                "label"=>lang('admin_topmenu_publish_image_for_list'),//"Header image(jpeg,jpg,gif,png)",
                "input"=>[
                    [
                        "name"=>"IMAGE_FOR_LIST_YN",
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "text"=>lang('delete')
                    ],
                        [
                        "name"=>"IMAGE_FOR_LIST",
                        "type"=>"file",
                        "help"=>lang("admin_topmenu_publish_image_for_list_help"),
                    ],

                ],
                "help" => lang('admin_topmenu_list_button_image_help')
            ],
            [
                "name" => "OVERLAY_YN",
                "type" => "radio",
                "label" => lang('admin_topmenu_publish_overlay'),//"Overlay",
                "default" => "Y",
                "options" => [
                    "Y" => lang('admin_topmenu_publish_overlay_effectiveness'),//"Effectiveness",
                    "N" => lang('admin_topmenu_publish_overlay_invalid')//"Invalid"
                ],
                "help" => lang('admin_topmenu_publish_overlay_help')//"Display button image darker."
            ],
            [
                "name" => "TITLE_DISP_YN",
                "type" => "radio",
                "label" => lang('admin_topmenu_publish_title_disp_yn'),//"Title display",
                "default" => "Y",
                "options" => [
                    "Y" => lang('admin_topmenu_publish_title_disp_yn_indicate'),//"Indicate",
                    "N" => lang('admin_topmenu_publish_title_disp_yn_hidden')//"Hidden"
                ],
            ],
            [
                "name" => "TITLE_TEXT_COLOR",
                "type" => "color",
                "label" => lang('admin_topmenu_publish_title_text_color'),//"Title text color",
                "help" => lang('admin_topmenu_publish_title_text_color_help')//"You can set the text color of the title."
            ],
            [
                "name" => "TITLE_POSITION",
                "type" => "dropdown",
                "label" => lang('admin_topmenu_publish_title_position'),//"Title position",
                "default" => 1,
                "options" => [
                    1 => lang('admin_topmenu_publish_title_position_middle'),//"Middle",
                    2 => lang('admin_topmenu_publish_title_position_up'),//"Up",
                    3 => lang('admin_topmenu_publish_title_position_under')//"Under"
                ],
            ],
            [
                "name" => "BUTTON_BACK_COLOR",
                "type" => "color",
                "label" => lang('admin_topmenu_publish_title_position_button_back_color'),//"Button background color",
                "help" => lang('admin_topmenu_publish_title_position_button_back_color_help')//"You can set the background color when no image is set."
            ],
            [
                "label" => lang('member_show'),//"device",
                "name" => "IS_MEMBER",
                "type" => "checkbox",
                "text" => '',
                "checked" => "Y",
                "unchecked" => "N",
                "default" => "N",
                "show_text" => lang('indicate'),
                "hide_text" => lang('hidden')
            ],
            [
                "label" => lang('footer_menu_setting'),//"device",
                "name" => "IS_FOOTER",
                "type" => "checkbox",
                "text" => '',
                "checked" => "Y",
                "unchecked" => "N",
                "default" => "N",
                "show_text" => lang('indicate'),
                "hide_text" => lang('hidden')
            ],
            [
                "label"=> lang('footer_image_icon'),
                "name"=>"FOOTER_IMAGE_ICON",
                "type"=>"file",
                "help" => lang('admin_topmenu_footer_image_icon_help')
            ],
            [
                "label" => lang('header_icon_setting'),//"device",
                "name" => "IS_HEADER",
                "type" => "checkbox",
                "text" => '',
                "checked" => "Y",
                "unchecked" => "N",
                "default" => "N",
                "show_text" => lang('indicate'),
                "hide_text" => lang('hidden')
            ],
            [
                "label"=> lang('header_image_icon'),
                "name"=>"HEADER_IMAGE_ICON",
                "type"=>"file",
                "help" => lang('admin_topmenu_footer_image_icon_help')
            ]
        ];

        $this->list_description = lang('admin_topmenu_list_description');

        //$this->page_title = lang('admin_topmenu_top_menu_setting');//"Top menu setting";
        $this->bread_title = lang('admin_topmenu_top_menu_setting');//"Top menu setting";
        //$this->page_subtitle = lang('admin_topmenu_top_menu_setting');//"Top menu setting / edit";
        $this->success_return_text = lang('success_return_text');//Return to membership card color page setting / editing
        $this->table_fields = [
            [
                "label" => lang('admin_topmenu_index_fix'),//"Fix",
                "type" => "edit",
            ],
            [
                "label" => lang('admin_topmenu_index_delete'),//"Delete"
                "type" => "delete",
                "has_fixed"=>true,
            ],
            [
                "label" => lang('admin_topmenu_index_top_menu_name'),//"Top menu name",
                "type" => "text",
                "name" => "MENU_NAME"
            ],
            [
                "label" => lang('admin_topmenu_index_display'),//"display",
                "type" => "radio",
                "name" => "STATUS_SHOW",
                "options" => [
                    "Y" => lang('indicate'),
                    'N' => lang('hidden')
                ],
            ],
            [
                "name" => "ORDER",
                "type" => "order",
                "label" => lang('admin_topmenu_index_order'),//"order",
            ],
        ];
        $this->load->model('Layout_model', 'model');

        $this->preview_url = site_url("preview/main/topmenu_layout");
    }

    protected function db_get_row($id)
    {
        if ($this->has_index == true) $this->db->where('id', $id);
        else $this->db->where($this->branch_id_field, $this->auth['branch_id']);
        if (count($this->additional_conditions) > 0) $this->db->where($this->additional_conditions);
        $row = $this->db->get($this->table)->row_array();
        unset($row['LAYOUT_TYPE']);
        $external_fields = array('LINK_NAME', 'LINK');
        foreach ($external_fields as $key) {
            $row[$key] = array();
        }
        $externals = $this->db->select($external_fields)->where("TOPMENU_ID", $id)->get("topmenu_external_links")->result_array();
        foreach ($externals as $external) {
            foreach ($external as $key => $value) {
                $row[$key][] = $value;
            }
        }
        return $row;
    }

    protected function get_rows($params, $perpage, $page)
    {
        $rows = $this->db->where("BRANCH_ID", 0)->where("TYPE", 0)->get($this->table)->result_array();
        if ($this->db->where("BRANCH_ID", $this->auth["branch_id"])->where("TYPE", 0)->count_all_results($this->table) == 0) {
            if (count($rows)) {
                foreach ($rows as &$row) {
                    $row["BRANCH_ID"] = $this->auth['branch_id'];
                    $row["ID"] = '';
                }
                $this->db->insert_batch($this->table, $rows);
            }
        }

        $this->build_query($params);
        return $this->db->get($this->table, $perpage, $page)->result_array();
    }

    protected function db_before_save($id, &$data) {
        //$row = $this->db->where("ID", $id)->get($this->table)->row_array();
        //if ($row["TYPE"] == 0) $data["TYPE"] = 0;

    }

    protected function db_after_save($id, $data)
    {
        $links = element("LINK", $data, array());
        $link_names = element("LINK_NAME", $data, array());

        $this->db->where("TOPMENU_ID", $id)->delete('topmenu_external_links');
        $len = count($links);
        $rows = array();
        for ($i = 0; $i < $len; $i++) {
            $row = array();
            $row["TOPMENU_ID"] = $id;
            $row["LINK"] = $links[$i];
            $row["LINK_NAME"] = $link_names[$i];
            $row["CREATE_TIME"] = time();
            $row["UPDATE_TIME"] = time();
            $rows[] = $row;
        }
        if (count($rows) > 0)
            $this->db->insert_batch('topmenu_external_links', $rows);
    }
}
