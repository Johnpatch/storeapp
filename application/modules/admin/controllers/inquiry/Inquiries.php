<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Inquiries extends ADMIN_Controller
{
    protected $table = "inquiry";
    protected $route_prefix = 'admin/inquiry/inquiries/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $has_order = false;
    protected $has_add = false;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $index_has_preview = false;
    protected $publish_has_preview = false;
    protected $list_description = '';
    protected $index_has_search = false;
    protected $edit_title = '';
    protected $search_fields = [];

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->head['bread_title'] = lang("inquiry_form");
        $this->head['page_subtitle'] = lang("inquiry_list");
        $this->head['title'] = lang("inquiry_form");
        $this->head["description"] = "";
        $this->page_title = lang("inquiry_form");
        $this->page_subtitle = lang("inquiry_list");
        $this->panel_title = lang("inquiry_list");
        $this->list_description = '';
        $this->edit_title = lang("inquiry_list");
        $setting = $this->db->select("*")
            ->where("BRANCH_ID", $this->auth['branch_id'])
            ->where("STATUS_SHOW", 'Y')
            ->where("TYPE != ", 2)
            ->get("inquiry_setting")
            ->result_array();
        
        $rows = $this->db->where("BRANCH_ID", $this->auth["branch_id"])->get("users")->result_array();
        $users = array();
        foreach ($rows as $row) {
            $users[$row["USER_ID"]] = $row["NICK_NAME"];
        }
        $this->fields = [];
        
        /*if($setting['INQUIRIY_ADDR_YN'] == 'Y'){
            array_push($this->fields, [
                    "label"=>lang("inquiry_address"),
                    "name"=>"INQUIRIY_ADDR",
                    "type"=>"text",
                ]
            );
        }*/


        $this->table_fields = [
            [
                "label"=>lang('fix1'),
                "type"=>"edit",
            ],
            /*[
                "label"=>lang('delete'),//"Delete",
                "type"=>"delete"
            ],
            [
                "label"=>lang("inquiry_no"),
                "type"=>"text",
                "name"=>"ID"
            ],
            [
                "label"=>lang("user"),
                "name"=>"USER_ID",
                "type"=>"dropdown",
                "options"=>$users
            ],*/
            [
                "label"=>lang("send_date"),
                "name"=>"CREATE_TIME",
                "type"=>"milisec",
            ],
        ];
        $users[''] = lang('all');
        $this->search_fields = [
            [
                "name"=>"USER_ID",
                "type"=>"dropdown",
                "label"=>lang("user"),
                "default" => '',
                "options" => $users,
            ],
        ];
    }

    protected function build_query($params)
    {
        $user_id = element("USER_ID", $params);
        
        if(!empty($user_id)){
            $this->db->where($this->table.'.USER_ID', $user_id);
        }
        $this->db->where('BRANCH_ID', $this->auth['branch_id']);
        $this->db->group_by('CREATE_TIME');
        $this->db->order_by('CREATE_TIME', 'desc');
    }

    protected function count_rows($params) {
        $this->build_query($params);
        //$this->db->select("COUNT(*) AS cnt");
        return $this->db->count_all_results($this->table);
        $result = $this->db->get($this->table)->row_array();
        if ($result != NULL) element("cnt", $result);
        return 0;
    }
}
