<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Management extends ADMIN_Controller
{
    protected $table = "inquiry_setting";
    protected $route_prefix = 'admin/inquiry/management/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
    protected $has_order = false;
    protected $has_add = true;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $index_has_preview = false;
    protected $publish_has_preview = false;
    protected $list_description = '';
    protected $has_search = false;
    protected $search_fields = [];
    protected $edit_title = '';

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    public function index($page = 0)
    {
        $this->login_check();

        $result = $this->db->where('BRANCH_ID', $this->auth['branch_id'])->where('TYPE',2)->get('inquiry_setting')->result_array();
        if(count($result) == 0){
            $data = array(
                'STATUS_SHOW' => 'Y',
                'BRANCH_ID' => $this->auth['branch_id'],
                'NAME' => lang('inquiry_address'),
                'TYPE' => 2
            );
            $this->db->insert('inquiry_setting', $data);
            $list = array(lang('company_name'), lang('name'), lang('katakana'), lang('depart_sign'), lang('email_address'), lang('office_name'),lang('inquiries'));
            foreach($list as $val){
                $data = array(
                    'STATUS_SHOW' => 'Y',
                    'BRANCH_ID' => $this->auth['branch_id'],
                    'NAME' => $val,
                    'TYPE' => 1
                );
                $this->db->insert('inquiry_setting', $data);
            }
        }
        
        $data = array();
        $head = array();
	    $head = $this->head;
        $head['description'] = '';
        $head['title'] = lang('inquiry_form');
        
        $data['page_title'] = lang('inquiry_form');
        $data['page_subtitle'] = lang("create_a_new");
        $data['panel_title'] = lang("create_a_new");
        $data['bread_title'] = lang('inquiry_form');
        $data['form_description'] = '';
        
        $head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
        $data['rows'] = $this->db->where('BRANCH_ID', $this->auth['branch_id'])->get('inquiry_setting')->result_array();
        
        $this->load->view('_parts/header', $head);
        $this->load->view('inquiry/management', $data);
        $this->load->view('_parts/footer');
    }

    public function save_val(){
        $input = $this->input->post();
        if($input['type'] == 2){
            $this->db->set('ADMIN_EMAIL', $input['value']);
            $this->db->where('ID', $input['id']);
            $this->db->update('inquiry_setting');
        }else if($input['type'] == 1){
            $this->db->set('STATUS_SHOW', $input['value']);
            $this->db->set('NAME', $input['name']);
            $this->db->where('ID', $input['id']);
            $this->db->update('inquiry_setting');
        }
    }

    public function add_val(){
        $data = array(
            'STATUS_SHOW' => 'N',
            'BRANCH_ID' => $this->auth['branch_id'],
            'NAME' => '',
            'TYPE' => 1
        );
        $this->db->insert('inquiry_setting', $data);
        $data['status'] = true;
        echo json_encode($data);
    }

    public function remove_box(){
        $input = $this->input->post();
        $this->db->where('ID', $input['id']);
        $this->db->delete('inquiry_setting');
        $data['status'] = true;
        echo json_encode($data);
    }
}
