<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Time extends ADMIN_Controller
{
    protected $table = "reservation_time";
    protected $route_prefix = 'admin/reservation/time/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
    protected $has_order = false;
    protected $has_add = false;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $index_has_preview = false;
    protected $publish_has_preview = false;

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->head['bread_title'] = lang("reservation");
        $this->head['page_subtitle'] = lang("time_week_setting");
        $this->head['title'] = lang("time_week_setting");
        $this->head["description"] = "";
        $this->page_title = lang("reservation");
        $this->page_subtitle = lang("time_week_setting");
        $this->panel_title = lang("time_week_setting");

        
        $this->fields = [
            [
                "group" => true,
                "label" => lang("book_time_range"),
                "between"=>"<span class='between'>~</span>",
                "input" => [
                    [
                        "name" => "START_HOUR",
                        "type" => "time",
                        "class" => "from_to_book",
                        "required"=>true,
                        "rules"=>"required",
                    ],
                    [
                        "name" => "END_HOUR",
                        "type" => "time",
                        "class" => "from_to_book",
                        "required"=>true,
                        "rules"=>"required",
                    ]
                ],
                "required"=>true,
                "rules"=>"required",
            ],
            [
                "group"=>true,
                "label"=>lang("book_weekdays"),
                "input"=>[
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"Y",
                        "name"=>"TIME_WEEK_MON",
                        "text"=>lang('mon'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"Y",
                        "name"=>"TIME_WEEK_TUE",
                        "text"=>lang('tue'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"Y",
                        "name"=>"TIME_WEEK_WED",
                        "text"=>lang('wed'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"Y",
                        "name"=>"TIME_WEEK_THU",
                        "text"=>lang('thu'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"Y",
                        "name"=>"TIME_WEEK_FRI",
                        "text"=>lang('fri'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"Y",
                        "name"=>"TIME_WEEK_SAT",
                        "text"=>lang('sat'),
                        "class"=>"week_class "
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"Y",
                        "name"=>"TIME_WEEK_SUN",
                        "text"=>lang('san'),
                        "class"=>"week_class "
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"Y",
                        "name"=>"TIME_WEEK_ALL",
                        "text"=>lang('no_holiday'),
                        "class"=>"week_class all_time_class"
                    ],
                ],
                "required"=>true,
                "rules"=>"required",
            ],
            [
                "label"=>lang('book_time'),
                "name"=>'TIME_UNIT',
                "type"=>"dropdown",
                "options"=>[
                    "5" => lang('min5'),
                    "10" => lang('10min'),
                    "15" => lang('15min'),
                    "30" => lang('30min'),
                    "60" => lang('1hour'),
                    "120" => lang('2hour'),
                    "1440" => lang('1day'),
                ],
                "required"=>true,
                "rules"=>"required",
            ],
            [
                "label"=>lang("number_of_persons"),
                "name"=>"NUMBER_PERSON",
                "type"=>"number",
            ],
            
        ];
        $rows = $this->db->where('BRANCH_ID', $this->auth['branch_id'])->get("reservation_entry_setting")->row_array();
        
        if($rows['DESCRIPTION_YN'] == 'Y'){
            $temp = [
                "label"=>lang("description"),
                "name"=>"DESCRIPTION",
                "type"=>"textarea",
            ];
            array_push($this->fields, $temp);
        }
        

        $this->table_fields = [
            [
                "label"=>lang('fix'),//"Fix",
                "type"=>"edit",
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete"
            ],
            
        ];

        //$users[""] = lang('none');
        $this->search_fields = [
            
        ];
        //$this->preview_url = site_url("preview/coupon");
    }


    protected function build_query($params) {
        $USER_ID = element("USER_ID", $params);
        $RESERVATION_DATE = element("RESERVATION_DATE", $params);
        if ($USER_ID != "") $this->db->where("USER_ID", $USER_ID);
        $this->db->where('BRANCH_ID', $this->auth["branch_id"]);
        if ($RESERVATION_DATE != "") $this->db->like("RESERVATION_DATETIME", $RESERVATION_DATE);
    }
}
