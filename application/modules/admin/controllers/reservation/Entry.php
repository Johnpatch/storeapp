<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Entry extends ADMIN_Controller
{
    protected $table = "reservation_entry_setting";
    protected $route_prefix = 'admin/reservation/entry/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = false;//true if this controller has list pages
    protected $has_order = false;
    protected $has_add = true;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $index_has_preview = false;
    protected $publish_has_preview = false;
    protected $list_description = '';
    protected $has_search = false;
    protected $search_fields = [];
    protected $edit_title = '';

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->head['bread_title'] = lang("reservation");
        $this->head['page_subtitle'] = lang("book_input_setting");
        $this->head['title'] = lang("reservation");
        $this->head["description"] = "";
        $this->page_title = lang("reservation");
        $this->page_subtitle = lang("book_input_setting");
        $this->panel_title = lang("book_input_setting");
        $this->list_description = lang('inquiry_form_desc');
        $this->has_search = false;
        $this->edit_title = lang("book_input_setting");
        $this->fields = [
            [
                "label"=>lang("name"),
                "name"=>"NAME_YN",
                "type"=>"checkbox",
                "checked"=>"Y",
                "unchecked"=>"N",
                "default"=>"N",
                "show_text" => lang('showing'),
                "hide_text" => lang('Hide'),
            ],
            [
                "label"=>lang("phone"),
                "name"=>"PHONE_YN",
                "type"=>"checkbox",
                "checked"=>"Y",
                "unchecked"=>"N",
                "default"=>"N",
                "show_text" => lang('showing'),
                "hide_text" => lang('Hide'),
            ],
            [
                "label"=>lang("email_address"),
                "name"=>"EMAIL_YN",
                "type"=>"checkbox",
                "checked"=>"Y",
                "unchecked"=>"N",
                "default"=>"N",
                "show_text" => lang('showing'),
                "hide_text" => lang('Hide')
            ],
            [
                "label" => '',
                "name"=>"PERSON_YN",
                "type"=>"checkbox",
                "checked"=>"Y",
                "unchecked"=>"N",
                "default"=>"人数",
                "show_text" => lang('showing'),
                "hide_text" => lang('Hide'),
                "label_edit" => true,
                "label_name" => "PERSON_NAME"
            ],
            [
                "label"=>lang("description"),
                "name"=>"DESCRIPTION_YN",
                "type"=>"checkbox",
                "checked"=>"Y",
                "unchecked"=>"N",
                "default"=>"N",
                "show_text" => lang('showing'),
                "hide_text" => lang('Hide')
            ],
        ];
        
    }
}
