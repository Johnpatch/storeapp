<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Management extends ADMIN_Controller
{
    protected $table = "reservation";
    protected $route_prefix = 'admin/reservation/management/';//Dont't forget to attach '/' to the end of the string
    protected $has_index = true;//true if this controller has list pages
    protected $has_order = false;
    protected $has_add = false;
    protected $page_title = "";
    protected $page_subtitle = "";
    protected $panel_title = "";
    protected $index_has_preview = false;
    protected $publish_has_preview = false;

    //Publish settings
    protected $form_description="";
    //Success settings
    protected $success_return_text = "戻る";

    public function __construct()
    {
        parent::__construct();

        $this->login_check();
    }

    protected function initialize() {
        $this->head['bread_title'] = lang("reservation");
        $this->head['page_subtitle'] = lang("reservation_management");
        $this->head['title'] = lang("reservation_management");
        $this->head["description"] = "";
        $this->page_title = lang("reservation");
        $this->page_subtitle = lang("reservation_management_space");
        $this->panel_title = lang("reservation_management_space");

        $rows = $this->db->where("BRANCH_ID", $this->auth["branch_id"])
            ->get("users")->result_array();
        $users = array();
        foreach ($rows as $row) {
            $users[$row["USER_ID"]] = $row["NICK_NAME"];
        }
        $rows = $this->db->where('BRANCH_ID', $this->auth['branch_id'])->get("reservation_entry_setting")->row_array();

        $this->fields = [
            [
                "label"=>lang("user"),
                "name"=>"NAME",
                "type"=>"text",
            ],
            [
                "label"=>lang("reservation_date"),
                "name"=>"RESERVATION_DATE",
                "type"=>"datetime",
                "picker_type"=>'single'
            ],
            [
                "label"=>lang("reservation_time"),
                "name"=>"RESERVATION_TIME",
                "type"=>"text",
            ],
            [
                "label"=>lang("name"),
                "name"=>"NAME",
                "type"=>"text",
            ],
            [
                "label"=>lang("phone"),
                "name"=>"PHONE",
                "type"=>"text",
            ],
        ];
        if($rows['EMAIL_YN'] == 'Y'){
            $temp = [
                "label"=>lang("email_address"),
                "name"=>"EMAIL",
                "type"=>"text",
            ];
            array_push($this->fields, $temp);
        }
        if($rows['PERSON_YN'] == 'Y'){
            $temp = [
                "label"=>lang("number_of_persons"),
                "name"=>"NUMBER_OF_RPERSONS",
                "type"=>"number",
            ];
            array_push($this->fields, $temp);
        }
        

        $temp =[
            "label"=>lang("status"),
            "name"=>"STATUS",
            "type"=>"radio",
            "default"=>"1",
            "options"=>[
                "1"=>lang("on_going"),
                "2"=>lang("cancel"),
                "3"=>lang("accepted")
            ]
        ];
        array_push($this->fields, $temp);

        $this->table_fields = [
            [
                "label"=>lang('fix'),//"Fix",
                "type"=>"edit",
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete"
            ],
            [
                "label"=>lang("name"),
                "name"=>"NAME",
                "type"=>"text",
            ],
            [
                "label"=>lang("reservation_time"),
                "name"=>"RESERVATION_DATE",
                "type"=>"datetime",
                "picker_type"=>'single'
            ],
            [
                "label"=>lang("status"),
                "name"=>"STATUS",
                "type"=>"radio",
                "default"=>"1",
                "options"=>[
                    "1"=>lang("on_going"),
                    "2"=>lang("cancel"),
                    "3"=>lang("accepted")
                ],
            ],
        ];

        $this->search_fields = [
            [
                "label"=>lang("name"),
                "name"=>"NAME",
                "type"=>"text",
            ],
            [
                "label"=>lang("reservation_time"),
                "name"=>"RESERVATION_DATE",
                "type"=>"date",
            ],
        ];
        $this->preview_url = site_url("preview/coupon");
    }

    protected function build_query($params) {
        $NAME = element("NAME", $params);
        $RESERVATION_DATE = element("RESERVATION_DATE", $params);
        if ($NAME != "") $this->db->where("NAME", $NAME);
        $this->db->where('BRANCH_ID', $this->auth["branch_id"]);
        if ($RESERVATION_DATE != "") $this->db->like("RESERVATION_DATE", $RESERVATION_DATE);
    }
}
