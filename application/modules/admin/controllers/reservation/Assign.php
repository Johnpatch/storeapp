<?php

/*
 * @Author:    Kiril Kirkov
 *  Gitgub:    https://github.com/kirilkirkov
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Assign extends ADMIN_Controller
{

    protected $table = "reservation_assign";
    protected $route_prefix = 'admin/reservation/assign/'; //Dont't forget to attach '/' to the end of the string
    protected $has_index = true; //true if this controller has list pages
    protected $publish_has_preview = false;
    protected $index_has_preview = false;

    protected $page_icon = "<i class='fa fa-sitemap'></i>";

    protected $has_add = true;
    protected $has_order = false;
    protected $success_return_text = "戻る";
    /*
     * Input Fields Description
     * name: input name----Must be exactly equal to corresponding DB column name
     * type: input type [text|file|checkbox|radio|textarea|static|dropdown|color|...]
     * default: default value if initial value is not set
     * label: input label description
     * help: help block description
     * required: whether this field is required or not
     * showIf: array(key=>value). shows this element when (key=value)
     *          value can take array values.
     * rules: validation settings
     */
    protected $list_description;
    protected $confirm_fields;
    protected $preview_url;


    public function __construct()
    {
        parent::__construct();

        $this->login_check();
        $this->head['result'] = $this->db->where('ID',$this->auth['PARENT_ID'])->get('basic_setting')->row_array();
        $this->head['store_name'] = $this->db->where('ID',$this->auth['branch_id'])->get('branches')->row_array();
    }

    protected function initialize()
    {
        $this->head['title'] = lang('reservation');
        $this->head["description"] = "";
        $this->page_title = lang('reservation');
        $this->page_subtitle = lang('assignee_settings');
        $this->panel_title = lang('assignee_settings');
        $this->head["bread_title"] =  lang("reservation");
        $this->fields = [
            [
                "label" => lang('name'),
                "type" => "text",
                "name" => "NAME"
            ],
            [
                "label" => lang("phone"),
                "type" => "text",
                "name" => "PHONE"
            ],
            [
                "group" => true,
                "label" => lang("work_hour"),
                "between"=>"<span class='between'>~</span>",
                "input" => [
                    [
                        "name" => "WORK_START_TIME",
                        "type" => "time",
                        "class" => "from_to_book",
                    ],
                    [
                        "name" => "WORK_END_TIME",
                        "type" => "time",
                        "class" => "from_to_book",
                    ]
                ],
            ],
            [
                "group"=>true,
                "label"=>lang("rest_week"),
                "input"=>[
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"N",
                        "name"=>"REST_MON",
                        "text"=>lang('mon'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"N",
                        "name"=>"REST_TUE",
                        "text"=>lang('tue'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"N",
                        "name"=>"REST_WED",
                        "text"=>lang('wed'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"N",
                        "name"=>"REST_THU",
                        "text"=>lang('thu'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"N",
                        "name"=>"REST_FRI",
                        "text"=>lang('fri'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"N",
                        "name"=>"REST_SAT",
                        "text"=>lang('sat'),
                        "class"=>"week_class"
                    ],
                    [
                        "type"=>"checkbox",
                        "checked"=>"Y",
                        "unchecked"=>"N",
                        "default"=>"N",
                        "name"=>"REST_SUN",
                        "text"=>lang('san'),
                        "class"=>"week_class"
                    ],
                ],
            ],
            
        ];
        $this->table_fields = [
            [
                "label"=>lang('fix'),
                "type"=>"edit",
            ],
            [
                "label"=>lang("delete"),
                "type"=>"delete"
            ],
            [
                "label" => lang('name'),
                "type" => "text",
                "name" => "NAME"
            ]
        ];

        $this->search_fields = [
            [
                "name" => 'NAME',
                "type" => 'text',
                "label" => lang('name'),
            ],
        ];
        $this->form_description = '';
    }

    protected function build_query($params) {
        $this->db->where('BRANCH_ID', $this->auth["branch_id"]);
        $name = element("NAME", $params);
        if ($name != "") $this->db->like("NAME", $name);
    }
}
