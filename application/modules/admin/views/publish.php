<?php 
$form_description = "本部の情報を入力してください。<br>
                    ※本部の名前と住所は、本部からプッシュ通知配信の際に、配信元情報として表示されます。" ;
$page_title = "アプリ作成";
$page_subtitle = "編集"; 
$errors = 0;
$has_preview = true;
?>
<style>
    .faq-panel{
 
 color: #333;
 background-color: #fcfcfc;
 border-color: #ddd;

}
.faq-panel>.panel-heading{
 
 color: #333;
 background-color: #fcfcfc;
 border-color: #ddd;
}
.faq-panel>.panel-heading>.heading-elements {

 background-color: #f5f5f5 !important;
}
</style>
    <script type="text/javascript" src="<?= base_url('assets/code/highcharts.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/code/modules/exporting.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/code/modules/export-data.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/code/modules/accessibility.js"')?>"></script>
    <style type="text/css">
        .highcharts-figure, .highcharts-data-table table {
            min-width: 320px; 
            max-width: 800px;
            margin: 1em auto;
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #EBEBEB;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }
        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }
        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }
        .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
            padding: 0.5em;
        }
        .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }
        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }


        input[type="number"] {
            min-width: 50px;
        }

        </style>
<div class="content-wrapper" >
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title" >
                <h4> <span class="page-maintitle"><?=$page_title?></span> 
                    <span class="page-subtitle"> <?=$page_subtitle?><span></h4>
            </div>
        </div>
        <div class = "cms-breadcrumb">
        <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><a href="#" class= "breadcrumb-1"><?=lang('home')?></a></li>
                <li><a href="#" class= "breadcrumb-1">アプリ作成</a></li>
                <li class="active breadcrumb-2"> 編集</li>
            </ul>
        </div>
        </div>
    </div>

    <div class="panel-body home-page">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"> <?=lang('graph')?></h6>
                    </div>
                    <div class="panel-body" style="padding-top:10px;">
                        <figure class="highcharts-figure">
                            <div id="linecontainer"></div>
                            <p class="highcharts-description">
                                
                            </p>
                        </figure>
                    </div>
                 </div>
             </div>
        </div>
    </div>
  
    <script type="text/javascript">
        Highcharts.chart('linecontainer', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Monthly Average Temperature'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                // categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
                title: {
                    text: 'Temperature (°C)'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                },
                series: {
                    label: {
                        connectorAllowed: true
                    },
                    pointStart: 1
                }
            },
            series: [{
                name: 'Android',
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }, {
                name: 'IOS',
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }]
        });
    </script>
    <!-- pie chart -->
      
    <div class="panel-body home-page">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"><?=lang('graph')?></h6>
                    </div>
                    <div class="panel-body" style="padding-top:10px;">             
                        <figure class="highcharts-figure">
                            <div id="piecontainer"></div>
                            <p class="highcharts-description">
                            
                            </p>
                        </figure>
                    </div>
                 </div>
             </div>
        </div>
    </div>
    <script type="text/javascript">
        Highcharts.chart('piecontainer', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Browser market shares in January, 2018'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Chrome',
                    y: 61.41,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Internet Explorer',
                    y: 11.84
                }, {
                    name: 'Firefox',
                    y: 10.85
                }, {
                    name: 'Edge',
                    y: 4.67
                }, {
                    name: 'Safari',
                    y: 4.18
                }, {
                    name: 'Sogou Explorer',
                    y: 1.64
                }, {
                    name: 'Opera',
                    y: 1.6
                }, {
                    name: 'QQ',
                    y: 1.2
                }, {
                    name: 'Other',
                    y: 2.61
                }]
            }]
        });
                </script>
    <div class="">
        
        <div class="panel" >
            
            <div class="panel-heading" >基本情報</div>
            <div class="panel-body" style="padding-top:10px;">
                <div class = "row">
                    <div class = "col-md-9">
                        <p class="content-group">
                            
                            <?=$form_description?>
                            <?php if ($errors) {
                                foreach ($errors as $error) {
                                    echo $error;
                                }
                            }?>
                        </p>

                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <!-- <tr>
                                        <th class = "col-md-3">Class</th>
                                        <th class = "col-md-9">Description</th>
                                    </tr> -->
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class = "col-md-3" > 
                                            <span class='control-label left-label'>お店の名前
                                            </span>
                                            <span class='label label-rounded label-danger'> 必須
                                            </span>
                                        </td>
                                        <td class = "col-md-9" > 
                                            <input type="text" class="form-control" placeholder="お店の名前">
                                            <span class='help-block help-area help-style'>お店の名前   を入力して下さい。（全角20文字以内）
                                                    ※絵文字はご利用できません</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-3" > 
                                            <span class='control-label left-label'>お店の名前
                                            </span>
                                            <span class='label label-rounded label-danger'> 必須
                                            </span>
                                        </td>
                                        <td class = "col-md-9" > 
                                            <input type="text" class="form-control" placeholder="お店の名前">
                                            <span class='help-block help-area help-style'>お店の名前   を入力して下さい。（全角20文字以内）
                                                    ※絵文字はご利用できません</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-3" > 
                                            <span class='control-label left-label'>お店の名前
                                            </span>
                                            <span class='label label-rounded label-danger'> 必須
                                            </span>
                                        </td>
                                        <td class = "col-md-9" > 
                                            <input type="text" class="form-control" placeholder="お店の名前">
                                            <span class='help-block help-area help-style'>お店の名前   を入力して下さい。（全角20文字以内）
                                                    ※絵文字はご利用できません</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-3" > 
                                            <span class='control-label left-label'>お店の名前
                                            </span>
                                            <span class='label label-rounded label-danger'> 必須
                                            </span>
                                        </td>
                                        <td class = "col-md-9" > 
                                            <input type="text" class="form-control" placeholder="お店の名前">
                                            <span class='help-block help-area help-style'>お店の名前   を入力して下さい。（全角20文字以内）
                                                    ※絵文字はご利用できません</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-3" > 
                                            <span class='control-label left-label'>お店の名前
                                            </span>
                                            <span class='label label-rounded label-danger'> 必須
                                            </span>
                                        </td>
                                        <td class = "col-md-9" > 
                                            <input type="text" class="form-control" placeholder="お店の名前">
                                            <span class='help-block help-area help-style'>お店の名前   を入力して下さい。（全角20文字以内）
                                                    ※絵文字はご利用できません</span>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                    <div class = "col-md-3">
                        <?php if ($has_preview) {
                            $this->load->view('_parts/preview');
                        }
                        ?>
                    </div >
                </div>
            </div>
        </div>
      
    </div>
</div>

<script>

    <?php if ($has_preview):?>
    function updatePreview() {
        var temp_form = $("#layout_form").clone();
        temp_form.attr("action", "<?= $preview_url?>");
        temp_form.attr("target", "preview-frame");
        /*temp_form.find("input").each(function(i, target) {
         $(target).attr("name", $(target).attr("name").toUpperCase());
         });*/
        $("body").append(temp_form);
        temp_form.submit();
        
        temp_form.remove();
    }
    $(function() {
        updatePreview();
        $("input, textarea, select").change(function() {
            updatePreview();
        });
    });
    <?php endif?>

    
</script>