<script>
    function webview_remove(no) {
        if(confirm("Do you want to delete?")){
            $('#webview_item'+no).remove();
        }
    }
</script>
<script>
    $(function() {
        $(document).on("click", ".confirm-delete", function (e) {
            e.preventDefault();
            var no = $(this).data('no');
            bootbox.confirm({
                message: "このデータを削除します。よろしいですか？",
                buttons: {
                    confirm: {
                        label: 'はい',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: '取り消す',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $('#webview_item'+no).remove();
                    }
                }
            });
        });

        $('#add_btn').click(function() {
            var title = $('#webview_title').val();
            var url = $('#webview_url').val();
            $('#webview_title').val("");
            $('#webview_url').val("");
            var show = $("input[name='webview_option']:checked").val();
            if(url == '' || url == undefined) {
                return;
            } else {
                var no = $('#webview_items').data('no');
                var html = '<div class="row" style="margin-top: 10px;" id="webview_item' + no + '">';
                html +=     '<div class="col-md-4">';
                html +=         '<input type="text" class="form-control" id="real-webview-title'+no+'" data-no="' + no + '" value="' + title + '" placeholder="<?php echo lang('webview_title');?>"/>';
                html +=     '</div>';
                html +=     '<div class="col-md-4">';
                html +=         '<input type="text" class="form-control real-webview-url" data-no="' + no + '" value="' + url + '" placeholder="<?php echo lang('webview_url');?>"/>';
                html +=     '</div>';
                html +=     '<div class="col-md-3">';
                if(show == '1') {
                    html +=         '<label class="radio-inline"><input type="radio" name="webview_option' + no + '" checked value="1" class="styled"><?php echo lang('Show');?></input></label>';
                    html +=         '<label class="radio-inline"><input type="radio" name="webview_option' + no + '" value="0" class="styled"><?php echo lang('Hide');?></input></label>';
                } else {
                    html +=         '<label class="radio-inline"><input type="radio" name="webview_option' + no + '" value="1" class="styled"><?php echo lang('Show');?></input></label>';
                    html +=         '<label class="radio-inline"><input type="radio" name="webview_option' + no + '" checked value="0" class="styled"><?php echo lang('Hide');?></input></label>';
                }
                html +=     '</div>';
                html +=     '<div class="col-md-1">';
                html +=         '<button type="button" class="photo-item-remove btn common-btn-operation-trash custom-btn confirm-delete" data-no="'+no+'"><i class="fa fa-trash-o"></i></button>';
                html +=     '</div>';
                html += '</div>';
                $('#webview_items').append(html);
                $('#webview_items').data('no', parseInt(no) + 1);

                $(".styled, .multiselect-container input").uniform({
                    radioClass: 'choice'
                });
            }
        });

        $('#save_btn').click(function() {
            var html = '';
            $('.real-webview-url').each(function(){
                var url = $(this).val();
                var no = $(this).data('no');
                var title = $("#real-webview-title"+no).val();
                if(url != '') {
                    var show = $("input[name='webview_option" + no + "']:checked").val();
                    html += '<div class="row" style="margin-top: 10px;">';
                    html +=     '<div class="col-md-4">' + title + '</div>';
                    html +=     '<div class="col-md-5">' + url + '</div>';
                    if(show == "1") {
                        html +=     '<div class="col-md-3"><?php echo lang('Show');?></div>';
                    } else {
                        html +=     '<div class="col-md-3"><?php echo lang('Hide');?></div>';
                    }
                    html += '</div>';
                }
            });
            if(html != '') {
                $('#webview_result').html("");
                $('#webview_result').append(html);
                $('#webview_result').show();
                $('#webview_items').hide();
                $('#save_btn').hide();
                $('#register_btn').show();
                $('#edit_btn').show();
            }
        });

        $('#edit_btn').click(function() {
            $('#webview_result').hide();
            $('#webview_items').show();
            $('#save_btn').show();
            $('#register_btn').hide();
            $('#edit_btn').hide();
        });

        $('#return_repair_btn').click(function() {
            location.reload();
        });

        $('#register_btn').click(function() {
            var urls = '';
            var titles = '';
            var shows = '';
            $('.real-webview-url').each(function(){
                var url = $(this).val();
                if(url != '') {
                    var no = $(this).data('no');
                    var title = $("#real-webview-title"+no).val();
                    var show = $("input[name='webview_option" + no + "']:checked").val();
                    urls += url + '||||';
                    titles += title + '||||';
                    shows += show + '||||';   
                }
            });
            if(urls != '') {
                urls = urls.substring(0, urls.length-4);
                titles = titles.substring(0, titles.length-4);
                shows = shows.substring(0, urls.length-4);
                $.ajax({
                    url: '<?php echo site_url("admin/webview/add_url")."?token=".$token;?>',
                    type: 'POST',
                    data: {
                        url: urls,
                        title: titles,
                        show: shows
                    },
                    success: function (resp) {
                        $('#edit_formgroup').hide();
                        $('#success_formgroup').show();

                        $('#register_btn').hide();
                        $('#edit_btn').hide();
                    },
                    error: function () {
                    }
                });
            }            
        });
    });
</script>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/home') ?>" class="breadcrumb-1"><?= lang('basic_information') ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done') ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $page_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group">
                            <?= $form_description ?>
                        </p>

                        <form action="" id="layout_form" method="post" class="form-horizontal form-bordered">
                            <input type="hidden" name="back_url" value="<?=current_url()?>">
                            <input type="hidden" name="BRANCH_ID" value="<?= $branch_id?>">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <tr id="edit_formgroup">
                                            <td class="col-md-3"><?php echo lang('webview_settings_text'); ?></td>
                                            <td class="col-md-9" id="webview_items" data-no="<?php echo count($webview_items) + 1;?>">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" id="webview_title" placeholder="<?php echo lang('webview_title');?>"/>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <input type="text" class="form-control" id="webview_url" placeholder="<?php echo lang('webview_url');?>"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="webview_option" value="1" checked="checked" class="styled">
                                                            <?php echo lang('Show');?>
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="webview_option" value="0" class="styled">
                                                            <?php echo lang('Hide');?>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 10px;">
                                                    <div class="col-md-12">
                                                        <button type="button" class="btn common-btn-green-small custom-btn" id="add_btn">
                                                            <i class="fa fa-plus-circle"></i>
                                                            <?php echo lang('webview_add_btn_label');?>
                                                        </button>
                                                    </div>
                                                </div>
                                                <hr>
                                                <label><?php echo lang('list_of_webviews');?></label>
                                                <?php foreach ($webview_items as $key => $webview_item) { ?>
                                                <div class="row" style="margin-top: 10px;" id="webview_item<?php echo intval($key)+1;?>">
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" id="real-webview-title<?php echo intval($key)+1;?>" data-no="<?php echo intval($key)+1;?>" value="<?php echo $webview_item['TITLE'];?>" placeholder="<?php echo lang('webview_title');?>"/>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control real-webview-url" data-no="<?php echo intval($key)+1;?>" value="<?php echo $webview_item['URL'];?>" placeholder="<?php echo lang('webview_url');?>"/>
                                                    </div>
                                                    <div class="col-md-3">
                                                    <?php if ($webview_item['IS_SHOW'] == '1')  { ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="webview_option<?php echo intval($key)+1;?>" checked value="1" class="styled"></input>
                                                            <?php echo lang('Show');?>
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="webview_option<?php echo intval($key)+1;?>" value="0" class="styled"><?php echo lang('Hide');?></input>
                                                        </label>
                                                    <?php } else { ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="webview_option<?php echo intval($key)+1;?>" value="1" class="styled"><?php echo lang('Show');?></input>
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="webview_option<?php echo intval($key)+1;?>" checked value="0" class="styled"><?php echo lang('Hide');?></input>
                                                        </label>
                                                    <?php } ?>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <button type="button" class="photo-item-remove btn common-btn-operation-trash custom-btn confirm-delete" data-no="<?php echo intval($key)+1;?>"><i class="fa fa-trash-o"></i></button>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </td>
                                            <td class="col-md-9" id="webview_result" style="display: none">
                                            </td>
                                        </tr>
                                        <tr id="success_formgroup" style="display: none;">
                                            <td style="text-align: center;">
                                                <label><?php echo lang('register_success');?></label>
                                            </td>
                                            <td style="text-align: center;">
                                                <button type="button" class="btn common-btn-red-medium custom-btn" style="margin-right: 20px;" id="return_repair_btn"><i class="fa fa-check"></i> <?= lang('return_to_repair') ?></button>
                                                <a href="<?php echo site_url('admin/home');?>" type="button" class="btn common-btn-green-medium custom-btn"><i class="fa fa-check"></i> <?= lang('return_to_toppage') ?></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="button" class="btn common-btn-green-small custom-btn" id="save_btn"><?= lang('save') ?></button>
                                <button type="button" class="btn common-btn-red-medium custom-btn" id="register_btn" style="display: none;"><i class="fa fa-check"></i> <?= lang('register') ?></button>
                                <button type="button" class="btn common-btn-green-small custom-btn" id="edit_btn" style="display: none;"><?= lang('return_to_repair') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>