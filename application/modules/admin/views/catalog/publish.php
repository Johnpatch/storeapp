<script>
    function renderInputs() {
        var checked_type = $("input[name='TYPE']:checked").val();
        if(checked_type == 1) {//Checked PDF
            $("tr.THUMBNAIL").next().addClass('hide');
            $("tr.PDF").removeClass('hide');
        } else {// Checked PDF
            $("tr.THUMBNAIL").next().removeClass('hide');
            $("tr.PDF").addClass('hide');
        }
    }
    renderInputs();
    $("input[name='TYPE']").change(function() {
        renderInputs();
    });
</script>