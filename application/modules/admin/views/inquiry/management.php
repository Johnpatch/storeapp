<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done')?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $panel_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mb-20">
                            <button onclick="add_val()" class='common-btn-green-small custom-btn'>
                                <i class="fa fa-plus-circle"></i>&nbsp;&nbsp;<?= lang('sign_up') ?>
                            </button>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="table-responsive clear-both">
                            <table class="table table-bordered" id="setting_table">
                                <?php foreach($rows as $row) { ?>
                                <tr>
                                    <td width="100">
                                        <button class='btn common-btn-operation-edit custom-btn' style="padding: 7px 12px;" onclick="edit_box(<?= $row['ID'] ?>, <?= $row['TYPE'] ?>)" id="btn-change-<?= $row['ID'] ?>">
                                            <span><?= lang('edit') ?></span>
                                        </button>
                                    </td>
                                    <td width="100">
                                        <?php 
                                        if($row['TYPE'] == 1) { ?>
                                        <button class='btn common-btn-operation-trash custom-btn' style="padding: 7px 12px;" onclick="remove_box(<?= $row['ID'] ?>)" >
                                            <span><?= lang('delete') ?></span>
                                        </button>
                                        <?php } ?>
                                    </td>
                                    <td class="td-valign">
                                        <?php 
                                        if($row['TYPE'] == 1)
                                            echo "<input readonly class='form-control' type='text' id='name-input-".$row['ID']."' value='".$row['NAME']."' />";
                                        else
                                            echo $row['NAME'];
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $id = $row['ID'];
                                        if($row['TYPE'] == 1){
                                            $text = lang('publishing_settings');
                                            $checked = $row['STATUS_SHOW'] ;
                                        
                                            $result = "
                                                <div id='disable-$id' class='custom-checkbox-content checkbox " . ($checked == 'Y' ? ' selected ' : '') .$class . "' disabled data-checked-v='Y' data-unchecked-v='N' data-id='$id'>
                                                    <div class='check-icon' style='display: inline-block;'>
                                                        <i class='fa fa-check' style='display: none;'></i>
                                                    </div>
                                                    <label style='vertical-align: top'>$text</label>
                                                </div>
                                                <input type='hidden' value='$checked' id='check-input-$id' />
                                            ";
                                            echo $result;
                                        } else if($row['TYPE'] == 2){
                                            $result = "<input readonly class='form-control' type='text' name='check-input-$id' id='check-input-$id' value='".$row['ADMIN_EMAIL']."' />";
                                            echo $result;
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php } ?>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function edit_box(id, type){
    if(type == 2){
        if($('#check-input-'+id).is('[readonly]')){
            document.getElementById('btn-change-'+id).innerHTML   = '<?= lang('save') ?>';
            $('#check-input-'+id).prop('readonly', false);
            $('#btn-change-'+id).removeClass('common-btn-operation-edit');
            $('#btn-change-'+id).addClass('common-btn-green-small');
        }else{
            document.getElementById('btn-change-'+id).innerHTML   = '<?= lang('edit') ?>';
            $('#check-input-'+id).prop('readonly', true);
            $('#btn-change-'+id).removeClass('common-btn-green-small');
            $('#btn-change-'+id).addClass('common-btn-operation-edit');
            request(id,type);
        }
    }
    else if(type == 1){
        if($('#name-input-'+id).is('[readonly]')){
            document.getElementById('btn-change-'+id).innerHTML   = '<?= lang('save') ?>';
            $('#name-input-'+id).prop('readonly', false);
            $('#disable-'+id).removeAttr('disabled');
            $('#btn-change-'+id).removeClass('common-btn-operation-edit');
            $('#btn-change-'+id).addClass('common-btn-green-small');
        }else{
            document.getElementById('btn-change-'+id).innerHTML   = '<?= lang('edit') ?>';
            $('#name-input-'+id).prop('readonly', true);
            $('#disable-'+id).attr('disabled', true);
            $('#btn-change-'+id).removeClass('common-btn-green-small');
            $('#btn-change-'+id).addClass('common-btn-operation-edit');
            request(id,type);
        }
    }
}
function request(id,type){
    if(type == 2){
        $.ajax({
            url: '<?php echo site_url("admin/inquiry/management/save_val")."?token=".$token;?>',
            type: 'POST',
            data: {
                type: type,
                value: $('#check-input-'+id).val(),
                id: id
            },
            success: function (resp) {
            },
        });
    }else if(type == 1){
        $.ajax({
            url: '<?php echo site_url("admin/inquiry/management/save_val")."?token=".$token;?>',
            type: 'POST',
            data: {
                type: type,
                value: $('#check-input-'+id).val(),
                name: $('#name-input-'+id).val(),
                id: id
            },
            success: function (resp) {
            },
        });
    }
}
function add_val(){
    $.ajax({
        url: '<?php echo site_url("admin/inquiry/management/add_val")."?token=".$token;?>',
        type: 'POST',
        data: {
        },
        success: function (resp) {
            location.reload();
        },
    });
}
function remove_box(id){
    if(!confirm('このデータを削除します。よろしいですか？')){
        return;
    }else{
        $.ajax({
            url: '<?php echo site_url("admin/inquiry/management/remove_box")."?token=".$token;?>',
            type: 'POST',
            data: {
                id: id
            },
            success: function (resp) {
                location.reload();
            },
        });
    }
}
</script>