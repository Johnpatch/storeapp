<?php
$form_description = lang("analysis_push_ctr_description");

$search_description = lang("analysis_pushnotificaiotn_serach_description");

$page_title = lang("analysis");
$page_subtitle = lang("push_notification_ctr");
$errors = 0;
$has_preview = true;
?>
<style>
    .faq-panel{ color: #333; background-color: #fcfcfc; border-color: #ddd;}
    .faq-panel>.panel-heading{ color: #333; background-color: #fcfcfc; border-color: #ddd;}
    .faq-panel>.panel-heading>.heading-elements { background-color: #f5f5f5 !important;}
    .highcharts-exporting-group{display: none !important;}
</style>
<script type="text/javascript" src="<?= base_url('assets/code/highcharts.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/exporting.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/export-data.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/accessibility.js"')?>"></script>
<style type="text/css">
    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
    input[type="number"] {
        min-width: 50px;
    }

</style>
<div class="content-wrapper" >
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title" >
                <h4> <span class="page-maintitle"><?=$page_title?></span>
                    <span class="page-subtitle"> <?=$page_subtitle?><span></h4>
            </div>
        </div>
        <div class = "cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle?></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading">
                <?= lang('search_condition') ?>
            </div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group"><?= $form_description ?></p>
                        <form method="GET" id="searchProductsForm" action="" class="form-horizontal form-bordered">
                            <div class="search-condition-content">
                            <input type="hidden" name="token" value="<?= $token ?>">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody>
                                    <!--tr>
                                        <td class='col-md-3 td-valign'><?= lang('branch_name')?></td>
                                        <td class='col-md-9 td-valign'>
                                        <select name="selected_branch" class="form-control cursor-pointer" >
                                            <?php foreach ($branch_name as $row) { ?>
                                                <option value='<?= $row->ID ?>'><?= $row->NAME ?></option>
                                            <?php } ?>
                                            </select>
                                        </td>
                                    </tr-->
                                    <tr>
                                        <td class='col-md-3 td-valign'><?= lang('year_month')?></td>
                                        <td class='col-md-9 td-valign'>
                                            <select name="selected_year" class="select_year_month form-control year-count-select cursor-pointer" >
                                                <?php for($i = $start_year; $i < $last_year; $i ++):?>
                                                    <?php if(isset($_GET['selected_year'])):?>
                                                        <option <?= ($_GET['selected_year'] == $i) ? 'selected' : ''?> value="<?= $i?>"><?= $i?></option>
                                                    <?php else:?>
                                                        <option <?= (date('Y') == $i) ? 'selected' : ''?> value="<?= $i?>"><?= $i?></option>
                                                    <?php endif;?>
                                                <?php endfor;?>
                                            </select><span>&nbsp;&nbsp;&nbsp;&nbsp;年&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <select name="selected_month" class="select_year_month form-control cursor-pointer" >
                                                <?php for($j = 1; $j <= 12; $j ++):?>
                                                    <?php if(isset($_GET['selected_month'])):?>
                                                        <option <?= ($_GET['selected_month'] == $j) ? 'selected' : ''?> value="<?= $j?>"><?= $j?></option>
                                                    <?php else:?>
                                                        <option <?= (date('m') == $j) ? 'selected' : ''?> value="<?= $j?>"><?= $j?></option>
                                                    <?php endif;?>
                                                <?php endfor;?>
                                            </select><span>&nbsp;&nbsp;&nbsp;&nbsp;月</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="center" class="text-center" colspan="2">
                                            <button type="submit" class="btn btn-danger btn-lg gray-btn"><i class="fa fa-search"></i>&nbsp;<?= lang('search')?></button>
                                            <a href="<?=base_url('admin/analysis/pushnotification_ctr/CSVDownload?token='.$token)?>" class="btn btn-primary btn-xlg green-btn"><i class="fa fa-download"></i>&nbsp;<?= lang('csvdownload')?></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel" >
            <div class="panel-heading" ><?= lang('search_list')?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class = "row">
                    <div class = "col-md-12">
                        <p class="content-group"><?= $search_description ?></p>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th rowspan="2" class = "col-md-2"><strong><?= lang('distribution_title')?></strong></th>
                                    <th rowspan="2" class = "col-md-2"><strong><?= lang('send_date')?></strong></th>
                                    <th colspan="4" class = "col-md-4"><strong><?= lang("android")?></strong></th>
                                    <th colspan="4" class = "col-md-4"><strong><?= lang("ios")?></strong></th>
                                </tr>
                                <tr>
                                    <th  class = "col-md-1"><strong><?= lang("unread")?></strong></th>
                                    <th class = "col-md-1"><strong><?= lang("already_read")?></strong></th>
                                    <th  class = "col-md-1"><strong><?= lang("delivery_number")?></strong></th>
                                    <th class = "col-md-1"><strong><?= lang("open_rate")?></strong></th>
                                    <th class = "col-md-1"><strong><?= lang("unread")?></strong></th>
                                    <th class = "col-md-1"><strong><?= lang("already_read")?></strong></th>
                                    <th class = "col-md-1"><strong><?= lang("delivery_number")?></strong></th>
                                    <th class = "col-md-1"><strong><?= lang("open_rate")?></strong></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($search_table as $row) { ?>
                                    <tr>
                                        <?php foreach ($row as $field) { ?>
                                            <td valign="center" class="text-center">
                                                <?php echo $field ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>