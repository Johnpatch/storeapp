<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?=lang('home')?></a></li>
                    <li><a href="#" class="breadcrumb-1"> <?= $page_title ?></a></li>
                    <li class="active breadcrumb-2"> </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $panel_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-9">
                        <p class="content-group">
                            <?= $form_description ?>
                            <?php if ($errors) {
                                foreach ($errors as $error) {
                                    echo $error;
                                }
                            } ?>
                        </p>

                        <form action="<?= $confirm_url ?>" id="layout_form" method="post"
                              onsubmit="return submitForm('<?= $confirm_url ?>')">
                            <input type="hidden" name="back_url" value="<?= current_url() ?>">
                            <input type="hidden" name="BRANCH_ID" value="<?= $branch_id ?>">

                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class = "mb-20">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn common-btn-green-small custom-btn" onclick="submitMainForm('')">
                                    <i class="fa fa-check"></i> <?= lang('save') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($has_preview) {
            $this->load->view('_parts/preview');
        }
        ?>
    </div>
</div>

<script>
    var isView = false;
    var fields = <?=json_encode($fields)?>;
    var inputs = [];
    initInputs(inputs, fields);
    generateForm(fields, $("#form-body"));
    //    console.log(fields);
    <?php if ($has_preview):?>
    function updatePreview() {
        var temp_form = $("#layout_form").clone();
        temp_form.attr("action", "<?= $preview_url?>");
        temp_form.attr("target", "preview-frame");
        /*temp_form.find("input").each(function(i, target) {
         $(target).attr("name", $(target).attr("name").toUpperCase());
         });*/
        $("body").append(temp_form);
        temp_form.submit();
        temp_form.remove();
    }
    $(function () {
//        updatePreview();
//        $("input, textarea, select").change(function() {
//            updatePreview();
//        });
    });
    <?php endif?>
</script>