<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="panel-title"><?= $page_title?></div>
        <div class="panel-title panel-sub"><span class="panel-sub-title"><a href="<?= base_url('admin/home')?>"><?= lang('home')?></a> &gt; <?= $page_subtitle?></span></div>
    </div>
    <div class="panel-body home-page">
        <div class="row">
            <?php if ($has_search):?>
            <?php if($has_preview):?>
            <div class="col-md-9 layout-content">
                <?php else: ?>
                <div class="col-md-12">
                    <?php endif;?>
                    <div class="portlet box grey">
                        <div class="portlet-title condition-content">
                            <div class="caption">
                                <?= lang('search_condition') ?>
                                <div class="search-condition-detail">
                                    This is a search condition detail content.This is a search condition detail content.This is a search condition detail content.<br>
                                    This is a search condition detail content.This is a search condition detail content.<br>
                                    This is a search condition detail content.
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form method="GET" id="searchProductsForm" action="" class="form-horizontal form-bordered">
                                <div class='form-body'>
                                    <?php foreach($search_fields as $field) {
                                        if (element('group', $field) == true) {
                                            $data = array();
                                            foreach ($field["input"] as $field_input) {
                                                $data[$field_input["name"]] = $params[$field_input["name"]];
                                            }
                                            echo form_element($field, $data);
                                        }else {
                                            echo form_element($field, $params[$field["name"]]);
                                        }
                                    }?>
                                    <div class="search-action">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                               <?=lang('number_search_results_displayed')?>
                                                <?=perpage_select('perpage', $perpage_options, $perpage)?>
                                                <button type="submit" class="btn search-btn"><i class="fa fa-search"></i>&nbsp;&nbsp;<?= lang('search') ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <?php endif;?>
                <?php if($has_preview):?>
                <div class="col-md-9 layout-content">
                    <?php else: ?>
                    <div class="col-md-12">
                        <?php endif;?>
                        <div class="portlet box grey">
                            <div class="portlet-body form">
                                <div class="col-md-12 none-padding">
                                    <p class='page-info'>
                                        <?php if($title_index):?>
                                            <?=$list_description?>
                                        <?php else:?>
                                            <?=$list_description?>
                                        <?php endif;?>
                                    </p>
                                    <hr>
                                    <?php if ($has_add):?>
                                        <a href='<?= $add_url ?>' class='btn btn-success table-oper-btn'>
                                            <i class='fa fa-plus'></i>&nbsp;&nbsp;<?= lang('sign_up') ?>
                                        </a>
                                    <?php endif;?>
                                    <?php if ($has_order):?>
                                        <ul class="nav navbar-nav dropdown-right-bulk bulk-btn-menu">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle f-right bulk-btn" data-toggle="dropdown">Bulk Update <span class="caret"></span></a>
                                                <ul class="dropdown-menu dropdown-menu-right">
                                                    <li><a id="bulk-action" onclick="$('#order-update').submit()"><span class="bulk-option"><i class="fa fa-refresh"></i> Bulk Update</span></a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    <?php endif;?>
                                    <!--table @test start-->
                                    <div class="table-responsive clear-both">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <?php foreach ($table_fields as $column):?>
                                                    <th align="<?=element('align', $column, "center")?>"><?= $column['label'] ?></th>
                                                <?php endforeach;?>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <form id="order-update" method="POST" action="<?=$order_url?>">
                                                <?php foreach ($rows as $row):?>
                                                    <tr>
                                                        <?php foreach ($table_fields as $column):?>
                                                            <td align="<?=element('align', $column, "center")?>" class="td-valign">
                                                                <?php $type = element('type', $column);
                                                                $group = element('group', $column);
                                                                $between = element('between', $column);
                                                                ?>
                                                                <?php if ($group == false):?>
                                                                    <?php if ($type == "edit"):?>
                                                                        <a href="<?=$add_url.$row["ID"]?>" class="btn btn-info"><i class='fa fa-edit'></i></a>
                                                                    <?php elseif ($type == "delete"):?>
                                                                        <a href="<?=$delete_url.$row["ID"]?>" class="btn btn-danger confirm-delete"><i class='fa fa-trash'></i></a>
                                                                    <?php elseif ($type == "order"):?>
                                                                        <input type="number" class="order-input" name="ORDER[]" value="<?=$row["ORDER"]?>">
                                                                        <input type="hidden" name="ID[]" value="<?=$row["ID"]?>">
                                                                    <?php elseif ($type == "image"):?>
                                                                        <img src="<?=$row[$column["name"]]?>" alt="No Image" class="img-thumbnail" style="height:100px;">
                                                                    <?php else:?>
                                                                        <?=content_view($column, $row[$column["name"]])?>
                                                                    <?php endif;?>
                                                                <?php else:?>
                                                                    <?php
                                                                    $is_first = true;
                                                                    foreach ($column["input"] as $field) {
                                                                        if (!$is_first) echo $column["between"];
                                                                        echo content_view($field, $row[$field["name"]]);
                                                                        $is_first = false;
                                                                    }
                                                                    ?>
                                                                <?php endif;?>
                                                            </td>

                                                        <?php endforeach;?>
                                                    </tr>
                                                <?php endforeach;?>
                                            </form>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?= $links_pagination ?>
                                    <!--table test end-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($has_preview) $this->load->view('_parts/preview') ?>
                </div>
                <form id="layout_form" style="display:none;" method="POST" action="<?= $preview_url?>" target="preview-frame">
                    <input type="hidden" name="BRANCH_ID" value="<?= $branch_id?>">
                </form>
                <script>
                    $(function() {
                        $('#layout_form').submit();
                        $("#bulk-dropdown").click(function() {
                            $("#bulk-action").toggleClass("hide");
                        })
                    });
                </script>
            </div>
        </div>
    </div>