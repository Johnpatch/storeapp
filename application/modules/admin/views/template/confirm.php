<h1><?=$page_title?><small><?=$page_subtitle?></small></h1>
<hr>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li class="active">
                <?=$page_icon?> <?= $page_title ?> &gt; <?= $page_subtitle ?>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i><?=lang('confirm_input_contents')?>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="<?=$save_url?>" method="post" class="form-horizontal form-bordered">
                    <div class="form-body">
                        <!--Begin News Color Settings-->
                        <?php foreach($fields as $field) {
                            if (element('group', $field) == true) {
                                $data = array();
                                foreach ($field["input"] as $field_input) {
                                    $data[$field_input["name"]] = $row[$field_input["name"]];
                                }
                                echo form_element($field, $data, true);
                            }else {
                                echo form_element($field, $row[$field["name"]], true);
                            }
                        }?>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn blue"><i class="fa fa-check"></i> <?= lang('register_with_this_content') ?></button>
                                <a href="<?=$back_url?>" class="btn default"><?= lang('back_to_fix') ?></a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>

