<h1><?= lang('success_layout') ?></h1>
<hr>

<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i><?= lang('design_settings') ?>&gt; <?= lang('edit') ?> &gt; <?= lang('done') ?>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-check"></i><?= lang('done') ?>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="<?= site_url('admin/design/layout/save') ?>" method="post" class="form-horizontal form-bordered">
                    <div class="form-body" align="center">
                        <p class='text result-done'><?=lang('registration_has_been_completed')?></p>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <a href="<?=$back_url;?>" class="btn blue"><i class="fa fa-check"></i> <?= $back_text ?></a>
                            <a href="<?= site_url('admin/home') ?>" class="btn default"><i class="fa fa-check"></i><?= lang('go_back_to_the_top_page') ?></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

