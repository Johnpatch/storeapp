</div>
</div>

<a href="#" class="to-top"><i class="fa fa-chevron-up"></i></a href="#">
<script src="<?= base_url('assets/bootstrap-select-1.12.1/js/bootstrap-select.min.js') ?>"></script>

<script src="<?= base_url('assets/js/bootbox.min.js') ?>"></script>
<script src="<?= base_url('assets/color-picker/js/colpick.js') ?>"></script>
<script src="<?= base_url('assets/js/datepicker.js') ?>"></script>
<script src="<?= base_url('assets/js/mine_admin.js') ?>"></script>
<script>
    /*Color Picker start*/
    $('input[type=color]').colpick({
        onSubmit:function(hsb,hex,rgb,el,bySetColor) {
            $(el).val('#'+hex);
            $(el).colpickHide();
        }
    });
    /*Color Picker end*/
    /*Notification close*/
    $(".alert-close").click(function() {
        $(".alert").hide();
    });

    $(".sidebar-menu .header").click(function() {
        $(this).toggleClass("open");
        if ($(this).hasClass("open")) {
            $(this).find("i.pull-right").replaceWith("<i class='fa fa-angle-up pull-right'></i>");
        } else {
            $(this).find("i.pull-right").replaceWith("<i class='fa fa-angle-down pull-right'></i>");
        }
        $(this).next("ul").toggleClass("close");
    });

    function readURL(input, previewId, inputId, isUpdatePreview = true) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var base64 = e.target.result;
                if (isUpdatePreview) document.getElementById(previewId).src = base64;
                document.getElementById(inputId).value = base64;
                $('#' + previewId).removeClass('hidden');

                updatePreview();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    
        //File Input
        $('.btn_image').click(function() {
            var id = $(this).attr('data-id');
            $('#file-' + id).click();
        });

        $('#form-body').on('click', '.btn_image', function(){
            var id = $(this).attr('data-id');
            var addManual = $(this).attr('data-add');
            if(addManual == 'true')
                $('#file-' + id).click();
        })

        $('.file').change(function() {
            var id = $(this).attr('data-id');
            var type = $(this).attr('data-type');
            var fileName = $(this).val().split("\\");
            $("#filename-" + id).text(fileName[fileName.length - 1]);
            readURL(this, "image-" + id, 'input-' + id, type != 'pdf');
            $('#btn_select_' + id).addClass('hidden');
            $('#btn_change_' + id).removeClass('hidden');
        });

        $('#form-body').on('change', '.fileManual',function() {
            var id = $(this).attr('data-id');
            var type = $(this).attr('data-type');
            var fileName = $(this).val().split("\\");
            $("#filename-" + id).text(fileName[fileName.length - 1]);
            readURL(this, "image-" + id, 'input-' + id, type != 'pdf');
            $('#btn_select_' + id).addClass('hidden');
            $('#btn_change_' + id).removeClass('hidden');
        });

        /*photo information start*/
        $('.photo-selection-button').click(function() {
            var photo_file = $('#photo-selection-file');
            var id = Math.random();
            photo_file.attr('data-id', id);
            photo_file.click();
        });

        $('#photo-selection-file').change(function() {
            var id = $(this).attr('data-id');
            var image_name = $(this).attr('data-image-name');
            var comment_name = $(this).attr('data-comment-name');
            add_photo(id, image_name, '', '', comment_name, '', true);
            readURL(this, "photo-image-" + id, 'photo-image-input-' + id);
        });
        /*photo information end*/

        
        //Color picker
        $('.coloritem').click(function() {
            var id = $(this).attr('data-id');
            var color = $(this).attr('data-color');
            $('#color-input-' + id).val(color);
            $('div[data-id="' + id + '"]').removeClass('selected');
            $(this).addClass('selected');
            updatePreview();
        });
        
        //Date picker
        $("input[type=date]").datepicker();
        
        //datetimepicker
        $("input[type=datetime]").datepicker();

    /*Active List*/
    $(".left-menu-content .active-page").parent().parent().addClass("active");

    /*Mobile Show/Hide*/
    $(".mobile-preview-hide").click(function() {
        $(".preview-container").hide();
        $(".mobile-preview-hide").hide();
        $(".mobile-preview-show").removeClass("hide").show();
        $(".layout-content").removeClass("col-md-9").addClass("col-md-12");
        $(".layout-content").css('z-index', 0);
    });

    $(".mobile-preview-show").click(function() {
        $(".preview-container").show();
        $(".mobile-preview-show").hide();
        $(".mobile-preview-hide").removeClass("hide").show();
        $(".layout-content").removeClass("col-md-12").addClass("col-md-9");
        $(".layout-content").css('z-index', 9);
    });

    $('.checkbox').click(function() {
        var id = $(this).attr('data-id');
        var checked = $(this).attr('class').indexOf('selected') == -1;
        var checked_v = $(this).attr('data-checked-v');
        var unchecked_v = $(this).attr('data-unchecked-v');
        var disabled = $(this).attr('disabled');
        if(disabled != 'disabled'){
            $(this).removeClass('selected');
            if (checked) $(this).addClass('selected');

            $('#check-input-' + id).val(checked ? checked_v : unchecked_v);
            $("#check-input-" + id).change();
        }
        
    });

    $('.video-add-button ').click(function() {
        var id = Math.random();
        var video_name = $(this).attr('data-video-name');
        var comment_name = $(this).attr('data-comment-name');
        var video_value = $('#current_video_input').val();
        var comment_value = $('#current_comment_input').val();

        $('#current_video_input').val('');
        $('#current_comment_input').val('');
        add_video(id, video_name, video_value, comment_name, comment_value, true, true);
        updatePreview();
    });

    $('.hp-add-button ').click(function() {
        var id = Math.random();
        var video_name = $(this).attr('data-video-name');
        var comment_name = $(this).attr('data-comment-name');
        var video_value = $('#current_select_input').val();
        var comment_value = $('#current_comment_input').val();

        $('#current_select_input').val('');
        $('#current_comment_input').val('');
        add_hp(id, video_name, video_value, comment_name, comment_value, true);
        updatePreview();
    });
    function setDateTimeValue(input_id) {
        $('#'+input_id).val($("#anytime-month-numeric-"+input_id).val()+ ' ' + $("#anytime-time-"+input_id).val());
    }

    $(".timeField").datetimepicker({
        pickDate: false
    });
    jQuery(document).ready(function() {
        // initiate layout and plugins
        ComponentsPickers.init();
        $.ajax({
            url: '<?php echo site_url("admin/home/home/get_app_request_status")."?token=".$token;?>',
            type: 'POST',
            data: {
            },
            success: function (resp) {
                var res = jQuery.parseJSON(resp);
                if(res.STATUS == true){
                    $('#open_fa_modal').trigger('click');
                }
            },
            error: function () {
                
            }
        });
    });
    $('#fa_code_ok').click(function(){
        $.ajax({
            url: '<?php echo site_url("admin/home/home/add_facode")."?token=".$token;?>',
            type: 'POST',
            data: {
                fa_code: $('#fa_code_val').val(),
            },
            success: function (resp) {
            },
            error: function () {
                
            }
        });
    })

    function renderAutoPushInputs() {
        if ($("input[name=AUTO_PUSH_TYPE]:checked").val() == 2) {//Panel Layout
            $("tr.TYPE").removeClass('hide');
            $("select[name=START_PUSH]").parents().parents('tr').addClass('hide');
        } 
        if ($("input[name=AUTO_PUSH_TYPE]:checked").val() == 1){
            $("tr.TYPE").addClass('hide');
            $("select[name=START_PUSH]").parents().parents('tr').removeClass('hide');
        }
    }
    renderAutoPushInputs();
    $("input[name=AUTO_PUSH_TYPE]").change(function() {
        renderAutoPushInputs();
    });
</script>
</body>
</html>