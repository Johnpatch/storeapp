<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title ?></title>

<!--    Limitless Styles-->
    <link href="<?=base_url("assets/css/limitless/icons/icomoon/styles.css")?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url("assets/css/limitless/icons/glyphicons/glyphicons-halflings-regular.svg")?>" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?=base_url("assets/css/limitless/bootstrap.css")?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url("assets/css/limitless/core.css")?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url("assets/css/limitless/components.css")?>" rel="stylesheet" type="text/css">
    <link href="<?=base_url("assets/css/limitless/colors.css")?>" rel="stylesheet" type="text/css">
<!---->
    <link href="<?=base_url("assets/font-awesome/css/font-awesome.min.css")?>" rel="stylesheet" type="text/css">
    <!-- <link href="<?= base_url('assets/css/components.css') ?>" rel="stylesheet"> -->

    <link href="<?= base_url('assets/color-picker/css/colpick.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/metronic_library/css/datepicker3.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/metronic_library/css/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/metronic_library/css/bootstrap-timepicker.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/metronic_library/css/plugins.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/custom-admin.css') ?>" rel="stylesheet">

<!--    Limitless Scripts-->
    <!-- Core JS files -->
    <script type="text/javascript" src="<?=base_url("assets/js/plugins/loaders/pace.min.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("assets/js/core/libraries/jquery.min.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("assets/js/core/libraries/bootstrap.min.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("assets/js/plugins/loaders/blockui.min.js")?>"></script>
    <!-- /core JS files -->
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/forms/styling/uniform.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/forms/styling/switchery.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/forms/styling/switch.min.js')?>"></script>

    <script type="text/javascript" src="<?= base_url('assets/js/plugins/ui/moment/moment.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/pickers/daterangepicker.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/pickers/anytime.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/pickers/pickadate/picker.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/pickers/pickadate/picker.date.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/plugins/pickers/pickadate/picker.time.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/bootstrap-datepicker.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/bootstrap-timepicker.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/bootstrap-datetimepicker.min.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/metronic.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/layout.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/quick-sidebar.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/demo.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/metronic_library/js/components-pickers.js')?>"></script>
    <!-- Theme JS files -->
    <script type="text/javascript" src="<?= base_url("assets/js/core/app.js")?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/pages/form_checkboxes_radios.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/pages/picker_date.js')?>"></script>
    
    <script src="<?=base_url('assets/js/form-generator.js')?>"></script>
    <!-- /theme JS files -->
<!---->


<!--    <link rel="stylesheet" href="--><?//= base_url('assets/css/bootstrap.css') ?><!--">-->
<!--    <link rel="stylesheet" href="--><?//= base_url('assets/css/icons/icomoon/styles.css') ?><!--">-->
<!--    <link rel="stylesheet" href="--><?//= base_url('assets/bootstrap-select-1.12.1/bootstrap-select.min.css') ?><!--">-->


    <!--<link href='https://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>-->
<!--    <script src="--><?//= base_url('assets/js/core/libraries/jquery.min.js') ?><!--"></script>-->
    <script src="<?= base_url('assets/fileinput/fileinput.min.js') ?>"></script>
    <script src="<?= base_url('assets/ckeditor/ckeditor.js') ?>"></script>

          <script type="text/javascript">
            function logout() {
                window.parent.postMessage('data' , 'https://docodoor-app.firebaseapp.com/');
            }
        <?php if (!$logged_in) {
        ?>
        logout();
        <?php
        }
        ?>

        </script>

</head>
<body>
<?php if ($logged_in): ?>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= lang('input_2fa_code'); ?></h4>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control" id="fa_code_val" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="fa_code_ok" data-dismiss="modal">OK</button>
      </div>
    </div>

  </div>
</div>
<button type="button" class="btn btn-info btn-lg" style="display:none;" id="open_fa_modal" data-toggle="modal" data-target="#myModal">Open Modal</button>
<div class="navbar b_border">
            <div class="navbar-header" style="max-width: 270px;">
                <a class="navbar-brand site-logo-img" href="<?= generate_url('admin/home', $token)?>">
                
                    <?php if(count($result) > 0 && !empty($result['LOGO'])) {
                        ?>
                        <img src="<?php echo $result['LOGO']; ?>">
                    <?php } else { ?>
                        <img src="<?= base_url('assets/imgs/site-logo.svg') ?>">
                    <?php } ?>
                </a>

                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
        </div>
    <div class="navbar-collapse collapse header_nav" id="navbar-mobile">
        <ul class="nav navbar-nav header_nav_list dis-flex">
            <div class="site_content_icon navbar-nav">
                <!--div class="site-pc-content float-left">
                    <img src="<?= base_url('assets/icons/desktop.png') ?>" />
                </div>
                <div class="site-mobile-content float-left">
                    <img src="<?= base_url('assets/icons/mobile.png') ?>" />
                </div-->
            </div>
            <!-- <li class="float-left" id="site-text-contextbox"> -->
                <div class="site-text-content  mobile-preview-hide cursor-pointer">
                    <?= lang('mobile_show')?>
                </div>
                <div class="site-text-content  mobile-preview-show hide cursor-pointer">
                    <?= lang('mobile_hide')?>
                </div>
            <!-- </li> -->

            <div class="nav navbar-nav p_right" id="shopname">
                <!--div class="site-shop-content shop_name float-left">
                    <?= lang('shop_name')?><?= lang('OBU_shop')?>
                </div>
                <div class="site-shop-content shop_name_1 float-left">
                    <?= lang('OBU_shop')?>
                </div-->
                <div class="site-shop-content shop_name float-left">
                    <?= $store_name['NAME'] ?>
                </div>
                <div class="site-user-content float-left ">
                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user user-icon"></i><i class="fa fa-caret-down color-black user-menu-dropdown"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right user-info-dropdown">
                            <!--li><a href="#"><i class="icon-home"></i> <?= lang('my_profile')?></a></li>
                            <li><a href="#"><i class="icon-plus-circle2"></i> <?= lang('my_balance')?></a></li>
                            <li><a href="#"><i class="icon-plus-circle2"></i> <?= lang('messages')?></a></li>
                            <li><a href="#"><i class="icon-plus-circle2"></i> <?= lang('account_settings')?></a></li-->
                            <li><a href="javascript:void(0);" onclick="logout();"><i class="icon-exit"></i> <span><?= lang('logout')?></span></a></li>
                            
                        </ul>
                    </li>
                </div>
            </div>
        </ul>
    </div>
</div>
<?php endif;?>
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <?php if ($logged_in) : ?>
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion left-menu-content">
                            <li class="<?= strstr(uri_string(), 'admin/home') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/home' , $token)?>"><i class="icon-home5"></i> <span><?= lang('home')?></span></a></li>
                            <li class="<?= strstr(uri_string(), 'admin/topmenu') != false ? 'active' : '' ?>">
                                <a href="#"><img src=<?= base_url('assets/icons/menu.png'); ?> /> <span><?= lang('top_menu_settings')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= strstr(uri_string(), 'admin/topmenu') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/topmenu/' , $token)?>"><?= lang('edit_top_menu')?></a></li>
                                </ul>
                            </li>
                            <!--li class="<?= strstr(uri_string(), 'admin/basicsetting') != false ? 'active' : '' ?>">
                                <a href="#"><i class="icon-menu3"></i> <span><?= lang('basic_menu_settings')?></span></a>
                                <ul class="sub-menu-parent">
                                <li class="<?= strstr(uri_string(), 'admin/basicsetting/agency') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basicsetting/agency' , $token)?>"><?= lang('agency_basic_information')?></a></li>
                                <li class="<?= strstr(uri_string(), 'admin/basicsetting/webviewapp') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basicsetting/webviewapp' , $token)?>"><?= lang('webview_app')?></a></li>
                                </ul>
                            </li-->
                            
                            <li>
                                <a href="#"><img src=<?= base_url('assets/icons/brush.png'); ?> /> <span><?= lang('design_settings')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= strstr(uri_string(), 'admin/design/layout/publish') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/design/layout/publish' , $token)?>"><?= lang('layout_type')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/design/slide') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/design/slide/' , $token)?>"><?= lang('slide_image_settings')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/design/headerfooter') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/design/headerfooter/' , $token)?>"><?= lang('header_footer_image_settings')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/design/pagecolor') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/design/pagecolor/' , $token)?>"><?= lang('page_color_settings')?></a></li>
                                </ul>
                            </li>
                            <li class="<?= strstr(uri_string(), 'admin/catalog') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/catalog' , $token)?>"><img src=<?= base_url('assets/icons/catalog.png'); ?> /> <span><?= lang('catalog_production')?></span></a></li>
                            <li>
                                <a href="#"><img src=<?= base_url('assets/icons/movie.png'); ?> /> <span><?= lang('movie')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= strstr(uri_string(), 'admin/basic/video') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/video' , $token)?>"><?= lang('list_reg')?></a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src=<?= base_url('assets/icons/credit.png'); ?> /> <span><?= lang('coupon')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= (uri_string() == 'admin/coupon/publish') != false ? 'active-page' : '' ?>"> <a href="<?= generate_url('admin/coupon/publish' , $token)?>"><?= lang('new_reg')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/coupon') != false || strstr(uri_string(), 'admin/coupon/publish/') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/coupon' , $token)?>"><?= lang('list_reg')?></a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src=<?= base_url('assets/icons/detail.png'); ?> /> <span><?= lang('post_content')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= (uri_string() == 'admin/postcategory/publish') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/postcategory/publish' , $token)?>"><?= lang('category_reg')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/postcategory') != false || strstr(uri_string(), 'admin/postcategory/publish/') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/postcategory' , $token)?>"><?= lang('category_list')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/event/publish') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/event/publish' , $token)?>"><?= lang('post_reg')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/event') != false || strstr(uri_string(), 'admin/event/publish/') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/event' , $token)?>"><?= lang('post_list')?></a></li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><img src=<?= base_url('assets/icons/stamp.png'); ?> /> <span><?= lang('store_stamp_information')?></span></a>
                                <ul class="sub-menu-parent">
                                    <!--li class="<?= strstr(uri_string(), 'admin/stamp/stampsetting/publish') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/stamp/stampsetting' , $token)?>"><?= lang('stamp_usage_settings')?></a></li-->
                                    <!--li class="<?= strstr(uri_string(), 'admin/stamp/stampimage/publish') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/stamp/stampimage' , $token)?>"><?= lang('stamp_image_registration')?></a></li-->
                                    <li class="<?= (uri_string() == 'admin/stamp/stampprivilege/publish') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/stamp/stampprivilege/publish' , $token)?>"><?= lang('new_reg')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/stamp/stampprivilege') != false || strstr(uri_string() , 'admin/stamp/stampprivilege/publish/') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/stamp/stampprivilege' , $token)?>"><?= lang('list_reg')?></a></li>
                                    <li class="<?= strstr(uri_string() , 'admin/stamp/stampimage') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/stamp/stampimage' , $token)?>"><?= lang('stamp_image_registration')?></a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src=<?= base_url('assets/icons/detail.png'); ?> /> <span><?= lang('basic_information')?></span></a>
                                <ul class="sub-menu-parent">
                                    <!--li class="<?= strstr(uri_string(), 'admin/basic/store') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/store' , $token)?>"><?= lang('store_information')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/photo') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/photo' , $token)?>"><?= lang('photo_information')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/pdf') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/pdf' , $token)?>"><?= lang('pdf_information')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/link') != false ? 'active-page' : '' ?>"><a href="<?=generate_url('admin/basic/link' , $token)?>"><?= lang('list_of_links')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/president') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/president' , $token)?>"><?= lang('manage_from_president')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/staff') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/staff' , $token)?>"><?= lang('staff')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/corporate_philosophy') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/corporate_philosophy' , $token)?>"><?= lang('corporate_philosophy')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/business_introduction') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/business_introduction' , $token)?>"><?= lang('business_introduction')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/price') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/price' , $token)?>"><?= lang('price')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/menu') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/menu' , $token)?>"><?= lang('menu')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/business_history') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/business_history' , $token)?>"><?= lang('history')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/access') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/access' , $token)?>"><?= lang('access')?></a></li-->

                                    <!--li class="<?= strstr(uri_string(), 'admin/basic/social') != false ? 'active-page' : '' ?>">
                                        <a href="#"><?= lang('new_reg')?></a>
                                        <ul class="sub-menu-parent">
                                            <li class="<?= strstr(uri_string(), 'admin/basic/company_profile') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/company_profile' , $token)?>"><?= lang('company_profile') ?></a></li>
                                            <li><a href="#"><?= lang('store_guide') ?></a></li>
                                            <li><a href="#"><?= lang('pricing') ?></a></li>
                                            <li class="<?= strstr(uri_string(), 'admin/help/faq') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/help/faq' , $token)?>"><?= lang('FAQ')?></a></li>
                                            <li><a href="#"><?= lang('notice_setting') ?></a></li>

                                            <li class="<?= strstr(uri_string(), 'admin/basic/term') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/term' , $token)?>"><?= lang('terms_service')?></a></li>
                                            <li class="<?= strstr(uri_string(), 'admin/basic/policy') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/policy' , $token)?>"><?= lang('privacy_policy')?></a></li>
                                            <li class="<?= strstr(uri_string(), 'admin/basic/information') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/information' , $token)?>"><?= lang('information_security')?></a></li>
                                            <li class="<?= strstr(uri_string(), 'admin/basic/about') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/about' , $token)?>"><?= lang('about_this_app')?></a></li>
                                        </ul>
                                    </li-->
                                    <li class="<?= (uri_string() == 'admin/basic/content/publish') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/content/publish' , $token)?>"><?= lang('post_reg')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/basic/content') != false || strstr(uri_string(), 'admin/basic/content/publish/') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/content' , $token)?>"><?= lang('list_reg')?></a></li>
                                    
                                    <li class="<?= strstr(uri_string(), 'admin/basic/social') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/social' , $token)?>"><?= lang('intro_friend')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/webview') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/webview' , $token)?>"><?= lang('web_view_settings')?></a></li>
                                    <!--li class="<?= strstr(uri_string(), 'admin/basic/first_html') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/first_html' , $token)?>"><?= lang('html_1')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/second_html') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/second_html' , $token)?>"><?= lang('html_2')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/basic/third_html') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/third_html' , $token)?>"><?= lang('html_3')?></a></li-->
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src=<?= base_url('assets/icons/detail.png'); ?> /> <span><?= lang('free_content')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= (uri_string() == 'admin/basic/free/publish') != false ? 'active-page' : '' ?>"> <a href="<?= generate_url('admin/basic/free/publish' , $token)?>"><?= lang('new_reg')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/basic/free') != false || strstr(uri_string(), 'admin/basic/free/publish/') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basic/free' , $token)?>"><?= lang('list_reg')?></a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src=<?= base_url('assets/icons/inquiry.png'); ?> /> <span><?= lang('inquiry_form')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= strstr(uri_string() , 'admin/inquiry/management') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/inquiry/management' , $token)?>"><?= lang('create_a_new')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/inquiry/inquiries') != false || strstr(uri_string(), 'admin/inquiry/inquiries') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/inquiry/inquiries' , $token)?>"><?= lang('inquiry_list')?></a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src=<?= base_url('assets/icons/reservation.png'); ?> style="height: 18px;"/> <span><?= lang('reservation')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= strstr(uri_string(), 'admin/reservation/management') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/reservation/management' , $token)?>"><?= lang('reservation_management')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/reservation/entry') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/reservation/entry', $token)?>"><?= lang('book_input_setting')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/reservation/time') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/reservation/time', $token)?>"><?= lang('time_week_setting')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/reservation/assign') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/reservation/assign', $token)?>"><?= lang('assignee_settings')?></a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src="<?= base_url('assets/icons/survey.png'); ?>"/> <span><?= lang('questionnaire')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= (uri_string() == 'admin/survey/publish') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/survey/publish' , $token)?>"><?= lang('new_reg')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/survey') != false || strstr(uri_string(), 'admin/survey/publish/') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/survey' , $token)?>"><?= lang('list_reg')?></a></li>
                                    <!--li class="<?= strstr(uri_string(), 'admin/survey/aggregate') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/survey/aggregate' , $token)?>"><?= lang('aggregate')?></a></li-->
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src="<?= base_url('assets/icons/bell.png'); ?>"/> <span><?= lang('push_notification')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= (uri_string() == 'admin/push/publish') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/push/publish' , $token)?>"><?= lang('new_reg')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/push') != false || strstr(uri_string(), 'admin/push/publish/') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/push' , $token)?>"><?= lang('list_reg')?></a></li>
                                    <!--<li class="--><?//= strstr(uri_string(), 'admin/push/individual_push') != false ? 'active-page' : '' ?><!--"><a href="#">--><?//= lang('individual_push')?><!--</a></li>-->
                                    <!--<li class="--><?//= strstr(uri_string(), 'admin/push/history_statics') != false ? 'active-page' : '' ?><!--"><a href="#">--><?//= lang('history_statics')?><!--</a></li>-->
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src="<?= base_url('assets/icons/bell.png'); ?>"/> <span><?= lang('auto_push_notification')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= strstr(uri_string(), 'admin/autopush/birthday_push') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/autopush/birthday_push' , $token)?>"><?= lang('birthday_push')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/autopush/notification_setting') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/autopush/notification_setting' , $token)?>"><?= lang('step_send')?></a></li-->

                                    <!--li class="<?= strstr(uri_string(), 'admin/autopush/welcome_push') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/autopush/welcome_push' , $token)?>"><?= lang('welcome_push')?></a></li-->
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src="<?= base_url('assets/icons/user.png'); ?>" style="height: 18px;"/> <span><?= lang('user_information')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= strstr(uri_string(), 'admin/userinformation') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/userinformation' , $token)?>"><?= lang('user_search_list')?></a></li>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><img src="<?= base_url('assets/icons/analysis.png'); ?>"/> <span><?= lang('analysis')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= strstr(uri_string(), 'admin/analysis/segment_statistics') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/segment_statistics' , $token)?>"><?= lang('segment_statistics')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/analysis/downloads_day') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/downloads_day' , $token)?>"><?= lang('downloads_by_day')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/analysis/downloads_month') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/downloads_month' , $token)?>"><?= lang('downloads_by_month')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/analysis/stamp_day') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/stamp_day' , $token)?>"><?= lang('number_of_stamps_by_day')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/analysis/stamp_month') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/stamp_month' , $token)?>"><?= lang('number_of_stamps_monthly')?></a></li>
                                    <li class="<?= (uri_string() == 'admin/analysis/stamp_benefit') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/stamp_benefit' , $token)?>"><?= lang('number_of_stamp_benefits_used_by_day')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/analysis/stamp_benefitmonth') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/stamp_benefitmonth' , $token)?>"><?= lang('number_of_stamp_benefits_used_by_month')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/analysis/coupon_usage') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/coupon_usage' , $token)?>"><?= lang('coupon_usage_daily')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/analysis/pushnotification_ctr') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/pushnotification_ctr' , $token)?>"><?= lang('push_notification_ctr')?></a></li>
                                    <!--li class="<?= strstr(uri_string(), 'admin/analysis/favorite_day') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/favorite_day/' , $token)?>"><?= lang('favorites_by_day')?></a></li>
                                    <li class="<?= strstr(uri_string(), 'admin/analysis/favorite_month') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/favorite_month/' , $token)?>"><?= lang('favorites_by_month')?></a></li-->
                                    <li class="<?= strstr(uri_string(), 'admin/analysis/survey') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/analysis/survey/' , $token)?>"><?= lang('questionnaire')?></a></li>
                                </ul>
                            </li>
                            <li class="<?= strstr(uri_string(), 'admin/manage/fileinformation') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/manage/fileinformation' , $token)?>"><img src="<?= base_url('assets/icons/store.png'); ?>"/> <span><?= lang('application_file_for_information')?></span></a></li>
                            <li class="<?= strstr(uri_string(), 'admin/manage/application') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/manage/application' , $token)?>"><img src="<?= base_url('assets/icons/store.png'); ?>"/><span><?= lang('application')?></span></a></li>
                            <li class="<?= strstr(uri_string(), 'admin/basicsetting/agency') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/basicsetting/agency' , $token)?>"><img src="<?= base_url('assets/icons/cog.png'); ?>"/><span><?= lang('user_info_setting')?></span></a></li>
                            
                            <!--li>
                                <a href="#"><i class=" icon-graduation2"></i> <span><?= lang('management')?></span></a>
                                <ul class="sub-menu-parent">
                                    <li class="<?= strstr(uri_string(), 'admin/manage/qr') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/manage/qr' , $token)?>"><?= lang('qr_code')?></a></li>
                                    <li class="<?//= strstr(uri_string(), 'admin/manage/loginaccount') != false ? 'active-page' : '' ?>"><a href="#"><?//= lang('login_account_information')?></a></li>
                                </ul>
                            </li-->
                            <!--li>
                                <a href="#"><i class=" icon-help"></i> <span><?= lang('help')?></span></a>
                                <ul class="sub-menu-parent">
                                    
                                    <li class="<?= strstr(uri_string(), 'admin/help/manual') != false ? 'active-page' : '' ?>"><a href="<?= generate_url('admin/help/manual' , $token)?>"><?= lang('app_creation_manual')?></a></li>
                                </ul>
                            </li-->
                            <!--li class="<?= strstr(uri_string(), 'admin/member/') != false ? 'active-page' : '' ?>"><a href="#"><i class="icon-users"></i> <span><?= lang('member_page')?></span></a></li-->
                            
                            <li class="<?= strstr(uri_string(), 'admin/logout') != false ? 'active-page' : '' ?>"><a href="javascript:void(0);" onclick="logout();"><i class="icon-exit"></i> <span><?= lang('logout')?></span></a></li>
                            <li class="menu-hide-btn">
                                <a class="sidebar-control sidebar-main-toggle hidden-xs hide-menu-btn">
                                    <img src="<?= base_url('assets/icons/hide.png'); ?>" /><span><?= lang('sidebar_hide')?></span>
                                </a>
                            </li>
                            <li class="menu-show-btn hide">
                                <a class="sidebar-control sidebar-main-toggle hidden-xs show-menu-btn">
                                    <img src="<?= base_url('assets/icons/show.png'); ?>" style="margin-right:0px;"/><span class="hide"><?= lang('sidebar_hide')?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php endif;?>
