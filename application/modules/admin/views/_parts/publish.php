<script>
    function updatePreview() {
        <?php if (!$has_preview):?>
        return;
        <?php endif;?>
               
        var temp_form = $("#layout_form").clone();
        temp_form.attr("action", "<?= $preview_url?>");
        temp_form.attr("target", "preview-frame");
        /*temp_form.find("input").each(function(i, target) {
         $(target).attr("name", $(target).attr("name").toUpperCase());
         });*/

        $(temp_form).find('select[name="REFER_ID"]').val($('select[name="REFER_ID"]').val());
        $(temp_form).find('select[name="DELIVERY_TYPE"]').val($('select[name="DELIVERY_TYPE"]').val());

        $("body").append(temp_form);
        temp_form.submit();
        temp_form.remove();
    }
    function add_video(id, video_name, video_value, comment_name, comment_value, is_view = 'readonly', is_add = false) {
        readonly = is_view == 'readonly';
        var cur_html = $('.photo-comments-list').html();
        var html = "<div class='video-item' id='video-item-div-" + id + "' style='margin-bottom: 20px;'>";
        html +=         "<div class='video-item'>";
        html+=              "<input type='text' onchange='updatePreview()' class='form-control "+ (readonly ? 'hidden' : '') + "' name='" + video_name + "[]' placeholder='Video URL' value='" + video_value + "'/>";
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + video_value + "</label>";
        html +=             "<input type='text' onchange='updatePreview()' class='form-control "+ (readonly ? 'hidden' : '') + "' name='" + comment_name + "[]' placeholder='comment' value='" + comment_value + "'  />"
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + comment_value + "</label>";
        html +=             '<div class="display-inline ' + (readonly ? ' hidden ' : '') + '">';
        html +=                 '<a class="btn photo-item-remove btn-danger" onclick= "video_remove(\'' + id + '\')">';
        html +=                     '<i class="fa fa-trash-o"></i>';
        html +=                 '</a>';
        html +=             '</div>';
        html +=         '</div>';
        html +=     "</div>";
        cur_html = html + cur_html;
        if(is_add == false){
            $('.photo-comments-list').append(html);
        }else{
            $('.photo-comments-list').html(cur_html);
        }
        
    }

    function add_hp(id, video_name, video_value, comment_name, comment_value, is_view = 'readonly') {
        readonly = is_view == 'readonly';
        var select = $("#current_select_input").clone();

        var id=Math.random();
        select.attr("id",'select-hp-'+ id);
        select.val(video_value);

        if (readonly) {
            select.addClass("hidden");
        }
        select.removeAttr("id");
        select.addClass("abc");
        var video_item = $("<div class='video-item'></div>");
        video_item.append(select);
        var hidden="<input type='hidden' name='" + video_name + "[]' value='" + video_value + "' id='" + "hidden-hp-" + id + "'>";
        video_item.append(hidden);
        video_item.append("<label class='" + (readonly ? '' : 'hidden') + "'>" + select.find("option:selected").text() + "</label>");
        var html = "";
        html +=             "<input type='text' onchange='updatePreview()' class='form-control "+ (readonly ? 'hidden' : '') + "' name='" + comment_name + "[]' placeholder='comment' value='" + comment_value + "'  />"
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + comment_value + "</label>";
        video_item.append(html);
        var wrapper = $("<div class='video-item' id='video-item-div-" + id + "' style='margin-bottom: 20px;'></div>");

        var html = "";
        html +=             '<a class="btn photo-item-remove' + (readonly ? ' hidden ' : '') + ' btn-danger" onclick= "video_remove(\'' + id + '\')">';
        html +=                 '<i class="fa fa-trash-o"></i>';
        html +=              '</button>';
        video_item.append(html);
//        wrapper.append(html);
        wrapper.append(video_item);
        $('.photo-comments-list').append(wrapper);
        select.change(function(e) {
            $(e.target).next().val(e.target.value);
            updatePreview();
        });
    }

    function video_remove(id) {
        document.getElementById('video-item-div-' + id).remove();
        updatePreview();
    }

    function add_photo(id, image_name, image_value, preview_value, comment_name, comment_value, is_view = 'readonly') {
        readonly = is_view == 'readonly';

        var html = "<div class='col-md-6' id='photo-item-div-" + id + "' style='margin-bottom: 20px;'>";
        html +=         "<div style='margin-bottom: 10px'>";
        html +=             "<input id='photo-comment-" + id + "' name='" + comment_name + "[]' class='form-control " + (readonly ? ' hidden ' : '') +"' type='text' style='width: 100%; border-radius: 5px !important; ' value='" + comment_value + "' />";
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + comment_value + "</label>";
        html +=         "</div>";
        html +=         '<div style="margin-bottom: 10px;">';
        html +=             '<img class="photo-item-img" id="photo-image-' + id + '" src="' + preview_value + '" />';
        html +=         '</div>';
        html +=         '<div class="' + (readonly ? ' hidden ' : '') + '">';
        html +=             '<button type="button" class="photo-item-remove btn-danger" onclick= "photo_remove(\'' + id + '\')">';
        html +=                 '<i class="fa fa-trash-o"></i>';
        html +=              '</button>';
        html +=         '</div>';
        html +=         '<input class="hidden" id="photo-image-input-' + id + '" name="' + image_name + '[]" value="'+image_value+'" />';
        html +=     "</div>";

        $('.photo-comments-list').append(html);
    }

    function photo_remove(id) {
        document.getElementById('photo-item-div-' + id).remove();
        updatePreview();
    }
</script>
    <div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= generate_url($route_prefix , $token)?>" class="breadcrumb-1"><?= $bread_title?></a><?php if (!empty($page_subtitle)) : ?>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i><?php endif; ?></li>
                    <?php if (!empty($page_subtitle)) : ?>
                    <li><?= $page_subtitle?></li>
                    <?php endif; ?>
                    <!--li class="active breadcrumb-2"><?= lang('done')?></li-->
                </ul>
            </div>
        </div>
    </div>

    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $panel_title ?> </div>
            <div class="panel-body" style="padding-top:0px;">
                <div class="row">
                    <div class="layout-content col-md-<?=$has_preview ? 9 : 12?>">
                        <p class="content-group">
                            <?= $form_description ?>
                        </p>
                        <form action="<?= $confirm_url ?>" id="layout_form" method="post">
                            <input type="hidden" name="token" value="<?= $token ?>" />
                            <input type="hidden" name="back_url" value="<?= current_url() ?>">
                            <?php if (isset($branch_id)):?>
                            <input type="hidden" name="BRANCH_ID" value="<?= $branch_id ?>">
                            <?php endif;?>

                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <!--Begin News Color Settings-->
                                        <?php foreach($fields as $field) {
                                            echo render_element($field);
                                        }?>
                                        <?php
                                        if(isset($is_survey_option) && $is_survey_option){
                                            if(empty($survey_options)){
                                                for($i = 1;$i<=5;$i++){
                                                    echo "<tr>
                                                        <td>".lang('option')." $i</td>
                                                        <td><input type='text' class='form-control' name='option_value[]' /></td>
                                                    </tr>";
                                                }
                                            }else{
                                                foreach($survey_options as $key => $option){
                                                    echo "<tr>
                                                        <td>".lang('option').($key+1)."</td>
                                                        <td><input type='text' class='form-control' name='option_value[]' value='".$option['OPTION_VALUE']."' /></td>
                                                    </tr>";
                                                }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if(isset($is_survey_option) && $is_survey_option){
                                echo "<input type='hidden' name='is_survey_option' value='1' />";
                            } ?>
                            <BR>
                            <div class="text-center">
                                <?php if(!empty($only_view)) { ?>
                                    <a href="javascript:history.back()" class="btn common-btn-green-medium custom-btn"><?= lang('back') ?></a>
                                <?php } else { ?>
                                    <button type="submit" class="btn common-btn-green-small custom-btn"><?= lang('save') ?></button>
                                <?php } if(isset($is_survey_option) && $is_survey_option){?>
                                    <button type="button" onclick="option_add()" class="btn common-btn-green-small custom-btn"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;<?= lang('option_add');?></button>
                                <?php } if(isset($is_basic_add) && $is_basic_add){?>
                                    <button type="button" onclick="basic_add()" class="btn common-btn-green-small custom-btn"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;<?= lang('add');?></button>
                                <?php } ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($has_questions && $id > 0) {
            $this->load->view('_parts/questions', ['SURVEY_ID'=>$id]);
        }
        ?>
        <?php if ($has_preview) {
            $this->load->view('_parts/preview');
        }
        ?>

    </div>
</div>

<script>
    jQuery(window).resize(function(){
        if($(window).width() < 800){
            $(".preview-container").hide();
            $(".mobile-preview-hide").hide();
            $(".mobile-preview-show").removeClass("hide").show();
            $(".layout-content").removeClass("col-md-9").addClass("col-md-12");
            $(".layout-content").css('z-index', 0);
        }
    });

    $('input[name=TIME_WEEK_ALL]').change(function(){
        if($('input[name=TIME_WEEK_ALL]').val() == 'Y'){
            $('input[name=TIME_WEEK_MON]').val('Y')
            $('.week_class').addClass('selected')
        }else{
            $('input[name=TIME_WEEK_MON]').val('N')
            $('.week_class').removeClass('selected')
        }
    })

    $('input[name=TIME_WEEK_MON] , input[name=TIME_WEEK_TUE], input[name=TIME_WEEK_WED], input[name=TIME_WEEK_THU], input[name=TIME_WEEK_FRI], input[name=TIME_WEEK_SAT], input[name=TIME_WEEK_SUN]').change(function(){
        if($('input[name=TIME_WEEK_MON]').val() == 'N' || 
            $('input[name=TIME_WEEK_TUE]').val() == 'N' || 
            $('input[name=TIME_WEEK_WED]').val() == 'N' || 
            $('input[name=TIME_WEEK_THU]').val() == 'N' || 
            $('input[name=TIME_WEEK_FRI]').val() == 'N' || 
            $('input[name=TIME_WEEK_SAT]').val() == 'N' || 
            $('input[name=TIME_WEEK_SUN]').val() == 'N'){

            $('input[name=TIME_WEEK_ALL]').val('N')
            $('.all_time_class').removeClass('selected')

        } else if($('input[name=TIME_WEEK_MON]').val() == 'Y' && 
                $('input[name=TIME_WEEK_TUE]').val() == 'Y' && 
                $('input[name=TIME_WEEK_WED]').val() == 'Y' && 
                $('input[name=TIME_WEEK_THU]').val() == 'Y' && 
                $('input[name=TIME_WEEK_FRI]').val() == 'Y' && 
                $('input[name=TIME_WEEK_SAT]').val() == 'Y' && 
                $('input[name=TIME_WEEK_SUN]').val() == 'Y'){

            $('input[name=TIME_WEEK_ALL]').val('Y')
            $('.all_time_class').addClass('selected')
        }
    })

    function render_member_function(){
        var val = $("input[name=MEMBER_FUNCTION]:checked").val();
        if (val == 'ON') {
            $("tr.MEMBER_DETAIL").removeClass("hide");
            $("tr.ALL_APP").removeClass("hide");
        } else {
            $("tr.MEMBER_DETAIL").addClass("hide");
            $("tr.ALL_APP").addClass("hide");
        }
    }
    function render_member_detail(){
        var val = $("input[name=MEMBER_DETAIL]:checked").val();
        if (val == '1') {
            $("input[name=MEMBER_PASSWORD").addClass("hide");
        } else {
            $("input[name=MEMBER_PASSWORD").removeClass("hide");
        }
    }

    $("input[name=MEMBER_FUNCTION]").change(function(){
        render_member_function();
    });
    $("input[name=MEMBER_DETAIL]").change(function(){
        render_member_detail();
    });
    render_member_function();
    render_member_detail();

    <?php if ($has_preview) {?>
    $(function() {
        updatePreview();
        $("input, textarea, select").change(function() {
            updatePreview();
        });
    });
    <?php }?>

    function option_add(){
        var length = $('input[name="option_value[]"]').length + 1; 
        var newRow = '<tr><td><?= lang('option') ?> '+length+'</td><td><input type="text" class="form-control" name="option_value[]" value="" /></td></tr>';
        $("table.table").append(newRow);
    }

    function basic_add(){
        var randID = Math.floor(Math.random() * 1000000000 + 1);
        var basicItem1 = '<tr class="IMAGE"><td class="col-md-3">写真 </td><td class="col-md-9">'+
        '<img class="preview hidden" id="image-'+randID+'" src="">'+
        '<div class="custom-btn-select ">'+
            '<a data-id="'+randID+'" data-add="true" class="btn btn-primary image-selection-btn btn-xlg btn_image" id="btn_select_'+randID+'">'+
            '画像選択'+
            '</a>'+
            '<a data-id="'+randID+'" data-add="true" class="btn btn-primary btn-xlg change-btn btn_image btn_image_change hidden" id="btn_change_'+randID+'">'+
            '画像選択'+
            '</a>'+
            '<div class="hidden">'+
                '<input type="text" data-id="'+randID+'" id="input-'+randID+'" name="IMAGE[]" value="">'+
                '<div class="file-input file-input-new"><div class="file-preview ">'+
                '<div class="close fileinput-remove">×</div>'+
                '<div class="file-drop-disabled">'+
                '<div class="file-preview-thumbnails">'+
                '</div>'+
                '<div class="clearfix"></div>    <div class="file-preview-status text-center text-success"></div>'+
                '<div class="kv-fileinput-error file-error-message" style="display: none;"></div>'+
                '</div>'+
                '</div>'+
                '<div class="kv-upload-progress hide"><div class="progress">'+
                '<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">'+
                '0%'+
                '</div>'+
                '</div></div>'+
                '<div class="input-group file-caption-main">'+
                '<div tabindex="500" class="form-control file-caption  kv-fileinput-caption">'+
                '<div class="file-caption-name"></div>'+
                '</div>'+

                '<div class="input-group-btn">'+
                '<button type="button" tabindex="500" title="Clear selected files" class="btn btn-default fileinput-remove fileinput-remove-button"><i class="glyphicon glyphicon-trash"></i>  <span class="hidden-xs">Remove</span></button>'+
                '<button type="button" tabindex="500" title="Abort ongoing upload" class="btn btn-default hide fileinput-cancel fileinput-cancel-button"><i class="glyphicon glyphicon-ban-circle"></i>  <span class="hidden-xs">Cancel</span></button>'+
                '<button type="submit" tabindex="500" title="Upload selected files" class="btn btn-default fileinput-upload fileinput-upload-button"><i class="glyphicon glyphicon-upload"></i>  <span class="hidden-xs">Upload</span></button>'+
                '<div tabindex="500" class="btn btn-primary btn-file"><i class="glyphicon glyphicon-folder-open"></i>&nbsp;  <span class="hidden-xs">Browse …</span><input type="file" data-id="'+randID+'" id="file-'+randID+'" class="fileManual" accept="image"></div>'+
                '</div>'+
                '</div></div>'+
                '</div>'+
                '</div>'+
                '<span class="help-block help-area"><span class="help-tab-gray">フォーマット</span>jpeg,jpg,gif,png 画像を登録してください。<br>'+
                '<span class="help-tab-gray">推奨サイズ</span> 600*600<br>'+
                '<span class="help-tab-gray">容量</span> 300 KBで画像を登録してください。</span></td></tr>';

        var basicItem2 = '<tr class="TITLE"><td class="col-md-3">タイトル </td><td class="col-md-9"><input class="form-control" type="text" onchange="updatePreview()" name="TITLE[]" value=""></td></tr>';
        var basicItem3 = '<tr class="COMMENT"><td class="col-md-3">コメント </td><td class="col-md-9"><textarea class="form-control custom-text-area" onchange="updatePreview()" rows="8" cols="12" name="COMMENT[]"></textarea></td></tr>';
        
        $('#form-body').append(basicItem1);
        $('#form-body').append(basicItem2);
        $('#form-body').append(basicItem3);
        
        
    }
</script>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>