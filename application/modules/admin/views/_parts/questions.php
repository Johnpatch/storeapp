<?php
$rows = $this->db->where("SURVEY_ID", $SURVEY_ID)->order_by("SURVEY_NO")->get("survey_questions")->result_array();
$id_array = [];
foreach ($rows as $row) {
    $id_array[] = $row["ID"];
}
?>

<div class="panel">
    <div class="panel-heading"><?= "質問リスト" ?></div>
    <div class="panel-body" style="padding-top:10px;">
        <div class="row">
            <div class="col-md-12">
                <p class="content-group">

                <div class= "text-center">
                    <span class="alert alert-info no-border center">
                        <?= lang('10_queries') ?>
                    </span>
                    <div class="btn-group dropdown-right-bulk ulk-btn-menu">
                    <a href="<?= site_url("admin/survey/publish_question/$SURVEY_ID") . '?token=' . $token ?>"
                    class="btn btn-primary btn-xlg w-icon-red-btn"><i
                            class="icon-plus-circle2"></i>&nbsp;<?= lang('add') ?></a>
                </div>
               
                <!--table @test start-->
                <?php if (count($rows) > 0): ?>
                    <form method="POST" action="<?= site_url("admin/survey/order_question/$SURVEY_ID").'?token='.$token ?>">
                        <input name="orders" type="hidden" id="orders" value='<?=json_encode($id_array)?>'>
                        <div class="table-responsive clear-both">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th><?= lang('correction_confirmation') ?></th>
                                    <th><?= lang('question_number') ?></th>
                                    <th><?= lang('question') ?></th>
                                    <th><?= lang('publishing_settings') ?></th>
                                </tr>
                                </thead>
                                <tbody>

                                <tbody id="sortable-questions">
                                <?php foreach ($rows as $row): ?>
                                    <tr data-id="<?= $row["ID"] ?>" class="<?=$row["STATUS_SHOW"] == 'Y' ? 'status-active' : ""?>">
                                        <td align="center"><a
                                                href="<?= site_url("admin/survey/publish_question/$SURVEY_ID/$row[SURVEY_NO]").'?token='.$token ?>"
                                                class="btn common-btn-operation-edit custom-btn" style="padding: 7px 12px;"><span>編集</span></a></td>
                                        <td align="center"><?= $row["SURVEY_NO"] ?></td>
                                        <td align="center"><?= $row["QUESTION"] ?></td>
                                        <td align="center"><?= radio_input([
                                                "name" => "STATUS_SHOW" . $row["ID"],
                                                "value" => $row["STATUS_SHOW"],
                                                "inline" => true,
                                                "options" => [
                                                    "Y" => lang("indicate"),
                                                    "N" => lang("hidden")
                                                ]
                                            ], false) ?></td>
                                    </tr>

                                <?php endforeach; ?>
                            </table>
                        </div>
                        <div class = "mt-20">
                        </div>
                        <div class="text-center">
                            <span class="alert alert-info no-border center">
                                <?= lang('sort_by_dragging') ?>
                            </span>
                            &nbsp;
                            &nbsp;
                            &nbsp;
                            <button type="submit" class="btn common-btn-green-small custom-btn"
                                    onclick="submitMainForm('')">
                                <?= lang('save') ?></button>
                        </div>
                    </form>
                <?php else: ?>
                
                <div class = "mt-20">
                        </div>
                    <div class="alert alert-warning no-border">
                        <?= lang('no_matching_data_found') ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url("assets/Sortable/js/Sortable.js") ?>"></script>
<script>
    Sortable.create(document.getElementById("sortable-questions"), {
        group: "questions",
        animation: 150,
        store: {
            get: function (sortable) {
//                var order = localStorage.getItem(sortable.options.group);
//                return order ? order.split('|') : [];
                return [];
            },
            set: function (sortable) {
//                console.log(sortable);
                var order = sortable.toArray();
                $("#orders").val(JSON.stringify(order));
//                alert($("#orders").val());
                // $.post(base_url + 'home/change_picture_order', {order:order}, function(data, status){
//                localStorage.setItem(sortable.options.group, order.join('|'));
            }
        },
    });

    $("input[type=radio]").change(function (evt) {
        var val = $(this).parent().find("input[type=radio]:checked").val();
        var tr = $(this).parents("tr");
        if (val == 'Y') {
            tr.addClass("status-active");
        } else {
            tr.removeClass("status-active");
        }
    });

</script>