
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <!--li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li-->
                    <li class="active breadcrumb-2">  <?= lang('done') ?></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="">
        <div class="panel">
            <!--div class="panel-heading"><?= $page_title ?></div-->
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-body" align="center">
                            <p class='text'><?=lang('registration_has_been_completed')?></p>
                        </div>
                        <div class="row">
                            <div class="text-center">
                                <a href="<?=$back_url;?>" class="btn common-btn-red-medium custom-btn"><i class="fa fa-check"></i> <?= $back_text ?></a>
                                <a href="<?= site_url('admin/home').'?token='.$token ?>" class="btn common-btn-green-medium custom-btn"><i class="fa fa-check"></i><?= lang('go_back_to_the_top_page') ?></a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
       
    </div>
</div>

<script>
    
</script>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>