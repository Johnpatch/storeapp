<?php
//var_dump($save_url);
//exit;
?>
<script>
    function add_hp(id, video_name, video_value, comment_name, comment_value, is_view = 'readonly') {
        readonly = is_view == 'readonly';

        var select = $("#current_select_input").clone();
        select.attr("name", video_name + "[]");
        select.val(video_value);

        if (readonly) {
            select.addClass("hidden");
        }
        var video_item = $("<div class='video-item'></div>");
        video_item.append(select);
        video_item.append("<label class='" + (readonly ? '' : 'hidden') + "'>" + select.find("option:selected").text() + "</label>");
        var html = "";
        html +=             "<input type='text' onchange='updatePreview()' class='form-control "+ (readonly ? 'hidden' : '') + "' name='" + comment_name + "[]' placeholder='comment' value='" + comment_value + "'  />"
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + comment_value + "</label>";
        video_item.append(html);
        var wrapper = $("<div class='video-item' id='video-item-div-" + id + "' style='margin-bottom: 20px;'></div>");
        wrapper.append(video_item);

        var html = "";
        html +=         '<div class="' + (readonly ? ' hidden ' : '') + '">';
        html +=             '<button type="button" class="photo-item-remove btn-danger" onclick= "video_remove(\'' + id + '\')">';
        html +=                 '<i class="fa fa-trash-o"></i>';
        html +=              '</button>';
        html +=         '</div>';
        wrapper.append(html)
        $('.photo-comments-list').append(wrapper);
    }

    function add_video(id, video_name, video_value, comment_name, comment_value, is_view = 'readonly') {
        readonly = is_view == 'readonly';

        var html = "<div class='video-item' id='video-item-div-" + id + "' style='margin-bottom: 20px;'>";
        html +=         "<div class='video-item'>";
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + comment_value + ": </label>";
        html+=              "<input type='text' class='form-control "+ (readonly ? 'hidden' : '') + "' name='" + video_name + "[]' placeholder='Video URL' value='" + video_value + "'/>";
        html +=             "<a class='" + (readonly ? '' : 'hidden') + "' href=" + video_value + "'>" + video_value + "</a>";
        html +=             "<input type='text' class='form-control "+ (readonly ? 'hidden' : '') + "' name='" + comment_name + "[]' placeholder='comment' value='" + comment_value + "'  />"

        html +=         "</div>";
        html +=         '<div class="' + (readonly ? ' hidden ' : '') + '">';
        html +=             '<button type="button" class="photo-item-remove btn-danger" onclick= "video_remove(\'' + id + '\')">';
        html +=                 '<i class="fa fa-trash-o"></i>';
        html +=              '</button>';
        html +=         '</div>';
        html +=     "</div>";

        $('.photo-comments-list').append(html);
    }

    function add_photo(id, image_name, image_value, preview_value, comment_name, comment_value, is_view = 'readonly') {
        readonly = is_view == 'readonly';

        var html = "<div class='col-md-6' id='photo-item-div-" + id + "' style='margin-bottom: 20px;'>";
        html +=         "<div style='margin-bottom: 10px'>";
        html +=             "<input id='photo-comment-" + id + "' name='" + comment_name + "[]' class='form-control " + (readonly ? ' hidden ' : '') +"' type='text' style='width: 100%; border-radius: 5px !important; ' value='" + comment_value + "' />";
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + comment_value + "</label>";
        html +=         "</div>";
        html +=         '<div style="margin-bottom: 10px;">';
        html +=             '<img class="photo-item-img" id="photo-image-' + id + '" src="' + preview_value + '" />';
        html +=         '</div>';
        html +=         '<div class="' + (readonly ? ' hidden ' : '') + '">';
        html +=             '<button type="button" class="photo-item-remove btn-danger" onclick= "photo_remove(\'' + id + '\')">';
        html +=                 '<i class="fa fa-trash-o"></i>';
        html +=              '</button>';
        html +=         '</div>';
        html +=         '<input class="hidden" id="photo-image-input-' + id + '" name="' + image_name + '[]" value="'+image_value+'" />';
        html +=     "</div>";

        $('.photo-comments-list').append(html);
    }

    
</script>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= generate_url($route_prefix , $token)?>" class="breadcrumb-1"><?= $bread_title ?></a><?php if(!empty($page_subtitle) || $page_subtitle != '') { ?>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i><?php } ?></li>
                    
                    <?php if(!empty($page_subtitle) || $page_subtitle != '') { ?>
                    <li><?= $page_subtitle ?></li>
                    <?php } ?>
                    <!--li class="active breadcrumb-2"> <?= lang('confirm')?></li-->
                </ul>
            </div>
        </div>
    </div>

    <div class="">
        <div class="panel">
            <div class="panel-heading"><?=lang('confirm_input_contents')?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group">
                           
                        </p>
                    
                        <form action="<?= $save_url ?>" id="layout_form" method="post">
                            <input type="hidden" name="token" value="<?= $token ?>" />
                             <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <!--Begin News Color Settings-->
                                        <?php foreach($fields as $field) {
                                            echo render_element($field, true);
                                        }?>
                                        <?php
                                        if(isset($is_survey_option) && $is_survey_option){
                                            $i = 1;
                                            foreach($survey_options as $option){
                                                if(!empty($option)){
                                                    echo "<tr>
                                                        <td>".lang('option').$i."</td>
                                                        <td><input type='hidden' class='form-control' name='option_value[]' value='".$option."' />".$option."</td>
                                                    </tr>";
                                                    $i++;
                                                }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class = "mb-20">
                                    </div>
                            <div class="text-center">
                                <button type="submit" class="btn common-btn-red-medium custom-btn"><span><i class="fa fa-plus-circle"></i> <?= lang('register_with_this_content') ?></span></button>
                                <a href="<?=$back_url?>" class="btn common-btn-green-medium custom-btn"><?= lang('back_to_fix') ?></a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
</div>

<script>
    function render_member_function(){
        var val = $("input[name=MEMBER_FUNCTION]").val();
        if (val == 'ON') {
            $("tr.MEMBER_DETAIL").removeClass("hide");
            $("tr.ALL_APP").removeClass("hide");
        } else {
            $("tr.MEMBER_DETAIL").addClass("hide");
            $("tr.ALL_APP").addClass("hide");
        }
    }
    function render_member_detail(){
        var val = $("input[name=MEMBER_DETAIL]").val();
        if (val == '1') {
            $("input[name=MEMBER_PASSWORD").addClass("hide");
        } else {
            $("input[name=MEMBER_PASSWORD").removeClass("hide");
        }
    }

    
    render_member_function();
    render_member_detail();    
</script>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>
