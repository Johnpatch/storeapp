<script>
    function renderInputs() {
        
        if ($("input[name=AUTO_PUSH_TYPE]:checked").val() == 2) {//Panel Layout
            $("tr.TYPE").removeClass('hide');
            $("select[name=START_PUSH]:checked").parents('tr').addClass('hide');
        } else {
            $("tr.TYPE").addClass('hide');
            $("select[name=START_PUSH]:checked").parents('tr').removeClass('hide');
        }
    }
    renderInputs();
    $("input[name=AUTO_PUSH_TYPE]").change(function() {
        renderInputs();
    });
</script>