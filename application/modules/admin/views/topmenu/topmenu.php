<script>
    function renderInputs() {
        var layout_type = $("input[name='LAYOUT_TYPE']").val();
        if(layout_type == 1) {//Selected Panel Layout
            $(".BUTTON_TYPE, .IMAGE_FOR_LIST, .OVERLAY_YN, .TITLE_DISP_YN, .TITLE_TEXT_COLOR, .TITLE_POSITION, .BUTTON_BACK_COLOR").addClass('hide');
        } else {////Selected List Layout
            $(".BUTTON_TYPE, .IMAGE_FOR_LIST, .OVERLAY_YN, .TITLE_DISP_YN, .TITLE_TEXT_COLOR, .TITLE_POSITION, .BUTTON_BACK_COLOR").removeClass('hide');
        }

        var select_type = $("select[name='TYPE']").val();
        
        if(select_type == 1){//Web page format
            $("select[name=SUBTYPE]").removeClass('hide');
            var select = $("select[name=SUBTYPE]");
            select.html("");
            var option = $("<option></option>");
            option.attr("value", 1);
            option.text('<?= lang('catalog') ?>');
            select.append(option);
            option = $("<option></option>");
            option.attr("value", 2);
            option.text('<?= lang('photo_gallery') ?>');
            select.append(option);
        }else if(select_type == 2 || select_type == 3 || select_type == 5 || select_type == 8 || select_type == 9 || select_type == 10){
            $("select[name=SUBTYPE]").addClass('hide');
        } else if(select_type == 4){
            $("select[name=SUBTYPE]").removeClass('hide');
            $.post("<?=site_url('admin/ajax/get_page_options')?>", {
                BRANCH_ID: $("input[name=BRANCH_ID]").val(),
                LINK: 'top_post_content_category'
            }, function(data, status) {
                var select = $("select[name=SUBTYPE]");
                select.html("");
                var options = JSON.parse(data);
                var val = select.attr("data-value");
                var keys = Object.keys(options);
                for (var i = 0; i < keys.length; i++) {
                    var option = $("<option></option>");
                    option.attr("value", keys[i]);
                    option.text(options[keys[i]]);
                    if (keys[i] == val) option.attr("selected", "selected");
                    select.append(option);
                }
            })
        }else if(select_type == 7){
            $("select[name=SUBTYPE]").removeClass('hide');
            $.post("<?=site_url('admin/ajax/get_freecontent')?>", {
                BRANCH_ID: $("input[name=BRANCH_ID]").val(),
            }, function(data, status) {
                var select = $("select[name=SUBTYPE]");
                select.html("");
                var options = JSON.parse(data);
                var val = select.attr("data-value");
                var keys = Object.keys(options);
                for (var i = 0; i < keys.length; i++) {
                    var option = $("<option></option>");
                    option.attr("value", keys[i]);
                    option.text(options[keys[i]]);
                    if (keys[i] == val) option.attr("selected", "selected");
                    select.append(option);
                }
            })
        } else if(select_type == 6){
            $("select[name=SUBTYPE]").removeClass('hide');
            var select = $("select[name=SUBTYPE]");
            select.html("");
            var sub_menu_list = [
                '<?= lang('company_profile') ?>',
                '<?= lang('store_guide') ?>',
                '<?= lang('pricing') ?>',
                '<?= lang('FAQ') ?>',
                '<?= lang('notice_setting') ?>',
                '<?= lang('terms_service') ?>',
                '<?= lang('privacy_policy') ?>',
                '<?= lang('information_security') ?>',
                '<?= lang('about_this_app') ?>',
                '<?= lang('intro_friend') ?>'
            ]
            var val = select.attr("data-value");
            for (var i = 0; i < sub_menu_list.length; i++) {
                var option = $("<option></option>");
                option.attr("value", i+1);
                option.text(sub_menu_list[i]);
                if (val == i+1) option.attr("selected", "selected");
                select.append(option);
            }
        }else if(select_type == 11){
            $("select[name=SUBTYPE]").removeClass('hide');
            $.post("<?=site_url('admin/ajax/get_webviewlist')?>", {
                BRANCH_ID: $("input[name=BRANCH_ID]").val(),
            }, function(data, status) {
                var select = $("select[name=SUBTYPE]");
                select.html("");
                var options = JSON.parse(data);
                var val = select.attr("data-value");
                var keys = Object.keys(options);
                for (var i = 0; i < keys.length; i++) {
                    var option = $("<option></option>");
                    option.attr("value", keys[i]);
                    option.text(options[keys[i]]);
                    if (keys[i] == val) option.attr("selected", "selected");
                    select.append(option);
                }
            })
        }
    }
    renderInputs();

    $("select[name='TYPE']").change(function() {
        renderInputs();
    });
</script>