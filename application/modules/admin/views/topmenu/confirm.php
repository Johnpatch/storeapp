<script>
    function renderInputs() {
        var layout_type = $("input[name='LAYOUT_TYPE']").val();
        if(layout_type == 1) {//Selected Panel Layout
            $(".BUTTON_TYPE, .IMAGE_FOR_LIST, .OVERLAY_YN, .TITLE_DISP_YN, .TITLE_TEXT_COLOR, .TITLE_POSITION, .BUTTON_BACK_COLOR").addClass('hide');
        } else {////Selected List Layout
            $(".BUTTON_TYPE, .IMAGE_FOR_LIST, .OVERLAY_YN, .TITLE_DISP_YN, .TITLE_TEXT_COLOR, .TITLE_POSITION, .BUTTON_BACK_COLOR").removeClass('hide');
        }

        var select_type = $("input[name='TYPE']").val();
        if(select_type == 1){//Web page format
            $(".VIEW_PAGE_URL").removeClass('hide');
            $(".IMAGE_FOR_PANEL").next().addClass('hide');
        }
        if(select_type == 2) {//List format
            $(".VIEW_PAGE_URL").addClass('hide');
            $(".IMAGE_FOR_PANEL").next().removeClass('hide');
        }
        if(select_type == 3 || select_type == 4) {// menu, Image
            $(".VIEW_PAGE_URL").addClass('hide');
            $(".IMAGE_FOR_PANEL").next().addClass('hide');
        }
    }
    renderInputs();
</script>