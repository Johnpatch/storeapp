<script>
    function renderInputs() {
        if ($("input[name=LAYOUT_TYPE]").val() == 1) {//Panel Layout
            $("tr").addClass('hide');
            $("input[name=LAYOUT_TYPE]").parents("tr").removeClass('hide');
            $("tr.LAYOUT_TEMPLATE_TYPE").removeClass('hide');
            if($("input[name=LAYOUT_TEMPLATE_TYPE]").val() == 11){
                $("tr.language_setting").removeClass('hide');
            }else{
                $("tr.language_setting").addClass('hide');
            }
        } else {
            $("tr").removeClass('hide');
            $("tr.LAYOUT_TEMPLATE_TYPE").addClass('hide');
            $("tr.language_setting").addClass('hide');
        }
    }
    renderInputs();
</script>