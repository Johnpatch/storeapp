<script>
    function renderInputs() {
        updateSelect();
        var checked_input_val = $("input[name=LINK]:checked").val();

        if(checked_input_val == 1 || checked_input_val == 2) {
            $(".URL").addClass('hide');
            $("select[name=PAGE_ID]").removeClass("hide");
        } else {
            $("select[name=PAGE_ID]").addClass("hide");
        }

        if(checked_input_val == 3) {
            $(".URL").removeClass('hide');
        }
        if(checked_input_val == 4) {
            $(".URL").addClass('hide');
        }
    }
    renderInputs();
    function updateSelect() {

        $.post("<?=site_url('admin/ajax/get_page_options')?>", {
            BRANCH_ID: $("input[name=BRANCH_ID]").val(),
            LINK: $("input[name=LINK]:checked").val()
        }, function(data, status) {
            var select = $("select[name=PAGE_ID]");
            select.html("");
            var options = JSON.parse(data);
            var val = select.attr("data-value");
            var keys = Object.keys(options);
            for (var i = 0; i < keys.length; i++) {
                var option = $("<option></option>");
                option.attr("value", keys[i]);
                option.text(options[keys[i]]);
                if (keys[i] == val) option.attr("selected", "selected");
                select.append(option);
            }
        })
    }

    $("input[name=LINK]").change(function() {
        updateSelect();
        renderInputs();
    });
</script>