<script src="<?= base_url('assets/highcharts/highcharts.js') ?>"></script>
<script src="<?= base_url('assets/highcharts/data.js') ?>"></script>
<script src="<?= base_url('assets/highcharts/drilldown.js') ?>"></script>

<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li>Home</li>
                    
                </ul>
            </div>
        </div>
    </div>

    <div class="panel-body home-page">
        <div class="row">
            <div class="col-md-6">
                <!--div class="col-md-12">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h6 class="panel-title"><i class="fa fa-bullhorn"></i><?=lang('notice')?></h6>
                        </div>
                        <div class="panel-body" style="padding-top:10px;">
                            <p class="content-group">
                                <?=lang('no_new_announcements_now')?>
                            </p>
                        </div>
                    </div>
                </div-->
                <div class="col-md-12">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title"><?=lang('manual')?></h5>
                        </div>
                        <div class="panel-body" style="padding-top:10px;">
                            <?php if(!empty($result['MANUAL_NAME'])) { ?>
                                <p class="content-group">
                                    <a href="http://oemstoreapp.excill.com/uploads/<?= $result['MANUAL'] ?>" target="_blank"><?= $result['MANUAL_NAME']?></a>
                                </p>
                            <?php } ?>
                            <?php if(!empty($result['MANUAL_NAME1'])) { ?>
                                <p class="content-group">
                                    <a href="http://oemstoreapp.excill.com/uploads/<?= $result['MANUAL1'] ?>" target="_blank"><?= $result['MANUAL_NAME1']?></a>
                                </p>
                            <?php } ?>
                            <?php if(!empty($result['MANUAL_NAME2'])) { ?>
                                <p class="content-group">
                                    <a href="http://oemstoreapp.excill.com/uploads/<?= $result['MANUAL2'] ?>" target="_blank"><?= $result['MANUAL_NAME2']?></a>
                                </p>
                            <?php } ?>
                            <?php if(!empty($result['MANUAL_NAME3'])) { ?>
                                <p class="content-group">
                                    <a href="http://oemstoreapp.excill.com/uploads/<?= $result['MANUAL3'] ?>" target="_blank"><?= $result['MANUAL_NAME3']?></a>
                                </p>
                            <?php } ?>
                            <?php if(!empty($result['MANUAL_NAME4'])) { ?>
                                <p class="content-group">
                                    <a href="http://oemstoreapp.excill.com/uploads/<?= $result['MANUAL4'] ?>" target="_blank"><?= $result['MANUAL_NAME4']?></a>
                                </p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title"><i class="fa fa-bar-chart"></i> <?=lang('analysis')?></h5>
                        </div>
                        <div class="panel-body" style="padding-top:10px;">
                            <p class="content-group">
                                <?=lang('total_number_app_downloads')?><br>
                                <?=lang('uninstall_substraction_no_addition')?>
                            <ul>
                                <li style="display: list-item;"> <?=lang('android')?> <?php if(!isset($android) || empty($android)) { echo '0';} else{ echo count($android);} ?></li><br>
                                <li style="display: list-item;"> <?=lang('ios')?>  <?php if(!isset($ios) || empty($ios)) { echo '0';} else{ echo count($ios);} ?></li>
                                
                            </ul>
                            <br>
                            <?=lang('total_last_push_in_total')?> <?php echo count($android)+count($ios); ?>
                            <br>
                            <?=lang('total_push_notification_ctr')?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
