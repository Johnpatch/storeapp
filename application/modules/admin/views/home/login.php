<style>
    body {

        background-position: bottom  right;
        background-repeat: no-repeat;
        /*background-color:#548fd0;*/
    }

</style>
<div class="login-title">
<?php if($result['LOGO'] != '') { ?>
    <img src="<?php echo $result['LOGO']; ?>">
<?php } else { ?>
    <img src="<?= base_url('assets/imgs/site-logo.svg') ?>">
<?php } ?>
</div>
<div class="container">
    <div class="login-container">
        <div id="output">
            <?php
            if ($this->session->flashdata('err_login')) {
                ?>
                <div class="alert alert-danger"><?= $this->session->flashdata('err_login') ?></div>
                <?php
            }
            ?>
        </div>

        <div class="form-box">
            <form action="" method="POST">
                <div class="align-left">
                    <label class="login-input-label">ユーザーID</label>
                    <input type="text" class="b-margin-20" name="username" placeholder="ユーザーID">
                </div>
                <div class="align-left">
                    <label class="login-input-label"><?= lang('password')?></label>
                    <input type="password" name="password" placeholder="<?= lang('password')?>">
                </div>
                <button class="btn btn-info btn-block login login-btn" type="submit"><?=lang('login')?></button>
                <!--a href="#" class="reset-password"><?= lang('reset_password')?></a-->
            </form>
        </div>
    </div>
</div>