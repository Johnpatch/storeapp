<?php
$form_description = lang("downloads_month_description");
$page_title = lang("analysis");
$page_subtitle = lang("downloads_by_month");
$errors = 0;
$has_preview = true;
?>
<style>
    .faq-panel{ color: #333; background-color: #fcfcfc; border-color: #ddd;}
    .faq-panel>.panel-heading{ color: #333; background-color: #fcfcfc; border-color: #ddd;}
    .faq-panel>.panel-heading>.heading-elements { background-color: #f5f5f5 !important;}
    .highcharts-exporting-group{display: none !important;}
</style>
<script type="text/javascript" src="<?= base_url('assets/code/highcharts.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/exporting.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/export-data.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/accessibility.js"')?>"></script>
<style type="text/css">
    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
    input[type="number"] {
        min-width: 50px;
    }

</style>
<div class="content-wrapper" >
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title" >
                <h4> <span class="page-maintitle"><?=$page_title?></span>
                    <span class="page-subtitle"> <?=$page_subtitle?><span></h4>
            </div>
        </div>
        <div class = "cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle?></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group"><?= $form_description ?></p>
                        <form method="GET" id="searchProductsForm" action="" class="form-horizontal form-bordered">
                            <input type="hidden" value="<?= $token ?>" name="token" />
                            <div class="search-condition-content">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody>
                                        <tr>
                                            <td class='col-md-3 td-valign'><?= lang('year')?></td>
                                            <td class='col-md-9 td-valign'>
                                                <select name="selected_year" class="select_year_month form-control year-count-select cursor-pointer" onchange="getMonthlyFavoriteCount(this.value)">
                                                    <?php for($i = $start_year; $i < $last_year; $i ++):?>
                                                        <?php if(isset($_GET['selected_year'])):?>
                                                            <option <?= ($_GET['selected_year'] == $i) ? 'selected' : ''?> value="<?= $i?>"><?= $i?></option>
                                                        <?php else:?>
                                                            <option <?= (date('Y') == $i) ? 'selected' : ''?> value="<?= $i?>"><?= $i?></option>
                                                        <?php endif;?>
                                                    <?php endfor;?>
                                                </select><span>&nbsp;&nbsp;&nbsp;&nbsp;<?= lang('year')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="center" class="text-center" colspan="2">
                                                <button type="submit" class="btn btn-danger btn-lg gray-btn"><i class="fa fa-search"></i>&nbsp;<?= lang('search')?></button>
                                                <a href="<?=base_url('admin/analysis/downloads_month/CSVDownload?token='.$token)?>" class="btn btn-primary btn-xlg green-btn"><i class="fa fa-download"></i>&nbsp;<?= lang('csvdownload')?></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-heading">
                <?= lang('search_condition') ?>
            </div>
    <div class="panel-body home-page">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"> <?= lang('graph') ?></h6>
                    </div>
                    <div class="panel-body" style="padding-top:10px;">
                        <figure class="highcharts-figure">
                            <div id="linecontainer"></div>
                            <p class="highcharts-description">
                            </p>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        Highcharts.chart('linecontainer', {
            chart: {
                type: 'line'
            },
            title: {
                text: '<?= isset($_GET["selected_year"]) ? $_GET["selected_year"] : date("Y")?>年'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                 categories: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                },
                series: {
                    label: {
                        connectorAllowed: true
                    },
                    pointStart: 0
                }
            },
            series: [{
                name: 'Android',
                data: <?= $android_array?>
            }, {
                name: 'IOS',
                data: <?= $ios_array ?>
            }]
        });

        function getMonthlyFavoriteCount(selected_val) {
//            alert($(".year-count-select").val());
        }
    </script>
    <div class="">
        <div class="panel" >
            <div class="panel-heading" ><?= lang('search_list')?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class = "row">
                    <div class = "col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class = "col-md-3"><?= lang('month')?></M></th>
                                    <th class = "col-md-3"><?= lang('android')?></th>
                                    <th class = "col-md-3"><?= lang("ios")?></th>
                                    <th class = "col-md-3"><?= lang("total")?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($search_table as $row) { ?>
                                    <tr>
                                        <?php foreach ($row as $field) { ?>
                                            <td valign="center" class="text-center">
                                                <?php echo $field ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>