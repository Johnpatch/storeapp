<script type="text/javascript" src="<?= base_url('assets/code/highcharts.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/exporting.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/export-data.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/accessibility.js"') ?>"></script>
<style type="text/css">
    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }

    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }

    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }

    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }

    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }

    input[type="number"] {
        min-width: 50px;
    }

</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle?></a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php if ($has_search) : ?>
        <div class="">
            <div class="panel">
                
                <div class="panel-body" style="padding-top:10px;">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="content-group">
                                <?= lang("analysis_segment_statistics_description"); ?>
                                <br>
                                <b><span class="text-danger">
                               <?= lang("analysis_segment_statistics_error"); ?>
                            </span></b>
                            </p>

                            <form method="GET" id="searchProductsForm"  class="form-horizontal form-bordered">
                                <div class="">
                                <input type="hidden" name="token" value="<?= $token ?>">
                                    <table class="table table-bordered" style="background-color: #FFF;">
                                        <tbody>
                                        <tr class="" width="200">
                                            <?php
                                            $field = [
                                                "label"=>lang("specify_year_month"),
                                                "name"=>'USE',
                                                "type"=>'radio',
                                                "options"=>[
                                                    'N'=>lang('do_not_use'),
                                                    'Y'=>lang('use')
                                                ],
                                                "value"=>(isset($_GET['USE']))? $_GET["USE"] : 'N'
                                            ];
                                            echo render_element($field)?>
                                                  
                                        </tr>
                                        <tr id = "Year_Month" class="<?= (isset($_GET['USE']) && $_GET['USE'] == 'Y')? '' : 'hide'?>">
                                            <td width="200"><?= lang('year_month')?></td>
                                            <td>
                                                <select name="selected_year" class="select_year_month form-control year-count-select cursor-pointer" >
                                                    <?php for($i = $start_year; $i < $last_year; $i ++):?>
                                                        <?php if(isset($_GET['selected_year'])):?>
                                                            <option <?= ($_GET['selected_year'] == $i) ? 'selected' : ''?> value="<?= $i?>"><?= $i?></option>
                                                        <?php else:?>
                                                            <option <?= (date('Y') == $i) ? 'selected' : ''?> value="<?= $i?>"><?= $i?></option>
                                                        <?php endif;?>
                                                    <?php endfor;?>
                                                </select><span><?= lang('year_jp')?></span>
                                                <select name="selected_month" class="select_year_month form-control cursor-pointer" >
                                                    <?php for($j = 1; $j <= 12; $j ++):?>
                                                        <?php if(isset($_GET['selected_month'])):?>
                                                            <option <?= ($_GET['selected_month'] == $j) ? 'selected' : ''?> value="<?= $j?>"><?= $j?></option>
                                                        <?php else:?>
                                                            <option <?= (date('m') == $j) ? 'selected' : ''?> value="<?= $j?>"><?= $j?></option>
                                                        <?php endif;?>
                                                    <?php endfor;?>
                                                </select><span><?= lang('month_jp')?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td valign="center" class="text-center" colspan="2">
                                            <button type="submit" class="btn btn-danger btn-lg gray-btn"><i class="fa fa-search"></i>&nbsp;<?= lang('search') ?></button>
                                            <a href="<?=base_url('admin/analysis/segment_statistics/CSVDownload?token='.$token)?>" class="btn btn-primary btn-xlg green-btn"><i class="fa fa-download"></i>&nbsp;<?= lang('csvdownload') ?></a>
                                        </td>
                                         </tr>
                                        </tbody>
                                    </table>
                                </div>
                              
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>


    <div class="panel-heading">
    <?= lang('search_condition') ?>        
    </div>
    <div class="panel-body home-page">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"> <?= lang('os') ?></h6>
                    </div>
                    <div class="panel-body" style="padding-top:10px;">
                        <figure class="highcharts-figure">
                            <div id="piecontainer-os"></div>
                            <p class="highcharts-description">

                            </p>
                        </figure>

                    </div>
                </div>

            </div>
            <!--div class="col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"> <?= lang('age') ?></h6>
                    </div>
                    <div class="panel-body" style="padding-top:10px;">
                        <figure class="highcharts-figure">
                            <div id="piecontainer-age"></div>
                            <p class="highcharts-description">

                            </p>
                        </figure>
                    </div>
                </div>
            </div-->
        </div>
    </div>
    <div class="panel-body home-page">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"> <?= lang('sex') ?></h6>
                    </div>
                    <div class="panel-body" style="padding-top:10px;">
                        <figure class="highcharts-figure">
                            <div id="piecontainer-sex"></div>
                            <p class="highcharts-description">

                            </p>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-body home-page">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"> <?= lang('search_lists') ?></h6>
                    </div>
                    <div class="panel-body" style="padding-top:10px;">
                        <div class="col-md-6">
                            <table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th> <?= lang('number_of_download') ?></th>
                                    <th> <?= $download_count?></th>
                                </tr>
                                <thead>
                            </table>
                            <div class="mt-20"></div>

                            <table class="table table-bordered ">

                                <thead>
                                <tr>
                                    <th colspan="2"><?= lang('os') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($os_table as $row) { ?>
                                    <tr>
                                        <?php foreach ($row as $field) { ?>
                                            <td valign="center" class="text-center">
                                                <?php echo $field ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <div class="mt-20"></div>
                            <!--table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th colspan="2"><?= lang('age') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($age_table as $row) { ?>
                                    <tr>
                                        <?php foreach ($row as $field) { ?>
                                            <td valign="center" class="text-center">
                                                <?php echo $field ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table-->
                        </div>
                        <div class="col-md-6">
                            <!--table class="table table-bordered ">
                                <thead>
                                <tr>
                                    <th colspan="2"><?= lang('sex') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($sex_table as $row) { ?>
                                    <tr>
                                        <?php foreach ($row as $field) { ?>
                                            <td valign="center" class="text-center">
                                                <?php echo $field ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table-->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<?php 
$year = isset($_GET["selected_year"]) ? $_GET["selected_year"] : date("Y");
$month = isset($_GET["selected_month"]) ? $_GET["selected_month"] : date("m");
$use = isset($_GET["USE"]) ? $_GET["USE"] : 'N';
if ($use == 'Y')
    $title = $year.lang('year_jp').$month.lang('month_jp');
else{
    $title = $start_year.lang('year_jp').$start_month.lang('month_jp').'～'.date('Y').lang('year_jp').date('m').lang('month_jp');

}
    
?>
<script type="text/javascript">
    
    $("input[name=USE]").change(function(){
        var val = $("input[name=USE]:checked").val();
        if (val == 'Y') {
            $(this).parents("tr").next().removeClass("hide");
        } else {
            $(this).parents("tr").next().addClass("hide");
        }
    });
    
    Highcharts.chart('piecontainer-sex', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
                text: '<?= $title?> <?= lang('sex') ?>'
        },
        tooltip: {
            pointFormat: '<?= lang('series_format') ?>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        exporting: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<?= lang('point_format') ?>'
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: <?= $sex_chart ?>
        }]
    });
    Highcharts.chart('piecontainer-os', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
                text: '<?= $title?> OS'
        },
        tooltip: {
            pointFormat: '<?= lang('series_format') ?>'
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        exporting: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<?= lang('point_format') ?>'
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: <?= $os_chart ?>
        }]
    });
    Highcharts.chart('piecontainer-age', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
                text: '<?= $title?> <?= lang('age') ?>'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        exporting: {
            enabled: false
        },
        accessibility: {
            point: {
                valueSuffix: '%'
            }
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: <?= $age_chart ?>
        }]
    });
</script>

<script>

    $(function () {
        <?php if($has_preview) :?>

        $('#layout_form').submit();
        <?php endif; ?>
        $("#bulk-dropdown").click(function () {
            $("#bulk-action").toggleClass("hide");
        })
    });
</script>
