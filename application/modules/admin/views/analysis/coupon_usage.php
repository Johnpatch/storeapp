<?php
$form_description = lang("analysis_coupon_usage_description");

$search_table_count = count($search_table); 
$table_displaying = "";

$page_title = lang("number_of_analysis");
$page_subtitle = lang("coupon_usage_daily");
$errors = 0;
$has_preview = true;
?>

<style>
    .faq-panel{ color: #333; background-color: #fcfcfc; border-color: #ddd;}
    .faq-panel>.panel-heading{ color: #333; background-color: #fcfcfc; border-color: #ddd;}
    .faq-panel>.panel-heading>.heading-elements { background-color: #f5f5f5 !important;}
    .highcharts-exporting-group{display: none !important;}
</style>
<script type="text/javascript" src="<?= base_url('assets/code/highcharts.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/exporting.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/export-data.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/accessibility.js"')?>"></script>
<style type="text/css">
    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
    input[type="number"] {
        min-width: 50px;
    }

</style>
<div class="content-wrapper" >
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title" >
                <h4> <span class="page-maintitle"><?=$page_title?></span>
                    <span class="page-subtitle"> <?=$page_subtitle?><span></h4>
            </div>
        </div>
        <div class = "cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle?></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading">
                <?= lang('search_condition') ?>
            </div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group"><?= $form_description ?></p>
                        <form method="GET" id="searchProductsForm" action="" class="form-horizontal form-bordered">
                            <div class="search-condition-content">
                            <input type="hidden" name="token" value="<?= $token ?>">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody> 
                                    
                                        <tr>
                                            <td class = "col-md-3"><?= lang('year_month')?></td>
                                            <td class = "col-md-9">
                                            
                                                <select name="selected_year" class="select_year_month form-control year-count-select cursor-pointer" >
                                                    <?php for($i = $start_year; $i < $last_year; $i ++):?>
                                                        <?php if(isset($_GET['selected_year'])):?>
                                                            <option <?= ($_GET['selected_year'] == $i) ? 'selected' : ''?> value="<?= $i?>"><?= $i?></option>
                                                        <?php else:?>
                                                            <option <?= (date('Y') == $i) ? 'selected' : ''?> value="<?= $i?>"><?= $i?></option>
                                                        <?php endif;?>
                                                    <?php endfor;?>
                                                </select><span><?= lang('year_jp')?></span>
                                                <select name="selected_month" class="select_year_month form-control cursor-pointer" >
                                                    <?php for($j = 1; $j <= 12; $j ++):?>
                                                        <?php if(isset($_GET['selected_month'])):?>
                                                            <option <?= ($_GET['selected_month'] == $j) ? 'selected' : ''?> value="<?= $j?>"><?= $j?></option>
                                                        <?php else:?>
                                                            <option <?= (date('m') == $j) ? 'selected' : ''?> value="<?= $j?>"><?= $j?></option>
                                                        <?php endif;?>
                                                    <?php endfor;?>
                                                </select><span><?= lang('month_jp')?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class='col-md-3 td-valign'><?= lang('coupon')?></td>
                                            <td class='col-md-9 td-valign'>
                                                <select name="selected_coupon_id" class="form-control">
                                                    <?php if(!isset($_GET['selected_coupon_id'])):?>
                                                    <option value="" ><?= lang('please_select');?></option>
                                                    <?php endif; ?>
                                                    <?php for($j = 0; $j < count($coupon_array); $j ++) {?>
                                                        <?php if(isset($_GET['selected_coupon_id'])):?>
                                                            <option <?= ($_GET['selected_coupon_id'] == $coupon_array[$j]['ID']) ? 'selected' : ''?> value="<?= $coupon_array[$j]['ID']?>"><?= $coupon_array[$j]["COUPON_TITLE"]?></option>
                                                        <?php else:?>
                                                            <option value="<?= $coupon_array[$j]['ID']?>"><?= $coupon_array[$j]["COUPON_TITLE"]?></option>
                                                        <?php endif;?>
                                                   <?php }?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" valign="center">
                                                <div class="col-md-12 text-center">
                                                    <button type="submit" class="btn btn-danger btn-lg gray-btn"><i
                                                            class="fa fa-search"></i>&nbsp;&nbsp;<?= lang('search') ?>
                                                    </button>
                                                    <a href="<?=base_url('admin/analysis/coupon_usage/CSVDownload?token='.$token)?>" class="btn btn-primary btn-xlg green-btn"><i class="fa fa-download"></i>&nbsp;<?= lang('csvdownload') ?></a>

                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </form>

                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body home-page">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"> <?= lang('graph')?></h6>
                    </div>
                    <div class="panel-body" style="padding-top:10px;">
                        <figure class="highcharts-figure">
                            <div id="linecontainer"></div>
                            <p class="highcharts-description">
                            </p>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        Highcharts.chart('linecontainer', {
            chart: {
                type: 'line'
            },
            title: {
                text: '<?= isset($_GET["selected_year"]) ? $_GET["selected_year"] : date("Y")?>年<?= isset($_GET["selected_month"]) ? $_GET["selected_month"] : date("m")?>月'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日','13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日','25日', '26日', '27日', '28日', '29日', '30日', '31日']
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                },
                series: {
                    label: {
                        connectorAllowed: true
                    },
                    pointStart: 0
                }
            },
            series: [{
                name: 'Android',
                data: <?= $android_array?>

            }, 
            {
                name: 'IOS',
                data: <?= $ios_array?>
            }]
        });
    </script>
    <div class="">
        <div class="panel" >
            <div class="panel-heading" ><?= lang('search_list')?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class = "row">
                    <div class = "col-md-12">
                        <div class="table-responsive">
                            <p class="content-group"><?= $table_displaying ?></p>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class = "col-md-3"><strong><?= lang('date')?></strong></th>
                                    <th class = "col-md-3"><strong><?= lang('coupon_name')?></strong></th>
                                    <th class = "col-md-3"><strong><?= lang("store_name")?></strong></th>
                                    <th class = "col-md-3"><strong><?= lang("total")?></strong></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($search_table as $row) { ?>
                                    <tr>
                                        <?php foreach ($row as $field) { ?>
                                            <td valign="center" class="text-center">
                                                <?php echo $field ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                
                            </table>

                        </div>
                        <p class="content-group"><?= $table_displaying ?></p>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>