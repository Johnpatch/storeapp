<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done') ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $panel_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group">
                            <?php if ($title_index): ?>
                                <?= $list_description ?>
                            <?php else: ?>
                                <?= $list_description ?>
                            <?php endif; ?>
                        </p>

                        <div class="table-responsive clear-both">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <?php foreach ($table_fields as $column): ?>
                                        <th align="<?= element('align', $column, "center") ?>">
                                            <?= $column['label'] ?>
                                            <?php
                                            $sortable = element("sortable", $column);
                                            if ($sortable == true):?>
                                                <?php $direction = $this->input->get($column["name"]);
                                                if ($direction != "ASC"):?>
                                                    <a onclick="$('input[name=<?= $column["name"] ?>]').val('ASC'); $('#searchForm').submit();"><i
                                                            class="icon-arrow-down7"></i></a>
                                                <?php endif; ?>
                                                <?php if ($direction != "DESC"): ?>
                                                    <a onclick="$('input[name=<?= $column["name"] ?>]').val('DESC'); $('#searchForm').submit();"><i
                                                            class="icon-arrow-up7"></i></a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </th>

                                    <?php endforeach; ?>
                                </tr>
                                </thead>
                                <tbody>
                                <form id="order-update" method="POST" action="<?= $order_url ?>">
                                    <?php foreach ($rows as $row): ?>
                                        <tr>
                                            <?php
                                            foreach ($table_fields as $column):?>
                                                <td align="<?= element('align', $column, "center") ?>"
                                                    class="td-valign">
                                                    <?php $type = element('type', $column);
                                                    $group = element('group', $column);
                                                    $between = element('between', $column);
                                                    ?>
                                                    <?php if ($group == false): ?>
                                                        <?php if ($type == "search"): ?>
                                                            <a href="<?= base_url('admin/userinformation/detail/') . $row["ID"] . '?token='.$token ?>"
                                                               class="btn common-btn-operation-edit custom-btn" style="padding: 7px 12px;"><span>確認</span></a>
                                                        <?php elseif ($type == "mail"): ?>
                                                            <a href="#"
                                                               class="btn common-btn-operation-mail custom-btn" style="padding: 7px 12px;"><span>設定</span></a>
                                                        <?php else: ?>
                                                            <?= content_view($column, $row[$column["name"]]) ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            <?php endforeach; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                </form>
                                </tbody>
                            </table>
                        </div>
                        <?= $links_pagination ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($has_search) : ?>
        <div class="">
            <div class="panel">
                <div class="panel-heading">
                    <?= lang('search_condition') ?>
                </div>
                <div class="panel-body" style="padding-top:10px;">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="content-group">
                                <?= $form_description ?>
                            </p>

                            <form method="GET" id="searchForm" action="" class="form-horizontal form-bordered">
                                <input type="hidden" name="token" value="<?= $token; ?>" />
                                <div class="search-condition-content">
                                    <table class="table table-bordered" style="background-color: #FFF;">
                                        <tbody>
                                        <tr>
                                            <?php foreach ($search_fields as $field): ?>
                                                <td><?= $field['label'] ?></td>
                                                <td><input type="text" class="form-control" name="<?= $field['name'] ?>"
                                                           value="<?= $field["value"] ?>"></td>
                                            <?php endforeach; ?>
                                        </tr>
                                        <tr>
                                            <td colspan="4" valign="center">
                                                <div class="col-md-12 text-center">
                                                    <?=lang('number_search_results_displayed')?>
                                                    <?= perpage_select('perpage', $perpage_options, $perpage) ?>
                                                    <button type="submit" class="btn common-btn-search custom-btn"><i
                                                            class="fa fa-search"></i>&nbsp;&nbsp;<?= lang('search') ?>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <?php if (is_array($table_fields)) {
                                    foreach ($table_fields as $field) {
                                        if (element("sortable", $field) == true) {
                                            ?>
                                            <input type="hidden" name="<?= $field["name"] ?>"
                                                   value="<?= $this->input->get($field["name"]) ?>">
                                        <?php }
                                    }
                                } ?>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
