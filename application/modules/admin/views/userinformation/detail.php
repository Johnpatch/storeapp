
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> Edit</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="">
        <div class="panel">
            <div class="panel-heading"><?=lang('confirm_input_contents')?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group">
                           
                        </p>
                        <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <!--Begin News Color Settings-->
                                        <?php foreach($fields as $field) {
                                            echo render_element($field, true);
                                        }?>
                                    </tbody>
                                </table>
                            </div>
                            <div class = "mb-20">
                                    </div>
                            <div class="text-center">
                                <a href="<?=$back_url.'?token='.$token?>" class="btn common-btn-green-medium custom-btn"><?= lang('back') ?></a>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
       
    </div>
</div>

<script>
    
</script>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>