<?php
$form_description = lang("analysis_description");

$page_title = lang("analysis");
$page_subtitle = lang("favorite_by_day");
$errors = 0;
$has_preview = true;
?>
<style>
    .faq-panel{ color: #333; background-color: #fcfcfc; border-color: #ddd;}
    .faq-panel>.panel-heading{ color: #333; background-color: #fcfcfc; border-color: #ddd;}
    .faq-panel>.panel-heading>.heading-elements { background-color: #f5f5f5 !important;}
    .highcharts-exporting-group{display: none !important;}
</style>
<script type="text/javascript" src="<?= base_url('assets/code/highcharts.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/exporting.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/export-data.js')?>"></script>
<script type="text/javascript" src="<?= base_url('assets/code/modules/accessibility.js"')?>"></script>
<style type="text/css">
    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px;
        max-width: 800px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
    input[type="number"] {
        min-width: 50px;
    }

</style>
<div class="content-wrapper" >
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title" >
                <h4> <span class="page-maintitle"><?=$page_title?></span>
                    <span class="page-subtitle"> <?=$page_subtitle?><span></h4>
            </div>
        </div>
        <div class = "cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done')?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading">
                <?= lang('search_condition') ?>
            </div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group"><?= $form_description ?></p>
                        <form method="GET" id="searchProductsForm" action="" class="form-horizontal form-bordered">
                            <input type="hidden" value="<?= $token ?>" name="token" />
                            <div class="search-condition-content">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody>
                                    <tr>
                                        <td class='col-md-3 td-valign'><?= lang('branch_name')?></td>
                                        <td class='col-md-9 td-valign'>
                                            <select class="form-control">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='col-md-3 td-valign'><?= lang('year_month')?></td>
                                        <td class='col-md-9 td-valign'>
                                            <select name="selected_year" class="select_year_month form-control year-count-select cursor-pointer" >
                                                <?php for($i = $start_year; $i < $last_year; $i ++):?>
                                                    <?php if(isset($_GET['selected_year'])):?>
                                                        <option <?= ($_GET['selected_year'] == $i) ? 'selected' : ''?> value="<?= $i?>"><?= $i?></option>
                                                    <?php else:?>
                                                        <option <?= (date('Y') == $i) ? 'selected' : ''?> value="<?= $i?>"><?= $i?></option>
                                                    <?php endif;?>
                                                <?php endfor;?>
                                            </select><span>&nbsp;&nbsp;&nbsp;&nbsp;<?= lang('year')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <select name="selected_month" class="select_year_month form-control cursor-pointer" >
                                                <?php for($j = 1; $j <= 12; $j ++):?>
                                                    <?php if(isset($_GET['selected_month'])):?>
                                                        <option <?= ($_GET['selected_month'] == $j) ? 'selected' : ''?> value="<?= $j?>"><?= $j?></option>
                                                    <?php else:?>
                                                        <option <?= (date('m') == $j) ? 'selected' : ''?> value="<?= $j?>"><?= $j?></option>
                                                    <?php endif;?>
                                                <?php endfor;?>
                                            </select><span>&nbsp;&nbsp;&nbsp;&nbsp;<?= lang('month')?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="center" class="text-center" colspan="2">
                                            <button type="submit" class="btn common-btn-search custom-btn search-btn"><i class="fa fa-search"></i>&nbsp;検索 search</button>
                                            <button type="button" class="btn common-btn-green-medium custom-btn"><i class="fa fa-download"></i>&nbsp;CSVダウンロード</button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-body home-page">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"><?=lang('graph')?></h6>
                    </div>
                    <div class="panel-body" style="padding-top:10px;">
                        <figure class="highcharts-figure">
                            <div id="linecontainer"></div>
                            <p class="highcharts-description">
                            </p>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        Highcharts.chart('linecontainer', {
            chart: {
                type: 'line'
            },
            title: {
                text: '<?= isset($_GET["selected_year"]) ? $_GET["selected_year"] : date("Y")?>年<?= isset($_GET["selected_month"]) ? $_GET["selected_month"] : date("m")?>月'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日','13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日','25日', '26日', '27日', '28日', '29日', '30日', '31日']
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                },
                series: {
                    label: {
                        connectorAllowed: true
                    },
                    pointStart: 1
                }
            },
            series: [{
                name: 'Android',
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }, {
                name: 'IOS',
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }]
        });
    </script>
    <div class="">
        <div class="panel" >
            <div class="panel-heading" ><?= lang('search_list')?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class = "row">
                    <div class = "col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class = "col-md-4"><strong><?= lang('date')?></strong></th>
                                    <th class = "col-md-4"><strong><?= lang('android')?></strong></th>
                                    <th class = "col-md-4"><strong><?= lang("ios")?></strong></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">01<span style="color: red"><(<?=lang('san')?>)</span></td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">02<(<?=lang('mon')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">03<(<?=lang('tue')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">04<(<?=lang('wed')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">05<(<?=lang('thu')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">06<(<?=lang('fri')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">07<span style="color: blue"><(<?=lang('sat')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">08<span style="color: red"><(<?=lang('san')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">09<(<?=lang('mon')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">10th<(<?=lang('tue')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">11th<(<?=lang('wed')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;">12th<(<?=lang('thu')?>)</td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                    <tr>
                                        <td class = "col-md-4" style="text-align: center;"><?=lang('total')?></td>
                                        <td class = "col-md-4">0</td>
                                        <td class = "col-md-4">0</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>