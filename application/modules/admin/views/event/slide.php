<script>
    function renderInputs() {
        var checked_input_val = $("input[name=LINK]:checked").val();
        if(checked_input_val == 1 || checked_input_val == 2) {
            $("input[name=URL]").parent().parent().addClass('hide');
        }
        if(checked_input_val == 3) {
            $("input[name=URL]").parent().parent().removeClass('hide');
        }
        if(checked_input_val == 4) {
            $("input[name=URL]").parent().parent().addClass('hide');
        }
    }
    renderInputs();
    $("input[name=LINK]").change(function() {
        renderInputs();
    });
</script>