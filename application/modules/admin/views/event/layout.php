<script>
    function renderInputs() {
        if ($("input[name=PUBLIC_PERIOD_SETTING]:checked").val() == "N") {//Panel Layout
            $("input[name=PUBLIC_DATE]").parents("tr").addClass('hide');
        } else {
            $("input[name=PUBLIC_DATE]").parents("tr").removeClass('hide');
        }
    }
    renderInputs();
    $("input[name=PUBLIC_PERIOD_SETTING]").change(function() {
        renderInputs();
    });
</script>