<script>
    function renderInputs() {
        var is_active_twitter = $("input[name='TWITTER_ACTIVE_YN']:checked").val();
        if(is_active_twitter == "Y") {
            $("tr.TWITTER_LINK").removeClass('hide');
        } else {
            $("tr.TWITTER_LINK").addClass('hide');
        }

        var is_active_facebook = $("input[name='FACEBOOK_ACTIVE_YN']:checked").val();
        if(is_active_facebook == "Y") {
            $("tr.FACEBOOK_LINK").removeClass('hide');
        } else {
            $("tr.FACEBOOK_LINK").addClass('hide');
        }
    }
    renderInputs();
    $("input[name='TWITTER_ACTIVE_YN']").change(function() {
        renderInputs();
    });
    $("input[name='FACEBOOK_ACTIVE_YN']").change(function() {
        renderInputs();
    });
</script>