<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done')?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $panel_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group">
                            <?= $form_description ?>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered" style="background-color: #FFF;">
                                <tbody id="form-body">
                                <tr>
                                    <td width="150">Android</td>
                                    <td>
                                        <div style="padding-bottom: 10px;">
                                            <span>アプリ名</span>
                                            <input class="form-control" type="text" name="android_name" id="android_name" value="<?php echo $android[0]['APP_NAME']; ?>"/>
                                        </div>
                                        <div style="padding-top: 10px;padding-bottom: 10px;">
                                            <span>Package</span>
                                            <input class="form-control" type="text" name="android_bundle_id" id="android_bundle_id" value="<?php echo $android[0]['BUNDLE_ID']; ?>"/>
                                        </div>
                                        <?php
                                        if(!empty($android) && $android[0]['STATUS'] != 1 || empty($android)){
                                        ?>
                                        <button type="button" onclick="request(1)" class="btn common-btn-red-small custom-btn"><i class="fa fa-check-circle"></i>アプリ申請</button>
                                        <?php } else { ?>
                                        <button type="button" class="btn custom-btn" style="background-color: #a0a0a0;"><i class="fa fa-check-circle"></i>アプリ申請</button>
                                        <?php } ?>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td width="150">iOS</td>
                                    <td>
                                        <form id="ios_request" method="post" enctype="multipart/form-data">
                                            <div>
                                                <span>アプリ名</span>
                                                <input class="form-control" type="text" name="app_name" id="app_name" value="<?php echo $ios[0]['APP_NAME']; ?>"/>
                                            </div>
                                            <div style="padding-top: 10px;">
                                                <span>Bundle ID</span>
                                                <input class="form-control" type="text" name="bundle_id" id="bundle_id" value="<?php echo $ios[0]['BUNDLE_ID']; ?>"/>
                                            </div>
                                            <!--div style="padding-top: 10px;">
                                                <span>Team ID</span>
                                                <input class="form-control" type="text" name="team_id" id="team_id" value="<?php echo $ios[0]['TEAM_ID']; ?>"/>
                                            </div-->
                                            <div style="padding-top: 10px;">
                                                <span>Apple ID</span>
                                                <input class="form-control" type="text" name="apple_id" id="apple_id" value="<?php echo $ios[0]['APPLE_ID']; ?>"/>
                                            </div>
                                            
                                            <div style="padding-top: 10px;padding-bottom: 10px;">
                                                <span>Apple パスワード</span>
                                                <input class="form-control" type="text" name="apple_password" id="apple_password" value="<?php echo $ios[0]['APPLE_PASSWORD']; ?>"/>
                                            </div>

                                            <!--div style="padding-top: 10px;">
                                                <span>P12 ファイル</span>
                                                <input class="form-control" type="file" name="p12_file" id="p12_file"/>
                                                <input type="hidden" id="p12_file_name" value="<?php echo $ios[0]['P12']; ?>" />
                                                <?php if(!empty($ios[0]['P12'])) { ?>
                                                <a href="<?php echo base_url('ipa/'.$ios[0]['P12']) ?>" download><?php echo $ios[0]['P12']; ?></a>
                                                <?php } ?>
                                            </div>

                                            <div style="padding-top: 10px;">
                                                <span>P12 パスワード</span>
                                                <input class="form-control" type="text" name="p12_password" id="p12_password" value="<?php echo $ios[0]['P12_PASSWORD']; ?>"/>
                                            </div>

                                            <div style="padding-top: 10px;">
                                                <span>プッシュ通知キー</span>
                                                <input class="form-control" type="text" name="push_key" id="push_key" value="<?php echo $ios[0]['PUSH_KEY']; ?>"/>
                                            </div>

                                            <div style="padding-top: 10px;">
                                                <span>プッシュ通知キーファイル</span>
                                                <input class="form-control" type="file" name="push_key_file" id="push_key_file"/>
                                                <input type="hidden" id="push_key_file_name" value="<?php echo $ios[0]['PUSH']; ?>" />
                                                <?php if(!empty($ios[0]['PUSH'])) { ?>
                                                <a href="<?php echo base_url('ipa/'.$ios[0]['PUSH']) ?>" download><?php echo $ios[0]['PUSH']; ?></a>
                                                <?php } ?>
                                            </div>

                                            <div style="padding-top: 10px;padding-bottom: 10px;">
                                                <span>プロヴィジョンプロファイルのファイルを選択してください。</span>
                                                <input class="form-control" type="file" name="provision_file" id="provision_file"/>
                                                <input type="hidden" id="provision_file_name" value="<?php echo $ios[0]['PROVISION']; ?>" />
                                                <?php if(!empty($ios[0]['PROVISION'])) { ?>
                                                <a href="<?php echo base_url('ipa/'.$ios[0]['PROVISION']) ?>" download><?php echo $ios[0]['PROVISION']; ?></a>
                                                <?php } ?>
                                            </div-->

                                            <?php
                                            if(!empty($ios) && $ios[0]['STATUS'] != 1 || empty($ios)){
                                            ?>
                                            <button type="submit" class="btn common-btn-red-small custom-btn"><i class="fa fa-check-circle"></i>アプリ申請</button>
                                            <?php } else { ?>
                                                <button type="button" class="btn custom-btn" style="background-color: #a0a0a0;"><i class="fa fa-check-circle"></i>アプリ申請</button>
                                            <?php } ?>
                                        </form>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function request(type){
    if($('#android_bundle_id').val() == '' || $('#android_name').val() == ''){
        alert("ANDROIDを申請するためには全てのフォームを入力しなければなりません。")
    }else{
        $.ajax({
            url: '<?php echo site_url("admin/manage/application/request_app")."?token=".$token;?>',
            type: 'POST',
            data: {
                type: type,
                app_name: $('#android_name').val(),
                bundleId: $('#android_bundle_id').val()
            },
            success: function (resp) {
                alert("申請からストア登録にQRコードが表示されるまでにしばらく時間がかります。")
                location.reload();
            },
            error: function () {
                
            }
        });
    }
}

$("form#ios_request").submit(function(e) {
    e.preventDefault();    
    var isValidate = 0
    
    if($('#app_name').val() == '')
        isValidate = 1;
    else if($('#bundle_id').val() == '')
        isValidate = 1;
    else if($('#apple_id').val() == '')
        isValidate = 1;
    else if($('#apple_password').val() == '')
        isValidate = 1;
    /*else if($('#team_id').val() == '')
        isValidate = 1;
    else if($('#p12_password').val() == '')
        isValidate = 1;
    else if($('#push_key').val() == '')
        isValidate = 1;
    else if($('#p12_file').val() == '' && $('#p12_file_name').val() == '')
        isValidate = 1;
    else if($('#provision_file').val() == '' && $('#provision_file_name').val() == '')
        isValidate = 1;*/
    if(isValidate == 1)
        alert("IPAを申請するためには全てのフォームを入力しなければなりません。")
    else{
        var formData = new FormData(this);
        formData.append('type', 2)
        formData.append('subtype', 1)
        $.ajax({
            url: '<?php echo site_url("admin/manage/application/request_app")."?token=".$token;?>',
            type: 'POST',
            data: formData,
            success: function (data) {
                alert("申請からストア登録にQRコードが表示されるまでにしばらく時間がかります。")
                location.reload();
                /*formData.set('subtype', 2)
                $.ajax({
                    url: '<?php echo site_url("admin/manage/application/request_app")."?token=".$token;?>',
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        formData.set('subtype', 3)
                        $.ajax({
                            url: '<?php echo site_url("admin/manage/application/request_app")."?token=".$token;?>',
                            type: 'POST',
                            data: formData,
                            success: function (data) {
                                alert("申請からストア登録にQRコードが表示されるまでにしばらく時間がかります。")
                                location.reload();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });*/
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
    
});
</script>