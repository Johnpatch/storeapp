<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>
        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done')?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $panel_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group">
                            <?= $form_description ?>
                        </p>
                        <div class="table-responsive">
                            <table class="table table-bordered" style="background-color: #FFF;">
                                <tbody id="form-body">
                                <tr>
                                    <td width="150">Android</td>
                                    <td>
                                    	<?php if(array_key_exists('android',$qr_code)){
                                            if(array_key_exists('STATUS', $qr_code['android'])){ ?>
                                            <div><?= lang('app_build_failed') ?></div>
                                            <?php } else { ?>
                                        <div class="margin-bottom-10">
											<img src="<?= base_url('qrcode/').$qr_code['android']['qrcode']?>">
                                        </div>
                                        <button type="button" class="btn common-btn-red-small custom-btn" onclick="location.href='mailto:?body=<?= $qr_code['android']['app_url']?>'"><i class="fa fa-check-circle"></i>&nbsp; QRをメールで送る</button>
                                        <a href="<?= $qr_code['android']['app_url']?>">
                                            <button type="button" class="btn common-btn-red-small custom-btn"><i class="fa fa-check-circle"></i>&nbsp;アプリをインストール(Androidのみ)</button>
                                        </a>
                                        <?php } } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="150">iOS</td>
                                    <td>
                                    	<?php if(array_key_exists('ios', $qr_code)){ 
                                            if($qr_code['ios']['STATUS'] == 3){ ?>
                                                <div><?= lang('app_build_failed') ?></div>
                                                <?php } elseif($qr_code['ios']['STATUS'] == 2) { ?>
                                        <div class="margin-bottom-10">
												<!--img src="<?= base_url('qrcode/').$qr_code['ios']['qrcode']?>"-->
											
                                            <a href="<?= base_url('ipa/'.$qr_code['ios']['branch_id']."/ios.zip") ?>" download><?= lang('download') ?></a>
                                        </div>
                                        <!--button type="button" class="btn common-btn-red-small custom-btn" onclick="location.href='mailto:?body=<?= $qr_code['ios']['app_url']?>'"><i class="fa fa-check-circle"></i>&nbsp; QRをメールで送る</button>
                                        <a href="<?= $qr_code['ios']['app_url']?>">
                                        	<button type="button" class="btn common-btn-red-small custom-btn"><i class="fa fa-check-circle"></i>&nbsp;アプリをインストール(iOSのみ)</button>
                                        </a-->
                                        	<?php } } ?>
                                        
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
