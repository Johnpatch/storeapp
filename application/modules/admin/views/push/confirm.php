<script>
    function renderInputs() {
        updateSelect()
        /*Delivery Type*/
        var checked_delivery_type = $("input[name='DELIVERY_TYPE']").val();
        $("tr").removeClass('hide');
        
        if($("input[name='DELIVERY_TIME_SETTING']").val() == 1) {
            $("tr.DELIVERY_DATETIME").addClass('hide');
        } else {
            $("tr.DELIVERY_DATETIME").removeClass('hide');
        }
        if(checked_delivery_type == 1) { // News/Event and Coupon
            $("tr.DELIVERY_USER").addClass("hide");
        }
        if(checked_delivery_type == 2){
            $("tr.DELIVERY_SURVEY").addClass("hide");
        }
        if(checked_delivery_type == 3){
            $("tr.DELIVERY_USER").addClass("hide");
        }
    }
    renderInputs();

    function updateSelect() {
        var delivery_type_val = $("input[name=DELIVERY_TYPE]").val();
        var delivery_select_button = "";
        if(delivery_type_val == 1 || delivery_type_val == 3) {//News/Event or Individual push
            delivery_select_button =  "<a href='<?= base_url("admin/event/publish")?>' class='btn common-btn-red-medium custom-btn delivery-type-add-btn'><i class='fa fa-plus-circle'></i>&nbsp;新しくお知らせを作成</a>";
            delivery_type_val = 1;
        } else if(delivery_type_val == 2) {//Coupon
            delivery_select_button =  "<a href='<?= base_url("admin/coupon/publish")?>' class='btn common-btn-red-medium custom-btn delivery-type-add-btn'><i class='fa fa-plus-circle'></i>&nbsp;新しくクーポンを作成</a>";
        } else if(delivery_type_val == 4) {//Survey
            delivery_select_button =  "";
            delivery_select_button =  "<a href='<?= base_url("admin/survey/publish")?>' class='btn common-btn-red-medium custom-btn delivery-type-add-btn'><i class='fa fa-plus-circle'></i>&nbsp;新しくクーポンを作成</a>";
        }
        $.post("<?=site_url('admin/ajax/get_page_options')?>", {
            BRANCH_ID: $("input[name=BRANCH_ID]").val(),
            LINK: delivery_type_val
        }, function(data, status) {
            var select = $("select[name=REFER_ID]");
            select.html("");
            select.addClass("delivery-type-select");
            var options = JSON.parse(data);
            var val = select.attr("data-value");
            var keys = Object.keys(options);
            for (var i = 0; i < keys.length; i++) {
                var option = $("<option></option>");
                option.attr("value", keys[i]);
                option.text(options[keys[i]]);
                if (keys[i] == val) option.attr("selected", "selected");
                select.append(option);
            }
            $(".delivery-type-add-btn").remove();
            select.parent().append(delivery_select_button);
        })
    }
</script>