<script>
    function renderInputs() {
        // updateSelect();
        /*Delivery Type*/
        var checked_delivery_type = $("input[name='DELIVERY_TYPE']:checked").val();
        var checked_delivery_time = $("input[name='DELIVERY_TIME_SETTING']:checked").val();
        if(checked_delivery_type == 1) { // News/Event and Coupon
            $("tr.DELIVERY_USER").addClass("hide");
            $("tr.DELIVERY_SURVEY_SETTING").addClass("hide");
        } else if(checked_delivery_type == 2){
            $("tr.DELIVERY_USER").removeClass("hide");
            $("tr.DELIVERY_SURVEY_SETTING").addClass("hide");
        } else if(checked_delivery_type == 3){
            $("tr.DELIVERY_USER").addClass("hide");
            $("tr.DELIVERY_SURVEY_SETTING").removeClass("hide");
        }
        if(checked_delivery_time == 1){
            $("tr.DELIVERY_DATETIME").addClass("hide");
        }else{
            $("tr.DELIVERY_DATETIME").removeClass("hide");
        }

        updatePreview();
    }

    renderInputs();

    function updateSelect() {
        var delivery_type_val = $("input[name=DELIVERY_TYPE]:checked").val();
        var delivery_select_button = "";
        if(delivery_type_val == 1 || delivery_type_val == 3) {//News/Event or Individual push
            delivery_select_button =  "<a href='<?= base_url("admin/event/publish")."?token=".$token?>' class='btn common-btn-red-medium custom-btn delivery-type-add-btn'><i class='fa fa-plus-circle'></i>&nbsp;新しくお知らせを作成</a>";
            delivery_type_val = 1;
        } else if(delivery_type_val == 2) {//Coupon
            delivery_select_button =  "<a href='<?= base_url("admin/coupon/publish")."?token=".$token?>' class='btn common-btn-red-medium custom-btn delivery-type-add-btn'><i class='fa fa-plus-circle'></i>&nbsp;新しくクーポンを作成</a>";
        } else if(delivery_type_val == 4) {//Survey
            delivery_select_button =  "";
            delivery_select_button =  "<a href='<?= base_url("admin/survey/publish")."?token=".$token?>' class='btn common-btn-red-medium custom-btn delivery-type-add-btn'><i class='fa fa-plus-circle'></i>&nbsp;新しくクーポンを作成</a>";
        }
        $.post("<?=site_url('admin/ajax/get_page_options')?>", {
            BRANCH_ID: $("input[name=BRANCH_ID]").val(),
            LINK: delivery_type_val
        }, function(data, status) {
            var select = $("select[name=REFER_ID]");
            select.html("");
            select.addClass("delivery-type-select");
            var options = JSON.parse(data);
            var val = select.attr("data-value");
            var has_value = false;
            for (var key in options) {
                var option = $("<option></option>");
                option.attr("value", key);
                option.text(options[key]);
                if (key == val) {
                    option.attr("selected", "selected");
                    has_value = true;
                }
                select.append(option);
            }
            if (has_value == false) {
                if (select.find("option").length > 0) {
                    $(select.find("option")[0]).attr("selected", "selected");
                }
            }
            $(".delivery-type-add-btn").remove();
            if(delivery_type_val != 4) {
                select.parent().append(delivery_select_button);
            }

            renderInputs();
        })
    }
    /*Delivery Type*/
    $("input[name='DELIVERY_TYPE']").change(function() {
        renderInputs();
    });

    $("input[name='DELIVERY_TIME_SETTING']").change(function() {
        renderInputs();
    });

    function get_survey_question_list(){
        $.post("<?=site_url('admin/ajax/get_survey_question_list')?>", {
            ID: $("select[name='DELIVERY_SURVEY']").val(),
            BRANCH_ID: $("input[name=BRANCH_ID]").val(),
        }, function(data, status) {
            var select = $("select[name=DELIVERY_SURVEY_QUESTION]");
            select.html("");
            var result = JSON.parse(data);
            var options = result['list'];
            var type = result['TYPE'];
            var val = select.attr("data-value");
            var keys = Object.keys(options);
            for (var i = 0; i < keys.length; i++) {
                var option = $("<option></option>");
                option.attr("value", keys[i]);
                option.text(options[keys[i]]);
                if (keys[i] == val) option.attr("selected", "selected");
                select.append(option);
            }
            if(type == 1 || type == 2){
                $('input[name=DELIVERY_SURVEY_ANSWER]').addClass('hide');
            }else{
                $('input[name=DELIVERY_SURVEY_ANSWER]').removeClass('hide');
            }
            get_survey_option_list();
        })
    }

    function get_survey_option_list(){
        $.post("<?=site_url('admin/ajax/get_survey_option_list')?>", {
            ID: $("select[name='DELIVERY_SURVEY_QUESTION']").val(),
            BRANCH_ID: $("input[name=BRANCH_ID]").val(),
        }, function(data, status) {
            var select = $("select[name=DELIVERY_SURVEY_OPTION]");
            select.html("");
            var options = JSON.parse(data);
            console.log(options)
            var val = select.attr("data-value");
            var keys = Object.keys(options);
            for (var i = 0; i < keys.length; i++) {
                var option = $("<option></option>");
                option.attr("value", keys[i]);
                option.text(options[keys[i]]);
                if (keys[i] == val) option.attr("selected", "selected");
                select.append(option);
            }
            
        })
    }

    $("select[name='DELIVERY_SURVEY']").change(function() {
        get_survey_question_list();
    })

    $("select[name='DELIVERY_SURVEY_QUESTION']").change(function() {
        get_survey_option_list();
    })

    //get_survey_question_list();

    $("#layout_form").submit(function() {
        var deliveryTimeSetting = $("input[name='DELIVERY_TIME_SETTING']:checked").val();
        if (deliveryTimeSetting == 1) {//10 minutes from now
            var date = new Date();
            date = date.getTime() + 60 * 10 * 1000;
            date = new Date(date);
            var dateStr = date.toISOString();
            dateStr =   dateStr.replace("T", " ").substr(0, 19);

            $("input[name=DELIVERY_DATETIME]").val(dateStr);
        }
    });
</script>