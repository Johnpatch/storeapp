
<style>
.faq-panel{
 
    color: #333;
    background-color: #fcfcfc;
    border-color: #ddd;

}
.faq-panel>.panel-heading{
    
    color: #333;
    background-color: #fcfcfc;
    border-color: #ddd;
}
.faq-panel>.panel-heading>.heading-elements {
 
    background-color: #f5f5f5 !important;
}
.icons-list a[data-action=collapse]:after {
    content: '\e9c2';
}
</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="#" class="breadcrumb-1"><?= $bread_title ?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?=lang("FAQ")?></a>&nbsp;&nbsp;&nbsp;<i class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done')?></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="panel-body home-page">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title"><?= lang("FAQ")?></h6>
                    </div>
                    <div class="panel-body" style="padding-top:10px;">
                        <div class="panel faq-panel panel-collapsed panel-1 cursor-pointer">
                            <div class="panel-heading" onclick="FaqActions(1)">
                                <h6 class="panel-title"><?=lang('no_know_how_to_see_app')?>
                                    <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li onclick="FaqActions(1)"><a data-action="collapse" class="" id="img_1"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body" style="padding-top:10px;" id="panel_1">
                                <?=lang('read_qr_code_by_app')?>
                            </div>
                        </div>
                        <div class="panel faq-panel panel-collapsed panel-2 cursor-pointer">
                            <div class="panel-heading" onclick="FaqActions(2)">
                                <h6 class="panel-title"><?=lang('want_create_edit_app')?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li onclick="FaqActions(2)"><a data-action="collapse" class="" id="img_2"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body" style="padding-top:10px; display: block;" id="panel_2">
                                <?=lang('read_qr_code_by_app_on_terminal')?>
                            </div>
                        </div>
                        <div class="panel faq-panel panel-collapsed panel-3 cursor-pointer">
                            <div class="panel-heading" onclick="FaqActions(3)">
                                <h6 class="panel-title"><?=lang('want_make_coupon')?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li onclick="FaqActions(3)"><a data-action="collapse" class="" id="img_3"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body" style="padding-top:10px; display: block;" id="panel_3">
                                <?=lang('read_qr_code_by_app_on_terminal')?>
                            </div>
                        </div>
                        <div class="panel faq-panel panel-collapsed panel-4 cursor-pointer">
                            <div class="panel-heading" onclick="FaqActions(4)">
                                <h6 class="panel-title"><?=lang('app_will_teminated_forcibly')?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li onclick="FaqActions(4)"><a data-action="collapse" class="" id="img_4"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body" style="padding-top:10px; display: block;" id="panel_4">
                                <?=lang('read_qr_code_by_app_on_terminal')?>
                            </div>
                        </div>
                        <div class="panel faq-panel panel-collapsed panel-5 cursor-pointer">
                            <div class="panel-heading" onclick="FaqActions(5)">
                                <h6 class="panel-title"><?=lang('what_pust_notification')?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li onclick="FaqActions(5)"><a data-action="collapse" class="" id="img_5"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body" style="padding-top:10px; display: block;" id="panel_5">
                                <?=lang('read_qr_code_by_app_on_terminal')?>
                            </div>
                        </div>
                        <div class="panel faq-panel panel-collapsed panel-6 cursor-pointer">
                            <div class="panel-heading" onclick="FaqActions(6)">
                                <h6 class="panel-title"><?=lang('pust_notification_not_delivered')?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li onclick="FaqActions(6)"><a data-action="collapse" class="" id="img_6"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body" style="padding-top:10px; display: block;" id="panel_6">
                                <?=lang('read_qr_code_by_app_on_terminal') ?>
                            </div>
                        </div>
                        <div class="panel faq-panel panel-collapsed panel-7 cursor-pointer">
                            <div class="panel-heading" onclick="FaqActions(7)">
                                <h6 class="panel-title"><?=lang('displayed_cannot_install')?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li onclick="FaqActions(7)"><a data-action="collapse" class="" id="img_7"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body" style="padding-top:10px; display: block;" id="panel_7">
                                <?=lang('read_qr_code_by_app_on_terminal')?>
                            </div>
                        </div>
                        <div class="panel faq-panel panel-collapsed panel-8 cursor-pointer">
                            <div class="panel-heading" onclick="FaqActions(8)">
                                <h6 class="panel-title"><?=lang('need_reinstall_app_everytime')?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li onclick="FaqActions(8)"><a data-action="collapse" class="" id="img_8"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body" style="padding-top:10px; display: block;" id="panel_8">
                                <?=lang('read_qr_code_by_app_on_terminal')?>
                            </div>
                        </div>
                        <div class="panel faq-panel panel-collapsed panel-9 cursor-pointer">
                            <div class="panel-heading" onclick="FaqActions(9)">
                                <h6 class="panel-title"><?=lang('i_editted_photos_info')?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li onclick="FaqActions(9)"><a data-action="collapse" class="" id="img_9"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body" style="padding-top:10px; display: block;" id="panel_9">
                                <?=lang('read_qr_code_by_app_on_terminal')?>
                            </div>
                        </div>
                        <div class="panel faq-panel panel-collapsed panel-10 cursor-pointer">
                            <div class="panel-heading" onclick="FaqActions(10)">
                                <h6 class="panel-title"><?=lang('can_change_name_app')?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li onclick="FaqActions(10)"><a data-action="collapse" class="" id="img_10"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body" style="padding-top:10px; display: block;" id="panel_10">
                                <?=lang('read_qr_code_by_app_on_terminal')?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<script>
    var number = [];
    function panel_toggle(num){
        number.push(num);
        if(number.length > 1){
            var panel_num = number[number.length - 2];
            if(num != panel_num){
            $("#panel_" + panel_num).css('display', 'none');
                $("#img_" + panel_num).addClass("rotate-180");
            }
        }
    }
    function FaqActions(arg) {
        panel_toggle(arg);
        $(".panel-"+arg+" a").toggleClass('rotate-180');
        if($(".panel-"+arg+" .panel-body").css('display') == 'block') {
            $(".panel-"+arg+" .panel-body").css('display', 'none');
        } else {
            $(".panel-"+arg+" .panel-body").css('display', 'block');
        }

    }
</script>
