<script>
    function updatePreview() {
        console.log($("input[name='STAMP_IMAGE']:checked").data('url'));
        var bonus_image_delete = "0";
        if( $('#bonus_image_delete').is(":checked") ) {
            bonus_image_delete = "1";
        }
        $('#bonus_image_del').val(bonus_image_delete);
        var stamp_mark_url = $("input[name='STAMP_IMAGE']:checked").data('url');
        console.log(stamp_mark_url);
        $('#stamp_image_url').val(stamp_mark_url);

        var temp_form = $("#layout_form").clone();
        temp_form.attr("action", "<?= $preview_url?>");
        temp_form.attr("target", "preview-frame");
        $("body").append(temp_form);
        temp_form.submit();
        temp_form.remove();
    }
</script>
<script>
    $(function() {
        $('#bonus_image_selection').click(function() {
            $("#bonus_image_file").click();
        });

        $(document).on("change", "#bonus_image_file", function() {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var ext = input.value.match(/\.(.+)$/)[1];
                if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif') {
                    reader.onload = function(e) {
                        $('#bonus_image_preview').show();
                        $('#bonus_image_preview').attr('src', e.target.result);
                        $('#result_bonus_image_preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });

        $('#save_btn').click(function() {
            var stamp_title = $('#stamp_title').val();
            var stamp_details = $('#stamp_details').val();
            var stamp_number = $('#stamp_number').val();
            var stamp_max_number = $('#stamp_max_number').val();
            var stamp_once_a_day = $("input[name='stamp_once_a_day']:checked").val();
            var stamp_complete_number = $('#stamp_complete_number').val();
            var stamp_card_valid_days = $('#stamp_card_valid_days').val();
            var stamp_mark = $("input[name='STAMP_IMAGE']:checked").val();
            var privilege_details = $('#privilege_details').val();
            var reward_expiration_date = $('#reward_expiration_date').val();
            var terms_and_conditions = $('#terms_and_conditions').val();
            
            if(stamp_title == '') {
                $("#form_alerts").html("* <?php echo lang('alert_stamp_title'); ?>");
                return;
            }
            if(stamp_details == '') {
                $("#form_alerts").html("* <?php echo lang('alert_stamp_details'); ?>");
                return;
            }
            if(stamp_number == '' || !jQuery.isNumeric(stamp_number) || parseInt(stamp_number) > 100 || parseInt(stamp_number) < 1) {
                $("#form_alerts").html("* <?php echo lang('alert_stamp_number'); ?>");
                return;
            }
            if(stamp_complete_number == '' || !jQuery.isNumeric(stamp_complete_number) || parseInt(stamp_complete_number) > 100 || parseInt(stamp_complete_number) < 4) {
                $("#form_alerts").html("* <?php echo lang('alert_stamp_complete_number'); ?>");
                return;
            }
            if(stamp_card_valid_days == '' || !jQuery.isNumeric(stamp_card_valid_days) || parseInt(stamp_card_valid_days) > 7300 || parseInt(stamp_card_valid_days) < 1) {
                $("#form_alerts").html("* <?php echo lang('alert_stamp_card_valid_days'); ?>");
                return;
            }
            if(stamp_mark == '' || stamp_mark == undefined) {
                $("#form_alerts").html("* <?php echo lang('alert_stamp_mark'); ?>");
                return;
            }
            if(privilege_details == '') {
                $("#form_alerts").html("* <?php echo lang('alert_privilege_details'); ?>");
                return;
            }
            if(reward_expiration_date == '' || !jQuery.isNumeric(reward_expiration_date) || parseInt(reward_expiration_date) > 7300 || parseInt(reward_expiration_date) < 1) {
                $("#form_alerts").html("* <?php echo lang('alert_reward_expiration_date'); ?>");
                return;
            }

            var stamp_mark_url = $("input[name='STAMP_IMAGE']:checked").data('url');
            $("#result_stamp_title").html(stamp_title);
            $("#result_stamp_details").html(stamp_details);
            $("#result_stamp_number").html(stamp_number);
            $("#result_stamp_max_number").html(stamp_max_number);
            $("#result_stamp_once_a_day").html(stamp_once_a_day);
            $("#result_stamp_complete_number").html(stamp_complete_number);
            $("#result_stamp_card_valid_days").html(stamp_card_valid_days);
            $("#result_image_preview").attr("src", stamp_mark_url);
            $("#result_privilege_details").html(privilege_details);
            $("#result_reward_expiration_date").html(reward_expiration_date);
            $("#result_terms_and_conditions").html(terms_and_conditions);

            if($('#bonus_image_delete').is(":checked")) {
                $('#result_bonus_image_label').show();
                $('#result_bonus_image_preview').hide();
            } else {
                $('#result_bonus_image_label').hide();
                $('#result_bonus_image_preview').show();
            }

            $('#result_content').show();
            $('#setting_content').hide();
            $('#save_btn').hide();
            $('#register_btn').show();
            $('#edit_btn').show();
        });

        $('#edit_btn').click(function() {
            $('#result_content').hide();
            $('#setting_content').show();
            $('#save_btn').show();
            $('#register_btn').hide();
            $('#edit_btn').hide();
        });

        $('#return_repair_btn').click(function() {
            location.reload();
        });

        $('#register_btn').click(function() {
            var stamp_title = $('#stamp_title').val();
            var stamp_details = $('#stamp_details').val();
            var stamp_number = $('#stamp_number').val();
            var stamp_max_number = $('#stamp_max_number').val();
            var stamp_once_a_day = $("input[name='stamp_once_a_day']:checked").val();
            var stamp_complete_number = $('#stamp_complete_number').val();
            var stamp_card_valid_days = $('#stamp_card_valid_days').val();
            var stamp_mark = $("input[name='STAMP_IMAGE']:checked").val();
            var privilege_details = $('#privilege_details').val();
            var reward_expiration_date = $('#reward_expiration_date').val();
            var terms_and_conditions = $('#terms_and_conditions').val();
            var bonus_image_delete = "0";
            if( $('#bonus_image_delete').is(":checked") ) {
                bonus_image_delete = "1";
            }

            var fd = new FormData();
            var files = $('#bonus_image_file')[0].files[0];
            if(files != undefined) {
                fd.append('file', files);
            }
            fd.append('TITLE', stamp_title);
            fd.append('DETAIL', stamp_details);
            fd.append('NUMBER_OF_STAMPS', stamp_number);
            fd.append('MAXIMUM_STAMPS', stamp_max_number);
            fd.append('STAMP_ONCE_PER_DAY', stamp_once_a_day);
            fd.append('COMPLETE_NUMBER', stamp_complete_number);
            fd.append('STAMP_CARD_VALID_DAYS', stamp_card_valid_days);
            fd.append('STAMP_IMAGE_ID', stamp_mark);
            fd.append('PRIVILEGE_DETAIL', privilege_details);
            fd.append('REWARD_EXPIRATION_DATE', reward_expiration_date);
            fd.append('TERMS_CONDITIONS', terms_and_conditions);
            fd.append('bonus_image_delete', bonus_image_delete);

            $.ajax({
                url: '<?php echo site_url("admin/stamp/stampprivilege/updateInfo");?>',
                type: 'POST',
                data: fd,
                contentType: false,
                processData: false,
                success: function (resp) {
                    $('#edit_formgroup').hide();
                    $('#success_formgroup').show();

                    $('#register_btn').hide();
                    $('#edit_btn').hide();
                },
                error: function () {
                }
            });
        });
    });
</script>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/home') ?>" class="breadcrumb-1"><?= $page_title ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done') ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $panel_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="layout-content col-md-<?=$has_preview ? 9 : 12?>">
                        <p class="content-group">
                            <?= $form_description ?>
                            <br>
                            <?php if($stamp_setting == "0") { ?>
                                <span style="color: red;"> <?php echo lang('alert_stamp_setting_off');?> </span><br>
                            <?php } ?>
                            <span style="color: red;" id="form_alerts"> </span>
                        </p>

                        <form action="" id="layout_form" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data">
                            <input type="hidden" name="back_url" value="<?=current_url()?>">
                            <input type="hidden" name="BRANCH_ID" value="<?= $branch_id?>">
                            <input type="hidden" name="bonus_image_url" value="<?= $stamp_info['BONUS_IMAGE'] ?>">
                            <div class="table-responsive">
                                <div id="edit_formgroup">
                                    <div id="setting_content">
                                        <table class="table table-bordered" style="background-color: #FFF;">
                                            <tbody id="form-body">
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_title'); ?> <span class="required-mark"><?= lang('required');?></span> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <textarea rows="3" class="form-control" name="TITLE" id="stamp_title"><?php echo $stamp_info['TITLE'];?></textarea>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <span class="help-block help-area">
                                                                    <span class="help-tab-gray"><?php echo lang('entry_example'); ?></span> <?php echo lang('stamp_title_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_details'); ?> <span class="required-mark"><?= lang('required');?></span> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <textarea rows="3" class="form-control" name="DETAIL" id="stamp_title"><?php echo $stamp_info['DETAIL'];?></textarea>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <span class="help-block help-area">
                                                                    <span class="help-tab-gray"><?php echo lang('entry_example'); ?></span> <?php echo lang('stamp_detail_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_number'); ?> <span class="required-mark"><?= lang('required');?></span> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input name="NUMBER_OF_STAMPS" class="form-control" id="stamp_number" value="<?php echo $stamp_info['NUMBER_OF_STAMPS'];?>"/>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <span class="help-block help-area">
                                                                    <?php echo lang('stamp_number_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_max_number'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input name="MAXIMUM_STAMPS" class="form-control" id="stamp_max_number" value="<?php echo $stamp_info['MAXIMUM_STAMPS'];?>"/>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <span class="help-block help-area">
                                                                    <?php echo lang('stamp_max_number_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_once_a_day'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <?php if($stamp_info['STAMP_ONCE_PER_DAY'] != 'Y') { ?>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" name="stamp_once_a_day" value="1" class="styled"><?php echo lang('effectiveness');?></input>
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" name="stamp_once_a_day" checked value="0" class="styled"><?php echo lang('invalid');?></input>
                                                                    </label>
                                                                <?php } else { ?>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" name="stamp_once_a_day" checked value="1" class="styled"><?php echo lang('effectiveness');?></input>
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" name="stamp_once_a_day" value="0" class="styled"><?php echo lang('invalid');?></input>
                                                                    </label>
                                                                <?php }?>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <span class="help-block help-area">
                                                                    <?php echo lang('stamp_once_a_day_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_per_day'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input name="STAMP_PER_DAY" class="form-control" id="stamp_per_day" value="<?php echo $stamp_info['STAMP_PER_DAY'];?>"/>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_complete_number'); ?> <span class="required-mark"><?= lang('required');?></span> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input name="COMPLETE_NUMBER" class="form-control" id="stamp_complete_number" value="<?php echo $stamp_info['COMPLETE_NUMBER'];?>"/>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <span class="help-block help-area">
                                                                    <?php echo lang('stamp_complete_number_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_card_valid_days'); ?> <span class="required-mark"><?= lang('required');?></span> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input name="STAMP_CARD_VALID_DAYS" id="stamp_card_valid_days" value="<?php echo $stamp_info['STAMP_CARD_VALID_DAYS'];?>"/> <?php echo lang('day'); ?>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <span class="help-block help-area">
                                                                    <?php echo lang('stamp_card_valid_days_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!--tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_mark'); ?> <span class="required-mark"><?= lang('required');?></span> </td>
                                                    <td class="col-md-9" style="padding-bottom: 25px;">
                                                        <div class="row">
                                                            <input name="STAMP_IMAGE_URL" id="stamp_image_url" type="hidden" />
                                                            <?php foreach ($stamps as $stamp) { ?>
                                                                <div class="col-md-6">
                                                                    <img src="<?php echo base_url().$stamp['URL']; ?>" style="border: 1px solid #ddd; height: 120px; width: 120px;" />
                                                                    <div>
                                                                    <?php if($stamp_info['STAMP_IMAGE_ID'] == $stamp['ID']) { ?>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="stamp_mark" checked data-url="<?php echo base_url().$stamp['URL']; ?>" value="<?php echo $stamp['ID'];?>" class="styled"></input>
                                                                        </label>
                                                                    <?php } else { ?>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="stamp_mark" data-url="<?php echo base_url().$stamp['URL']; ?>" value="<?php echo $stamp['ID'];?>" class="styled"></input>
                                                                        </label>
                                                                    <?php } ?>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr-->
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('privilege_details'); ?> <span class="required-mark"><?= lang('required');?></span> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <textarea rows="3" class="form-control" name="PRIVILEGE_DETAIL" id="privilege_details"><?php echo $stamp_info['PRIVILEGE_DETAIL'];?></textarea>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <span class="help-block help-area">
                                                                    <span class="help-tab-gray"><?php echo lang('entry_example'); ?></span> <?php echo lang('privilege_details_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('bonus_image'); ?> <span class="required-mark"><?= lang('required');?></span> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12" style="margin-top: 10px;">
                                                            <?php if( $stamp_info['BONUS_IMAGE'] == "" ) { ?>
                                                                <img id="bonus_image_preview" src="#" style="border: 1px solid #ddd; height: 62.5px; width: 100px; display: none;" />
                                                            <?php } else { ?>
                                                                <img id="bonus_image_preview" src="<?php echo base_url().$stamp_info['BONUS_IMAGE']; ?>" style="border: 1px solid #ddd; height: 62.5px; width: 100px;" />
                                                            <?php } ?>
                                                            </div>
                                                            <div class="col-md-12" style="margin-top: 10px;">
                                                                <input name="bonus_image_delete" id="bonus_image_del" hidden />
                                                                <input type="checkbox" id="bonus_image_delete" style="margin-right: 10px;" /><?php echo lang("delete");?>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <hr>
                                                            </div>
                                                            <div class="col-md-12" style="margin-top: 10px;">
                                                                <input type="file" name="file" id="bonus_image_file" style="display: none;" accept="image/x-png,image/gif,image/jpeg"/>
                                                                <button type="button" class="btn common-btn-red-medium custom-btn" style="margin-right: 20px;" id="bonus_image_selection"><?= lang('image_selection') ?></button>
                                                            </div>
                                                            <div class="col-md-12" style="margin-top: 10px;">
                                                                <span class="help-block help-area">
                                                                    <span class="help-tab-gray"><?php echo lang('format'); ?>:</span> jpeg, jpg, gif, png
                                                                    <br/>
                                                                    <span class="help-tab-gray"><?php echo lang('recommended_size'); ?>:</span> 640 × 430
                                                                    <br/>
                                                                    <span class="help-tab-gray"><?php echo lang('capacity'); ?>:</span> <?php echo lang('capacity_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('reward_expiration_date'); ?> <span class="required-mark"><?= lang('required');?></span> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <input name="REWARD_EXPIRATION_DATE" id="reward_expiration_date" value="<?php echo $stamp_info['REWARD_EXPIRATION_DATE'];?>"/> <?php echo lang('day'); ?>
                                                            </div>
                                                            <div class="col-md-12" style="margin-top: 5px;">
                                                                <span class="help-block help-area">
                                                                    <?php echo lang('reward_expiration_date_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('terms_and_conditions'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <textarea name="TERMS_CONDITIONS" class="form-control" rows="3" id="terms_and_conditions"><?php echo $stamp_info['TERMS_CONDITIONS'];?></textarea>
                                                            </div>
                                                            <div class="col-md-12" style="margin-top: 5px;">
                                                                <span class="help-block help-area">
                                                                    <?php echo lang('reward_expiration_date_desc'); ?>
                                                                    <br/>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="result_content" style="display: none;">
                                        <table class="table table-bordered" style="background-color: #FFF;">
                                            <tbody>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_title'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_stamp_title"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_details'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_stamp_details"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_number'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_stamp_number"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_max_number'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_stamp_max_number"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_once_a_day'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_stamp_once_a_day"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_complete_number'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_stamp_complete_number"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_card_valid_days'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_stamp_card_valid_days"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!--tr>
                                                    <td class="col-md-3"> <?php echo lang('stamp_mark'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <img id="result_image_preview" src="#" style="border: 1px solid #ddd; height: 120px; width: 120px;" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr-->
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('privilege_details'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_privilege_details"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('bonus_image'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12" id="result_bonus_image_div">
                                                                <label id="result_bonus_image_label"><?php echo lang('delete'); ?></label>
                                                                <img id="result_bonus_image_preview" src="<?php echo base_url().$stamp_info['BONUS_IMAGE']; ?>" style="border: 1px solid #ddd; height: 62.5px; width: 100px;" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('reward_expiration_date'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_reward_expiration_date"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('reward_expiration_date'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_reward_expiration_date"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-3"> <?php echo lang('terms_and_conditions'); ?> </td>
                                                    <td class="col-md-9">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label id="result_terms_and_conditions"></label>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <br>
                                <div class="text-center">
                                    <button type="button" class="btn common-btn-green-small custom-btn" id="save_btn"><?= lang('save') ?></button>
                                    <button type="button" class="btn common-btn-red-medium custom-btn" id="register_btn" style="display: none;"><i class="fa fa-check"></i> <?= lang('register_with_this_content') ?></button>
                                    <button type="button" class="btn common-btn-green-small custom-btn" id="edit_btn" style="display: none;"><?= lang('return_to_repair') ?></button>
                                </div>
                            </div>
                            <div id="success_formgroup" style="display: none;">
                                <div class="row">
                                    <div class="col-md-12" style="text-align: center;">
                                        <label><?php echo lang('register_success');?></label>
                                    </div>
                                    <div class="col-md-12" style="text-align: center;">
                                        <button type="button" class="btn common-btn-red-medium custom-btn" style="margin-right: 20px;" id="return_repair_btn"><i class="fa fa-check"></i> <?= lang('return_to_repair') ?></button>
                                        <a href="<?php echo site_url('admin/home');?>" type="button" class="btn common-btn-green-medium custom-btn"><i class="fa fa-check"></i> <?= lang('return_to_toppage') ?></a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($has_preview) {
            $this->load->view('_parts/preview');
        }
        ?>
    </div>
</div>

<script>
    <?php if ($has_preview) {?>
    $(function() {
        updatePreview();
        $("input, textarea, select").change(function() {
            updatePreview();
        });
    });
    <?php }?>
</script>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>