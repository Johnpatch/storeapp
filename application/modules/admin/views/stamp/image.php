<script>
    function remove_item(id) {
        if(confirm("Do you want to delete?")){
            $.ajax({
                url: '<?php echo site_url("admin/stamp/stampimage/delete_row");?>',
                type: 'POST',
                data: {
                    id: id
                },
                success: function (resp) {
                    location.reload();
                },
                error: function () {
                }
            });
        }
    }
</script>
<script>
    $(function() {
        $(document).on("click", ".confirm-delete", function (e) {
            e.preventDefault();
            var no = $(this).data('no');
            bootbox.confirm({
                message: "このデータを削除します。よろしいですか？",
                buttons: {
                    confirm: {
                        label: 'はい',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: '取り消す',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            url: '<?php echo site_url("admin/stamp/stampimage/delete_row");?>',
                            type: 'POST',
                            data: {
                                id: no
                            },
                            success: function (resp) {
                                location.reload();
                            },
                            error: function () {
                            }
                        });
                    }
                }
            });
        });

        $('#save_btn').click(function() {
            var html = '';
            var show = $("input[name='setting_option']:checked").val();
            if(show == "1") {
                html += "<?php echo lang('effectiveness');?>"
            } else {
                html += "<?php echo lang('invalid');?>"
            }
            $('#result_content').html("");
            $('#result_content').append(html);
            $('#result_content').show();
            $('#setting_content').hide();
            $('#save_btn').hide();
            $('#register_btn').show();
            $('#edit_btn').show();
        });

        $('#edit_btn').click(function() {
            $('#result_content').hide();
            $('#setting_content').show();
            $('#save_btn').show();
            $('#register_btn').hide();
            $('#edit_btn').hide();
        });

        $('#return_repair_btn').click(function() {
            location.reload();
        });

        $('#register_btn').click(function() {
            var effectiveness = $("input[name='setting_option']:checked").val();
            if(effectiveness != "1") {
                effectiveness = "0";
            }
            $.ajax({
                url: '<?php echo site_url("admin/stamp/stampsetting/updateSetting");?>',
                type: 'POST',
                data: {
                    effectiveness: effectiveness
                },
                success: function (resp) {
                    $('#edit_formgroup').hide();
                    $('#success_formgroup').show();

                    $('#register_btn').hide();
                    $('#edit_btn').hide();
                },
                error: function () {
                }
            });
        });
    });
</script>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/home') ?>" class="breadcrumb-1"><?= $page_title ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done') ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $page_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group">
                            <?= $form_description ?>
                        </p>
                        <a type="button" class="btn common-btn-red-medium custom-btn" href="<?= base_url('admin/stamp/stampimage/addview')?>" style="margin-bottom: 10px;">
                            <i class="fa fa-plus-circle"></i>
                            <?php echo lang('add_stamp_image');?>
                        </a>
                        <form action="" id="layout_form" method="post" class="form-horizontal form-bordered">
                            <input type="hidden" name="back_url" value="<?=current_url()?>">
                            <input type="hidden" name="BRANCH_ID" value="<?= $branch_id?>">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <tr id="edit_formgroup">
                                            <td><?php echo lang('delete'); ?></td>
                                            <td><?php echo lang('image'); ?></td>
                                        </tr>
                                        <?php foreach ($stamps as $stamp) { ?>
                                        <tr>
                                            <td align="center">
                                                <button type="button" class="btn common-btn-operation-trash custom-btn confirm-delete" data-no="<?php echo $stamp['ID'];?>">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <img src="<?php echo base_url().$stamp['URL'];?>" style='width: 120px; height: 120px;'/>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<h1><?=$page_title?><small><?=$page_subtitle?></small></h1>
<hr>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li class="active">
                <?=$page_icon?> <?= $page_title ?> &gt; <?= $page_subtitle ?>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <?php if ($has_preview):?>
    <div class="col-md-8 layout-content">
    <?php else:?>
    <div class="col-md-12">
    <?php endif;?>
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <?=$page_icon?><?=$panel_title?>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-title">
                    <?=$form_description?>

                    <?php if ($errors) {
                        foreach ($errors as $error) {
                            echo $error;
                        }
                    }?>
                </div>
                <!-- BEGIN FORM-->
                <form action="" id="layout_form" method="post" class="form-horizontal form-bordered">
                    <input type="hidden" name="back_url" value="<?=current_url()?>">
                    <input type="hidden" name="BRANCH_ID" value="<?= $branch_id?>">
                    <div class="form-body">
                        <!--Begin News Color Settings-->
                        <div class="form-group" id="edit_formgroup">
                            <div class="row">
                                <div class="col-md-12">
                                    <a type="button" class="btn blue" href="<?= base_url('admin/stamp/stampimage/addview')?>">
                                        <i class="fa fa-plus"></i>
                                        <?php echo lang('add_stamp_image');?>
                                    </a>
                                </div>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <td align="center"><?php echo lang('delete');?></td>
                                                <td align="center"><?php echo lang('image');?></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($stamps as $stamp) { ?>
                                                <tr>
                                                    <td align="center">
                                                        <button type="button" class="btn-danger" onclick="remove_item(<?php echo $stamp['ID'];?>)">
                                                            <i class="fa fa-trash-o"></i>
                                                        </button>
                                                    </td>
                                                    <td align="center">
                                                        <img src="<?php echo base_url().$stamp['URL'];?>" style='width: 120px; height: 120px;'/>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-9" id="result_content" style="display: none">
                            </div>
                        </div>
                        <div class="form-group" id="success_formgroup" style="display: none;">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <label><?php echo lang('register_success');?></label>
                                </div>
                                <div class="col-md-12" style="text-align: center;">
                                    <button type="button" class="btn blue" style="margin-right: 20px;" id="return_repair_btn"><i class="fa fa-check"></i> <?= lang('return_to_repair') ?></button>
                                    <a href="<?php echo site_url('admin/home');?>" type="button" class="btn default"><i class="fa fa-check"></i> <?= lang('return_to_toppage') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn blue" id="register_btn" style="display: none;"><i class="fa fa-check"></i> <?= lang('register') ?></button>
                                <button type="button" class="btn default" id="edit_btn" style="display: none;"><?= lang('return_to_repair') ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>