<script>
    $(function() {
        $('#save_btn').click(function() {
            var html = '';
            var show = $("input[name='setting_option']:checked").val();
            if(show == "1") {
                html += "<?php echo lang('effectiveness');?>"
            } else {
                html += "<?php echo lang('invalid');?>"
            }
            $('#result_content').html("");
            $('#result_content').append(html);
            $('#result_content').show();
            $('#setting_content').hide();
            $('#save_btn').hide();
            $('#register_btn').show();
            $('#edit_btn').show();
        });

        $('#edit_btn').click(function() {
            $('#result_content').hide();
            $('#setting_content').show();
            $('#save_btn').show();
            $('#register_btn').hide();
            $('#edit_btn').hide();
        });

        $('#return_repair_btn').click(function() {
            location.reload();
        });

        $('#register_btn').click(function() {
            var value = $("input[name='setting_option']:checked").val();
            if(value != "1") {
                value = "0";
            }
            $.ajax({
                url: '<?php echo site_url("admin/stamp/stampsetting/updateSetting")."?token=".$token;?>',
                type: 'POST',
                data: {
                    VALUE: value
                },
                success: function (resp) {
                    $('#edit_formgroup').hide();
                    $('#success_formgroup').show();

                    $('#register_btn').hide();
                    $('#edit_btn').hide();
                },
                error: function () {
                }
            });            
        });
    });
</script>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/home') ?>" class="breadcrumb-1"><?= $page_title ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done') ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $page_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group">
                            <?= $form_description ?>
                        </p>

                        <form action="" id="layout_form" method="post" class="form-horizontal form-bordered">
                            <input type="hidden" name="back_url" value="<?=current_url()?>">
                            <input type="hidden" name="BRANCH_ID" value="<?= $branch_id?>">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <tr id="edit_formgroup">
                                            <td class="col-md-3"><?php echo lang('use_of_stamp'); ?></td>
                                            <td class="col-md-9" id="setting_content">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                    <?php if($stamp_status != '1') { ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="setting_option" value="1" class="styled"><?php echo lang('effectiveness');?></input>
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="setting_option" checked value="0" class="styled"><?php echo lang('invalid');?></input>
                                                        </label>
                                                    <?php } else { ?>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="setting_option" checked value="1" class="styled"><?php echo lang('effectiveness');?></input>
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="setting_option" value="0" class="styled"><?php echo lang('invalid');?></input>
                                                        </label>
                                                    <?php }?>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="col-md-9" id="result_content" style="display: none">
                                            </td>
                                        </tr>
                                        <tr id="success_formgroup" style="display: none;">
                                            <td style="text-align: center;">
                                                <label><?php echo lang('register_success');?></label>
                                            </td>
                                            <td style="text-align: center;">
                                                <button type="button" class="btn common-btn-red-medium custom-btn" style="margin-right: 20px;" id="return_repair_btn"><i class="fa fa-check"></i> <?= lang('return_to_repair') ?></button>
                                                <a href="<?php echo site_url('admin/home');?>" type="button" class="btn common-btn-green-medium custom-btn"><i class="fa fa-check"></i> <?= lang('return_to_toppage') ?></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br/>
                            <div class="text-center">
                                <button type="button" class="btn common-btn-green-small custom-btn" id="save_btn"><?= lang('save') ?></button>
                                <button type="button" class="btn common-btn-red-medium custom-btn" id="register_btn" style="display: none;"><i class="fa fa-check"></i> <?= lang('register_with_this_content') ?></button>
                                <button type="button" class="btn common-btn-green-small custom-btn" id="edit_btn" style="display: none;"><?= lang('return_to_repair') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>