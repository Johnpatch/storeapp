<script>
    function updatePreview() {
        var temp_form = $("#layout_form").clone();
        temp_form.attr("action", "<?= $preview_url?>");
        temp_form.attr("target", "preview-frame");
        /*temp_form.find("input").each(function(i, target) {
         $(target).attr("name", $(target).attr("name").toUpperCase());
         });*/
        $("body").append(temp_form);
        temp_form.submit();
        
        temp_form.remove();
    }
</script>
<script>
    $(function() {
        $('#image_selection').click(function() {
            $("#image_file").click();
        });

        $(document).on("change", "#image_file", function() {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var ext = input.value.match(/\.(.+)$/)[1];
                if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif') {
                    reader.onload = function(e) {
                        $('#image_preview').show();
                        $('#image_preview').attr('src', e.target.result);
                        $('#image_result_preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });

        $('#save_btn').click(function() {
            if( $('#image_result_preview').attr('src') != "#" ) {
                $('#result_content').show();
                $('#setting_content').hide();
                $('#save_btn').hide();
                $('#register_btn').show();
                $('#edit_btn').show();
            }
        });

        $('#edit_btn').click(function() {
        });

        $('#register_btn').click(function() {
            var fd = new FormData();
            var files = $('#image_file')[0].files[0];
            fd.append('file',files);
            console.log(fd);
            $.ajax({
                url: '<?php echo site_url("admin/stamp/stampimage/uploadStamp");?>',
                type: 'POST',
                data: fd,
                contentType: false,
                processData: false,
                success: function (resp) {
                    var res = jQuery.parseJSON(resp);
                    if(res.state == "error") {
                        $("#error").text(res.msg);
                        $("#error").show();
                        $('#result_content').hide();
                        $('#setting_content').show();
                    } else {
                        $("#error").hide();

                        $('#edit_formgroup').hide();
                        $('#success_formgroup').show();

                        $('#register_btn').hide();
                        $('#edit_btn').hide();
                    }
                },
                error: function (resp) {
                    
                }
            });
        });
    });
</script>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4><span class="page-maintitle"><?= $page_title ?></span>
                    <span class="page-subtitle"> <?= $page_subtitle ?><span></h4>
            </div>
        </div>

        <div class="cms-breadcrumb">
            <div class="breadcrumb-line"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
                <ul class="breadcrumb">
                    <li><a href="<?= base_url('admin/home') ?>" class="breadcrumb-1"><?= $page_title ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li><a href="#" class="breadcrumb-1"><?= $page_subtitle ?></a>&nbsp;&nbsp;&nbsp;<i
                            class="fa fa-angle-right breadcrumb-size"></i></li>
                    <li class="active breadcrumb-2"> <?= lang('done') ?></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="">
        <div class="panel">
            <div class="panel-heading"><?= $page_title ?></div>
            <div class="panel-body" style="padding-top:10px;">
                <div class="row">
                    <div class="col-md-12">
                        <p class="content-group">
                            <?= $form_description ?>
                        </p>

                        <form action="" id="layout_form" method="post" class="form-horizontal form-bordered">
                            <input type="hidden" name="back_url" value="<?=current_url()?>">
                            <input type="hidden" name="BRANCH_ID" value="<?= $branch_id?>">
                            <div class="table-responsive">
                                <table class="table table-bordered" style="background-color: #FFF;">
                                    <tbody id="form-body">
                                        <tr id="edit_formgroup">
                                            <td class="col-md-3"><?php echo lang('stamp_image'); ?></td>
                                            <td class="col-md-9" id="setting_content">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="alert alert-error" style="color: #b94a48; background-color: #f2dede; border-color: #eed3d7; display: none;" id="error">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <img id="image_preview" src="#" style="border: 1px solid #ddd; height: 120px; width: 120px; display: none;" />
                                                    </div>
                                                    <div class="col-md-12" style="margin-top: 10px;">
                                                        <input type="file" id="image_file" style="display: none;" accept="image/x-png,image/gif,image/jpeg"/>
                                                        <button type="button" class="btn common-btn-red-medium custom-btn" style="margin-right: 20px;" id="image_selection"><?= lang('image_selection') ?></button>
                                                    </div>
                                                    <div class="col-md-12" style="margin-top: 10px;">
                                                        <span class="help-block help-area">
                                                            <span class="help-tab-gray"><?php echo lang('format'); ?>:</span> jpeg, jpg, gif, png
                                                            <br/>
                                                            <span class="help-tab-gray"><?php echo lang('recommended_size'); ?>:</span> 640 × 430
                                                            <br/>
                                                            <span class="help-tab-gray"><?php echo lang('capacity'); ?>:</span> <?php echo lang('capacity_desc'); ?>
                                                            <br/>
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="col-md-9" id="result_content" style="display: none">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <img id="image_result_preview" src="#" style="border: 1px solid #ddd; height: 120px; width: 120px;" />
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr id="success_formgroup" style="display: none;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-12" style="text-align: center;">
                                                        <label><?php echo lang('register_success');?></label>
                                                    </div>
                                                    <div class="col-md-12" style="text-align: center;">
                                                        <a href="<?php echo site_url('admin/stamp/stampimage');?>" type="button" class="btn common-btn-red-medium custom-btn" style="margin-right: 20px;"><i class="fa fa-check"></i> <?= lang('return_to_list') ?></a>
                                                        <a href="<?php echo site_url('admin/home');?>" type="button" class="btn common-btn-green-medium custom-btn"><i class="fa fa-check"></i> <?= lang('return_to_toppage') ?></a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="button" class="btn common-btn-green-small custom-btn" id="save_btn"><?= lang('save') ?></button>
                                <button type="button" class="btn common-btn-red-medium custom-btn" id="register_btn" style="display: none;"><i class="fa fa-check"></i> <?= lang('register_with_this_content') ?></button>
                                <a href="<?php echo site_url('admin/stamp/stampimage/addview'); ?>" type="button" class="btn common-btn-green-small custom-btn" id="edit_btn" style="display: none;"><?= lang('return_to_repair') ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>