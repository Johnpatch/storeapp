<script>
    function updatePreview() {
        var temp_form = $("#layout_form").clone();
        temp_form.attr("action", "<?= $preview_url?>");
        temp_form.attr("target", "preview-frame");
        /*temp_form.find("input").each(function(i, target) {
         $(target).attr("name", $(target).attr("name").toUpperCase());
         });*/
        $("body").append(temp_form);
        temp_form.submit();
        
        temp_form.remove();
    }

    function add_photo(id, image_name, image_value, comment_name, comment_value, is_view = 'readonly') {
        readonly = is_view == 'readonly';

        var html = "<div class='col-md-6' id='photo-item-div-" + id + "' style='margin-bottom: 20px;'>";
        html +=         "<div style='margin-bottom: 10px'>";
        html +=             "<input id='photo-comment-" + id + "' name='" + comment_name + "[]' class='form-control " + (readonly ? ' hidden ' : '') +"' type='text' style='width: 100%; border-radius: 5px !important; ' value='" + comment_value + "' />";
        html +=             "<label class='" + (readonly ? '' : 'hidden') + "'>" + comment_value + "</label>";
        html +=         "</div>";
        html +=         '<div style="margin-bottom: 10px;">';
        html +=             '<img class="photo-item-img" id="photo-image-' + id + '" src="' + image_value + '" />';
        html +=         '</div>';
        html +=         '<div class="' + (readonly ? ' hidden ' : '') + '">';
        html +=             '<button type="button" class="photo-item-remove btn-danger" onclick= "photo_remove(\'' + id + '\')">';
        html +=                 '<i class="fa fa-trash-o"></i>';
        html +=              '</button>';
        html +=         '</div>';
        html +=         '<input class="hidden" id="photo-image-input-' + id + '" name="' + image_name + '[]" value="'+image_value+'" />';
        html +=     "</div>";

        $('.photo-comments-list').append(html);
    }

    function photo_remove(id) {
        document.getElementById('photo-item-div-' + id).remove();
        updatePreview();
    }

</script>
<h1><?=$page_title?><small><?=$page_subtitle?></small></h1>
<hr>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li class="active">
                <?=$page_icon?> <?= $page_title ?> &gt; <?= $page_subtitle ?>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <?php if ($has_preview):?>
    <div class="col-md-8 layout-content">
    <?php else:?>
    <div class="col-md-12">
    <?php endif;?>
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <?=$page_icon?><?=$panel_title?>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-title">
                    <?=$form_description?>

                    <?php if ($errors) {
                        foreach ($errors as $error) {
                            echo $error;
                        }
                    }?>
                </div>
                <!-- BEGIN FORM-->
                <form action="<?=$confirm_url?>" id="layout_form" method="post" class="form-horizontal form-bordered">
                    <input type="hidden" name="back_url" value="<?=current_url()?>">
                    <input type="hidden" name="BRANCH_ID" value="<?= $branch_id?>">
                    <div class="form-body">
                        <!--Begin News Color Settings-->
                        <?php
                            foreach ($fields as $field) {
                                echo form_group(control_label(element('label', $field), element('required', $field)).form_input_wrap(photo_input($field, $row)));
                            }
                        ?>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn blue"><i class="fa fa-check"></i> <?= lang('save') ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php if ($has_preview) {
        $this->load->view('_parts/preview');
    }
    ?>
</div>
<script>
    <?php if ($has_preview) {?>
    $(function() {
        updatePreview();
        $("input, textarea, select").change(function() {
            updatePreview();
        });
    });
    <?php }?>
</script>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>