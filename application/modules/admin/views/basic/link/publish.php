<script>
    function updatePreview() {
        var temp_form = $("#layout_form").clone();
        temp_form.attr("action", "<?= $preview_url?>");
        temp_form.attr("target", "preview-frame");
        /*temp_form.find("input").each(function(i, target) {
         $(target).attr("name", $(target).attr("name").toUpperCase());
         });*/
        $("body").append(temp_form);
        temp_form.submit();
        
        temp_form.remove();
    }
</script>
<h1><?=$page_title?><small><?=$page_subtitle?></small></h1>
<hr>
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li class="active">
                <?=$page_icon?> <?= $page_title ?> &gt; <?= $page_subtitle ?>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <?php if ($has_preview):?>
    <div class="col-md-8 layout-content">
    <?php else:?>
    <div class="col-md-12">
    <?php endif;?>
        <div class="portlet box grey">
            <div class="portlet-title">
                <div class="caption">
                    <?=$page_icon?><?=$panel_title?>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="form-title">
                    <?=$form_description?>

                    <?php if ($errors) {
                        foreach ($errors as $error) {
                            echo $error;
                        }
                    }?>
                </div>
                <!-- BEGIN FORM-->
                <form action="<?=$confirm_url?>" id="layout_form" method="post" class="form-horizontal form-bordered">
                    <input type="hidden" name="back_url" value="<?=current_url()?>">
                    <input type="hidden" name="BRANCH_ID" value="<?= $branch_id?>">
                    <div class="form-body">
                        <!--Begin News Color Settings-->
                        <div class="form-group">
                            <label class="control-label col-md-3">Twitter link</label>
                            <div class="col-md-9">
                                <input type="radio" name="ACTIVE_YN_T" checked> <?=lang('post_twitter_link')?>
                                <input type="radio" name="ACTIVE_YN_T"> <?=lang('not_post_twitter_link')?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"><?=lang('twitter_account')?></label>
                            <div class="col-md-9">
                                <p><?=lang('enter_twitter_account_post')?></p>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon">http://www.twitter.com/</span>
                                    <input type="text" class="form-control" name="LINK_F">
                                </div>
                                <span class="help-block help-area"><?=lang('login_twitter_enter_number_setting')?><br>
                                <?=lang('page_info_twitter_page')?><br><span class="help-tab-gray"><?=lang('entry_example')?></span>1234567890</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"><?=lang('facebook_link')?></label>
                            <div class="col-md-9">
                                <input type="radio" name="ACTIVE_YN_F" checked> <?=lang('post_twitter_link')?>
                                <input type="radio" name="ACTIVE_YN_F"> <?=lang('not_post_twitter_link')?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"><?=lang('facebook_account')?></label>
                            <div class="col-md-9">
                                <p><?=lang('enter_facebook_accout')?></p>
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon">http://www.facebook.com/</span>
                                    <input type="text" class="form-control" name="LINK_F">
                                </div>
                                <span class="help-block help-area"><?=lang('login_twitter_enter_number_setting')?><br>
                                <?=lang('page_info_facebook_pageid')?><br><span class="help-tab-gray"><?=lang('entry_example')?></span>1234567890</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3"><?=lang('facebook_account')?></label>
                            <div class="col-md-9">
                                <p><?=lang('can_post_link_blog_homepage')?></p>
                                <span class="red"><?=lang('enter_url_press_addbutton')?></span>
                                <div>
                                    <select class="col-md-5">
                                        <option><?=lang('please_select')?></option>
                                        <option><?=lang('blog')?></option>
                                        <option><?=lang('pc_homepage')?></option>
                                        <option><?=lang('smartphone_page')?></option>
                                        <option><?=lang('hot_pepper')?></option>
                                        <option><?=lang('gournavi')?></option>
                                        <option><?=lang('shopping')?></option>
                                        <option><?=lang('reservation')?></option>
                                        <option><?=lang('job_information')?></option>
                                    </select>
                                    <input type="text" class="col-md-5" name="LINK_F">
                                    <button class="btn btn-success"><i class="fa fa-plus"></i><?=lang('add_above_site')?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn blue"><i class="fa fa-check"></i> <?= lang('save') ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php if ($has_preview) {
        $this->load->view('_parts/preview');
    }
    ?>
</div>
<script>
    <?php if ($has_preview) {?>
    $(function() {
        updatePreview();
        $("input, textarea, select").change(function() {
            updatePreview();
        });
    });
    <?php }?>
</script>
<?php if (isset($script_url) && $script_url != "") $this->load->view($script_url);?>