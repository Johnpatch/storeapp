<?php

class Slide_model extends MY_Model
{
    protected $table = 'slide_settings';
    public function __construct()
    {
        parent::__construct();
    }
    
    public function count_rows($params) {
        if ($params["STATUS_SHOW"] == "Y") {
            $this->db->where($this->table.'.STATUS_SHOW', 'Y');
        }
        return $this->db->count_all_results($this->table);
    }
    
    public function get_rows($params, $perpage, $page) {
        $this->db
                ->select('branches.name AS BRANCH_NAME, '.$this->table.'.*')
                ->join('branches', 'branches.id = '.$this->table.'.branch_id', 'left');
        if ($params["STATUS_SHOW"] == "Y") {
            $this->db->where($this->table.'.STATUS_SHOW', 'Y');
        }
        return $this->db->get($this->table, $perpage, max($page - 1, 0) * $perpage)->result_array();
    }
}
