<?php

class Coupon_model extends MY_Model
{
    protected $table = 'coupon';
    public function __construct()
    {
        parent::__construct();
    }

    public function count_rows($params) {
        $this->db->like("COUPON_TITLE", $params["COUPON_TITLE"]);
        if ($params["STATUS_SHOW"] == 'Y')
            $this->db->where("STATUS_SHOW", "Y");
        return $this->db->count_all_results($this->table);
    }

    public function get_rows($params, $perpage, $page) {
        $this->db->like("COUPON_TITLE", $params["COUPON_TITLE"]);
        if ($params["STATUS_SHOW"] == 'Y')
            $this->db->where("STATUS_SHOW", "Y");
        $this->db->order_by("ORDER");
        return $this->db->get($this->table, $perpage, max($page - 1, 0) * $perpage)->result_array();
    }
}
