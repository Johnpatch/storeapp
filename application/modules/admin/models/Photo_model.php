<?php

class Photo_model extends MY_Model
{
    protected $table = 'photo_video_info';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_rows($conditions) {
        foreach ($conditions as $key => $value) {
            $this->db->where($key, $value);
        }
        $row = $this->db->get($this->table)->result_array();
        return $row;
    }
}
