<?php

class Catalog_model extends MY_Model
{
    protected $table = 'catalogs';
    public function __construct(){
		parent::__construct();
		$this->load->database();
	}

    public function get_rows($conditions) {
        foreach ($conditions as $key => $value) {
            $this->db->where($key, $value);
        }
        $row = $this->db->get($this->table)->result_array();
        return $row;
    }
}
