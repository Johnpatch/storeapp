<?php

class Settings_model extends MY_Model
{
    protected $table = 'settings';
    public function __construct()
    {
        parent::__construct();
    }

    public function insert_row($params) {
        $this->db->insert($this->table, $params);
    }

    public function update_value($params) {
    	if ($params["BRANCH_ID"] != NULL) {
            $this->db->where('BRANCH_ID', $params["BRANCH_ID"]);
        }
        if ($params["VARIABLE"] != NULL) {
            $this->db->where('VARIABLE', $params["VARIABLE"]);
        }
        $this->db->update($this->table, array(
            'VALUE' => $params["VALUE"],
        ));
    }

    public function get_row($params) {
        if ($params["BRANCH_ID"] != NULL) {
            $this->db->where('BRANCH_ID', $params["BRANCH_ID"]);
        }
        if ($params["VARIABLE"] != NULL) {
            $this->db->where('VARIABLE', $params["VARIABLE"]);
        }
        return $this->db->get($this->table)->row_array();
    }
}
