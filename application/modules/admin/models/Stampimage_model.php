<?php

class Stampimage_model extends MY_Model
{
    protected $table = 'stamp_image';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_rows_from_branch($id) {
        $this->db->where('BRANCH_ID', $id);
        $row = $this->db->get($this->table)->result_array();
        return $row;
    }

    public function insert_row($params) {
        $this->db->insert($this->table, $params);
        return true;
    }

    public function delete_row($id) {
        $this->db->where('ID', $id);
        $this->db->delete($this->table);
        return true;
    }
}
