<?php

class Term_model extends MY_Model
{
    protected $table = 'main_info';
    public function __construct()
    {
        parent::__construct();
    }

    public function count_rows($params) {
        return $this->db->count_all_results($this->table);
    }

    public function get_rows($params, $perpage, $page) {
        return $this->db->get($this->table, $perpage, max($page - 1, 0) * $perpage)->result_array();
    }
}
