<?php

class President_model extends MY_Model
{
    protected $table = 'presidents';
    public function __construct()
    {
        parent::__construct();
    }
    public function count_rows($params) {

        return $this->db->count_all_results($this->table);
    }

    public function get_rows($params, $perpage, $page) {
        return $this->db->order_by("ID")->get($this->table)->result_array();
    }
}
