<?php

class Webview_model extends MY_Model
{
    protected $table = 'webview_settings';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_rows() {
        $row = $this->db->get($this->table)->result_array();
        return $row;
    }

    public function get_rows_from_branch($branch_id) {
        $this->db->where('BRANCH_ID', $branch_id);
        $row = $this->db->get($this->table)->result_array();
        return $row;
    }

    public function delete_rows() {
        $this->db->truncate($this->table);
        return true;
    }

    public function insert_row($post) {
        $this->db->insert($this->table, array(
            'BRANCH_ID' => $post['BRANCH_ID'],
            'URL' => $post['URL'],
            'TITLE' => $post['TITLE'],
            'IS_SHOW' => $post['IS_SHOW'],
        ));
        return true;
    }
}
