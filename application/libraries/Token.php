<?php
class Token_Controller extends MX_Controller
{
	public $autoload = array();
	public function __construct() 
	{
		$class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
		log_message('debug', $class." MX_Controller Initialized");
		Modules::$registry[strtolower($class)] = $this;	
		
		/* copy a loader instance and initialize */
		$this->load = clone load_class('Loader');
		$this->load->initialize($this);	
		
		/* autoload module items */
		$this->load->_autoloader($this->autoload);
		$this->load->database();
	}	
	public function __get($class) 
	{
		return CI::$APP->$class;
	}
	public function get_session_data($token , $key) {
		$query = $this->db->query("select DATA from token_table where TOKEN='".$token."'");
		$result = $query->result_array();
		$data = json_decode($result[0]['DATA'], TRUE);
		return $data[$key];
	}
	public function set_session_data($token , $key , $val) {
		$query = $this->db->query("select DATA from token_table where TOKEN='".$token."'");
		$result = $query->result_array();
		$data = json_decode($result[0]['DATA'], TRUE);
		$data[$key] = $val;
		$str = json_encode($data);
		$sql = "update token_table set DATA = '".$str."' where TOKEN='".$token."'"; 
		$this->db->query($sql);
	}
}